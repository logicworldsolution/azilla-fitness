<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addStyleSheet(JURI::base() . 'templates/' . $this->template . '/css/template.css', $type = 'text/css');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/custom.js', 'text/javascript');
// Load optional rtl Bootstrap css and Bootstrap bugfixes
JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);

// Add current user information
$user = JFactory::getUser();
// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
	$span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span9";
}
else
{
	$span = "span12";
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="'. JURI::root() . $this->params->get('logoFile') .'" alt="'. $sitename .'" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. htmlspecialchars($this->params->get('sitetitle')) .'</span>';
}
else
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. $sitename .'</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<jdoc:include type="head" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<?php if ($this->params->get('googleFont')): // Use of Google Font 	?>
<link href='http://fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName');?>' rel='stylesheet' type='text/css' />
<style type="text/css">
h1, h2, h3, h4, h5, h6, .site-title {
	font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName'));?>', sans-serif;
}
</style>
<?php endif; ?>
<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
<![endif]-->
</head>
<body class="site <?php echo $option . " view-" . $view . " layout-" . $layout . " task-" . $task . " itemid-" . $itemid . " ";?> <?php if ($this->params->get('fluidContainer')) { echo "fluid"; } ?>">
<!-- Body -->
<div class="body">
  <!-- Header -->
  <div class="header">
    <div class="header-inner"> <a class="brand pull-left" href="<?php echo JURI::root(); ?>"> <?php echo $logo;?>
      <?php if ($this->params->get('sitedescription')) { echo '<div class="site-description">'. htmlspecialchars($this->params->get('sitedescription')) .'</div>'; } ?>
      </a>
      <div class="header-search pull-right">
        <jdoc:include type="modules" name="position-0" style="none" />
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="mainmenu">
    <jdoc:include type="modules" name="mainmenu" style="none" />
  </div>
  <?php if ($this->countModules('slider')): ?>
    <jdoc:include type="modules" name="slider" style="none" />
  <?php endif; ?>
  <?php if ($this->countModules('topbar')): ?>
    <div class="bs-docs-social">
      <div class="container">
        <jdoc:include type="modules" name="topbar" style="none" />
      </div>
    </div>
    <?php endif; ?>
  <div class="container<?php if ($this->params->get('fluidContainer')) { echo "-fluid"; } ?>">
    <?php if ($this->countModules('position-1')): ?>
    <div class="navigation">
      <jdoc:include type="modules" name="position-1" style="none" />
    </div>
    <?php endif; ?>
    <div class="row-fluid">
      <?php if ($this->countModules('leftsidebar')): ?>
      <div class="span3 aazilla_span3">
        <jdoc:include type="modules" name="leftsidebar" style="none" />
		<?php if ($this->countModules('menucalendar')): ?>
		<jdoc:include type="modules" name="menucalendar" style="xhtml" />
		<?php endif; ?>
		<div id="left_side_bottombar">
	        <h2 class="mytoolbox">My Toolbox</h2>
        </div>
        <?php if ($this->countModules('workoutWeek')): ?>  	
	   		<jdoc:include type="modules" name="workoutWeek" style="none" />
        <?php endif; ?>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('position-8')): ?> 
      <!-- Begin Sidebar -->
      <div id="sidebar" class="span3">
        <div class="sidebar-nav">
          <jdoc:include type="modules" name="position-8" style="xhtml" />
        </div>
      </div>
      <!-- End Sidebar aazilla_span9 span12 -->
      <?php endif; ?>
      <?php if ($this->countModules('position-3')): ?>
      <jdoc:include type="modules" name="position-3" style="xhtml" />
      <?php endif; ?>
      <div id="content" class="<?php if ($this->countModules('leftsidebar')) echo "aazilla_span9"; else echo "span12";  ?>" >
        <!-- Begin Content -->
        <jdoc:include type="message" />
        <jdoc:include type="component" />
        <!-- End Content -->
      </div>
      <?php if ($this->countModules('position-7')) : ?>
      <div id="aside" class="span3">
        <!-- Begin Right Sidebar -->
        <jdoc:include type="modules" name="position-7" style="well" />
        <!-- End Right Sidebar -->
      </div>
      <?php endif; ?>
    </div>
  </div>
</div>
<div class="bottom-block">
  <div class="testimonials">
    <jdoc:include type="modules" name="testimonials" style="none" />
  </div>
  <!-- Footer -->
  <div class="footer">
    <div class="container<?php if ($this->params->get('fluidContainer')) { echo "-fluid"; } ?>">    
      <jdoc:include type="modules" name="footer" style="none" />
      <p class="copy">Copyright &copy; <?php echo $sitename; ?> <?php echo date('Y');?></p>
    </div>
  </div>
</div>
<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>