(function($) {
	$().ready(function(){
		$("#datepicker1").datepicker({ showOtherMonths: true});
		$('.dropdown-toggle').dropdown();
		
		if($('#left_menu li ul.nav-child li').length>0){
			$('#left_menu li ul.nav-child').appendTo('#left_side_bottombar').addClass('toolbox_list');	
		}else{
			$('.mytoolbox').hide();
		}
		$('#workoutprog').append('<b class="caret"></b>');
		$('#workoutprog').click(function(e) {
			$('#workoutprogslide').slideToggle('slow');
		});
		
		$('#miles_snakes_accordion .accordion-toggle').click(function(){																	  
			if($(this).hasClass('collapsed')){
				$('#miles_snakes_accordion .accordion-toggle').addClass('collapsed');
				$(this).removeClass('collapsed');
			}															  
		});
	});
})(jQuery)