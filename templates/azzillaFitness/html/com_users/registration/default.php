<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
?>
<div class="registration<?php echo $this->pageclass_sfx?>">
<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
	</div>
<?php endif; ?>

	<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
	
<?php 
if(isset($_GET['id']))
{
?>
<div class="reg-left">
<?php
foreach ($this->form->getFieldsets() as $fieldset): // Iterate through the form fieldsets and display each one.?>
	<?php $fields = $this->form->getFieldset($fieldset->name);?>
	<?php if (count($fields)):?>
		<fieldset>
		<?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.
		?>
			<legend><?php echo JText::_($fieldset->label);?></legend>
		<?php endif;?>
		<?php foreach ($fields as $field) :// Iterate through the fields in the set and display them.?>
			<?php if ($field->hidden):// If the field is hidden, just display the input.?>
				<?php echo $field->input;?>
			<?php else:?>
				<div class="control-group">
					<div class="control-label">
					<?php echo $field->label; ?>
					<?php if (!$field->required && $field->type != 'Spacer') : ?>
						<span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL');?></span>
					<?php endif; ?>
					</div>
					<div class="controls">
						<?php echo $field->input;?>
					</div>
				</div>
			<?php endif;?>
		<?php endforeach;?>
		</fieldset>
	<?php endif;?>
<?php endforeach;?>
</div>
<div class="reg-right">
<?php

if($_GET['id'] == 1)
{
?>
<fieldset>
	<legend>Affiliated Member Payment</legend>
	<input type="hidden" value="<?php echo $_GET['id']; ?>" name="typeid" />
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please Enter Referral Code" class="hasTooltip required" for="jref_code" id="jref_code-lbl">Referral Code:</label>										
		</div>
		<div class="controls">
			<input type="text" size="30" value="" id="jref_code" name="jref_code">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter paypal id" class="hasTooltip required" for="jpaypal_id" id="jpaypal_id-lbl">Paypal Id:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jpaypal_id" name="jpaypal_id">
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please select credit card" class="hasTooltip required" for="jcard_type" id="jcard_type-lbl">Credit Card Type:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" class="required" id="jcard_type" name="jcard_type">
				<option value="">Select Card</option>
				<option value="Visa">Visa</option>
				<option value="MasterCard">Master Card</option>
				<option value="Amex">American Standard</option>
				<option value="Discover">Discover</option>
			</select>
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Enter credit card number" class="hasTooltip required" for="jcard_number" id="jcard_number-lbl">Credit Card Number:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_number" name="jcard_number">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter Card holder name" class="hasTooltip required" for="jcard_name" id="jcard_name-lbl">Card Holder Name:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_name" name="jcard_name">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please select expiration date" class="hasTooltip required" for="jcard_month" id="jcard_month-lbl">Exp. Date:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" style="width:60px;" class="required" id="jcard_month" name="jcard_month">	
				<?php for($i=1; $i<10; $i++) { ?>
				<option value="0<?php echo $i; ?>">0<?php echo $i; ?></option>
				<?php } 
				for($i=10; $i<13; $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
			<select required="required" style="width:75px;" class="required" id="jcard_year" name="jcard_year">	
				<?php $date = date("Y"); for($i=$date; $i<($date+50); $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php }  ?>
			</select>			
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter CVV or CVV2" class="hasTooltip required" for="jcard_cvv" id="jcard_cvv-lbl">CVV/CVV2:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_cvv" name="jcard_cvv">					
		</div>
	</div>
	
</fieldset>
<?php
}
if($_GET['id'] == 2)
{
?>
<fieldset>
	<legend>Regular Member Payment</legend>
	<input type="hidden" value="<?php echo $_GET['id']; ?>" name="typeid" />
	<div class="control-group">
		<div class="control-label">
			<label title="Please select credit card" class="hasTooltip required" for="jcard_type" id="jcard_type-lbl">Credit Card Type:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" class="required" id="jcard_type" name="jcard_type">
				<option value="">Select Card</option>
				<option value="Visa">Visa</option>
				<option value="MasterCard">Master Card</option>
				<option value="Amex">American Standard</option>
				<option value="Discover">Discover</option>
			</select>
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Enter credit card number" class="hasTooltip required" for="jcard_number" id="jcard_number-lbl">Credit Card Number:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_number" name="jcard_number">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter Card holder name" class="hasTooltip required" for="jcard_name" id="jcard_name-lbl">Card Holder Name:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_name" name="jcard_name">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please select expiration date" class="hasTooltip required" for="jcard_month" id="jcard_month-lbl">Exp. Date:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" style="width:60px;" class="required" id="jcard_month" name="jcard_month">	
				<?php for($i=1; $i<10; $i++) { ?>
				<option value="0<?php echo $i; ?>">0<?php echo $i; ?></option>
				<?php } 
				for($i=10; $i<13; $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
			<select required="required" style="width:75px;" class="required" id="jcard_year" name="jcard_year">	
				<?php $date = date("Y"); for($i=$date; $i<($date+50); $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php }  ?>
			</select>			
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter CVV or CVV2" class="hasTooltip required" for="jcard_cvv" id="jcard_cvv-lbl">CVV/CVV2:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_cvv" name="jcard_cvv">					
		</div>
	</div>
	
</fieldset>
<?php
}
if($_GET['id'] == 3)
{
?>
<fieldset>
	<input type="hidden" value="<?php echo $_GET['id']; ?>" name="typeid" />
	<legend>Monthly Payment for Affiliated Member</legend>
	<div class="control-group">
		<div class="control-label">
			<label title="Please select credit card" class="hasTooltip required" for="jcard_type" id="jcard_type-lbl">Credit Card Type:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" class="required" id="jcard_type" name="jcard_type">
				<option value="">Select Card</option>
				<option value="Visa">Visa</option>
				<option value="MasterCard">Master Card</option>
				<option value="Amex">American Standard</option>
				<option value="Discover">Discover</option>
			</select>
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Enter credit card number" class="hasTooltip required" for="jcard_number" id="jcard_number-lbl">Credit Card Number:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_number" name="jcard_number">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter Card holder name" class="hasTooltip required" for="jcard_name" id="jcard_name-lbl">Card Holder Name:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_name" name="jcard_name">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please select expiration date" class="hasTooltip required" for="jcard_month" id="jcard_month-lbl">Exp. Date:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" style="width:60px;" class="required" id="jcard_month" name="jcard_month">	
				<?php for($i=1; $i<10; $i++) { ?>
				<option value="0<?php echo $i; ?>">0<?php echo $i; ?></option>
				<?php } 
				for($i=10; $i<13; $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
			<select required="required" style="width:75px;" class="required" id="jcard_year" name="jcard_year">	
				<?php $date = date("Y"); for($i=$date; $i<($date+50); $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php }  ?>
			</select>			
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter CVV or CVV2" class="hasTooltip required" for="jcard_cvv" id="jcard_cvv-lbl">CVV/CVV2:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_cvv" name="jcard_cvv">					
		</div>
	</div>
	
</fieldset>
<?php
}
}
else
{
$app	= JFactory::getApplication();
$app->redirect(JRoute::_("index.php?option=com_usermembership&view=membershiptypes", false),"Please Select Membership Type", 'notice');
}
?>
	</div>
	<div class="reg-clear"></div>
	<div class="form-actions">
	<div class="reg-buttons">
			<button type="submit" class="btn btn-primary validate"><?php echo JText::_('JREGISTER');?></button>
			<a class="btn" href="<?php echo JRoute::_('');?>" title="<?php echo JText::_('JCANCEL');?>"><?php echo JText::_('JCANCEL');?></a>
			<input type="hidden" name="option" value="com_users" />
			<input type="hidden" name="task" value="registration.register" />
			<?php echo JHtml::_('form.token');?>
			</div>
		</div>
	</form>
</div>
