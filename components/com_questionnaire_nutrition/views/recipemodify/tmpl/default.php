<?php 
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');	 

$editor =JFactory::getEditor();
$params = array( 'smilies'=> '0' , 'style'  => '1' ,'layer'  => '0' ,'table'  => '0','clear_entities'=>'0' );

$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/recipemodify/tmpl/css/recipeadd.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/recipemodify/tmpl/js/recipeadd.js');

function is_recipe_selected($user_data,$db_data){
	$user_data_arr=explode(',',$user_data);
	foreach($user_data_arr as $user_data_arrVal){
		if($user_data_arrVal==$db_data){
			return "selected='selected'";
		}
	}
	return '';
}

$user			= JFactory::getUser();
$loginUserId	= (int) $user->get('id');

$db = JFactory::getDbo();	

$jinput = JFactory::getApplication()->input;
$recipe_id = $jinput->get('recipe_id', 'default_value', 'filter');

$query="SELECT * FROM #__questionnaire_nutrition_recipes WHERE id=$recipe_id";
$db->setQuery($query);
$user_recipe_list=$db->loadObject();

$query="SELECT name FROM #__questionnaire_nutrition_units WHERE state=1 ORDER BY ordering ASC";
$db->setQuery($query);
$recipe_units=$db->loadObjectList();

$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE state=1 and parent_id=0 ORDER BY ordering ASC";
$db->setQuery($query);
$recipe_list=$db->loadObjectList();

$query="SELECT eat_a_day_meal_snacks FROM #__questionnaire_question WHERE id=$loginUserId";
$db->setQuery($query);
$eat_a_day_meal_snacks=$db->loadObject()->eat_a_day_meal_snacks;

if($eat_a_day_meal_snacks==2){
	$eat_a_day_meal_snacks=$eat_a_day_meal_snacks+3;
}
elseif($eat_a_day_meal_snacks==3){
	$eat_a_day_meal_snacks=$eat_a_day_meal_snacks+2;
}
elseif(in_array($eat_a_day_meal_snacks,array(4,5,6,7))){
	$eat_a_day_meal_snacks=$eat_a_day_meal_snacks+1;
}
?>
<h2 class="page_head">Modify Recipe</h2>
<div id="recipe_container">
<form action="index.php?option=com_questionnaire_nutrition&task=questionform.add_recipe" enctype="multipart/form-data" id="add_recipe_form" method="post" class="margin0">
<div class="addrecipeblk" >
  <div class="recipedata">
  <h3 class="mealnamecontainer">Recipe Name <input type="text" name="name" value="<?php echo $user_recipe_list->name ?>" class="mealname" ></h3>
    <table class="maintable" >
      <tr>
        <td width="23%"><table class="recipeinnertable" >
            <tr>
              <td class="recipephto">
              	<div class="padingleft2">
              	<?php if(!empty($user_recipe_list->picture)): ?>
              	<img id="recipebrowseimg" src="<?php echo JRoute::_(JUri::base() . 'administrator/components/com_questionnaire_nutrition/asset/'.$user_recipe_list->picture);?>" width="130" alt="Image" />
                <input type="hidden" name="modify_image" value="<?php echo $user_recipe_list->picture; ?>" />
                <?php else: ?>
                <img id="recipebrowseimg" class="imagehide" width="130" src="#" alt="Image" />
                <?php endif; ?>
              	</div>
              <button type="button" class="btn btn-success reset_button" id="recipebrowse" >Browse</button>
              <input type="file" name="picture" id="recipfile" class="hide"> </td>
            </tr>
            <tr>
              <td class="recipetotla">Totals</td>
            </tr>
            <tr>
              <td><div class="padingleft2">Carbohydrates</div>
                <div class="floatright paddingrigh2"><input name="total_carbohydrates" value="<?php echo $user_recipe_list->total_carbohydrates ?>" type="text" ></div></td>
            </tr>
            <tr>
              <td><div class="padingleft2">Fat</div>
                <div class="floatright paddingrigh2"><input name="total_fat" value="<?php echo $user_recipe_list->total_fat ?>" type="text" ></div></td>
            </tr>
            <tr>
              <td><div class="padingleft2">Protein</div>
                <div class="floatright paddingrigh2"><input name="total_protein" value="<?php echo $user_recipe_list->total_protein ?>" type="text" ></div></td>
            </tr>
          </table></td>
        <td width="54%"><table class="recipeinnertable2">
            <tr>
              <td class="color555">Brand Name</td>
              <td><input type="text" value="<?php echo $user_recipe_list->brand_name ?>" name="brand_name" ></td>
            </tr>
            <tr>
              <td class="color555">Serving Size</td>
              <td><input type="text" value="<?php echo $user_recipe_list->serv_size ?>" name="serv_size" ></td>
            </tr>
             <tr>
              <td class="color555">Unit of Measurement <?php echo $user_recipe_list->unit_of_measurement;  ?></td>
              <td>
              	<select name="unit_of_measurement" class="recipe_type" >
                <option value="" >Select</option>
                <?php foreach($recipe_units as $recipe_unitsObj){
						if($user_recipe_list->unit_of_measurement==$recipe_unitsObj->name){
							echo "<option selected='selected' value='".$recipe_unitsObj->name."'>{$recipe_unitsObj->name}</option>";
						}else{
							echo "<option value='".$recipe_unitsObj->name."'>{$recipe_unitsObj->name}</option>";
						}
                    }
                 ?>
	            </select>
			</td>
            </tr>
            <tr>
              <td class="color555">Calories</td>
              <td><input type="text" value="<?php echo $user_recipe_list->calories ?>" name="calories" ></td>
            </tr>
            <tr>
              <td class="color555">Carbohydrates</td>
              <td><input type="text" value="<?php echo $user_recipe_list->carbohydrates ?>" name="carbohydrates" ></td>
            </tr>
            <tr>
              <td class="color555">Fat</td>
              <td><input type="text" value="<?php echo $user_recipe_list->fat ?>" name="fat" ></td>
            </tr>
            <tr class="color555">
              <td class="color555">Protein</td>
              <td><input type="text" name="protein" value="<?php echo $user_recipe_list->protein ?>" ></td>
            </tr>
             <tr class="color555">
              <td class="color555">Recipe Type</td>
              <td><select name="recipe_type" class="recipe_type" >
              <option value=''>Select</option>
              <?php foreach($recipe_list as $recipe_listVal){
			  			$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE state=1 and parent_id=$recipe_listVal->id";
						$db->setQuery($query);
						$sub_recipe_list=$db->loadObjectList();
						if(count($sub_recipe_list)>0){
							echo "<optgroup label='$recipe_listVal->name'>";
								foreach($sub_recipe_list as $sub_recipe_listVal){
									$sub_recipe_name=@explode('_',$sub_recipe_listVal->name);
									$sub_recipe_name_str=@$sub_recipe_name[1];
									$selected=is_recipe_selected($user_recipe_list->recipe_type_id,$sub_recipe_listVal->id);
								echo	"<option value='$sub_recipe_listVal->id' $selected >$sub_recipe_name_str</option>";
								}
							echo "</optgroup>";
						
						}else{
							$selected=is_recipe_selected($user_recipe_list->recipe_type_id,$recipe_listVal->id);
			  				echo "<option value='$recipe_listVal->id' $selected >$recipe_listVal->name</option>";
						}
			  
			  } ?>
              </select>
             
              </td>
            </tr>
			<tr class="noborder">
              <td class="color555">Meal</td>
              <td><select name="meal_id" class="recipe_type" >
              <option value=''>Select</option>
              <?php for($i=1;$i<=$eat_a_day_meal_snacks;$i++){
			  			$selected=($user_recipe_list->meal_id==$i)?"selected='selected'":'';
		  				echo "<option value='$i' $selected >Meal $i</option>";
			  } ?>              
              </select>             
              </td>
            </tr>
          </table></td>
        <td width="23%"><table class="recipeinnertable3">
            <tr>
              <td class="recipenotes">Notes</td>
            </tr>
            <tr>
              <td class="recipenotesdata"><textarea name="notes" class="notes"><?php echo $user_recipe_list->notes ?></textarea></td>
            </tr>
          </table>
          <div class="addnewmeal">
            <button type="button" class="btn btn-success reset_button" id="add_ownrecipe">Update</button>
          </div></td>
      </tr>
      <tr>
            <td colspan="3" width="100%">
                <div class="ingredents_div" >
                    <h4 class="recipe_heading">INGREDIENTS</h4>
					<?php 
				    echo $editor->display( 'ingredients',$user_recipe_list->ingredients, '100%', '100%', '10', '10' , false, null, null, null, $params); 
					?>
                </div>
            </td>
       </tr>
       <tr>
            <td colspan="3" width="100%">
                <div class="ingredents_divright" >
                    <h4 class="recipe_heading">DIRECTIONS</h4>
					<?php 
				    echo $editor->display( 'cooking_directions', $user_recipe_list->cooking_directions, '100%', '', '', '' , false, null, null, null, $params); 
					?>
                </div>
            </td>
      </tr>      
    </table>
  </div>
</div>
 <input type="hidden" name="recipe_id" value="<?php echo $recipe_id; ?>">
 <input type="hidden" name="update_recipe" value="true"  />
</form>