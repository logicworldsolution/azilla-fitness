jQuery().ready(function($){
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();
	
			reader.onload = function (e) {
				$('#recipebrowseimg').attr('src', e.target.result).show();
			}
	
			reader.readAsDataURL(input.files[0]);
		}
	}

	$('#recipebrowse').click(function(){
		$('#recipfile').trigger('click');
		
	});
	
	$("#recipfile").change(function(){
    	readURL(this);
	});
	
	$('#add_ownrecipe').click(function(){
		
		var chkinvalid=false;							   
		
		$('input:text , select , textarea.notes').each(function(){
			var chkempty=$.trim($(this).val());		
			if(chkempty==''){
				chkinvalid=true;
				$(this).addClass('invalid');							 
			}else{
				$(this).removeClass('invalid');			
			}
		});
		
		if(chkinvalid){
			return false;
		}else{
			$('#add_recipe_form').trigger('submit');
		}
	});
	
});