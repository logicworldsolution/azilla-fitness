<?php 
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');

$editor =JFactory::getEditor();
$params = array( 'smilies'=> '0' , 'style'  => '1' ,'layer'  => '0' ,'table'  => '0','clear_entities'=>'0' );

$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/recipeadd/tmpl/css/recipeadd.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/recipeadd/tmpl/js/recipeadd.js');

$user			= JFactory::getUser();
$loginUserId	= (int) $user->get('id');

$db = JFactory::getDbo();	

$jinput = JFactory::getApplication()->input;
$usermeals_meal_id = $jinput->get('usermeals_meal_id', '', 'filter');
$usermeals_meal_own_id= $jinput->get('usermeals_meal_own_id', '', 'filter');
$own_recipe=$jinput->get('own_recipe', '', 'filter');

$query="SELECT name FROM #__questionnaire_nutrition_units WHERE state=1 ORDER BY ordering ASC";
$db->setQuery($query);
$recipe_units=$db->loadObjectList();

$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE state=1 and parent_id=0 ORDER BY ordering ASC";
$db->setQuery($query);
$recipe_list=$db->loadObjectList();
?>
<h2 class="page_head">Add Recipe</h2>
<div id="recipe_container">

<?php if(isset($usermeals_meal_id) && is_numeric($usermeals_meal_id)): ?>

<form action="index.php?option=com_questionnaire_nutrition&task=questionform.add_recipe" enctype="multipart/form-data" id="add_recipe_form" method="post" class="margin0">
<input type='hidden' name='usermeals_meal_id' value='<?php echo $usermeals_meal_id; ?>'> 

<?php elseif(isset($usermeals_meal_own_id) && is_numeric($usermeals_meal_own_id)): ?>

<form action="index.php?option=com_questionnaire_nutrition&task=questionformown.add_recipe" enctype="multipart/form-data" id="add_recipe_form" method="post" class="margin0">
<input type='hidden' name='usermeals_meal_own_id' value='<?php echo $usermeals_meal_own_id; ?>'> 

<?php endif; ?>

<div class="addrecipeblk" >
  <div class="recipedata">
  <h3 class="mealnamecontainer">Recipe Name <input type="text" name="name" class="mealname" ></h3>
    <table class="maintable" >
      <tr>
        <td width="23%"><table class="recipeinnertable" >
            <tr>
              <td class="recipephto"><div class="padingleft2"><img id="recipebrowseimg" class="imagehide" width="130" src="#" alt="Image" /></div>
              <button type="button" class="btn btn-success reset_button" id="recipebrowse" >Browse</button>
              <input type="file" name="picture" id="recipfile" class="hide"> </td>
            </tr>                       
          </table></td>
        <td width="54%"><table class="recipeinnertable2">
            <tr>
              <td class="color555">Brand Name</td>
              <td><input type="text" name="brand_name" ></td>

            </tr>
            <tr>
              <td class="color555">Serving Size</td>
              <td><input type="text" name="serv_size" ></td>
            </tr>
             <tr>
              <td class="color555">Unit of Measurement</td>
              <td>
              	<select name="unit_of_measurement" class="recipe_type" >
                <option value="" selected="selected">Select</option>
                <?php foreach($recipe_units as $recipe_unitsObj){
                            echo "<option value='".$recipe_unitsObj->name."'>{$recipe_unitsObj->name}</option>";
                    }
                 ?>
	            </select>
			</td>
            </tr>
            <tr>
              <td class="color555">Calories</td>
              <td><input type="text" name="calories" ></td>
            </tr>
            <tr>
              <td class="color555">Carbohydrates</td>
              <td><input type="text" name="carbohydrates" ></td>
            </tr>
            <tr>
              <td class="color555">Fat</td>
              <td><input type="text" name="fat" ></td>
            </tr>
            <tr class="color555">
              <td class="color555">Protein</td>
              <td><input type="text" name="protein" ></td>
            </tr>
             <tr class="noborder">
              <td class="color555">Recipe Type</td>
              <td><select name="recipe_type" class="recipe_type" >
              <option value=''>Select</option>
              <?php foreach($recipe_list as $recipe_listVal){
					$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE state=1 and parent_id=$recipe_listVal->id";
					$db->setQuery($query);
					$sub_recipe_list=$db->loadObjectList();
					if(count($sub_recipe_list)>0){
						echo "<optgroup label='$recipe_listVal->name'>";
							foreach($sub_recipe_list as $sub_recipe_listVal){
								$sub_recipe_name=@explode('_',$sub_recipe_listVal->name);
								$sub_recipe_name_str=@$sub_recipe_name[1];
							echo	"<option value='$sub_recipe_listVal->id'>$sub_recipe_name_str</option>";
							}
						echo "</optgroup>";
					
					}else{
						echo "<option value='$recipe_listVal->id'>$recipe_listVal->name</option>";
					}
			  
			  } ?>
              </select>             
              </td>
            </tr>
          </table></td>
        <td width="23%"><table class="recipeinnertable3">
            <tr>
              <td class="recipenotes">Notes</td>
            </tr>
            <tr>
              <td class="recipenotesdata"><textarea name="notes" class="notes"></textarea></td>
            </tr>
          </table>
          <div class="addnewmeal">
            <button type="button" class="btn btn-success reset_button" id="add_ownrecipe">Save</button>
          </div></td>
      </tr>
       <tr>
            <td colspan="3" width="100%">
                <div class="ingredents_div" >
                    <h4 class="recipe_heading">INGREDIENTS</h4>
					<?php 
				    echo $editor->display( 'ingredients', '', '100%', '', '', '' , false, null, null, null, $params); 
					?>
                </div>
            </td>
       </tr>
       <tr>
            <td colspan="3" width="100%">
                <div class="ingredents_divright" >
                    <h4 class="recipe_heading">DIRECTIONS</h4>
					<?php 
				    echo $editor->display( 'cooking_directions', '', '100%', '', '', '' , false, null, null, null, $params); 
					?>
                </div>
            </td>
       </tr>
    </table>
  </div>
</div>
</form>
</div>