<?php
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');
$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/grocery/tmpl/css/grocery.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/grocery/tmpl/js/grocery.js');

$app			= JFactory::getApplication();
$db = JFactory::getDbo();	
$loginUserId	= JFactory::getUser()->get('id');

$query="SELECT A.nutrition_program_start FROM #__questionnaire_question A LEFT JOIN #__userworkouts_info B  ON A.id=B.user_id WHERE A.id=$loginUserId";
$db->setQuery($query);
$user_nutrition_program_start=$db->loadObject()->nutrition_program_start;

//Get the last food date
$query="SELECT count(start_date) as last_date  FROM #__usermeals_level WHERE user_id=$loginUserId";
$db->setQuery($query);
//check user nutrition food items are created
if($db->loadObject()->last_date==0){
	$app->redirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=243", false),"Please create nutrition program.", 'notice');
}

//check user grocery are created for future dates
$query="SELECT  COUNT(id) as usergrocery_numb FROM #__usergrocerylist WHERE user_id=$loginUserId  AND end_date >= curdate()";
$db->setQuery($query);

if($db->loadObject()->usergrocery_numb==0){

	//Get the start date of nutrition , if last date from db is not exist then use questionnaire date start date 
	$query="SELECT max(end_date) as last_date  FROM #__usergrocerylist WHERE user_id=$loginUserId";
	$db->setQuery($query);

	if(isset($db->loadObject()->last_date)){
		$nutrition_program_strtdate=date('Y-m-d',strtotime('+1 day',strtotime($db->loadObject()->last_date)));
	}else{
		$nutrition_program_strtdate=$user_nutrition_program_start;
	}
	//Create one month long nutrition program
	$nutrition_program_enddate=date('Y-m-d',strtotime('+1 months',strtotime($nutrition_program_strtdate)));
	
	while($nutrition_program_strtdate<$nutrition_program_enddate){
		$nutrition_program_db_enddate=date('Y-m-d',strtotime("next sunday",strtotime($nutrition_program_strtdate)));

		if($nutrition_program_db_enddate>$nutrition_program_enddate){
			$nutrition_program_db_enddate=$nutrition_program_enddate;
		}		
		
		$query="INSERT INTO #__usergrocerylist(user_id,start_date,end_date) VALUES($loginUserId,'$nutrition_program_strtdate','$nutrition_program_db_enddate')";
		$db->setQuery($query);
		$db->query();		
		
		$nutrition_program_strtdate=date('Y-m-d',strtotime("+1 day",strtotime($nutrition_program_db_enddate)));	
	}
	
	//Create Grocery list based on user gorcery days
	$query="SELECT * FROM #__usergrocerylist WHERE user_id=$loginUserId order by start_date ASC";
	$db->setQuery($query);
	foreach($db->loadObjectList() as $usergrocery_listObj){
		$query="SELECT DISTINCT A.food_id,A.serv_size FROM mejwq_usermeals A
			JOIN  mejwq_usermeals_meal B ON
			A.usermeals_meal_id=B.id
			JOIN mejwq_usermeals_level C ON
			C.id=B.usermeals_level_id 
			WHERE C.user_id=$loginUserId AND C.start_date BETWEEN '$usergrocery_listObj->start_date' AND '$usergrocery_listObj->end_date'";

		$db->setQuery($query);

		foreach($db->loadObjectList() as $userGroceryFood){
		  $query="INSERT INTO #__usergrocery_meals(usergrocerylist_id,food_id,food_serv_size) VALUES ($usergrocery_listObj->id,$userGroceryFood->food_id,'$userGroceryFood->serv_size')";
		  $db->setQuery($query);
		  $db->query();
		}
	}
}

$query="SELECT * FROM  (SELECT * FROM #__usergrocerylist WHERE user_id=$loginUserId ORDER BY end_date DESC LIMIT 5) grocery_tbl ORDER BY end_date ASC";
$db->setQuery($query);
$user_glosery_list=$db->loadObjectList();
?>
<div id="grocery_list">
  <h2 class="page_head">My Grocery List</h2>
  <div class="accordion" id="miles_snakes_accordion">
    <?php foreach($user_glosery_list as $user_glosery_listVal):
  $weekdate_start = new DateTime($user_glosery_listVal->start_date);
  $weekdate_end = new DateTime($user_glosery_listVal->end_date);
   ?>
    <div class="accordion-group acc_group_marginnone">
      <div class="accordion-heading acc_top_border"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#collapse<?php echo $user_glosery_listVal->id; ?>" > <span class="plus_icon"> <?php echo $weekdate_start->format('M j').' - '.$weekdate_end->format('M j, Y'); ?></span> <span class="up_click"></span> </a> </div>
      <div id="collapse<?php echo $user_glosery_listVal->id; ?>" class="accordion-body collapse">
        <div class="accordion-inner bakround_none">
          <div class="grocery_list" >
          <?php
		  	$query="SELECT A.id,A.state,A.food_serv_size,B.name, B.unit_of_measurement  FROM #__usergrocery_meals A JOIN #__questionnaire_nutrition_food B ON A.food_id=B.id WHERE A.usergrocerylist_id=$user_glosery_listVal->id ORDER BY B.name ASC "; 
		  	$db->setQuery($query);
		   ?>
            <ul class="grocerylist">
			<?php foreach($db->loadObjectList() as $usergroceryItems):?>
              <li class="<?php if($usergroceryItems->state==0){ echo 'overstrike';} ?>" > 
              	<?php if($usergroceryItems->state==0){ echo '<img src="components/com_questionnaire_nutrition/views/grocery/tmpl/css/images/overstrike.png" />';} ?>
                <span> <span class="foodQty"> <?php echo $usergroceryItems->food_serv_size;  ?>&nbsp;</span><span class="foodMear">  <?php echo $usergroceryItems->unit_of_measurement;  ?></span></span>
                <p>
                 <a href="#grocery_list_qty_popup" class="grocery_list_qty_edt" usergrocery_meals_id="<?php echo $usergroceryItems->id; ?>" data-lightbox="on" ><?php echo $usergroceryItems->name;  ?></a>
                 <a class="input_field" usergrocery_meals_state="<?php echo $usergroceryItems->state; ?>" usergrocery_meals_id="<?php echo $usergroceryItems->id; ?>" href="javascript:;"> &nbsp;</a>
                </p>
              </li>
            <?php endforeach; ?>
            </ul>            
            <ul class="nopadding">
              <li class="notes">Notes</li>
              <li class="notes_data" grocery_id="<?php echo $user_glosery_listVal->id; ?>" >
              <?php if(empty($user_glosery_listVal->notes)){
			  		echo '<div class="note_inner"></div><button type="button"  class="btn btn-success reset_button update_note">Add</button>';		  
			  } else{ 
			 		echo '<div class="note_inner">'.$user_glosery_listVal->notes.'</div><button type="button"  class="btn btn-success reset_button update_note">Update</button>';				
			  } 
			  ?>              	
              </li>
            </ul>
            <a href="#grocery_list_popup" grocery_id="<?php echo $user_glosery_listVal->id; ?>"  class="grocery_list_popup_lnk" data-lightbox="on"><button type="button"  class="btn btn-success reset_button add_new_btn"> Add new item </button></a>
          </div>
        </div>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
</div>
<div class="hide">
  <div id="grocery_list_popup" >
    <h4 class="center" ></h4>
    <div class="center">
      <table align="center" width="90%">
        <tr>
          <td><div id="grocery_list_popup_data">
          <select id="nutrition_food_id">
          	<option value="">Select</option>
            <?php 
			$query="SELECT A.name as catname , B.id , B.name as foodname , B.serv_size, B.unit_of_measurement  FROM #__questionnaire_nutrition_category A JOIN `#__questionnaire_nutrition_food` B ON A.id=B.category_id ORDER BY catname,foodname ASC";
			$db->setQuery($query);
			$current_cat='';
			foreach($db->loadObjectList() as $foodList){

				if($foodList->catname!=$current_cat){
					if($current_cat!='') { echo "</optgroup>"; }
					echo " <optgroup label='$foodList->catname'>";
					$current_cat=$foodList->catname;
				}
				echo "<option qty='$foodList->serv_size' measure='$foodList->unit_of_measurement' value='$foodList->id'>$foodList->foodname</option>";			
			}
			 ?>          
            </optgroup>
          </select>          
        
          </div></td>
        </tr>
        <tr>
        	<td><span class="addqtr_main">Quantity &nbsp;</span><input type="text"  name="food_qty" class="addqty" id="food_qty" >&nbsp;<span id="food_measure" ></span></td>
        </tr>
        <tr>
          <td><button type="button" id="add_grocery" class="btn btn-success reset_button bgcolor3188">Add</button></td>
        </tr>
      </table>
    </div>
  </div>  
  <div id="grocery_list_qty_popup" >
    <div class="center">
		<div id="grocery_list_qty_popup_data">
          <table>
          	<tr>
            	<td><div id="nutrition_food_name_edt_id"></div></td>  
            </tr>
            <tr>    
          		<td><input type="text" value="" id="nutrition_food_qty_edt_id" />&nbsp;<span id="nutrition_food_Mear_edt_id" ></span> </td>
          	</tr>
            <tr>
            	<td> <button type="button" id="update_grocery" class="btn btn-success reset_button bgcolor3188">Update</button></td>
            </tr>
          </table>
        </div>
    </div>
  </div>
</div>