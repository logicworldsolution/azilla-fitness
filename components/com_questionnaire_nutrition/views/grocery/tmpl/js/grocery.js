jQuery().ready(function($){
	$('#grocery_list .input_field').live('click',function(evt){
														  
		evt.preventDefault();
		
		var current_list_item=$(this);
		
		var usergrocery_meals_id=current_list_item.attr('usergrocery_meals_id');
		
		var usergrocery_meals_state=current_list_item.attr('usergrocery_meals_state');		
		
		var	food_item_status=(usergrocery_meals_state=='1')?0:1;

		$.ajax({
			 url: "index.php?option=com_questionnaire_nutrition&task=questionform.ajax_add_grocery_food_status_qty",
			 type: "POST",
			 data:'usergrocery_meals_id='+usergrocery_meals_id+'&food_item_status='+food_item_status,
			 dataType:'json',
			 success:function(response){
				if(response.status==1){
					if(food_item_status==1){
						current_list_item.parents('li').removeClass('overstrike').find('img').remove();
						current_list_item.attr('usergrocery_meals_state',1);		
					}else{
						current_list_item.parents('li').addClass('overstrike').prepend('<img src="components/com_questionnaire_nutrition/views/grocery/tmpl/css/images/overstrike.png">');
						current_list_item.attr('usergrocery_meals_state',0);
					}
				}
			 } 
		});		
	});
	
	var nutrition_food_id='';
	var grocerylist_id='';
	var current_grocery_addbtn='';
	var food_serv_size='';
	
	$('.grocery_list_popup_lnk').click(function(){
		grocerylist_id=$(this).attr('grocery_id');	
		current_grocery_addbtn=$(this);
	});
	
	var grocery_list_qty_edt_current_item='';	
	
	$('.grocery_list_qty_edt').live('click',function(){
		grocery_list_qty_edt_current_item=$(this);
		$('#nutrition_food_name_edt_id').html(grocery_list_qty_edt_current_item.text());
		$('#nutrition_food_qty_edt_id').val($.trim(grocery_list_qty_edt_current_item.parent().prev('span').find('.foodQty').text()));	
		$('#nutrition_food_Mear_edt_id').text(grocery_list_qty_edt_current_item.parent().prev('span').find('.foodMear').text());
	});
	
	$('#update_grocery').live('click',function(){
		var grocery_food_qty=$('#nutrition_food_qty_edt_id').val();			
		var usergrocery_meals_id=grocery_list_qty_edt_current_item.attr('usergrocery_meals_id');

		$.ajax({
			 url: "index.php?option=com_questionnaire_nutrition&task=questionform.ajax_add_grocery_food_status_qty",
			 type: "POST",
			 data:'usergrocery_meals_id='+usergrocery_meals_id+'&grocery_food_qty='+grocery_food_qty,
			 dataType:'json',
			 success:function(response){
				if(response.status==1){
					grocery_list_qty_edt_current_item.parent().prev('span').find('.foodQty').text($('#nutrition_food_qty_edt_id').val());
					$('#lightbox-close').trigger('click');
				}else{
					$('#lightbox-close').trigger('click');
				}
			 } 
		});		
	});	
	
	$('#nutrition_food_id').change(function(){
		food_measure=$('#nutrition_food_id option:selected').attr('measure');	
		food_serv_size=$('#nutrition_food_id option:selected').attr('qty');	
		$('#food_measure').text(food_measure);
		$('#food_qty').val(food_serv_size);
	});
	
	$('#add_grocery').live('click',function(){
		nutrition_food_id=$('#nutrition_food_id option:selected').val();
		food_serv_size=$('#nutrition_food_id option:selected').attr('qty');
		if(nutrition_food_id==''){
			alert('Please select a food item');
			$('#nutrition_food_id').focus();
			return false;	
		}
		$.ajax({						 
			 url: "index.php?option=com_questionnaire_nutrition&task=questionform.ajax_add_grocery_food",
			 type: "POST",
			 data:'nutrition_food_id='+nutrition_food_id+'&grocerylist_id='+grocerylist_id+'&food_serv_size='+food_serv_size,
			 dataType:'json',
			 success:function(response){
				if(response.status==1){
					var nutrition_food_name=$("#nutrition_food_id option:selected").text();
					var nutrition_food_qty=$("#nutrition_food_id option:selected").attr('qty');
					var nutrition_food_measure=$("#nutrition_food_id option:selected").attr('measure');
					
					current_grocery_addbtn.parents('.grocery_list').find('ul.grocerylist').append("<li><span><span class='foodQty'>"+nutrition_food_qty+"&nbsp;</span>"+nutrition_food_measure+"</span><p> "+nutrition_food_name+"<a href='javascript:;' usergrocery_meals_id="+response.grocery_id+" usergrocery_meals_state='1' class='input_field'> &nbsp;</a></p></li>");
					$('#lightbox-close').trigger('click');	
				}else{
					alert('Food already added.');
					$('#lightbox-close').trigger('click');
				}
			 }
		});		
	});
	
	$('.update_note').live('click',function(){
		if($(this).attr('ajax')==1){
			var currentLi=$(this).parent();
			var usergrocerylist_id=currentLi.attr('grocery_id');		
			var usergrocerylist_data=currentLi.find('.notes_dataarea').val();	
			$.ajax({						 
			 url: "index.php?option=com_questionnaire_nutrition&task=questionform.ajax_update_grocery_note",
			 type: "POST",
			 data:'usergrocerylist_id='+usergrocerylist_id+'&usergrocerylist_data='+usergrocerylist_data,
			 dataType:'json',
			 success:function(response){
				if(response.status==1){
					currentLi.find('.note_inner').html(usergrocerylist_data);				
					$('#lightbox-close').trigger('click');		
					currentLi.find('.update_note').text('Update').removeAttr('ajax');
				}
			 }
			});	
			
		}else{
			var currentData=$.trim($(this).parent().find('.note_inner').text());
			$(this).parent().find('.note_inner').html('<textarea class="notes_dataarea">'+currentData+'</textarea>');		
			$(this).attr('ajax',1)
		}
	});	
});