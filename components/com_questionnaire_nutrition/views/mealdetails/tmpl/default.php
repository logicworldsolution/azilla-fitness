<?php 
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');	 
require(JPATH_SITE.'/components/com_questionnaire_nutrition/helpers/function.php');	

$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/mealdetails/tmpl/css/recipe.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/mealdetails/tmpl/js/recipe.js');

$db = JFactory::getDbo();	

$jinput = JFactory::getApplication()->input;
$loginUserId	= (int)JFactory::getUser()->get('id');


$open_id=$jinput->get('open_id', '', 'filter');
$usermeals_meal_id=$jinput->get('usermeals_meal_id', '', 'filter');
$usermeals_meal_own_id=$jinput->get('usermeals_meal_own_id', '', 'filter');

//Date Pagination
if(!empty($usermeals_meal_id)){
	$query="SELECT B.start_date,meal_number FROM `#__usermeals_meal` A JOIN #__usermeals_level B ON A.usermeals_level_id=B.id WHERE A.id =$usermeals_meal_id";
	$db->setQuery($query);
	$current_date= $db->loadObject()->start_date;
	$meal_number=  $db->loadObject()->meal_number;

	$next_date=date('Y-m-d',strtotime('+1 day',strtotime($current_date)));	
	$next_date_meal_id=meal_program_page_dates($loginUserId,$next_date,$meal_number);

	$back_date=date('Y-m-d',strtotime('-1 day',strtotime($current_date)));
	$back_date_meal_id=meal_program_page_dates($loginUserId,$back_date,$meal_number);

	$next_week=date('Y-m-d',strtotime('+1 week',strtotime($current_date)));
	$next_week_meal_id=meal_program_page_dates($loginUserId,$next_week,$meal_number);

	$back_week=date('Y-m-d',strtotime('-1 week',strtotime($current_date)));
	$back_week_meal_id=meal_program_page_dates($loginUserId,$back_week,$meal_number);
}
if(!empty($usermeals_meal_own_id)){ 
	$query="SELECT B.start_date,meal_number FROM `#__usermeals_meal_own` A JOIN #__usermeals_level_own B ON A.usermeals_level_own_id=B.id WHERE A.id =$usermeals_meal_own_id";
	$db->setQuery($query);
	$current_date= $db->loadObject()->start_date;
	$meal_number=  $db->loadObject()->meal_number;

	$next_date=date('Y-m-d',strtotime('+1 day',strtotime($current_date)));	
	$next_date_meal_id=meal_program_page_dates_own($loginUserId,$next_date,$meal_number);

	$back_date=date('Y-m-d',strtotime('-1 day',strtotime($current_date)));
	$back_date_meal_id=meal_program_page_dates_own($loginUserId,$back_date,$meal_number);

	$next_week=date('Y-m-d',strtotime('+1 week',strtotime($current_date)));
	$next_week_meal_id=meal_program_page_dates_own($loginUserId,$next_week,$meal_number);

	$back_week=date('Y-m-d',strtotime('-1 week',strtotime($current_date)));
	$back_week_meal_id=meal_program_page_dates_own($loginUserId,$back_week,$meal_number);
}


if(isset($usermeals_meal_id) && is_numeric($usermeals_meal_id)){
	/*	$query="(SELECT A.id,A.name,A.brand_name,B.serv_size,A.unit_of_measurement,A.recipe_type_id,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture
		FROM #__questionnaire_nutrition_recipes A 
		JOIN #__usermeals B ON (A.id=B.food_id AND B.is_custom_food=0) 
		WHERE B.user_id=$loginUserId and B.usermeals_meal_id=$usermeals_meal_id) 
	  UNION 
	  (SELECT A.id,A.name,A.brand_name,B.serv_size,A.unit_of_measurement,A.recipe_type_id,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,A.ingredients,A.cooking_directions
	   FROM #__usermeals_userrecipe A 
	   JOIN #__usermeals B ON (A.id=B.recipe_id AND B.is_custom_recipe=1) 
	   WHERE B.user_id=$loginUserId and B.usermeals_meal_id=$meal_id)
	 ORDER BY name ";*/
	
		$query="(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,'' recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,'' ingredients,'' cooking_directions,1 is_food,B.is_custom_food is_custom FROM #__questionnaire_nutrition_food A JOIN #__usermeals B ON (A.id=B.food_id AND B.is_custom_food=0) WHERE  B.usermeals_meal_id=$usermeals_meal_id)
	    UNION
		(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,'' recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,'' ingredients,'' cooking_directions,1 is_food,B.is_custom_food is_custom FROM #__usermeals_userfood A JOIN #__usermeals B ON (A.id=B.food_id AND B.is_custom_food=1) WHERE  B.usermeals_meal_id=$usermeals_meal_id)		
		UNION
		(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,A.recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,A.ingredients,A.cooking_directions,0 is_food,B.is_custom_recipe is_custom FROM #__questionnaire_nutrition_recipes A JOIN #__usermeals_recipe B ON (A.id=B.recipe_id AND B.is_custom_recipe=0) WHERE  B.usermeals_meal_id=$usermeals_meal_id)
		UNION
		(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,A.recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,A.ingredients,A.cooking_directions,0 is_food,B.is_custom_recipe is_custom FROM #__usermeals_userrecipe A JOIN #__usermeals_recipe B ON (A.id=B.recipe_id AND B.is_custom_recipe=1) WHERE  B.usermeals_meal_id=$usermeals_meal_id)";

		$db->setQuery( $query );
		$user_meal_details=$db->loadObjectList();//user_recipe_details
		
		$user_recipe_arr='';
		
		foreach($user_meal_details as $user_meal_detailsVal){
			$user_recipe_arr[]=$user_meal_detailsVal->recipe_type_id;
		}
		
		//Get Meal number
		$query="SELECT meal_number FROM #__usermeals_meal  WHERE id=$usermeals_meal_id";
		$db->setQuery($query);
		$meal_number=$db->loadObject()->meal_number;

}elseif(isset($usermeals_meal_own_id) && is_numeric($usermeals_meal_own_id)){
/*		
		$query="(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,A.recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,'' ingredients,'' cooking_directions,1 is_food FROM #__questionnaire_nutrition_food A JOIN #__usermeals_own B ON (A.id=B.food_id AND B.is_custom_food=0) WHERE  B.usermeals_meal_own_id=$usermeals_meal_own_id)
		UNION
		(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,A.recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,A.ingredients,A.cooking_directions,0 is_food FROM #__questionnaire_nutrition_recipes A JOIN #__usermeals_own_recipe B ON (A.id=B.recipe_id AND B.is_custom_recipe=0) WHERE  B.usermeals_meal_own_id=$usermeals_meal_own_id)";*/
		
		$query="(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,'' recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,'' ingredients,'' cooking_directions,1 is_food,B.is_custom_food is_custom FROM #__questionnaire_nutrition_food A JOIN #__usermeals_own B ON (A.id=B.food_id AND B.is_custom_food=0) WHERE  B.usermeals_meal_own_id=$usermeals_meal_own_id)
	    UNION
		(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,'' recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,'' ingredients,'' cooking_directions,1 is_food,B.is_custom_food is_custom FROM #__usermeals_userfood A JOIN #__usermeals_own B ON (A.id=B.food_id AND B.is_custom_food=1) WHERE  B.usermeals_meal_own_id=$usermeals_meal_own_id)		
		UNION
		(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,A.recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,A.ingredients,A.cooking_directions,0 is_food,B.is_custom_recipe is_custom FROM #__questionnaire_nutrition_recipes A JOIN #__usermeals_own_recipe B ON (A.id=B.recipe_id AND B.is_custom_recipe=0) WHERE  B.usermeals_meal_own_id=$usermeals_meal_own_id)
		UNION
		(SELECT A.id,A.name,A.brand_name,A.unit_of_measurement,A.recipe_type_id,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,A.notes,A.picture,A.ingredients,A.cooking_directions,0 is_food,B.is_custom_recipe is_custom FROM #__usermeals_userrecipe A JOIN #__usermeals_own_recipe B ON (A.id=B.recipe_id AND B.is_custom_recipe=1) WHERE  B.usermeals_meal_own_id=$usermeals_meal_own_id)";

		$db->setQuery( $query );
		$user_meal_details=$db->loadObjectList();//user_recipe_details

		$user_recipe_arr='';
		
		foreach($user_meal_details as $user_meal_detailsVal){
			$user_recipe_arr[]=$user_meal_detailsVal->recipe_type_id;
		}
		
		//Get Meal number
		$query="SELECT meal_number FROM #__usermeals_meal_own  WHERE id=$usermeals_meal_own_id";
		$db->setQuery($query);
		$meal_number=$db->loadObject()->meal_number;
}
?>
<h2 class="page_head">Meal <?php echo $meal_number; ?></h2>
<div id="recipe_container">
<ul id="recipe_cat">
<?php   
/*
if(isset($user_recipe_arr) and !empty($user_recipe_arr)){
	$user_recipe_arr=array_filter($user_recipe_arr);
	$user_recipe_str=implode(',',$user_recipe_arr);
	$user_recipe_arr=explode(',',$user_recipe_str);
	$user_recipe_arr=array_unique($user_recipe_arr);
	$user_recipe_str=implode(',',$user_recipe_arr);

	$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE id IN ($user_recipe_str) and  state=1 and parent_id=0";
	$db->setQuery($query);

	$bgColorArr=array('bgcolor_bf','bgcolor_lunch','bgcolor_dinner','bgcolor_vegetarian','bgcolor_vegan','bgcolor_bar','bgcolor_purple','bgcolor_green','bgcolor_blue','bgcolor_darkgreen');

	$i=0;
	foreach($db->loadObjectList() as $recipe_type_item){		
		$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE parent_id = $recipe_type_item->id";
		$db->setQuery($query);
		$chk_subItems=count($db->loadObjectList());
		if($i==10){$i=0;}
		$bgColorCurrent=$bgColorArr[$i++];
		if($chk_subItems==0){
			echo "<li class='$bgColorCurrent' ><a class='recipr_lnk'  href='javascript:;' nutrition-type-id='".$recipe_type_item->id."' >$recipe_type_item->name</a></li>";
		}else{
			echo "<li class='$bgColorCurrent'>";
			echo '<div class="dropdown">';
			echo	'<a class="dropdown-toggle" data-toggle="dropdown" data-target="#" >'.$recipe_type_item->name.'<b class="caret"></b></a>';
			echo		'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';

			foreach($db->loadObjectList() as $recipe_type_subitem){
				$recipe_type_subitem_arr=explode('_',$recipe_type_subitem->name);
				$recipe_type_subitem_name=!empty($recipe_type_subitem_arr)? $recipe_type_subitem_arr[1] :$recipe_type_subitem->name;
				echo	'<li class="'.$bgColorCurrent.'"><a class="sublink recipr_lnk" href="javascript:;" nutrition-type-id="'.$recipe_type_subitem->id.'">'.$recipe_type_subitem_name.'</a></li>';
			}	
			echo	"</ul></div></li>";
		}
	}
}
*/
?>
</ul>

<div class="accordion" id="miles_snakes_accordion">
<?php
 foreach($user_meal_details as $user_meal_detailsVal): 
 	$recipetype_id_arr=explode(',',$user_meal_detailsVal->recipe_type_id);
	foreach($recipetype_id_arr as $key=>$fruitsArr){
			$recipetype_id_arr[$key]='nuttype'.$fruitsArr;
	}
	$recipetype_id_class=implode(' ',$recipetype_id_arr);
 ?>
        <div class="accordion-group acc_group_marginnone <?php echo $recipetype_id_class; ?>">
          <div class="accordion-heading acc_top_border"> <a class="accordion-toggle <?php if($user_meal_detailsVal->id.'_'.$user_meal_detailsVal->is_custom!=$open_id){echo "collapsed";} ?> " data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#collapse<?php echo $user_meal_detailsVal->id; ?>" > <span class="plus_icon"><?php echo $user_meal_detailsVal->name; ?></span> <span class="up_click"></span> </a> </div>
          <div id="collapse<?php echo $user_meal_detailsVal->id; ?>" class="accordion-body collapse <?php if($user_meal_detailsVal->id.'_'.$user_meal_detailsVal->is_custom==
		  $open_id){echo "in";} ?>">
            <div class="accordion-inner bakround_none">
				<div class="recipedata">
                	<table class="maintable" >
                    	<tr>
                        	<td width="23%">
                            <table class="recipeinnertable" >
                            	<tr><td class="recipephto">                       
                                <?php if(!empty($user_meal_detailsVal->picture)): ?>
                                <img src="<?php echo JRoute::_(JUri::base() . 'administrator/components/com_questionnaire_nutrition/asset/'.$user_meal_detailsVal->picture);?>" width="130"  />						
                                <div class="report_image"><a href="#report_this_image" class="report_pop_lnk" data-lightbox="on" >Report This Image</a></div>
                                <?php else: ?>
                                <img style="margin-top:5px" src="<?php echo JRoute::_(JUri::base() . '/components/com_questionnaire_nutrition/asset/placeholder.png');?>"  />						
                                <?php endif; ?>
								</td></tr>
                            </table>
                            </td>
                        	<td width="54%">
                            	<table class="recipeinnertable2">
                                	<tr><td class="color555">Brand Name</td><td><?php echo $user_meal_detailsVal->brand_name; ?></td></tr>
                                    <tr><td class="color555">Serving Size</td><td><?php echo $user_meal_detailsVal->serv_size; ?>&nbsp;<?php echo $user_meal_detailsVal->unit_of_measurement; ?></td></tr>
                                    <tr><td class="color555">Calories</td><td><?php echo $user_meal_detailsVal->calories; ?></td></tr>
                                    <tr><td class="color555">Carbohydrates</td><td><?php echo $user_meal_detailsVal->carbohydrates; ?></td></tr>
                                    <tr><td class="color555">Fat</td><td><?php echo $user_meal_detailsVal->fat; ?></td></tr>
                                    <tr class="noborder"><td class="color555">Protein</td><td><?php echo $user_meal_detailsVal->protein; ?></td></tr>
                                </table>
                            </td>
                        	<td width="23%">
                            	<table class="recipeinnertable3">
                                	<tr><td class="recipenotes">Notes</td></tr>
                                    <tr><td class="recipenotesdata"><?php echo $user_meal_detailsVal->notes;?></td></tr> 
                                </table>
                            </td>
                        </tr>
                        <?php if($user_meal_detailsVal->is_food==0): ?>
                        <tr>
                        	<td colspan="3" width="100%">
                            	<div class="ingredents_div" >
                                	<h4 class="recipe_heading">INGREDIENTS</h4>
                                    <?php
									if(empty($user_meal_detailsVal->ingredients)){
									 echo "<p>No information available.</p>";
									}else{
									 echo $user_meal_detailsVal->ingredients; 
									}
									?>
                                </div>
                                <div class="ingredents_divright" >
                                	<h4 class="recipe_heading">DIRECTIONS</h4>
                                    <?php 
									if(empty($user_meal_detailsVal->cooking_directions)){
									 echo "<p>No information available.</p>";
									}else{
									 echo $user_meal_detailsVal->cooking_directions; 
									}
									?>
                                </div>
                            </td>
                       </tr>
                       <?php endif; ?>                        
                    </table>
                </div>
            </div>
          </div>
        </div>
<?php endforeach; ?>        
      </div>
</div>
<div class="repeated_process">
<?php if(!empty($usermeals_meal_id)): ?>
    <ul>
      <li><span> <a href="<?php if(check_program_page_dates_valid($loginUserId,$back_date_meal_id)){ echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=mealdetails&Itemid=242&usermeals_meal_id=$back_date_meal_id"); }else{ echo 'javascript:;'; } ?>" class="back_process">Back </a></span></li>
      <li><span> <a href="<?php if(check_program_page_dates_valid($loginUserId,$back_week_meal_id)){ echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=mealdetails&Itemid=242&usermeals_meal_id=$back_week_meal_id"); }else{ echo 'javascript:;'; } ?>" class="back_a_week">back a week </a> </span></li>
      <li><span> <a href="#"> print </a></span> </li>
      <li><span> <a href="<?php if(check_program_page_dates_valid($loginUserId,$next_week_meal_id)){  echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=mealdetails&Itemid=242&usermeals_meal_id=$next_week_meal_id"); }else{ echo 'javascript:;'; } ?>" class="repeat_process_next_week"> next week </a></span> </li>
      <li class="repeat_last_child"><span> <a href="<?php if(check_program_page_dates_valid($loginUserId,$next_date_meal_id)){ echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=mealdetails&Itemid=242&usermeals_meal_id=$next_date_meal_id"); }else{ echo 'javascript:;'; } ?>" class="repeat_process_next"> forward </a></span> </li>
    </ul>
<?php endif; ?>		
<?php if(!empty($usermeals_meal_own_id)): ?>
    <ul>
      <li><span> <a href="<?php if(check_program_page_dates_valid_own($loginUserId,$back_date_meal_id)){ echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=mealdetails&Itemid=242&usermeals_meal_own_id=$back_date_meal_id"); }else{ echo 'javascript:;'; } ?>" class="back_process">Back </a></span></li>
      <li><span> <a href="<?php if(check_program_page_dates_valid_own($loginUserId,$back_week_meal_id)){ echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=mealdetails&Itemid=242&usermeals_meal_own_id=$back_week_meal_id"); }else{ echo 'javascript:;'; } ?>" class="back_a_week">back a week </a> </span></li>
      <li><span> <a href="#"> print </a></span> </li>
      <li><span> <a href="<?php if(check_program_page_dates_valid_own($loginUserId,$next_week_meal_id)){  echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=mealdetails&Itemid=242&usermeals_meal_own_id=$next_week_meal_id"); }else{ echo 'javascript:;'; } ?>" class="repeat_process_next_week"> next week </a></span> </li>
      <li class="repeat_last_child"><span> <a href="<?php if(check_program_page_dates_valid_own($loginUserId,$next_date_meal_id)){ echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=mealdetails&Itemid=242&usermeals_meal_own_id=$next_date_meal_id"); }else{ echo 'javascript:;'; } ?>" class="repeat_process_next"> forward </a></span> </li>
    </ul>
<?php endif; ?>	
</div>
<div class="hide">
  <div id="report_this_image" >
    <h4 class="center" >Report This Image</h4>
    <div class="center">
      <table align="center" width="90%">
        <tr>
          <td><div id="report_this_image_data"></div></td>
        </tr>
      </table>
    </div>
  </div>
</div>  