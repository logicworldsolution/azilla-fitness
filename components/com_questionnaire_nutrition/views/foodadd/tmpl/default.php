<?php 
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');

$editor =JFactory::getEditor();
$params = array( 'smilies'=> '0' , 'style'  => '1' ,'layer'  => '0' ,'table'  => '0','clear_entities'=>'0' );

$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/foodadd/tmpl/css/foodadd.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/foodadd/tmpl/js/foodadd.js');

$user			= JFactory::getUser();
$loginUserId	= (int) $user->get('id');

$db = JFactory::getDbo();	

$jinput = JFactory::getApplication()->input;
$usermeals_meal_id=$jinput->get('usermeals_meal_id', '', 'filter');
$usermeals_meal_own_id=$jinput->get('usermeals_meal_own_id', '', 'filter');

$query="SELECT name FROM #__questionnaire_nutrition_units WHERE state=1 ORDER BY ordering ASC";
$db->setQuery($query);
$recipe_units=$db->loadObjectList();
?>
<h2 class="page_head">Add Food</h2>
<div id="recipe_container">

<?php if(isset($usermeals_meal_id) && is_numeric($usermeals_meal_id)): ?>

<form action="index.php?option=com_questionnaire_nutrition&task=questionform.add_food" enctype="multipart/form-data" id="add_recipe_form" method="post" class="margin0">
<input type='hidden' name='usermeals_meal_id' value='<?php echo $usermeals_meal_id; ?>'> 

<?php elseif(isset($usermeals_meal_own_id) && is_numeric($usermeals_meal_own_id)): ?>

<form action="index.php?option=com_questionnaire_nutrition&task=questionformown.add_food" enctype="multipart/form-data" id="add_recipe_form" method="post" class="margin0">
<input type='hidden' name='usermeals_meal_own_id' value='<?php echo $usermeals_meal_own_id; ?>'> 

<?php endif; ?>
 
  <div class="addrecipeblk" >
    <div class="recipedata">
      <h3 class="mealnamecontainer">Food Name
        <input type="text" name="name" class="mealname" >
      </h3>
      <table class="maintable" >
        <tr>
          <td width="23%"><table class="recipeinnertable" >
              <tr>
                <td class="recipephto"><div class="padingleft2"><img id="recipebrowseimg" class="imagehide" width="130" src="#" alt="Image" /></div>
                  <button type="button" class="btn btn-success reset_button" id="recipebrowse" >Browse</button>
                  <input type="file" name="picture" id="recipfile" class="hide">
                </td>
              </tr>
            </table></td>
          <td width="54%"><table class="recipeinnertable2">
              <tr>
                <td class="color555">Brand Name</td>
                <td><input type="text" name="brand_name" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Category</td>
                <td><select name="category_id" class="recipe_type" >
                    <option value=''>Select</option>
                    <?php 
					$query="SELECT * FROM #__questionnaire_nutrition_category WHERE state=1 AND id!=20";
					$db->setQuery($query);
					$sub_recipe_list=$db->loadObjectList();
					foreach($sub_recipe_list as $sub_recipe_listObj)
						echo "<option value='$sub_recipe_listObj->id'>$sub_recipe_listObj->name</option>";					
			  ?>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="color555">Serving Size</td>
                <td><input type="text" name="serv_size" ></td>
              </tr>
              <tr>
                <td class="color555">Unit of Measurement</td>
                <td><select name="unit_of_measurement" class="recipe_type" >
                    <option value="" selected="selected">Select</option>
                    <?php foreach($recipe_units as $recipe_unitsObj)
                          echo "<option value='".$recipe_unitsObj->name."'>{$recipe_unitsObj->name}</option>";
					?>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="color555">Blood Type</td>
                <td><select id="jform_blood_type" name="blood_type"  class="recipe_type" >
                    <option value="">Select</option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="AB">AB</option>
                    <option value="O">O</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="color555">Calories</td>
                <td><input type="text" name="calories" ></td>
              </tr>
              <tr>
                <td class="color555">Carbohydrates</td>
                <td><input type="text" name="carbohydrates" ></td>
              </tr>
              <tr>
                <td class="color555">Fat</td>
                <td><input type="text" name="fat" ></td>
              </tr>
              <tr>
                <td class="color555">Protein</td>
                <td><input type="text" name="protein" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Saturated Fat</td>
                <td><input type="text" name="saturated_fat" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Trans Fat</td>
                <td><input type="text" name="trans_fat" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Cholesterol</td>
                <td><input type="text" name="cholesterol" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Sodium</td>
                <td><input type="text" name="sodium" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Dietary Fiber</td>
                <td><input type="text" name="dietary_fiber" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Sugars</td>
                <td><input type="text" name="sugars" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Vitamin A</td>
                <td><input type="text" name="vitamin_a" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Vitamin C</td>
                <td><input type="text" name="vitamin_c" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Vitamin D</td>
                <td><input type="text" name="vitamin_d" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Vitamin K</td>
                <td><input type="text" name="vitamin_k" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Folic Acid</td>
                <td><input type="text" name="folic_acid" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Calcium</td>
                <td><input type="text" name="calcium" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Potassium</td>
                <td><input type="text" name="potassium" ></td>
              </tr>
              <tr class="color555">
                <td class="color555">Iron</td>
                <td><input type="text" name="iron" ></td>
              </tr>
              <tr class="noborder">
                <td class="color555" colspan="2">Additional Vitamins and Minerals<a href="#additional_vm" id="additional_vm_add_lnk" style="float:right"  data-lightbox="on">
                  <button type="button" class="btn btn-success">Add</button>
                  </a></td>
              </tr>
              <tr class="noborder">
                <td colspan="2">
				  <div id="jform_addvitamins"></div>
                  <div class='hide'>
                    <table id="additional_vm">
                      <tr>
                        <th colspan="2" align="center"><h3>Additional Vitamins and Minerals</h3></th>
                      </tr>
                      <tr>
                        <td><div style="float:left; padding-bottom:11px; font-weight:bold">Name:</div></td>
                        <td><input id="jform_addvitamins_name" type="text" size="40" class="inputbox jform_addvitamins_name" value="" ></td>
                      </tr>
                      <tr>
                        <td><div style="float:left; padding-bottom:11px; font-weight:bold">Value:</div></td>
                        <td><input id="jform_addvitamins_value" type="text" size="40" class="inputbox jform_addvitamins_value" value="" ></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><a href="javascript:;"  id="jform_addvitamins_submit" class="btn" >Submit</a> </td>
                      </tr>
                    </table>
                  </div></td>
              </tr>
            </table></td>
          <td width="23%"><table class="recipeinnertable3">
              <tr>
                <td class="recipenotes">Notes</td>
              </tr>
              <tr>
                <td class="recipenotesdata"><textarea name="notes" class="notes"></textarea></td>
              </tr>
            </table>
            <div class="addnewmeal">
              <button type="button" class="btn btn-success reset_button" id="add_food">Save</button>
            </div></td>
        </tr>
      </table>
    </div>
  </div>
</form>
</div>