jQuery().ready(function($){
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();
	
			reader.onload = function (e) {
				$('#recipebrowseimg').attr('src', e.target.result).show();
			}
	
			reader.readAsDataURL(input.files[0]);
		}
	}

	$('#recipebrowse').click(function(){
		$('#recipfile').trigger('click');
		
	});
	
	$("#recipfile").change(function(){
    	readURL(this);
	});
	
	$('#add_food').click(function(){
		
		var chkinvalid=false;							   
		
		$('input:text , select , textarea.notes').each(function(){
			if($(this).hasClass('inputbox'))return true;													
			var chkempty=$.trim($(this).val());		
			if(chkempty==''){
				chkinvalid=true;
				$(this).addClass('invalid');							 
			}else{
				$(this).removeClass('invalid');			
			}
		});
		
		if(chkinvalid){
			return false;
		}else{
			$('#add_recipe_form').trigger('submit');
		}
	});
	
	$('#additional_vm_add_lnk').click(function(){
			$('#jform_addvitamins_name').val('');			   
			$('#jform_addvitamins_value').val('');						   
	});	
	
	$('.remove_vam').live('click',function(){
		if(confirm('Are you sure to remove this items')) $(this).parent().remove();								
	});
	
	$('#jform_addvitamins_submit').live('click',function(){
		var jform_addvitamins_name=$.trim($('#jform_addvitamins_name').val());
		var jform_addvitamins_value=$.trim($('#jform_addvitamins_value').val());
		
		var boxnumber=$('.vitamin_min_row').length+1;
		
		if(jform_addvitamins_name==''){
			alert('Please enter vitamins and minerals name.');
			return false;
		}
		else if(jform_addvitamins_value==''){
			alert('Please enter vitamins and minerals value.');
			return false;
		}else{
			$('#jform_addvitamins').append("<div class='vitamin_min_row'><input type='text' class='vitamin_min_name' name='additional_vitamins_and_minerals["+boxnumber+"][name]' value="+jform_addvitamins_name+" >&nbsp;:&nbsp;"+"<input type='text' name='additional_vitamins_and_minerals["+boxnumber+"][value]' class='vitamin_min_val vitamin_min_class' value="+jform_addvitamins_value+" >&nbsp;<a class='remove_vam' href='javascript:;' >X</a></div>");
			$('#lightbox-close').trigger('click');	
		}
	});
	
	
});