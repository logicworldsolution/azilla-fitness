<?php 
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');	 

$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/recipe/tmpl/css/recipe.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/recipe/tmpl/js/recipe.js');

$loginUserId	= (int) JFactory::getUser()->get('id');

$db = JFactory::getDbo();	

$jinput = JFactory::getApplication()->input;

$recipe_id=$jinput->get('id', '0', 'filter');

$query="SELECT *
FROM #__questionnaire_nutrition_recipes
WHERE state =1 ORDER BY name ASC";
	
$db->setQuery( $query );
$user_recipe_details=$db->loadObjectList();

$user_recipe_arr='';
foreach($user_recipe_details as $user_recipe_detailsVal){
	$user_recipe_arr[]=$user_recipe_detailsVal->recipe_type_id;
}
?>
<h2 class="page_head">Recipes</h2>
<div id="recipe_container">
<ul id="recipe_cat">
<?php   
if(isset($user_recipe_arr) and !empty($user_recipe_arr)){
	
	$user_recipe_str=implode(',',$user_recipe_arr);
	$user_recipe_arr=explode(',',$user_recipe_str);
	$user_recipe_arr=array_unique($user_recipe_arr);
	$user_recipe_str=implode(',',$user_recipe_arr);
	
	$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE id IN ($user_recipe_str) and  state=1 and parent_id=0";
	$db->setQuery($query);

	$bgColorArr=array('bgcolor_bf','bgcolor_lunch','bgcolor_dinner','bgcolor_vegetarian','bgcolor_vegan','bgcolor_bar','bgcolor_purple','bgcolor_green','bgcolor_blue','bgcolor_darkgreen');

	$i=0;
	foreach($db->loadObjectList() as $recipe_type_item){		
		$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE parent_id = $recipe_type_item->id";
		$db->setQuery($query);
		$chk_subItems=count($db->loadObjectList());
		if($i==10){$i=0;}
		$bgColorCurrent=$bgColorArr[$i++];
		if($chk_subItems==0){
			echo "<li class='$bgColorCurrent' ><a class='recipr_lnk'  href='javascript:;' nutrition-type-id='".$recipe_type_item->id."' >$recipe_type_item->name</a></li>";
		}else{
			echo "<li class='$bgColorCurrent'>";
			echo '<div class="dropdown">';
			echo	'<a class="dropdown-toggle" data-toggle="dropdown" data-target="#" >'.$recipe_type_item->name.'<b class="caret"></b></a>';
			echo		'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';

			foreach($db->loadObjectList() as $recipe_type_subitem){
				$recipe_type_subitem_arr=explode('_',$recipe_type_subitem->name);
				$recipe_type_subitem_name=!empty($recipe_type_subitem_arr)? $recipe_type_subitem_arr[1] :$recipe_type_subitem->name;
				echo	'<li class="'.$bgColorCurrent.'"><a class="sublink recipr_lnk" href="javascript:;" nutrition-type-id="'.$recipe_type_subitem->id.'">'.$recipe_type_subitem_name.'</a></li>';
			}	
			echo	"</ul></div></li>";
		}
	}
}
?>
</ul>

<div class="accordion" id="miles_snakes_accordion">
<?php
if(count($user_recipe_details)==0){
	echo "<strong class='norecipefound'>No recipe item found</strong>";
}

 foreach($user_recipe_details as $recipes_details): 
 	$recipetype_id_arr=explode(',',$recipes_details->recipe_type_id);
	foreach($recipetype_id_arr as $key=>$fruitsArr){
			$recipetype_id_arr[$key]='nuttype'.$fruitsArr;
	}
	$recipetype_id_class=implode(' ',$recipetype_id_arr);
 ?>
        <div class="accordion-group acc_group_marginnone <?php echo $recipetype_id_class; ?>">
          <div class="accordion-heading acc_top_border"> <a class="accordion-toggle <?php if($recipes_details->id!=$recipe_id){echo "collapsed";} ?>" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#collapse<?php echo $recipes_details->id; ?>" > <span class="plus_icon"><?php echo $recipes_details->name; ?></span> <span class="up_click"></span> </a> </div>
          <div id="collapse<?php echo $recipes_details->id; ?>" class="accordion-body collapse <?php if($recipes_details->id==$recipe_id){echo "in"; } ?>">
            <div class="accordion-inner bakround_none">
				<div class="recipedata">
                	<table class="maintable" >
                    	<tr>
                        	<td width="23%">
                            <table class="recipeinnertable" >
                            	<tr><td class="recipephto">
                                 <?php if(!empty($recipes_details->picture)): ?>
                                <img src="<?php echo JRoute::_(JUri::base() . 'administrator/components/com_questionnaire_nutrition/asset/'.$recipes_details->picture);?>" width="130"  />						
                                <div class="report_image"><a href="#report_this_image" class="report_pop_lnk" data-lightbox="on" >Report This Image</a></div>
                                <?php else: ?>
                                <img style="margin-top:5px" src="<?php echo JRoute::_(JUri::base() . '/components/com_questionnaire_nutrition/asset/placeholder.png');?>"  />						
                                <?php endif; ?>
						</td></tr>
                            </table>
                            </td>
                        	<td width="54%">
                            	<table class="recipeinnertable2">
                                	<tr><td class="color555">Brand Name</td><td><?php echo $recipes_details->brand_name; ?></td></tr>
                                    <tr><td class="color555">Serving Size</td><td><?php echo $recipes_details->serv_size; ?>&nbsp;<?php echo $recipes_details->unit_of_measurement; ?></td></tr>
                                    <tr><td class="color555">Calories</td><td><?php echo $recipes_details->calories; ?></td></tr>
                                    <tr><td class="color555">Carbohydrates</td><td><?php echo $recipes_details->carbohydrates; ?></td></tr>
                                    <tr><td class="color555">Fat</td><td><?php echo $recipes_details->fat; ?></td></tr>
                                    <tr class="noborder"><td class="color555">Protein</td><td><?php echo $recipes_details->protein; ?></td></tr>
                                </table>
                            </td>
                        	<td width="23%">
                            	<table class="recipeinnertable3">
                                	<tr><td class="recipenotes">Notes</td></tr>
                                    <tr><td class="recipenotesdata"><?php echo $recipes_details->notes; ?></td></tr> 
                                </table>
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="3" width="100%">
                            	<div class="ingredents_div" >
                                	<h4 class="recipe_heading">INGREDIENTS</h4>
                                    <?php
									if(empty($recipes_details->ingredients)){
									 echo "<p>No information available.</p>";
									}else{
									 echo $recipes_details->ingredients; 
									}
									?>
                                </div>
                                <div class="ingredents_divright" >
                                	<h4 class="recipe_heading">DIRECTIONS</h4>
                                    <?php 
									if(empty($recipes_details->cooking_directions)){
									 echo "<p>No information available.</p>";
									}else{
									 echo $recipes_details->cooking_directions; 
									}
									?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
          </div>
        </div>
<?php endforeach; ?>        
      </div>
</div>
<!--<div class="repeated_process">
    <ul>
      <li><span> <a href="#" class="back_process">Back </a></span></li>
      <li><span> <a href="#" class="back_a_week">back a week </a> </span></li>
      <li><span> <a href="#"> print </a></span> </li>
      <li><span> <a href="#" class="repeat_process_next_week"> next week </a></span> </li>
      <li class="repeat_last_child"><span> <a href="#" class="repeat_process_next"> forward </a></span> </li>
    </ul>
</div>-->
<div class="hide">
  <div id="report_this_image" >
    <h4 class="center" >Report This Image</h4>
    <div class="center">
      <table align="center" width="90%">
        <tr>
          <td><div id="report_this_image_data"></div></td>
        </tr>
      </table>
    </div>
  </div>
</div>  