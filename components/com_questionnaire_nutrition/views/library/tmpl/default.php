<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);

$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');
$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/library/tmpl/css/nutritionlibrary.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/library/tmpl/js/nutritionlibrary.js');

$db = JFactory::getDbo();	
$jinput = JFactory::getApplication()->input;
$open_nid = $jinput->get('open_nid', '', 'filter');

$usermeals_meal_id=$jinput->get('usermeals_meal_id', '', 'filter');
$usermeals_meal_own_id=$jinput->get('usermeals_meal_own_id', '', 'filter');

$query="SELECT id,name FROM #__questionnaire_nutrition_category WHERE state=1 ORDER BY name ASC";
$db->setQuery($query);
?>
<h2 class="page_head">My Nutrition Library <div style="float:right;margin-top:2px;"><input type="text"  id="search_nutrition" value="Search nutrition"><button type="button" id="search_nutrition_btn" class="btn btn-success reset_button">Search</button></div></h2>
<div id="library_container">
  <div class="accordion" id="accordionMain">
<?php  foreach($db->loadObjectList() as $main_category): ?>
    <div class="accordion-group">
      <div class="accordion-heading library_main"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionMain" href="#collapsemain<?php echo $main_category->id;  ?>"> <?php echo $main_category->name; ?></a> </div>
      <div id="collapsemain<?php echo $main_category->id;  ?>" class="accordion-body <?php if($main_category->id==$open_nid) echo 'in';  ?> collapse">
<?php 
	  // List recipe items
	if($main_category->id==20){
		$query="SELECT id,name FROM #__questionnaire_nutrition_recipes WHERE state=1 ORDER BY name ASC";
		$db->setQuery($query);
	  	$childItems=count($db->loadObjectList());
		
	  if($childItems==0){
	  		echo "<div  class='accordion-inner'></div>";
	  }else{
	  foreach($db->loadObjectList() as $recipe_item):
	  	$recipe_id=$recipe_item->id;
	  ?>		  
         <div class="accordion-inner exercise_names_container">
            <div class='exercise_names'>
               <a href='<?php echo jRoute::_("index.php?option=com_questionnaire_nutrition&view=recipe&Itemid=242&id=$recipe_id"); ?>' ><?php echo $recipe_item->name; ?></a>
            </div>
         </div>
<?php endforeach; 
	  } 
	}else{
	  // List food items	
	  $query="SELECT id,name FROM #__questionnaire_nutrition_food WHERE state=1 and category_id=$main_category->id ORDER BY name ASC";
	  $db->setQuery($query);
  	  $childItems=count($db->loadObjectList());
		
	  if($childItems==0){
	  		echo "<div  class='accordion-inner'></div>";
	  }else{
	  foreach($db->loadObjectList() as $food_item):
	  	$id=$food_item->id;
	  ?>		  
         <div class="accordion-inner exercise_names_container">
            <div class='exercise_names'>
            	<?php if(!empty($usermeals_meal_id)): ?>
                <a href='<?php echo jRoute::_("index.php?option=com_questionnaire_nutrition&task=questionform.add_library_food&Itemid=242&id=$id&usermeals_meal_id=$usermeals_meal_id"); ?>' onclick="return confirm('Are you sure to add this meal')" ><?php echo $food_item->name; ?></a>
                <?php elseif(!empty($usermeals_meal_own_id)): ?>
                <a href='<?php echo jRoute::_("index.php?option=com_questionnaire_nutrition&task=questionformown.add_library_food&Itemid=242&id=$id&usermeals_meal_own_id=$usermeals_meal_own_id"); ?>' onclick="return confirm('Are you sure to add this meal')" ><?php echo $food_item->name; ?></a>
            	<?php else: ?>
                <a href='<?php echo jRoute::_("index.php?option=com_questionnaire_nutrition&view=food&Itemid=242&id=$id"); ?>'  ><?php echo $food_item->name; ?></a>
            	<?php endif; ?>
            </div>
         </div>
<?php endforeach; 
	  }
	}
	?>
      </div>
    </div>
<?php  endforeach; ?>
  </div>
</div>