jQuery().ready(function($){
	$('#search_nutrition').focus(function(){
		if($(this).val()=='Search nutrition'){ $(this).val('');}
	}).blur(function(){
		if($(this).val()==''){  $(this).val('Search nutrition');}
	});
	
	$('#search_nutrition').keypress(function(e) {
		if(e.which == 13) {
			$('#search_nutrition_btn').trigger('click');
		}
	});
	
	$('#search_nutrition_btn').click(function(e){
		var srch_val=$('#search_nutrition').val();
		$('#accordionMain .exercise_names a').each(function(){
			if($(this).text().match(srch_val,'i')){

				if ($(this).parents('.accordion-group').find('.accordion-body').hasClass('in')) {
					 $(this).parents('.accordion-group').find('.accordion-toggle').addClass('collapsed');
                    $(this).parents('.accordion-group').find('.accordion-body').removeClass('in');
                }
				var target_element=	$(this).parents('.accordion-group').find('.accordion-toggle');
				var $this = target_element,
				href, target = $this.attr('data-target') || e.preventDefault() || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
				,
				option = $(target).data('collapse') ? 'show' : $this.data();
				$(target).collapse(option);
				return false;						
			}
		});
	});
	
	$('.library_exercise_item').click(function(){
		if($(this).attr('usermeals_meal_id')){
			window.location='index.php?option=com_questionnaire_nutrition&task=questionform.add_library_food&usermeals_meal_id='+$(this).attr('usermeals_meal_id');
		return false;
		}		
	});	
});