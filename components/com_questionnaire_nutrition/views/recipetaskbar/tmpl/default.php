<?php 
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');	 

$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/recipetaskbar/tmpl/css/recipe.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/recipetaskbar/tmpl/js/recipe.js');

$db = JFactory::getDbo();	

$jinput = JFactory::getApplication()->input;

$meal_taskbar_id=$jinput->get('meal_taskbar_id');
$own_recipe=$jinput->get('own_recipe');

$user			= JFactory::getUser();
$loginUserId	= (int) $user->get('id');

//Get Recipe Title
if(isset($meal_taskbar_id) && is_numeric($meal_taskbar_id)){
	$query	= "SELECT name,recipe_id	FROM " . $db->quoteName( '#__questionnaire_nutrition_taskbar' ) . " WHERE id=$meal_taskbar_id";
	$db->setQuery( $query );
	$recipe_name=$db->loadObject()->name;
}

$recipe_ids=$db->loadObject()->recipe_id;

//Get Recipe items
if($own_recipe==1){
		$query="SELECT * FROM #__questionnaire_nutrition_recipes WHERE state=1 and id IN (SELECT recipe_id FROM #__questionnaire_nutrition_taskbar_user WHERE nutrition_taskbar_id=$meal_taskbar_id and own_nutrition_program=1 and user_id=$loginUserId) ORDER BY name ASC";
}else{
	if(isset($recipe_ids) and isset($meal_taskbar_id) and isset($loginUserId)){
		$query="SELECT * FROM #__questionnaire_nutrition_recipes WHERE state=1 and id IN($recipe_ids) or  id IN (SELECT recipe_id FROM #__questionnaire_nutrition_taskbar_user WHERE nutrition_taskbar_id=$meal_taskbar_id and own_nutrition_program=0 and user_id=$loginUserId) ORDER BY name ASC";
	}
}

$db->setQuery( $query );
$user_recipe_details=$db->loadObjectList();


$user_recipe_arr='';
foreach($user_recipe_details as $user_recipe_detailsVal){
	$user_recipe_arr[]=$user_recipe_detailsVal->recipe_type_id;
}
?>
<h2 class="page_head"><?php echo $recipe_name; ?></h2>
<div id="recipe_container">
<ul id="recipe_cat">
<?php   
if(isset($user_recipe_arr) and !empty($user_recipe_arr)){
	
	$user_recipe_str=implode(',',$user_recipe_arr);
	$user_recipe_arr=explode(',',$user_recipe_str);
	$user_recipe_arr=array_unique($user_recipe_arr);
	$user_recipe_str=implode(',',$user_recipe_arr);
	
	$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE id IN ($user_recipe_str) and  state=1 and parent_id=0 order by ordering asc";
	$db->setQuery($query);

	if($meal_taskbar_id==1 or $meal_taskbar_id==2){
		$meal_taskbar_array=array(
			array('id'=>1,'name'=>'Breakfast','bgcolor'=>'breakfast'),
			array('id'=>2,'name'=>'Lunch','bgcolor'=>'lunch'),
			array('id'=>27,'name'=>'Dinner','bgcolor'=>'dinner'),
			array('id'=>7,'name'=>'Vegan','bgcolor'=>'vegetarian'),
			array('id'=>3,'name'=>'Vegetarian','bgcolor'=>'vegan')
			);
	}elseif($meal_taskbar_id==3){
		$meal_taskbar_array=array(
			array('id'=>28,'name'=>'Salty Snacks','bgcolor'=>'light_blue'),
			array('id'=>11,'name'=>'High/Low Fat Snacks','bgcolor'=>'purplecolor'),
			array('id'=>12,'name'=>'High Protein Snacks','bgcolor'=>'light_blue'),
			array('id'=>13,'name'=>'Energy Snacks','bgcolor'=>'purplecolor'),
			array('id'=>14,'name'=>'Base Snacks','bgcolor'=>'light_blue'),
			array('id'=>15,'name'=>'My Custom Snacks','bgcolor'=>'purplecolor'),
			array('id'=>16,'name'=>'Antioxident Snacks','bgcolor'=>'light_blue'),
			array('id'=>17,'name'=>'Sweet Snacks','bgcolor'=>'purplecolor')
		);
	}elseif($meal_taskbar_id==4){
		$meal_taskbar_array=array(
			array('id'=>1,'name'=>'Breakfast','bgcolor'=>'light_blue'),
			array('id'=>2,'name'=>'Lunch','bgcolor'=>'purplecolor'),
			array('id'=>27,'name'=>'Dinner','bgcolor'=>'light_blue'),
			array('id'=>20,'name'=>'Dessert','bgcolor'=>'purplecolor'),
			array('id'=>21,'name'=>'Snacks','bgcolor'=>'light_blue'),
			array('id'=>22,'name'=>'Soups','bgcolor'=>'purplecolor'),
			array('id'=>23,'name'=>'Salads','bgcolor'=>'light_blue'),
			array('id'=>24,'name'=>'Smoothies','bgcolor'=>'purplecolor'),
			array('id'=>25,'name'=>'Juicing','bgcolor'=>'light_blue'),
			array('id'=>26,'name'=>'My Custom Recipes','bgcolor'=>'purplecolor')
		);
	}elseif($meal_taskbar_id==5){
		$meal_taskbar_array=array(
			array('id'=>18,'name'=>'Meal Replacement Shakes','bgcolor'=>'breakfast'),
			array('id'=>19,'name'=>'Meal Replacement Bars','bgcolor'=>'vegetarian')
		);
	}
	
	foreach($meal_taskbar_array as $meal_taskbar_array_item){		
		$recipe_type_item=(object)$meal_taskbar_array_item;

		$query="SELECT * FROM #__questionnaire_nutrition_recipe_type WHERE parent_id = $recipe_type_item->id";
		$db->setQuery($query);
		$chk_subItems=count($db->loadObjectList());

		//$bgColorCurrent=str_replace(array('/',' '),'_',strtolower($recipe_type_item->name));
		$bgColorCurrent=$recipe_type_item->bgcolor;
		
		if($chk_subItems==0){
			echo "<li class='$bgColorCurrent' ><a class='recipr_lnk'  href='javascript:;' nutrition-type-id='".$recipe_type_item->id."' >$recipe_type_item->name</a></li>";
		}else{
			echo "<li class='$bgColorCurrent'>";
			echo '<div class="dropdown">';
			echo	'<a class="dropdown-toggle" data-toggle="dropdown" data-target="#" >'.$recipe_type_item->name.'<b class="caret"></b></a>';
			echo		'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';

			foreach($db->loadObjectList() as $recipe_type_subitem){
				$recipe_type_subitem_arr=explode('_',$recipe_type_subitem->name);
				$recipe_type_subitem_name=!empty($recipe_type_subitem_arr)? $recipe_type_subitem_arr[1] :$recipe_type_subitem->name;
				echo	'<li class="'.$bgColorCurrent.'"><a class="sublink recipr_lnk" href="javascript:;" nutrition-type-id="'.$recipe_type_subitem->id.'">'.$recipe_type_subitem_name.'</a></li>';
			}	
			echo	"</ul></div></li>";
		}
	}
}
?>
</ul>

<div class="accordion" id="miles_snakes_accordion">
<?php
$i=0;
 foreach($user_recipe_details as $recipes_details): 
 	$recipetype_id_arr=explode(',',$recipes_details->recipe_type_id);
	foreach($recipetype_id_arr as $key=>$fruitsArr){
			$recipetype_id_arr[$key]='nuttype'.$fruitsArr;
	}
	$recipetype_id_class=implode(' ',$recipetype_id_arr);
 ?>
        <div class="accordion-group acc_group_marginnone <?php echo $recipetype_id_class; ?>">
          <div class="accordion-heading acc_top_border"> <a class="accordion-toggle <?php if($i==1){echo "collapsed";} ?>" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#collapse<?php echo $recipes_details->id; ?>" > <span class="plus_icon"><?php echo $recipes_details->name; ?></span> <span class="up_click"></span> </a> </div>
          <div id="collapse<?php echo $recipes_details->id; ?>" class="accordion-body collapse  <?php if($i==0){echo "in"; $i=1;} ?>">
            <div class="accordion-inner bakround_none">
				<div class="recipedata">
                	<table class="maintable" >
                    	<tr>
                        	<td width="23%">
                            <table class="recipeinnertable" >
                            	<tr><td class="recipephto">
                                <?php if(!empty($recipes_details->picture)): ?>
                                <img src="<?php echo JRoute::_(JUri::base() . 'administrator/components/com_questionnaire_nutrition/asset/'.$recipes_details->picture);?>" width="130"  />						
                                <div class="report_image"><a href="#report_this_image" class="report_pop_lnk" data-lightbox="on" >Report This Image</a></div>
                                <?php else: ?>
                                <img style="margin-top:5px" src="<?php echo JRoute::_(JUri::base() . '/components/com_questionnaire_nutrition/asset/placeholder.png');?>"  />						
                                <?php endif; ?>
				</td></tr>
                            </table>
                            </td>
                        	<td width="54%">
                            	<table class="recipeinnertable2">
                                	<tr><td class="color555">Brand Name</td><td><?php echo $recipes_details->brand_name; ?></td></tr>
                                    <tr><td class="color555">Serving Size</td><td><?php echo $recipes_details->serv_size; ?>&nbsp;<?php echo $recipes_details->unit_of_measurement; ?></td></tr>
                                    <tr><td class="color555">Calories</td><td><?php echo $recipes_details->calories; ?></td></tr>
                                    <tr><td class="color555">Carbohydrates</td><td><?php echo $recipes_details->carbohydrates; ?></td></tr>
                                    <tr><td class="color555">Fat</td><td><?php echo $recipes_details->fat; ?></td></tr>
                                    <tr class="noborder"><td class="color555">Protein</td><td><?php echo $recipes_details->protein; ?></td></tr>
                                </table>
                            </td>
                        	<td width="23%">
                            	<table class="recipeinnertable3">
                                	<tr><td class="recipenotes">Notes</td></tr>
                                    <tr><td class="recipenotesdata"><?php echo $recipes_details->notes; ?></td></tr> 
                                </table>
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="3" width="100%">
                            	<div class="ingredents_div" >
                                	<h4 class="recipe_heading">INGREDIENTS</h4>
                                    <?php
									if(empty($recipes_details->ingredients)){
									 echo "<p>No information available.</p>";
									}else{
									 echo $recipes_details->ingredients; 
									}
									?>
                                </div>
                                <div class="ingredents_divright" >
                                	<h4 class="recipe_heading">DIRECTIONS</h4>
                                    <?php 
									if(empty($recipes_details->cooking_directions)){
									 echo "<p>No information available.</p>";
									}else{
									 echo $recipes_details->cooking_directions; 
									}
									?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
          </div>
        </div>
<?php endforeach; ?>        
      </div>
</div>
<div class="repeated_process">
    <ul>
      <li><span> <a href="#" class="back_process">Back </a></span></li>
      <li><span> <a href="#" class="back_a_week">back a week </a> </span></li>
      <li><span> <a href="#"> print </a></span> </li>
      <li><span> <a href="#" class="repeat_process_next_week"> next week </a></span> </li>
      <li class="repeat_last_child"><span> <a href="#" class="repeat_process_next"> forward </a></span> </li>
    </ul>
</div>
<div class="hide">
  <div id="report_this_image" >
    <h4 class="center" >Report This Image</h4>
    <div class="center">
      <table align="center" width="90%">
        <tr>
          <td><div id="report_this_image_data"></div></td>
        </tr>
      </table>
    </div>
  </div>
</div>  