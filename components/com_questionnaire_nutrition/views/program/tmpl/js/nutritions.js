jQuery().ready(function($){

	$('.miles_snakes_accordion .accordion-toggle').click(function(){																	  
		if($(this).hasClass('collapsed')){
			$('.miles_snakes_accordion .accordion-toggle').addClass('collapsed');
			$(this).removeClass('collapsed');
		}															  
	});	
	
	$(".contentscroll").mCustomScrollbar({
		scrollButtons:{
			enable:true
		},
		theme:"dark"
	});
	
	$('.carousel').each(function(){
        $(this).carousel({
            interval: false
        });
    });
	
	$('.premade_details').live('click',function(){
		var premade_id=$(this).attr('premade_id');
		$.ajax({
			url:'index.php?option=com_questionnaire_nutrition&task=questionform.ajax_meal_block_detail',
			data:'premade_id='+premade_id,
			type:'post',
			dataType:'json',
			success:function(response){
				var table_data='<table class="meal_popup_tbl table"><tr><th class="food_item_name" >Food&nbsp;Name</th><th class="food_item_serv_size">Serving&nbsp;Size</th><th>Calories</th><th>Carbs</th><th>Fat</th><th>Protein</th></tr>';
				if(response.status==1){
					var i=1;
					var togg_class='';
					$.each(response.data,function(key,val){
						togg_class=(i%2==0)?'grey':'';i++;
						table_data=table_data+'<tr class="'+togg_class+'">\
							<td>'+val.name+'</td><td>'+val.serv_size+'</td><td>'+val.calories+'</td><td>'+val.carbohydrates+'</td><td>'+val.fat+'</td><td>'+val.protein+'</td>\
						 </tr>';
					});
					table_data=table_data+"</table>";
					
				}else{
					table_data=table_data+'<tr class="gray">\
							<td colspan="6">No item found</td>\
						 </tr>';
					table_data=table_data+"</table>";
				}
				$('#pre_made_snacks_data').html(table_data);
				$('#pre_made_snacks_anchr').trigger('click');
			}
		});		
	});
	
	$('#premade_snacksCarousel .carousel-inner-ul li a').click(function(e){ 
			$('#premade_snacksCarousel .carousel-inner-ul li').removeClass('active');
			$(this).parent().addClass('active');
			var calorie=$(this).text();
			$.ajax({
				url:'index.php?option=com_questionnaire_nutrition&task=questionform.ajax_meal_block',
				data:'calorie='+calorie,
				type:'post',
				dataType:'json',
				success:function(response){
					if(response.status==1){
						var li_data='';
						var i=0;
						var togg_class='';
						
						if(response.data.length<8){
							$('#meal_snacks_ullist').css('height','auto');
						}else{
							$('#meal_snacks_ullist').css('height','250px');	
						}
						
						$.each(response.data,function(key,val){
							togg_class=(i%2==0)?'grey':'';i++;
							li_data=li_data+'<p class="'+togg_class+'">\
								<span class="draggable ui-widget-content pre_made_meal_title" premade_title_id='+val.id+'  >'+val.name+'</span>\
									<button type="button" premade_id="'+val.id+'" class="btn btn-success premade_details">Details</button>\
			                 </p>';
						});
						$("#meal_snacks_ullist").html(li_data);	
						$("#meal_snacks_ullist .draggable").draggable({ appendTo: "body", helper: "clone",drag: function( event, ui ) { dragtrue=true;} });
						$("#meal_snacks_ullist.contentscroll").mCustomScrollbar("destroy");
							$("#meal_snacks_ullist.contentscroll").mCustomScrollbar({
									scrollButtons:{
										enable:true
									},
									theme:"dark"
						});
					}else{
						$('#meal_snacks_ullist').css('height','auto');
						$('#meal_snacks_ullist').html('<p class="grey center">No item found</p>');	
					}	
				}
			});			
			e.preventDefault();
	});
	
	var modify_link='';
	$('.mealmodify_pop_lnk').click(function(){
		modify_link=$(this);
		$('#meal_popup_modify_data').html($(this).parents('.meal1_diet_chart').find('.popuptblediv').html());		
	});
	
	$('.popup_update_meal').live('click',function(){
		if($(this).parents('#meal_popup_modify_data').find('.popuptble_form').serialize()!=''){	
			$.ajax({						 
				 url: "index.php?option=com_questionnaire_nutrition&task=questionform.ajax_recipe_update",
				 type: "POST",
				 data:$(this).parents('#meal_popup_modify_data').find('.popuptble_form').serialize(),
				 dataType:'json',
				 success:function(response){
					if(response.status=='1'){
						location.reload();
//						$.each(response.data,function(thiskey, thisvalue){
							//modify_link.parents('.meal1_diet_chart').find('.editable_quentity[meal-id='+thiskey+']').text(thisvalue.serve_size).next().text(thisvalue.carbohydrates).next().text(thisvalue.fat).next().text(thisvalue.protein);
						//	modify_link.parents('.meal1_diet_chart').find('input.popup_recipe_input[name='+thiskey+']').attr('value',thisvalue.serve_size);
		//				location.reload();
	//					});
			//		 $('#lightbox-close').trigger('click');	
					}
				 }
			});
		}
	});
	
	$('.popup_add_meal').live('click',function(){	  
		$.ajax({
			 url: "index.php?option=com_questionnaire_nutrition&task=questionform.ajax_add_meal",
			 dataType:'json',
			 success:function(response){
				 if(response.status=='1'){
					window.location.reload(true);
				 }else{
					alert('You can not create more than 8 meals.');
				 }
			 }
		});											  
    });
	
	$('.add_foo_item').live('click',function(){		
		window.location='index.php?option=com_questionnaire_nutrition&view=library&Itemid=242&usermeals_meal_id='+$(this).attr('usermeals_meal_id');	
		return false;
	});
	
	$('.create_custom_food').live('click',function(){
		window.location='index.php?option=com_questionnaire_nutrition&view=foodadd&Itemid=242&usermeals_meal_id='+$(this).attr('usermeals_meal_id');							  
	});
	
	$('.create_custom_recipe').live('click',function(){
		window.location='index.php?option=com_questionnaire_nutrition&view=recipeadd&Itemid=242&usermeals_meal_id='+$(this).attr('usermeals_meal_id');							  
	});
	
	var taskmeal_id='';
	var own_recipe='';
	//My nutrition taskbar add buttion popup
	$('.add_taskbar_popup_lnk').click(function(){
		taskmeal_id=$(this).attr('taskmeal-id');

		$('#meal_popup_add_taskbar_data').height('120px');	
		$('#meal_popup_add_taskbar_data').html('');
		$.ajax({
			 url: "index.php?option=com_questionnaire_nutrition&task=questionform.ajax_task_meal_list",
			 type: "POST",
			 data:'id='+taskmeal_id,
			 dataType:'json',
			 success:function(response){
				var select_list='';
				if(response.status==1){
					select_list='<select data-placeholder="Meal items" multiple class="chosen-select" style="width:200px;" id="meal_items_data">';	
						$.each(response.data,function(key,value){
							select_list=select_list+'<option  value="'+value.recipe_id+'" >'+value.name+'</option>';
						});
					select_list=select_list+'</select>';
				}else{
					select_list='<strong style="background-color:#EBEBEB;padding:1px 10px;">No meal items found</strong>'
				}
				
				//My taskbar add meal items using jquery chosen 
				$('#meal_popup_add_taskbar_data').html(select_list);
				$("#meal_items_data").chosen({display_selected_options:false}); 
				
				$('#meal_items_data').live('change',function(data){
					var meal_popup_box_height=120;										 
					if($('#meal_items_data').val()!=null){
						var number_of_sel=$('#meal_items_data').val().length;
						meal_popup_box_height=meal_popup_box_height+(26*number_of_sel);
					}
					$('#meal_popup_add_taskbar_data').height(meal_popup_box_height);													
				});
			 }
		});											   
	});	
	
	//MY nutrition taskbar add meal popup box
	$('#popup_add_recipe').click(function(){
		var number_of_sel=$('#meal_items_data').val();
		var recipe_ids=new Array();
		
		if(number_of_sel==null){
			 $('#lightbox-close').trigger('click');	
		}else{
			var i=0;
			$(number_of_sel).each(function(k,v){
				recipe_ids[i++]=v;						   
			});
			$.ajax({
				url: "index.php?option=com_questionnaire_nutrition&task=questionform.ajax_users_recipes",
				type: "POST",
				data:'taskmeal_id='+taskmeal_id+'&recipe_ids='+recipe_ids.join(),
				dataType:'json',
				success:function(response){
					var i=1;
					var toglle_class='';
					$.each(response,function(k,v){
						location.reload();
						//var	 toglle_class=(i%2==0)?"colone":"coltwo";i++;
						//$('.meal_blk_'+taskmeal_id).append("<p class='draggable ui-widget-content "+toglle_class+" ui-draggable'><span own_recipe='0' recipe_id="+v.id+">"+v.name+"</span></p>");
						//$( ".draggable" ).draggable({ appendTo: "body", helper: "clone",drag: function( event, ui ) { dragtrue=true;} });
						//$('#lightbox-close').trigger('click');	
					});
				}  
			});
		}	
	});

	
	var dragtrue=true;
	$( ".draggable" ).draggable({ appendTo: "body", helper: "clone",drag: function( event, ui ) { dragtrue=true;} });
	
	function pre_made_replace_meal(dragableData){
		var meal_id=$('.miles_snakes_accordion .accordion-group .collapse.in .meal1_diet_chart .droppable p').attr('meal-id');
		$.ajax({
			type:'post',
			dataType:'json',
			data:'premade_title_id='+dragableData.attr('premade_title_id')+'&meal_id='+meal_id,
			url:'index.php?option=com_questionnaire_nutrition&task=questionform.ajax_meal_block_replace',
			success:function(response){
				if(response.status==1){
					location.reload();
/*					var response_data='';
					$(response.data).each(function(key, value ){
						response_data=response_data+'<p meal-id='+meal_id+'>\
						  <span class="description">'+value.name+'</span>\
						  <span class="quentity editable_quentity">'+value.serv_size+'</span>\
						  <span class="carbs ">'+value.carbohydrates+'</span>\
						  <span class="fats" >'+value.fat+'</span>\
						  <span class="pro" >'+value.protein+'</span>\
						  <span class="cals" >'+value.calories+'</span>\
						 </p>';							
					});					
					$('.miles_snakes_accordion .accordion-group .collapse.in').find('.droppable').addClass( "ui-state-highlight" ).prepend(response_data);
					ui.draggable.remove();
					$('.miles_snakes_accordion .accordion-group .collapse.in').find(".contentscroll").mCustomScrollbar("destroy");
					$('.miles_snakes_accordion .accordion-group .collapse.in').find(".contentscroll").mCustomScrollbar({
							scrollButtons:{
								enable:true
							},
							theme:"dark"
					}); */
				}
			}
		});
	}
	
	$('.meal_completed').live('click',function(){
		$.ajax({
			type:'post',
			data:'usermeals_meal_id='+$(this).attr('usermeals_meal_id'),
			url:'index.php?option=com_questionnaire_nutrition&task=questionform.ajax_meal_complete',
			success:function(response){
				if(response=='success'){
					location.reload();
				}				
			}
	    });		
	})
	
	$('.never_show_again').live('click',function(){
		if(!never_show_again){
			if($('#never_show_again_checkbox').is(':checked')){
				never_show_again=true;	
			}
			if(is_confirm_yes){
				pre_made_replace_meal(dragableData);
				is_confirm_yes=false;
			}
			$('#lightbox-close').trigger('click');
		}
	});
	
	var never_show_again=false;
	var	is_confirm_yes=false;
	var dragableData='';
	
	$( ".droppable" ).droppable({
		activeClass: "ui-state-hover",
		hoverClass: "ui-state-active",
		drop: function( event, ui ) {
		//	alert($(this).find('p').attr('meal-id'));
			if(ui.draggable.hasClass('pre_made_meal_title')){	
			//Drop from pre-made meals
					is_confirm_yes=false;
					var pre_made_meal_msg="<div>Are you sure you want to replace  this meal?</div><br><div><button type='button' class='btn btn-success confirm_yes'>Yes</button><button type='button' class='btn btn-success confirm_no'>No</button></div>";
					var pre_made_never_show_again="<div class='width240'><input type='checkbox' name='never_show_again_checkbox' id='never_show_again_checkbox' value='1' / ><label class='floatright cursorpointer' for='never_show_again_checkbox'>Never show me this message again</label></div><br><button type='button' class='btn btn-success never_show_again'>Ok</button>";
					if(!never_show_again){
						$('#pre_made_snacks_confirm_box').html(pre_made_meal_msg);
						$('#dialogbox').trigger('click');
						
						$('.confirm_yes').live('click',function(){
							$('#pre_made_snacks_confirm_box').html(pre_made_never_show_again);
							$('#dialogbox').trigger('click');
							is_confirm_yes=true;
						});
						$('.confirm_no').live('click',function(){
							$('#pre_made_snacks_confirm_box').html(pre_made_never_show_again);
							$('#dialogbox').trigger('click');
						});
						dragableData=ui.draggable;
					}else{
						pre_made_replace_meal(ui.draggable);	
					}
			}else{
			//Drop from nutrition taskbar items				
				var meal_id=$('.miles_snakes_accordion .accordion-group .collapse.in .meal1_diet_chart .droppable p').attr('meal-id');
				if(dragtrue){
					$.ajax({
						url:'index.php?option=com_questionnaire_nutrition&task=questionform.ajax_get_recipe_detail',
						type:'POST',
						async :false,
						data:'recipe_id='+ui.draggable.find('span').attr('recipe_id')+'&meal_id='+meal_id,
						dataType:'JSON',
						success:function(response) {
							dragtrue=false;						
							if(response.status==1){
								location.reload();
								/*
								$('.miles_snakes_accordion .accordion-group .collapse.in').find('.droppable').addClass( "ui-state-highlight" ).prepend('<p meal-id='+meal_id+'>\
								  <span class="description">'+response.data.name+'</span>\
								  <span class="quentity editable_quentity">'+response.data.serv_size+'</span>\
								  <span class="carbs ">'+response.data.carbohydrates+'</span>\
								  <span class="fats" >'+response.data.fat+'</span>\
								  <span class="pro" >'+response.data.protein+'</span>\
								  <span class="cals" >'+response.data.calories+'</span>\
								 </p>');
								ui.draggable.remove();
								$('.miles_snakes_accordion .accordion-group .collapse.in').find(".contentscroll").mCustomScrollbar("destroy");
								$('.miles_snakes_accordion .accordion-group .collapse.in').find(".contentscroll").mCustomScrollbar({
										scrollButtons:{
											enable:true
										},
										theme:"dark"
								}); 
								*/
							}else{
								alert('Recipe Already added.');
							}
						}
					});
				}
			}
		}
	});	
	
	$('.reset_meal').click(function(){
		var is_reset=confirm('Are your sure to reset the food');	
		if(is_reset){
			$.ajax({
				url:'index.php?option=com_questionnaire_nutrition&task=questionform.ajax_food_reset',
				type:'POST',
				data:'usermeals_meal_id='+$(this).attr('usermeals_meal_id'),
				success:function(response) {
					if(response=='success'){
						location.reload();	
					}
				}
			});	
		}
	});
});