<?php 
	defined('_JEXEC') or die;
	$doc = JFactory::getDocument();
	JHtml::_('bootstrap.framework');
	require(JPATH_SITE.'/components/com_questionnaire_nutrition/helpers/function.php');	 

	$doc->addStyleSheet(JURI::base().'components/com_questionnaire_nutrition/views/ownprogram/tmpl/scroller/css/jquery.mCustomScrollbar.css');
	$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/ownprogram/tmpl/scroller/js/jquery.mCustomScrollbar.concat.min.js');
	$doc->addScript(JURI::base().'components/com_questionnaire_nutrition/views/ownprogram/tmpl/js/ownnutritions.js');
	$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/ownprogram/tmpl/css/ownnutrition.css', $type = 'text/css');
	
	//choosen
	$doc->addStyleSheet(JURI::base().'media/custom/chosen.css');
	$doc->addScript(JURI::base().'media/custom/chosen.jquery.min.js');
	
	$db = JFactory::getDbo();	
	$loginUserId	= (int) JFactory::getUser()->get('id');	
	$app			= JFactory::getApplication();
	
	//Date settings
	$ndate = $app->input->get('ndate');

	if(!empty($ndate)){
		$current_date=date('Y-m-d',strtotime($ndate));
	}else{
		$current_date=date('Y-m-d');
	}	

	$next_date=date('Y-m-d',strtotime('+1 day',strtotime($current_date)));
	$back_date=date('Y-m-d',strtotime('-1 day',strtotime($current_date)));
	$next_week=date('Y-m-d',strtotime('+1 week',strtotime($current_date)));
	$back_week=date('Y-m-d',strtotime('-1 week',strtotime($current_date)));

	//Get user question answers from questionnaire
	$query="SELECT A.eat_a_day_meal_snacks,A.foods_condiments_like,A.blood_type,A.nutrition_program_start,B.calories_intake,B.carbs_intake,B.fat_intake,B.protein_intake FROM #__questionnaire_question A JOIN #__userworkouts_info B ON A.id=B.user_id WHERE A.id=$loginUserId";
	$db->setQuery($query);
	$user_nutrition_infoObj=$db->loadObject();
	
	//Ensure user has enter all information in questionnaire 
	if(empty($user_nutrition_infoObj->foods_condiments_like)){
		$app->redirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=243", false),"Please choose your foods and condiments.", 'notice');
	}
	elseif(empty($user_nutrition_infoObj->eat_a_day_meal_snacks)){
		$app->redirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=243", false),"Please enter your eat a day meals and snacks.", 'notice');
	}elseif(empty($user_nutrition_infoObj->nutrition_program_start) and $user_nutrition_infoObj->nutrition_program_start=='0000-00-00'){
		$app->redirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=243", false),"Please enter nutrition program start date.", 'notice');
	}
	
	//Get the user food items like in questions
	$recipe_list=array();
	if(!empty($user_nutrition_infoObj->foods_condiments_like)){
	$userQuestioFood=json_decode($user_nutrition_infoObj->foods_condiments_like,'array');
		foreach($userQuestioFood as $recipr_cat=>$userQuestioFood_items){ 
			foreach($userQuestioFood_items as $userQuestioFood_itemsVal){
				$recipe_list[]=$userQuestioFood_itemsVal;
			}
		}
	}
	$recipe_in=implode(',',$recipe_list);
	
	//Check the user can create minimum 5 and maximum 8	meals
	$eat_a_day_meal_snacks=$user_nutrition_infoObj->eat_a_day_meal_snacks;
	if($eat_a_day_meal_snacks==2){
		$eat_a_day_meal_snacks=$eat_a_day_meal_snacks+3;
	}
	elseif($eat_a_day_meal_snacks==3){
		$eat_a_day_meal_snacks=$eat_a_day_meal_snacks+2;
	}
	elseif(in_array($eat_a_day_meal_snacks,array(4,5,6,7))){
		$eat_a_day_meal_snacks=$eat_a_day_meal_snacks+1;
	}
	
	$nutrition_program_start=$user_nutrition_infoObj->nutrition_program_start;
	$user_blood_type=$user_nutrition_infoObj->blood_type;

	//create custom nutritions
	$query="SELECT  COUNT(id) as usermeal_nubm FROM #__usermeals_level_own WHERE user_id=$loginUserId  AND start_date >= curdate()";
	$db->setQuery($query);

	if($db->loadObject()->usermeal_nubm==0){
			$query="INSERT INTO #__usermeals_level_own(user_id,start_date) VALUES($loginUserId,curdate())";
			$db->setQuery($query);
			$db->query();
			$usermeals_level_id=$db->insertid();
			//Checks user blood type to search recipe.
			if(!empty($user_blood_type)){
				if($user_blood_type=='A'){
					$carbs_intake='55 and 65';
					$fat_intake='20 and 25';
					$protein_intake='20 and 25';	
				}elseif($user_blood_type=='B'){
					$carbs_intake='45 and 55';
					$fat_intake='20 and 30';
					$protein_intake='25 and 35';
				}elseif($user_blood_type=='AB'){
					$carbs_intake='50 and 60';
					$fat_intake='20 and 25';
					$protein_intake='25 and 35';
				}elseif($user_blood_type=='O'){
					$carbs_intake='40 and 50';
					$fat_intake='25 and 30';
					$protein_intake='25 and 35';	
				}
			}else{
					$carbs_intake='50 and 60';
					$fat_intake='20 and 25';
					$protein_intake='25 and 35';
			}
			
			

			$user_meal_number=(int)$eat_a_day_meal_snacks;
			$food_item_limit1=rand(1,2);
		    $food_item_limit2=rand(2,3);
			//Create user meal like Meal 1, Meal 2,etc.
			for($j=1;$j<=$user_meal_number;$j++){
				$query="INSERT INTO	#__usermeals_meal_own(usermeals_level_own_id,meal_number)VALUES($usermeals_level_id,$j)";
				$db->setQuery($query);
				$db->query();
				$usermeals_meal_own_id=$db->insertid();
				
				//Query to search user recipes from recipe libraray based on user question/answers.
				$query="(SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 AND (id IN($recipe_in) AND (carbohydrates between $carbs_intake AND fat between $fat_intake AND protein between $protein_intake)) limit $food_item_limit1) UNION (SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 LIMIT $food_item_limit2)";
	
				$db->setQuery($query);
				//Create food items under each meals
				foreach($db->loadObjectList() as $fooddbList){
					$query="INSERT IGNORE INTO	#__usermeals_own(user_id,usermeals_meal_own_id,recipe_id,serv_size,carbohydrates,fat,protein,calories)
					VALUES(	$loginUserId,$usermeals_meal_own_id,$fooddbList->id,$fooddbList->serv_size,$fooddbList->carbohydrates,$fooddbList->fat,$fooddbList->protein,$fooddbList->calories)";
					$db->setQuery($query);
					$db->query();
				}
			}
		}
	
	//Get target calories
	$targetCalories=$user_nutrition_infoObj->calories_intake;
	$targetCarbs=$user_nutrition_infoObj->carbs_intake;
	$targetFats=$user_nutrition_infoObj->fat_intake;
	$targetProtein=$user_nutrition_infoObj->protein_intake;
	$totaltarget_carbs_fats_protein=$targetCarbs+$targetFats+$targetProtein;	
	
	create_user_meals($loginUserId,$nutrition_program_start,$recipe_in,$eat_a_day_meal_snacks,$user_blood_type);
	//Get user acucal calories intake
	$userActualCarbCalFatPro=calculate_cal_intake();
	
	$actualCalories=$userActualCarbCalFatPro['actualCalories'];
	
    $actualCarbs=$userActualCarbCalFatPro['actualCarbs'];
    $actualFats=$userActualCarbCalFatPro['actualFats'];
    $actualProtein=$userActualCarbCalFatPro['actualProtein'];
    $totalactual_carbs_fats_protein=$actualCarbs+$actualFats+$actualProtein; 
	
	//Get user current date recipe id
	$query="SELECT id FROM #__usermeals_level_own WHERE user_id=$loginUserId AND start_date='$current_date'";
	$db->setQuery($query);
	$usermeals_date_lvl_id= @$db->loadObject()->id;
?>
<div class="nutrition_home">
  <div class="row-fluid">
    <div class="aazilla_span9" id="content" style="width:100% !important; padding-left:2px !important">
      <!-- Begin Content -->
      <h2 class=" page_head text-uppercase"> Create My Own Nutrition Program</h2>
      <div class="row-fluid show-grid grid_bottom_level">
        <div id="target_calorie" class="accordion span6 diet_chart_home">
          <div class="accordion-group">
            <div class="accordion-heading">
              <h3 class="yield_target uppercase_text"> target calorie intake <a href="#collapseTargetOne" data-parent="#target_calorie" data-toggle="collapse"></a> </h3>
            </div>
            <div class="accordion-body in collapse diet_chart_inner" id="collapseTargetOne" style="height: auto;">
              <h1> <?php echo $targetCalories; ?> cal. </h1>
              <div class="nutrition_left">
                <ul class="nutrition_diet_list">
                  <li class="nutrition_width">
                    <div class="carbs-img"> </div>
                    Carbs </li>
                  <li><?php echo $percentageCarbs= round($targetCarbs/$totaltarget_carbs_fats_protein*100); ?>%</li>
                  <li  class="paddingright0"> <?php echo $targetCarbs; ?>g </li>
                </ul>
                <ul class="nutrition_diet_list">
                  <li class="nutrition_width">
                    <div class="carbs-img fat_color"> </div>
                    Fats </li>
                  <li><?php echo $percentageFats= round($targetFats/$totaltarget_carbs_fats_protein*100); ?>%</li>
                  <li  class="paddingright0"> <?php echo $targetFats; ?>g </li>
                </ul>
                <ul class="nutrition_diet_list nutrition_border_none">
                  <li class="nutrition_width">
                    <div class="carbs-img protin_color"> </div>
                    Protein </li>
                  <li><?php echo $percentageProtein= round($targetProtein/$totaltarget_carbs_fats_protein*100); ?>%</li>
                  <li class="paddingright0"> <?php echo $targetProtein; ?>g </li>
                </ul>
              </div>
              <div class="nutrition_wheel"><img src="https://chart.googleapis.com/chart?chs=126x117&cht=pc&chco=2C7575|53C2B5|BFDA71&chd=t:<?php echo $percentageCarbs.','.$percentageFats.','.$percentageProtein;  ?>"  alt="Target calorie intake chart" /></div>
            </div>
          </div>
        </div>
        <div id="actual_calorie" class="accordion span6 diet_chart_home">
          <div class="accordion-group">
            <div class="accordion-heading">
              <h3 class="yield_target uppercase_text"> actual calorie intake <a  href="#collapseActualTwo" data-parent="#actual_calorie" data-toggle="collapse"></a> </h3>
            </div>
            <div class="accordion-body in collapse diet_chart_inner" id="collapseActualTwo" style="height: auto;">
              <h1 class="extra_cal"> <?php echo $actualCalories; ?> cal. </h1>
              <div class="nutrition_left">
                <ul class="nutrition_diet_list">
                  <li class="nutrition_width">
                    <div class="carbs-img"> </div>
                    Carbs </li>
                  <li> <?php echo $percentageActualCarbs= round($actualCarbs/$totalactual_carbs_fats_protein*100); ?>% </li>
                  <li class="paddingright0"> <?php echo $actualCarbs; ?>g </li>
                </ul>
                <ul class="nutrition_diet_list">
                  <li class="nutrition_width">
                    <div class="carbs-img fat_color"> </div>
                    Fats </li>
                  <li><?php echo $percentageActualFats= round($actualFats/$totalactual_carbs_fats_protein*100); ?>%</li>
                  <li class="paddingright0"> <?php echo $actualFats; ?>g </li>
                </ul>
                <ul class="nutrition_diet_list nutrition_border_none">
                  <li class="nutrition_width">
                    <div class="carbs-img protin_color"> </div>
                    Protein </li>
                  <li><?php echo $percentageActualProtein= round($actualProtein/$totalactual_carbs_fats_protein*100); ?>%</li>
                  <li class="paddingright0"> <?php echo $actualProtein; ?>g </li>
                </ul>
              </div>
              <div class="nutrition_wheel"><img src="https://chart.googleapis.com/chart?chs=126x117&cht=pc&chco=2C7575|53C2B5|BFDA71&chd=t:<?php echo $percentageActualCarbs.','.$percentageActualFats.','.$percentageActualProtein;  ?>"  alt="Actual calorie intake chart" /></div>
            </div>
          </div>
        </div>
      </div>
      <div class="accordion" id="my_nutrition_taskbar">
        <div class="accordion-group">
          <div class="accordion-heading">
            <h3  class="yield_target uppercase_text borderbotbebe" >My Nutrition Task Bar <a  data-toggle="collapse" data-parent="#my_nutrition_taskbar" href="#collapseOne1"></a></h3>
          </div>
          <div id="collapseOne1" class="accordion-body collapse in">
            <div>
              <?php
			$query="SELECT id,name FROM #__questionnaire_nutrition_recipes WHERE (state=1 and id IN (SELECT recipe_id FROM #__questionnaire_nutrition_taskbar_user WHERE nutrition_taskbar_id=1 and own_nutrition_program=1 and user_id=$loginUserId)) order by name asc";
			$db->setQuery($query);
			$mealrecipeObject=$db->loadObjectList();

			$query="SELECT id,name FROM #__questionnaire_nutrition_recipes WHERE (state=1 and id IN (SELECT recipe_id FROM #__questionnaire_nutrition_taskbar_user WHERE nutrition_taskbar_id=2 and own_nutrition_program=1 and user_id=$loginUserId)) order by name asc";		
			$db->setQuery($query);
			$quickmealrecipeObject=$db->loadObjectList();

			$query="SELECT id,name FROM #__questionnaire_nutrition_recipes WHERE (state=1 and id IN (SELECT recipe_id FROM #__questionnaire_nutrition_taskbar_user WHERE nutrition_taskbar_id=3 and own_nutrition_program=1 and user_id=$loginUserId)) order by name asc";
			$db->setQuery($query);
			$snacksrecipeObject=$db->loadObjectList();

			$query="SELECT id,name FROM #__questionnaire_nutrition_recipes WHERE (state=1 and id IN (SELECT recipe_id FROM #__questionnaire_nutrition_taskbar_user WHERE nutrition_taskbar_id=4 and own_nutrition_program=1 and user_id=$loginUserId)) order by name asc";				
			$db->setQuery($query);
			$myrecipeObject=$db->loadObjectList();

			$query="SELECT id,name FROM #__questionnaire_nutrition_recipes WHERE (state=1 and id IN (SELECT recipe_id FROM #__questionnaire_nutrition_taskbar_user WHERE nutrition_taskbar_id=5 and own_nutrition_program=1 and user_id=$loginUserId)) order by name asc";
			
			$db->setQuery($query);
			$proteinrecipeObject=$db->loadObjectList();
			 ?>
              <table class="meal_chart_table" width="100%">
                <tr>
                  <th><a href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=1&own_recipe=1'); ?>">meals</a></th>
                  <th><a href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=2&own_recipe=1'); ?>">quick meals</a></th>
                  <th><a href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=3&own_recipe=1'); ?>">snacks</a></th>
                  <th><a href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=4&own_recipe=1'); ?>">My Recipes</a></th>
                  <th class="bordernone"><a href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=5&own_recipe=1'); ?>">Bars/Shakes</a></th>
                </tr>
                <tr>
                  <td><div class="contentscroll meal_lists" ><a class="meal_blk_1" href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=1&own_recipe=1'); ?>"  >
                      <?php 
				  $class_alt=1;
				  foreach($mealrecipeObject as $mealrecipeItems){
						  $alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
							echo '<p class="draggable ui-widget-content '.$alter_class.'"><span own_recipe="1" recipe_id="'.$mealrecipeItems->id.'">'.$mealrecipeItems->name.'</span></p>';		
				  } ?>
                      </a></div></td>
                  <td><div class="contentscroll meal_lists"><a class="meal_blk_2" href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=2&own_recipe=1'); ?>"  >
                      <?php
				  $class_alt=1;
				  foreach($quickmealrecipeObject as $mealrecipeItems){
				  		 $alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
						echo '<p class="draggable ui-widget-content '.$alter_class.'"><span  own_recipe="1" recipe_id="'.$mealrecipeItems->id.'">'.$mealrecipeItems->name.'</span></p>';							
				  } ?>
                      </a></div></td>
                  <td><div class="contentscroll meal_lists"><a class="meal_blk_3" href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=3&own_recipe=1'); ?>"  >
                      <?php 
				  $class_alt=1;
				  foreach($snacksrecipeObject as $mealrecipeItems){
						   $alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
							echo '<p class="draggable ui-widget-content '.$alter_class.'"><span  own_recipe="1" recipe_id="'.$mealrecipeItems->id.'">'.$mealrecipeItems->name.'</span></p>';		  
				  } ?>
                      </a></div></td>
                  <td><div class="contentscroll meal_lists"><a class="meal_blk_4" href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=4&own_recipe=1'); ?>"  >
                      <?php
				 $class_alt=1;
				 foreach($myrecipeObject as $mealrecipeItems){
						  $alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
							echo '<p class="draggable ui-widget-content '.$alter_class.'"><span own_recipe="1"  recipe_id="'.$mealrecipeItems->id.'">'.$mealrecipeItems->name.'</span></p>';	  
				  } ?>
                      </a></div></td>
                  <td class="bordernone"><div class="contentscroll meal_lists"><a class="meal_blk_5" href="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&view=recipetaskbar&Itemid=242&meal_taskbar_id=5&own_recipe=1'); ?>"  >
                      <?php
					$class_alt=1;
					foreach($proteinrecipeObject as $mealrecipeItems){
							 $alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
							echo '<p class="draggable ui-widget-content '.$alter_class.'"><span  own_recipe="1" recipe_id="'.$mealrecipeItems->id.'">'.$mealrecipeItems->name.'</span></p>';	  
				  } ?>
                      </a></div></td>
                </tr>
                <tr class="meal_add">
                  <td><a href="#meal_popup_add_taskbar" taskmeal-id="1"  own_recipe="0" class="add_taskbar_popup_lnk" data-lightbox="on">
                    <button type="button" class="btn btn-success food_item_add">Add</button>
                    </a></td>
                  <td><a href="#meal_popup_add_taskbar" taskmeal-id="2"  own_recipe="0" class="add_taskbar_popup_lnk" data-lightbox="on">
                    <button type="button" class="btn btn-success food_item_add">Add</button>
                    </a></td>
                  <td><a href="#meal_popup_add_taskbar" taskmeal-id="3"  own_recipe="0" class="add_taskbar_popup_lnk" data-lightbox="on">
                    <button type="button" class="btn btn-success food_item_add">Add</button>
                    </a></td>
                  <td><a href="#meal_popup_add_taskbar" taskmeal-id="4"  own_recipe="0" class="add_taskbar_popup_lnk" data-lightbox="on">
                    <button type="button" class="btn btn-success food_item_add">Add</button>
                    </a></td>
                  <td class="bordernone"><a href="#meal_popup_add_taskbar" taskmeal-id="5"  own_recipe="0" class="add_taskbar_popup_lnk" data-lightbox="on">
                    <button type="button" class="btn btn-success food_item_add">Add</button>
                    </a></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      
      <?php
	  //query to get pre-made meal based on calorie values.
	  $query="SELECT id,name FROM #__questionnaire_nutrition_meal_block WHERE calorie_count=100";
	  $db->setQuery($query);
	  $pre_made_meal_list=$db->loadObjectList();
	  ?>
      <div class="accordion" id="premade_snacks_meals">
        <div class="accordion-group">
          <div class="accordion-heading">
            <h3 class="yield_target uppercase_text" >Pre-Made Snacks/Meals<a  data-toggle="collapse" data-parent="#premade_snacks_meals" href="#premade_snacks_meals_inner"></a></h3>
          </div>
          <div id="premade_snacks_meals_inner" class="accordion-body collapse in">
            <div class="premade_snacks_meals_data">
                <div id="premade_snacksCarousel" class="carousel slide"> <a class="carousel-control left most_popular_left" href="#premade_snacksCarousel" data-slide="prev">&lsaquo;</a>
                  <div class="carousel-inner">
                    <div class="active item">
                      <ul class="carousel-inner-ul">
                        <li class="active"> <a href="#"> 100 </a> </li>
                        <li> <a href="#"> 150 </a></li>
                        <li> <a href="#"> 200</a></li>
                        <li><a href="#">250</a></li>
                        <li><a href="#">300</a></li>
                        <li><a href="#">350</a></li>
                        <li><a href="#">400</a></li>
                      </ul>
                    </div>
                    <div class="item">
                      <ul class="carousel-inner-ul">
                        <li> <a href="#">450</a></li>
                        <li> <a href="#">500</a></li>
                        <li><a href="#">550</a></li>
                        <li><a href="#">600</a></li>
                        <li><a href="#">650</a></li>
                        <li><a href="#">700</a></li>
                        <li><a href="#">750</a></li>
                      </ul>
                    </div>
                    <div class="item">
                      <ul class="carousel-inner-ul">
                        <li><a href="#">800</a></li>
                        <li><a href="#">850</a></li>
                        <li><a href="#">900</a></li>
                        <li><a href="#">950</a></li>
                        <li><a href="#">1000</a></li>
                      </ul>
                    </div>
                  </div>
                  <a class="carousel-control right most_popular_right" href="#premade_snacksCarousel" data-slide="next">&rsaquo;</a>
              </div>
              <div class="premade-snake-listing">
              <a href="#pre_made_snacks" id="pre_made_snacks_anchr" class="hide" data-lightbox="on"></a>
                <div id="meal_snacks_ullist" class="contentscroll meal_lists">
                <?php
				 $i=0;
				 foreach($pre_made_meal_list as $pre_made_meal_listVal): 
				 $calss_name=($i%2==0)?'grey':'';$i++;
				 ?>
                  <p class="<?php echo $calss_name; ?>"> <span premade_title_id="<?php echo $pre_made_meal_listVal->id; ?>" class="draggable ui-widget-content pre_made_meal_title" > <?php echo $pre_made_meal_listVal->name; ?> </span>
                   <button class="btn btn-success premade_details" premade_id="<?php echo $pre_made_meal_listVal->id; ?>" type="button">Details</button>
                  </p>
                <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

     <h1 class="meal_description_heading"> meal/snack description and breakdowns </h1>
	<table id="meal_snacks_tbl" width="100%" cellspacing="0" cellpadding="0" border="1">
    <tbody>
    <?php 
	  $sumtotalCarbs=0;
	  $sumtotalFats=0;
	  $sumtotalProtein=0;
	  $sumtotalCalories=0;

   $user_meal_list=array();
  
   if(!is_null($usermeals_date_lvl_id)){
	  $query="SELECT * FROM #__usermeals_meal_own WHERE  usermeals_level_own_id=$usermeals_date_lvl_id";
  	  $db->setQuery($query);
	  $user_meal_list= $db->loadAssocList();
   }	
   if(empty($user_meal_list)){  
		for($i=1;$i<=$eat_a_day_meal_snacks;$i++){
			 $user_meal_list[$i]['meal_number']=$i;
			 $user_meal_list[$i]['id']=0;
		}  
   }	

	  foreach($user_meal_list as $nutrition_mealObj):
	 	$meal_i=$nutrition_mealObj['meal_number'];
		$usermeals_meal_id=$nutrition_mealObj['id'];
	   ?>
       <tr>
       <td>
       <div class="accordion miles_snakes_accordion" >
        <div class="accordion-group acc_group_marginnone">
          <div class="accordion-heading acc_top_border"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent=".miles_snakes_accordion" href="#collapse<?php echo $meal_i; ?>" > <span class="plus_icon"> meal <?php echo $meal_i; ?></span> <span class="up_click"></span> </a> </div>
          <div id="collapse<?php echo $meal_i; ?>" meal-id="<?php echo $meal_i; ?>" class="accordion-body collapse <?php if($meal_i==1) echo "in"; ?>">
            <div class="accordion-inner bakround_none">
              <div class="meal1_description">
                <?php
				 $totalCarbs=0;
				 $totalFats=0;
				 $totalProtein=0;
				 $totalCalories=0;
				  
				if($usermeals_meal_id!=0){
			  	  // Get recipe items from system table(__questionnaire_nutrition_recipes) and user table(__usermeals_own)
				  $query="(SELECT A.id,A.name,A.unit_of_measurement,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,B.recipe_id,B.id mealId,B.is_custom_recipe FROM #__questionnaire_nutrition_food A JOIN #__usermeals_own B ON (A.id=B.recipe_id AND B.is_custom_recipe=0) WHERE  B.usermeals_meal_id=$usermeals_meal_id)
				  UNION 
				  (SELECT A.id,A.name,A.unit_of_measurement,B.serv_size,B.calories,B.carbohydrates,B.fat,B.protein,B.recipe_id,B.id mealId,B.is_custom_recipe FROM #__usermeals_userrecipe A JOIN #__usermeals_own B ON (A.id=B.recipe_id AND B.is_custom_recipe=1) WHERE B.usermeals_meal_id=$usermeals_meal_id)
				  ORDER BY name";
				  
				  $db->setQuery($query);
				
				   foreach($db->loadObjectList() as $mealItems){
				   		$totalCarbs+=	$mealItems->carbohydrates;		
						$totalFats+=	$mealItems->fat;		
						$totalProtein+=	$mealItems->protein;	
						$totalCalories+=$mealItems->calories;
				   }
				   
			   	   $sumtotalCarbs+=$totalCarbs;
				   $sumtotalFats+=$totalFats;
				   $sumtotalProtein+=$totalProtein;
				   $sumtotalCalories+=$totalCalories;
				   
				   $totalIngredents=$totalCarbs+$totalFats+$totalProtein;
				   $carbspercentage=@round($totalCarbs/$totalIngredents,2);
				   $fatpercentage=@round($totalFats/$totalIngredents,2);
				   $proteinpercentage=@round($totalProtein/$totalIngredents,2);
				   $chartpercentagedata=$carbspercentage.','.$fatpercentage.','.$proteinpercentage;
				} 
			  ?>
                <div class="nutrition_chart"> <img alt="Nutrition Chart" src="https://chart.googleapis.com/chart?chs=168x235&chxl=0:|Carbs|Fat|Protein&chxp=0,17,50,85&chxs=0,333333&chxt=x&chds=0,1&cht=bvs&chd=t:<?php echo $chartpercentagedata; ?>&chco=2c7575|53c2b5|bfda71&chbh=17,15&chm=N*p0*,000000,0,-1,11&chbh=a,15"> </div>
                <div class="meal1_diet_chart">
                      <h2 class="meal_description_title"> 
                        <ul class="bs-docs-tooltip-examples">
                          <li><a href="javascript:;" class="cals_pro description" > description </a></li>
                          <li><a href="javascript:;" class="cals_pro quentity" > quantity </a></li>
                          <li><a href="javascript:;" class="carbs_meals yield_production_child carbs" > carbs</a></li>
                          <li><a href="javascript:;" class="carbs_fats yield_production fats" >fats</a></li>
                          <li><a href="javascript:;"  class="carbs_pro yield_production pro" > pro </a></li>
                          <li class="calpad"><a href="javascript:;" class="cals_pro cals" > cals</a></li>
                        </ul>
                      </h2>
                  <div class="breakdown_list">
                    <div class="droppable" >
                      <?php
				  	  $modified_items=array();
					  $i=0;
					  if($usermeals_meal_id!=0):					  
					  foreach($db->loadObjectList() as $mealItems):
					  	$modified_items[$i]['id']=$mealItems->id;
						$modified_items[$i]['name']=$mealItems->name;
						$modified_items[$i]['recipe_id']=$mealItems->recipe_id;
						$modified_items[$i]['serv_size']=$mealItems->serv_size;
						$modified_items[$i]['unit_of_measurement']=$mealItems->unit_of_measurement;
						$modified_items[$i]['mealId']=$mealItems->mealId;	
						$modified_items[$i]['is_custom_recipe']=$mealItems->is_custom_recipe;							
						$i++;
					  ?>
                      <p  meal-id=<?php echo $usermeals_meal_id; ?> >
                      	<span class="description marginleft5" > <a href="<?php echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=recipemeal&Itemid=242&meal_id=$usermeals_meal_id&recipe_id=$mealItems->id&own_recipe=1", false); ?>" ><?php echo $mealItems->name; ?></a></span>
                        <span class="quentity editable_quentity" meal-id=<?php echo $mealItems->id ?> ><?php echo $mealItems->serv_size; ?>&nbsp;<?php echo $mealItems->unit_of_measurement; ?></span>
                        <span class="carbs " > <?php echo $mealItems->carbohydrates; ?></span>
                        <span class="fats"  > <?php echo $mealItems->fat; ?></span>
                        <span class="pro"  ><?php echo $mealItems->protein; ?></span>
                        <span class="cals" ><?php echo $mealItems->calories; ?></span>
                      </p>
                      <?php endforeach;
					  else:?>
					  <p  meal-id= >
                      	<span class="description marginleft5" > </span>
                        <span class="quentity editable_quentity" meal-id= ></span>
                        <span class="carbs " > </span>
                        <span class="fats"  > </span>
                        <span class="pro"  ></span>
                        <span class="cals" ></span>
                      </p>
					<?php endif; ?>
                    </div>
                    <div class="hide popuptblediv">
                      <form class="popuptble_form">
                        <table class="popuptble">
                          <tr>
                            <td colspan="3"><h3 class="margin0 paddingbot5">Meal <?php echo $meal_i; ?></h3></td>
                          </tr>
                          <?php foreach($modified_items  as $modified_itemsval): ?>
                          <tr class="borderccc">
                            <td class='popuptbleitemname'><div class="popupitems"><?php echo $modified_itemsval['name']; ?></div></td>
                            <td><input type="text" class="width50px popup_recipe_input" name="<?php echo $modified_itemsval['mealId']; ?>_<?php echo $modified_itemsval['recipe_id']; ?>_<?php echo $modified_itemsval['is_custom_recipe']; ?>" value="<?php echo $modified_itemsval['serv_size']; ?>"  />
                            </td>
                            <td align="left"><?php echo $modified_itemsval['unit_of_measurement']; ?></td>
                          </tr>
                          <?php endforeach;?>
                          <tr>
                            <td colspan="3" ><table class="popuptblebtn">
                                <tr>
                                  <td><button type="button" class="btn btn-success modify_button bgcolor3188 popup_update_meal" >Update</button></td>
                                </tr>
                                <tr>
                                  <td><div class="trspace">&nbsp;</div><td>
                                </tr>
                                <tr>
                                  <td><button class="btn  btn-success modify_button popup_add_meal"  onclick="return false" ><span>Add a meal</span></button></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>
                      </form>
                    </div>
                    <p class="padleft5"> <span class="description">&nbsp;</span> <span class="quentity color99D720">TOTAL</span> <span class="carbs color99D720"><?php echo $totalCarbs; ?>g</span> <span class="fats color99D720"><?php echo $totalFats; ?>g</span> <span class="pro color99D720"><?php echo $totalProtein; ?>g</span> <span class="cals color99D720"><?php echo $totalCalories; ?>g</span> </p>
                  </div>
                  <p> 
	               <a href="<?php echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=recipeadd&Itemid=242&food_list=1&own_recipe=1&meal_id=$meal_i", false); ?>" ><button class="btn btn-success food_item" type="button">add food item</button></a>      
                  <a href="#meal_popup_modify"  class="mealmodify_pop_lnk" data-lightbox="on">
                    <button class="btn btn-success modify_button"  type="button"> modify </button>
                    </a>
                    <button class="btn btn-success meal_completed"  type="button"> Meal Completed </button>
                    <button class="btn btn-success reset_button reset_meal" type="button"> reset </button>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        </td>
        </tr>
         <tr>
      <td><div class="hide">
          <div class="notes_style" id="foam_roll_notes">
            <strong>Isometries example2<a w_id="732" class="editNotes clk">Edit Note</a></strong><p></p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p></p>          </div>
        </div>
        <a data-lightbox="on" href="#foam_roll_notes">
        <button class="btn btn-success nutrition_note" type="button">NOTES</button>
        </a> </td>
    </tr>
    <?php endforeach; ?>
  </tbody></table>
      <?php $sumallcalfatprotein=$sumtotalCarbs+$sumtotalFats+$sumtotalProtein; ?>
      <div class="meal_description_title nutri_line_height marginleft0"> <span class="pddinglft10"> today menu total </span>
        <ul class="bs-docs-tooltip-examples floatrigth todays_menu">
          <li><a class="carbs_meals yield_production_child" data-original-title="Tooltip on right" href="javascript:;" data-toggle="tooltip" data-placement="right" title=""> cals:<?php echo @round($sumtotalCarbs/$sumallcalfatprotein*100); ?>% </a></li>
          <li><a class="carbs_fats yield_production" href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"> fats:<?php echo @round($sumtotalFats/$sumallcalfatprotein*100); ?>% </a></li>
          <li><a class="carbs_pro yield_production" data-original-title="Tooltip on left" href="javascript:;" data-toggle="tooltip" data-placement="left" title=""> pro:<?php echo @round($sumtotalProtein/$sumallcalfatprotein*100); ?>% </a></li>
          <li><a data-original-title="Tooltip on left" href="javascript:;" data-toggle="tooltip" data-placement="left" title="" class="today_menu_right_border"><?php echo $sumtotalCarbs; ?>g </a></li>
          <li><a data-original-title="Tooltip on left" href="javascript:;" data-toggle="tooltip" data-placement="left" title=""  class="today_menu_right_border"><?php echo $sumtotalFats; ?>g </a></li>
          <li><a data-original-title="Tooltip on left" href="javascript:;" data-toggle="tooltip" data-placement="left" title=""  class="today_menu_right_border"><?php echo $sumtotalProtein; ?>g </a></li>
          <li><a data-original-title="Tooltip on left" href="javascript:;" data-toggle="tooltip" data-placement="left" title="" ><?php echo $sumtotalCalories; ?>g</a></li>
        </ul>
      </div>
      <div class="food_creation">
        <p>
          <button type="button" class="btn btn-success food_item">
          create custom food <a class="bottom_icon" href="#"> </a>
          </button>
        </p>
      </div>
      <div class="repeated_process">
        <ul>
          <li><span> <a href="<?php echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=240&ndate=$back_date"); ?>" class="back_process">Back </a></span></li>
          <li><span> <a href="<?php echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=240&ndate=$back_week"); ?>" class="back_a_week">back a week </a> </span></li>
          <li><span> <a href="#"> print </a></span> </li>
          <li><span> <a href="<?php echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=240&ndate=$next_week"); ?>" class="repeat_process_next_week"> next week </a></span> </li>
          <li class="repeat_last_child"><span> <a href="<?php echo JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=240&ndate=$next_date"); ?>" class="repeat_process_next"> forward </a></span> </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="hide">
  <div id="meal_popup_modify" >
    <h4 class="center" ></h4>
    <div class="center">
      <table align="center" width="90%">
        <tr>
          <td><div id="meal_popup_modify_data"></div></td>
        </tr>
      </table>
    </div>
  </div>
  <div id="meal_popup_add_taskbar" >
    <h4 class="center color1487AA" >My Nutrition Task Bar Meal Items</h4>
    <div class="center">
      <table align="center" width="90%">
        <tr>
          <td><div id="meal_popup_add_taskbar_data"></div>
          <button type='button' class='btn btn-success bgcolor3188 marginTop10' id='popup_add_recipe'>Add</button>
          </td>
        </tr>
      </table>
    </div>
  </div>
  <div id="pre_made_snacks" >
    <h4 class="center color1487AA">PRE-MADE SNACKS/MEALS DETAILS</h4>
    <div class="center">
		<div id="pre_made_snacks_data"></div>
    </div>
  </div>
  <div id="pre_made_snacks_confirm" >
    <div class="center">
		<div id="pre_made_snacks_confirm_box"></div>
    </div>
  </div>
<a href="#pre_made_snacks_confirm" id="dialogbox" class="hide" data-lightbox="on" ></a>  
</div>