<?php 
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');	 

$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_nutrition/views/food/tmpl/css/food.css', $type = 'text/css');

$loginUserId	= (int) JFactory::getUser()->get('id');

$db = JFactory::getDbo();	

$jinput = JFactory::getApplication()->input;

$id=$jinput->get('id', 'default_value', 'filter');

if(isset($id) && is_numeric($id)){
	$query="SELECT * FROM " . $db->quoteName( '#__questionnaire_nutrition_food' ) . " WHERE id =$id";
	$db->setQuery($query);
	$foodDetails=$db->loadObject();

	$additionalData=array();
	$additionalData=json_decode($foodDetails->additional_vitamins_and_minerals);
}
?>

<h2 class="page_head"><?php echo $foodDetails->name; ?></h2>
<div id="recipe_container">
  <div class="accordion" id="miles_snakes_accordion">
    <div class="accordion-group acc_group_marginnone">
      <div class="accordion-heading acc_top_border"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#" > <span class="plus_icon"><?php echo $foodDetails->name; ?></span> <span class="up_click"></span> </a> </div>
      <div id="collapse1" class="accordion-body collapse in">
        <div class="accordion-inner bakround_none">
          <div class="recipedata">
            <table class="maintable" >
              <tr>
                <td width="23%"><table class="recipeinnertable" >
                    <tr>
                      <td class="recipephto"><?php if(!empty($foodDetails->picture)): ?>
                        <img src="<?php echo JRoute::_(JUri::base() . 'administrator/components/com_questionnaire_nutrition/asset/'.$foodDetails->picture);?>" width="130"  />
                        <div class="report_image"><a href="#report_this_image" class="report_pop_lnk" data-lightbox="on" >Report This Image</a></div>
                        <?php else: ?>
                        <img style="margin-top:5px" src="<?php echo JRoute::_(JUri::base() . '/components/com_questionnaire_nutrition/asset/placeholder.png');?>"  />
                        <?php endif; ?>
                      </td>
                    </tr>
                  </table></td>
                <td width="54%"><table class="recipeinnertable2">
                    <tr>
                      <td class="color555">Brand Name</td>
                      <td><?php echo $foodDetails->brand_name; ?></td>
                    </tr>
                    <tr>
                      <td class="color555">Serving Size</td>
                      <td><?php echo $foodDetails->serv_size; ?>&nbsp;<?php echo $foodDetails->unit_of_measurement; ?></td>
                    </tr>
                    <tr>
                      <td class="color555">Calories</td>
                      <td><?php echo $foodDetails->calories; ?></td>
                    </tr>
                    <tr>
                      <td class="color555">Carbohydrates</td>
                      <td><?php echo $foodDetails->carbohydrates; ?></td>
                    </tr>
                    <tr>
                      <td class="color555">Fat</td>
                      <td><?php echo $foodDetails->fat; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Protein</td>
                      <td><?php echo $foodDetails->protein; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Saturated Fat</td>
                      <td><?php echo $foodDetails->saturated_fat; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Trans Fat</td>
                      <td><?php echo $foodDetails->trans_fat; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Cholesterol</td>
                      <td><?php echo $foodDetails->cholesterol; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Sodium</td>
                      <td><?php echo $foodDetails->sodium; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Dietary Fiber</td>
                      <td><?php echo $foodDetails->dietary_fiber; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Sugars</td>
                      <td><?php echo $foodDetails->sugars; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Vitamin A</td>
                      <td><?php echo $foodDetails->vitamin_a; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Vitamin C</td>
                      <td><?php echo $foodDetails->vitamin_c; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Vitamin D</td>
                      <td><?php echo $foodDetails->vitamin_d; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Vitamin K</td>
                      <td><?php echo $foodDetails->vitamin_k; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Folic Acid</td>
                      <td><?php echo $foodDetails->folic_acid; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Calcium</td>
                      <td><?php echo $foodDetails->calcium; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Potassium</td>
                      <td><?php echo $foodDetails->potassium; ?></td>
                    </tr>
                    <tr class="color555">
                      <td class="color555">Iron</td>
                      <td><?php echo $foodDetails->iron; ?></td>
                    </tr>
                    <?php 
					if(!empty($additionalData)):
					foreach($additionalData as $additionalDataVal):?>
					<tr class="color555">
                      <td class="color555"><?php echo $additionalDataVal->name; ?></td>
                      <td><?php echo $additionalDataVal->value; ?></td>
                    </tr>
					<?php endforeach; endif; ?>                                                                                                                    
                  </table></td>
                <td width="23%"><table class="recipeinnertable3">
                    <tr>
                      <td class="recipenotes">Notes</td>
                    </tr>
                    <tr>
                      <td class="recipenotesdata"><?php echo $foodDetails->notes; ?></td>
                    </tr>
                  </table></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="hide">
  <div id="report_this_image" >
    <h4 class="center" >Report This Image</h4>
    <div class="center">
      <table align="center" width="90%">
        <tr>
          <td><div id="report_this_image_data"></div></td>
        </tr>
      </table>
    </div>
  </div>
</div>
