<?php

/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com>
 */

// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Question controller class.
 */
class Questionnaire_nutritionControllerQuestionForm extends Questionnaire_nutritionController
{
	//Add custom recipe
	function add_recipe(){			
		$app			= JFactory::getApplication();
		$loginUserId	= (int) JFactory::getUser()->get('id');
		$db			= JFactory::getDBO();

		$name = trim($app->input->post->getString('name'));
		$picture = $app->input->files->get('picture');
		$brand_name = trim($app->input->post->getString('brand_name'));
		$serv_size = trim($app->input->post->getString('serv_size'));
		$unit_of_measurement= trim($app->input->post->getString('unit_of_measurement'));
		$calories = trim($app->input->post->get('calories'));
		$carbohydrates = trim($app->input->post->get('carbohydrates'));
		$fat = trim($app->input->post->get('fat'));
		$protein = trim($app->input->post->get('protein'));
		$recipe_type=trim($app->input->post->get('recipe_type'));
		$notes = trim($app->input->post->getString('notes'));	
		$usermeals_meal_id = trim($app->input->post->get('usermeals_meal_id'));	
		
		$ingredients = @JRequest::getVar( 'ingredients', '', 'post', 'string', JREQUEST_ALLOWHTML ); 
		$cooking_directions = @JRequest::getVar( 'cooking_directions', '', 'post', 'string', JREQUEST_ALLOWHTML );

		$error_msg='';
		if(empty($name)){ $error_msg='Please enter recipe name.';}
		elseif(empty($brand_name)){ $error_msg='Please enter brand name.';}
		elseif(empty($serv_size)){ $error_msg='Please enter serv size.';}
		elseif(empty($unit_of_measurement)){ $error_msg='Please select unit of measurement.';}
		elseif(empty($calories)){ $error_msg='Please enter calories.';}
		elseif(empty($carbohydrates)){ $error_msg='Please enter carbohydrates.';}
		elseif(empty($fat)){ $error_msg='Please enter fat.';}
		elseif(empty($protein)){ $error_msg='Please enter protein.';}
		elseif(empty($recipe_type)){ $error_msg='Please select recipe type.';}
		elseif(empty($notes)){ $error_msg='Please enter notes.';}
		elseif(empty($usermeals_meal_id)){ $error_msg='Please select meal .';}
		
		$picture_name=$this->uploadfile($picture);								

		if (!empty($error_msg)) {
			$this->setMessage("$error_msg", 'notice');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=recipeadd&Itemid=242&recipe_id=$recipe_id", false));
			return false;
		}else{
			$sql = "INSERT INTO #__usermeals_userrecipe (
			created_by,`user_id`,`date`,
			recipe_type_id,
			name,
			`brand_name`,
			`serv_size`,
			 unit_of_measurement,
			`calories`, 
			`carbohydrates`,
			`fat`,
			`protein`, 
			`notes`, 
			`picture`,
			ingredients,
			cooking_directions) VALUES (
			$loginUserId,$loginUserId, now(),
			$recipe_type, 
			".$db->quote($name).",
			".$db->quote($brand_name).",
			".$db->quote($serv_size).", 
			'$unit_of_measurement' ,
			$calories,
			$carbohydrates, 
			$fat, 
			$protein, 
			".$db->quote($notes).", 
			'$picture_name',
			".$db->quote($ingredients).",
			".$db->quote($cooking_directions).");";
			$db->setQuery($sql);
			$db->query();

			$insertId=$db->insertid();

			if($insertId>0){
				$recipeQuery="INSERT IGNORE INTO	#__usermeals_recipe(user_id,usermeals_meal_id,recipe_id,serv_size,carbohydrates,fat,protein,calories,is_custom_recipe)
					VALUES(	$loginUserId,$usermeals_meal_id,$insertId,$serv_size,$carbohydrates,$fat,$protein,$calories,1)";
				$db->setQuery($recipeQuery);
				$db->query();
				$recipeId=$db->insertid();
				if($recipeId>0){
					$this->setMessage("Recipe added successfully.",'success');
					$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=240", false));		
				}
			}		
		}	
	}	
	
	//Addd custom food
	function add_food(){
		$app			= JFactory::getApplication();
		$loginUserId	= (int) JFactory::getUser()->get('id');
		$db			= JFactory::getDBO();

		$name = trim($app->input->post->getString('name'));
		$picture = $app->input->files->get('picture');
		$category_id = $app->input->post->get('category_id');
		$brand_name = trim($app->input->post->getString('brand_name'));
		$serv_size = trim($app->input->post->getString('serv_size'));
		$unit_of_measurement= trim($app->input->post->getString('unit_of_measurement'));
		$calories = trim($app->input->post->get('calories'));
		$carbohydrates = trim($app->input->post->get('carbohydrates'));
		$fat = trim($app->input->post->get('fat'));
		$protein = trim($app->input->post->get('protein'));
		$saturated_fat = trim($app->input->post->get('saturated_fat'));
		$trans_fat = trim($app->input->post->get('trans_fat'));
		$cholesterol = trim($app->input->post->get('cholesterol'));
		$sodium = trim($app->input->post->get('sodium'));
		$sugars = trim($app->input->post->get('sugars'));
		$vitamin_a = trim($app->input->post->get('vitamin_a'));
		$vitamin_c = trim($app->input->post->get('vitamin_c'));
		$vitamin_d = trim($app->input->post->get('vitamin_d'));
		$vitamin_k = trim($app->input->post->get('vitamin_k'));
		$folic_acid = trim($app->input->post->get('folic_acid'));
		$calcium = trim($app->input->post->get('calcium'));
		$potassium = trim($app->input->post->get('potassium'));
		$iron = trim($app->input->post->get('iron'));
		$saturated_fat = trim($app->input->post->get('saturated_fat'));						
		$blood_type = trim($app->input->post->get('blood_type'));
		$notes = trim($app->input->post->getString('notes'));	
		$usermeals_meal_id = trim($app->input->post->get('usermeals_meal_id'));
		
		$additional_vitamins_and_minerals=$app->input->getArray($_POST);
		
		$additional_vitamins_and_mineralsArr=array();
		if(isset($additional_vitamins_and_minerals['additional_vitamins_and_minerals']) and !empty($additional_vitamins_and_minerals['additional_vitamins_and_minerals'])){ 
			$i=1;
			foreach($additional_vitamins_and_minerals['additional_vitamins_and_minerals'] as $additional_vitamins_and_mineralsVal){
				$additional_vitamins_and_mineralsArr[$i]['name']=$additional_vitamins_and_mineralsVal['name'];
				$additional_vitamins_and_mineralsArr[$i]['value']=$additional_vitamins_and_mineralsVal['value'];
				$i++;
			}
		}
		$additional_vitamins_and_minerals_json=@json_encode($additional_vitamins_and_mineralsArr);

		$error_msg='';
		if(empty($name)){ $error_msg='Please enter food name.';}
		elseif(empty($picture)){ $error_msg='Please upload picture.';}
		elseif(empty($brand_name)){ $error_msg='Please enter brand name.';}
		elseif(empty($serv_size)){ $error_msg='Please enter serv size.';}
		elseif(empty($unit_of_measurement)){ $error_msg='Please select unit of measurement.';}
		elseif(empty($blood_type)){ $error_msg='Please select blood type.';}
		elseif(empty($calories)){ $error_msg='Please enter calories.';}
		elseif(empty($carbohydrates)){ $error_msg='Please enter carbohydrates.';}
		elseif(empty($fat)){ $error_msg='Please enter fat.';}
		elseif(empty($protein)){ $error_msg='Please enter protein.';}
		elseif(empty($notes)){ $error_msg='Please enter notes.';}
		elseif(empty($usermeals_meal_id)){ $error_msg='Please select meal .';}
	
		if (!empty($error_msg)) {
				$this->setMessage("$error_msg", 'notice');
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=foodadd&Itemid=242&usermeals_meal_id=$usermeals_meal_id", false));
				return false;
		}else{
			$picture_name=$this->uploadfile($picture);								

			$sql = "INSERT INTO #__usermeals_userfood (
			created_by,`user_id`,`date`, 
			`name`,
			`category_id`,
			`brand_name`,
			`serv_size`,
			 unit_of_measurement,
			`calories`,
			`carbohydrates`, 
			`fat`, 
			`protein`, 
			`saturated_fat`, 
			`trans_fat`, 
			`cholesterol`,
			`sodium`,
			`sugars`,
			`vitamin_a`,
			`vitamin_c`,
			`vitamin_d`,
			`vitamin_k`,
			`folic_acid`,
			`calcium`,
			`potassium`,
			`iron`,
			 additional_vitamins_and_minerals,	
			 blood_type,	
			`notes`, 
			`picture`) VALUES (
			$loginUserId,$loginUserId, now(),
			".$db->quote($name).",
			'$category_id', 
			".$db->quote($brand_name).",
			".$db->quote($serv_size).",
			'$unit_of_measurement' ,
			'$calories',
			'$carbohydrates',
			'$fat',
			'$protein',
			'$saturated_fat',
			'$trans_fat',
			'$cholesterol',
			'$sodium',
			'$sugars',
			'$vitamin_a',
			'$vitamin_c',
			'$vitamin_d',
			'$vitamin_k',
			'$folic_acid',
			'$calcium',
			'$potassium',
			'$iron',
			'$additional_vitamins_and_minerals_json',
			'$blood_type',
			".$db->quote($notes).",			
			'$picture_name');";
			$db->setQuery($sql);
			
			$db->query();

			$insertId=$db->insertid();

			if($insertId>0){
				$foodQuery="INSERT IGNORE INTO	#__usermeals(user_id,usermeals_meal_id,food_id,serv_size,carbohydrates,fat,protein,calories,is_custom_food)
					VALUES(	$loginUserId,$usermeals_meal_id,$insertId,$serv_size,$carbohydrates,$fat,$protein,$calories,1)";
				$db->setQuery($foodQuery);
				$db->query();
				$foodId=$db->insertid();
				if($foodId>0){
					$this->setMessage("Food added successfully.",'success');
					$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=240", false));		
				}
			}			
		}
	}	
	
	//Add food from library
	function add_library_food(){		
		$app			= JFactory::getApplication();
		$loginUserId	= (int) JFactory::getUser()->get('id');
		$db			= JFactory::getDBO();

		$food_id = $app->input->get->getInt('id');
		$usermeals_meal_id = $app->input->get->getInt('usermeals_meal_id');
		
		$query="SELECT * FROM #__usermeals WHERE usermeals_meal_id=$usermeals_meal_id AND food_id=$food_id AND user_id=$loginUserId";
		$db->setQuery($query);
		$countFoodExist=count($db->loadObject());

		if($countFoodExist>0){
			$this->setMessage("Food already added.",'notice');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=240", false));	
		}else{		
			$query="SELECT  id,serv_size,carbohydrates ,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE id=$food_id";
			$db->setQuery($query);
			$countFood=count($db->loadObject());
	
			if($countFood>0){
					$query="INSERT INTO #__usermeals(user_id,usermeals_meal_id,food_id,serv_size,carbohydrates,fat,protein,calories)
					VALUES($loginUserId,$usermeals_meal_id,
					{$db->loadObject()->id},{$db->loadObject()->serv_size},{$db->loadObject()->carbohydrates},
					{$db->loadObject()->fat},{$db->loadObject()->protein},{$db->loadObject()->calories})";
					$db->setQuery($query);
					$db->query();
					$foodId=$db->insertid();
				if($foodId>0){
					$this->setMessage("Food added successfully.",'success');
					$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_nutrition&view=program&Itemid=240", false));		
				}
			}
		}
	}
	
   	//meal/snack description and breakdowns modify button popup box update qty.
	function ajax_recipe_update(){ 
		$app			= JFactory::getApplication();
		$loginUserId	= (int) JFactory::getUser()->get('id');
		$db			= JFactory::getDBO();
		$data = $app->input->getArray($_POST);

		$output=array();

		foreach($data as $key=>$val){
			$recipe_serv_size=$val;

			$valArr=explode('_',$key);
			$usermeal_id=$valArr[0];
			$food_recipe_id=$valArr[1];
			$is_custom_recipe=$valArr[2];
			$is_food=$valArr[3];
			
			if($is_food==1){// Get foods
				if($is_custom_recipe==1){
					$query="SELECT serv_size,carbohydrates,fat,protein,calories FROM #__usermeals_userrecipe WHERE id=$food_recipe_id";
				}else{
					$query="SELECT serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE id=$food_recipe_id";
				}			
			}elseif($is_food==0){// Get recipes
				if($is_custom_recipe==1){
					$query="SELECT serv_size,carbohydrates,fat,protein,calories FROM #__usermeals_userrecipe WHERE id=$food_recipe_id";
				}else{
					$query="SELECT serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_recipes WHERE id=$food_recipe_id";
				}
			}

			$db->setQuery($query);

			$recipedb_carbohydrates=$db->loadObject()->carbohydrates;
			$recipedb_fat=$db->loadObject()->fat;
			$recipedb_protein=$db->loadObject()->protein;
			$recipedb_calories=$db->loadObject()->calories;
			
			$recipedb_serv_size=$db->loadObject()->serv_size;
			$match_db_arr=explode('/',$recipedb_serv_size);
			if(count($match_db_arr)>=2){
				$recipedb_serv_size=$match_db_arr[0]/$match_db_arr[1];
			}

			$updated_serv_size=$recipe_serv_size;
			$match_user_arr=explode('/',$updated_serv_size);
			if(count($match_user_arr)>=2){
				$updated_serv_size=$match_user_arr[0]/$match_user_arr[1];
			}

			$updated_carbohydrates=($recipedb_carbohydrates*$updated_serv_size)/$recipedb_serv_size;
			$updated_fat=($recipedb_fat*$updated_serv_size)/$recipedb_serv_size;
			$updated_protein=($recipedb_protein*$updated_serv_size)/$recipedb_serv_size;
			$updated_calories=($recipedb_calories*$updated_serv_size)/$recipedb_serv_size;
			
			if($is_food==1){ //Update Food
				$query="UPDATE #__usermeals SET 
				serv_size='$updated_serv_size',
				carbohydrates=$updated_carbohydrates, 
				fat=$updated_fat,
				protein=$updated_protein,
				calories=$updated_calories
				WHERE id=$usermeal_id AND food_id=$food_recipe_id AND user_id=$loginUserId";			
			}elseif($is_food==0){//Update Recipe
				$query="UPDATE #__usermeals_recipe SET 
				serv_size='$updated_serv_size',
				carbohydrates=$updated_carbohydrates, 
				fat=$updated_fat,
				protein=$updated_protein,
				calories=$updated_calories
				WHERE id=$usermeal_id AND recipe_id=$food_recipe_id AND user_id=$loginUserId";
			}
			
			
			$db->setQuery($query);
			$db->query();
		}

		$output=array('status'=>1,'data'=>$output);
		echo json_encode($output);
		die();
	}
	//meal/snack description and breakdowns modify popup box
	function ajax_add_meal(){	
		$db			= JFactory::getDBO();
		$loginUserId	= JFactory::getUser()->get('id');
		
		$query="SELECT eat_a_day_meal_snacks FROM #__questionnaire_question WHERE id=$loginUserId";
		$db->setQuery($query);
		if($db->loadObject()->eat_a_day_meal_snacks<7){
			$query="Update #__questionnaire_question SET eat_a_day_meal_snacks=eat_a_day_meal_snacks+1 WHERE id=$loginUserId";
			$db->setQuery($query);
			$db->query();
			if($db->getAffectedRows()>0){
				$output=array('status'=>1);
				echo json_encode($output);die;
			}
		}

		$output=array('status'=>0);
		echo json_encode($output);die;
	}
	//My nutrition taskbar recipe add popup box
	function ajax_task_meal_list(){ 
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		
		$user			= JFactory::getUser();
		$loginUserId	= (int) $user->get('id');

		$task_id = $app->input->getPost('id');

		//get all recipe in popupbox except the system recipe & user recipes.
		$query="SELECT id,name FROM (SELECT id,name FROM #__questionnaire_nutrition_recipes WHERE NOT FIND_IN_SET(id,(SELECT recipe_id FROM #__questionnaire_nutrition_taskbar WHERE id=$task_id and state=1)))as Tbl1 WHERE Tbl1.id NOT IN (SELECT recipe_id FROM #__questionnaire_nutrition_taskbar_user WHERE user_id=$loginUserId AND own_nutrition_program=0) ORDER BY name ASC";

		$db->setQuery($query);
		
		if(count($db->loadAssocList())==0){
			echo json_encode(array('status'=>0)); die;
		}else{
			$output=array();
			foreach($db->loadAssocList() as $recipe_list){
				$output[]=array('recipe_id'=>$recipe_list['id'],'name'=>$recipe_list['name']);		
			}
		}
		echo json_encode(array('status'=>1,'data'=>$output));
		die;
	}
	//MY nutrition taskbar add meal popup box add recipe to nutrition taskbar.
	function ajax_users_recipes(){ 
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		
		$user			= JFactory::getUser();
		$loginUserId	= (int) $user->get('id');

		$taskmeal_id = $app->input->getPost('taskmeal_id');
		$recipe_ids = $app->input->getPost('recipe_ids');

		$recipe_ids_arr=explode(',',$recipe_ids);
		foreach($recipe_ids_arr as $recipe_ids_arrVal){
			if(!empty($recipe_ids_arrVal)){
				$query="INSERT IGNORE INTO #__questionnaire_nutrition_taskbar_user(nutrition_taskbar_id,user_id,recipe_id) VALUES($taskmeal_id,$loginUserId,$recipe_ids_arrVal)";
				$db->setQuery($query);
				$db->query();
			}
		}
		
		$query="SELECT id,name FROM #__questionnaire_nutrition_recipes WHERE id IN($recipe_ids)";
		$db->setQuery($query);
		$recipe_list=$db->loadAssocList();
		echo json_encode($recipe_list);
		die;	
	}
	//Drag drop recipe from My Nutrition taskbar
	function ajax_get_recipe_detail(){ 
		$app			= JFactory::getApplication();
		$user			= JFactory::getUser();
		$loginUserId	= (int) $user->get('id');
		$recipe_id = $app->input->getPost('recipe_id');
		$usermeals_meal_id = $app->input->getPost('meal_id');
		
		$db			= JFactory::getDBO();
		
		//check recipe already included		
		$query="SELECT count(id) chkRecipe FROM #__usermeals_recipe WHERE recipe_id=$recipe_id AND user_id=$loginUserId AND usermeals_meal_id=$usermeals_meal_id";
		
		$db->setQuery($query);
		$chkRecipe=$db->loadObject()->chkRecipe;

		//check recipe already added.
		if($chkRecipe>=1){
			$arrayout=array('status'=>0);
			echo json_encode($arrayout); die;
		}
		
		$query="SELECT * FROM #__questionnaire_nutrition_recipes WHERE id=$recipe_id";
		$db->setQuery($query);
		$recipesdbList=$db->loadObject();
		
		$query="INSERT INTO	#__usermeals_recipe(user_id,usermeals_meal_id,recipe_id,serv_size,carbohydrates,fat,protein,calories) VALUES($loginUserId,$usermeals_meal_id,$recipe_id,$recipesdbList->serv_size,$recipesdbList->carbohydrates,$recipesdbList->fat,$recipesdbList->protein,$recipesdbList->calories)";	
		$db->setQuery($query);
		$db->query();
		
		$insertId= $db->insertid();

		if(!empty($insertId)){
			$arrayout=array('status'=>1,'data'=>array('name'=>$recipesdbList->name,'serv_size'=>$recipesdbList->serv_size,'calories'=>$recipesdbList->calories,'carbohydrates'=>$recipesdbList->carbohydrates,'fat'=>$recipesdbList->fat,'protein'=>$recipesdbList->protein));
			echo json_encode($arrayout);
			die;
		}
		else{
			$arrayout=array('status'=>0);
			echo json_encode($arrayout); die;
		}
		die;
	}
	
	function ajax_add_grocery_food(){ 
		$app			= JFactory::getApplication();
		$loginUserId			= JFactory::getUser()->get('id');

		$nutrition_food_id = $app->input->getPost('nutrition_food_id');
		$grocerylist_id = $app->input->getPost('grocerylist_id');
		$food_serv_size=$app->input->getPost('food_serv_size');
		
		$db			= JFactory::getDBO();
		
		$query="SELECT (id) as grocery_exist FROM #__usergrocerylist WHERE id=$grocerylist_id and user_id=$loginUserId";
		$db->setQuery($query);
		
		if(isset($db->loadObject()->grocery_exist)){
			$query="SELECT count(id) as food_cnt FROM #__usergrocery_meals WHERE usergrocerylist_id=$grocerylist_id AND food_id=$nutrition_food_id";
			$db->setQuery($query);
			if($db->loadObject()->food_cnt>0){
				$arrayout=array('status'=>0);
				echo json_encode($arrayout); die;
			}
			
			$query="INSERT INTO #__usergrocery_meals(usergrocerylist_id,food_id,food_serv_size) VALUES ($grocerylist_id,$nutrition_food_id,'$food_serv_size')";		
			$db->setQuery($query);
			$db->query();
		}

		$insertId= $db->insertid();
		if($insertId>0){
			$arrayout=array('status'=>1,'grocery_id'=>$insertId);
		}else{
			$arrayout=array('status'=>0);
		}
		echo json_encode($arrayout); die;
	}
	
	function ajax_add_grocery_food_status_qty(){ 
		$app			= JFactory::getApplication();
		$loginUserId			= JFactory::getUser()->get('id');

		$usergrocery_meals_id = $app->input->getPost('usergrocery_meals_id');
		$food_item_status = trim($app->input->getPost('food_item_status'));		
		$grocery_food_qty = trim($app->input->getPost('grocery_food_qty'));
		
		$db			= JFactory::getDBO();
		
		$query="SELECT A.id as grocery_exist FROM #__usergrocerylist A JOIN #__usergrocery_meals B ON A.id=B.usergrocerylist_id WHERE B.id=$usergrocery_meals_id and A.user_id=$loginUserId";
		$db->setQuery($query);
		
		if(isset($db->loadObject()->grocery_exist)){

			if(is_numeric($food_item_status)){ 
				$query="UPDATE #__usergrocery_meals SET state=$food_item_status WHERE id=$usergrocery_meals_id";
			}elseif(!empty($grocery_food_qty)){
				$query="UPDATE #__usergrocery_meals SET food_serv_size='$grocery_food_qty' WHERE id=$usergrocery_meals_id";
			}
			
			$db->setQuery($query);
			$db->query();
			
			if($db->getAffectedRows()>0){
				$output=array('status'=>1);
				echo json_encode($output);die;
			}
		}
		$output=array('status'=>0);
		echo json_encode($output);die;
	}
	
	function ajax_update_grocery_note(){ 
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		$loginUserId			= JFactory::getUser()->get('id');

		$usergrocerylist_id = $app->input->getPost('usergrocerylist_id');
		$usergrocerylist_data = $app->input->getPost('usergrocerylist_data');
		
		$query="UPDATE #__usergrocerylist SET notes='$usergrocerylist_data' WHERE id=$usergrocerylist_id and user_id=$loginUserId";
		$db->setQuery($query);
		$db->query();
		$output=array('status'=>1);
		echo json_encode($output);die;	
	}
	
	//get Pre-made snacks and meal list
	function ajax_meal_block(){ 
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		$calorie = $app->input->post->get('calorie');

		$query="SELECT id,name FROM #__questionnaire_nutrition_meal_block WHERE calorie_count=$calorie";
		$db->setQuery($query);
		$meal_list=$db->loadObjectList();
		
		if(!empty($meal_list)){
			$output=array('status'=>1,'data'=>$meal_list);
		}else{
			$output=array('status'=>0,'data'=>'');
		}
		
		echo json_encode($output);
		die;
	}
	
	//get Pre-made snacks and meal details
	function ajax_meal_block_detail(){ 
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		$premade_id = $app->input->post->get('premade_id');

		$query="SELECT food_items FROM #__questionnaire_nutrition_meal_block WHERE id=$premade_id";
		$db->setQuery($query);
		$meal_detail_list=@json_decode($db->loadObject()->food_items);

		$food_items=array();
		if(!empty($meal_detail_list)){
			foreach($meal_detail_list as $meal_detail_listObj){
				$food_id=$meal_detail_listObj->food_item_id;
				$query="SELECT name,concat(serv_size,' ',unit_of_measurement) serv_size,calories,carbohydrates,fat,protein FROM #__questionnaire_nutrition_food WHERE id=$food_id";
				$db->setQuery($query);
				$food_items[]=$db->loadObject();
			}
		}
		
		if(!empty($food_items)){
			$output=array('status'=>1,'data'=>$food_items);
		}else{
			$output=array('status'=>0,'data'=>'');
		}
		
		echo json_encode($output);
		die;
	}
	
	function ajax_meal_block_replace(){ 
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		$loginUserId			= JFactory::getUser()->get('id');
		
		$usermeals_meal_id = $app->input->post->get('meal_id');
		$premade_title_id = $app->input->post->get('premade_title_id');
		
		$query="DELETE FROM #__usermeals WHERE usermeals_meal_id=$usermeals_meal_id AND user_id=$loginUserId";
		$db->setQuery($query);
		$db->query();
		
		//Get Food items from premade meal block
		$query="SELECT food_items  FROM #__questionnaire_nutrition_meal_block WHERE id=$premade_title_id";
		$db->setQuery($query);
		$premade_foodItems=json_decode($db->loadObject()->food_items);
		
		$response=array();
		foreach($premade_foodItems as $premade_foodItemsObj){
			$food_id= $premade_foodItemsObj->food_item_id;
			
			$query="SELECT id,name,serv_size,concat(serv_size,' ',unit_of_measurement) serv_size2,calories,carbohydrates,fat,protein FROM #__questionnaire_nutrition_food WHERE id=$food_id";
			$db->setQuery($query);
			$result=$db->loadObject();
			
			if(!empty($result)){			
				$query="INSERT INTO #__usermeals(user_id,usermeals_meal_id,food_id,serv_size,calories,carbohydrates,fat,protein)
						VALUES($loginUserId,$usermeals_meal_id,".$result->id.",".$result->serv_size.",".$result->calories.",".$result->carbohydrates.",".$result->fat.",".$result->protein.")";
				$db->setQuery($query);
				$db->query();
				$response[]=array('name'=>$result->name,'serv_size'=>$result->serv_size2,'calories'=>$result->calories,'carbohydrates'=>$result->carbohydrates,'fat'=>$result->fat,'protein'=>$result->protein);			
			}
		}
		if(!empty($response)){
			$arrayout=array('status'=>1,'data'=>$response);			
		}
		else{
			$arrayout=array('status'=>0);
		}
		echo json_encode($arrayout);
		die;
	}
	
	function ajax_meal_complete(){
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		$loginUserId			= JFactory::getUser()->get('id');
		
		$usermeals_meal_id = (int)$app->input->post->get('usermeals_meal_id');
		
		$query="UPDATE #__usermeals_meal A JOIN #__usermeals_level B ON A.usermeals_level_id=B.id SET A.is_complete=1 WHERE A.id=$usermeals_meal_id AND B.user_id=$loginUserId";
		$db->setQuery($query);
		$db->query();
		$cnt= $db->getAffectedRows();		
		if($cnt>0){
			echo "success";
		}
		die;
	}
	
	function ajax_food_reset(){
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		$loginUserId			= JFactory::getUser()->get('id');

		$usermeals_meal_id = (int)$app->input->post->get('usermeals_meal_id');
		$query="SELECT A.usermeals_meal_breakdowns_user_id FROM #__usermeals_level A JOIN #__usermeals_meal B ON A.id=B.usermeals_level_id  WHERE A.user_id=$loginUserId AND B.id=$usermeals_meal_id";
		$db->setQuery($query);
		
		$usermeals_meal_breakdowns_user_id=$db->loadObject()->usermeals_meal_breakdowns_user_id;
		if(!empty($usermeals_meal_breakdowns_user_id)){
			$query="DELETE FROM #__usermeals WHERE usermeals_meal_id=$usermeals_meal_id AND user_id=$loginUserId";
			$db->setQuery($query);
			$db->query();
			$cnt= $db->getAffectedRows();		
			if($cnt>0){
				$query="SELECT * FROM #__usermeals_meal_breakdowns WHERE usermeals_meal_breakdowns_user_id=$usermeals_meal_breakdowns_user_id";
				$db->setQuery($query);
				foreach($db->loadObjectList() as  $fooddbList){
					$query="INSERT IGNORE INTO	#__usermeals(user_id,usermeals_meal_id,food_id,serv_size,carbohydrates,fat,protein,calories)
				VALUES(	$loginUserId,$usermeals_meal_id,$fooddbList->food_id,$fooddbList->serv_size,$fooddbList->carbohydrates,$fooddbList->fat,$fooddbList->protein,$fooddbList->calories)";
					$db->setQuery($query);
					$db->query();				
				}
			}		
		}
		
		$cnt= $db->getAffectedRows();			
		if($cnt>0){
			$app->enqueueMessage("Food reset successfully.", 'success');
			echo "success";
		}
		die;
	}
	
    function cancel() { 
		$menu = & JSite::getMenu();
        $item = $menu->getActive();
        $this->setRedirect(JRoute::_('index.php?option=com_questionnaire_nutrition', false));
    }
	
	function uploadfile($files){  
		//Support for file field: picture
		if(isset($files['name'])):
			jimport('joomla.filesystem.file');
			jimport('joomla.filesystem.file');
			$file = $files;

			//Check if the server found any error.
			$fileError = $file['error'];
			$message = '';
			if($fileError > 0 && $fileError != 4) {
				switch ($fileError) :
					case 1:
						$message = JText::_( 'File size exceeds allowed by the server');
						break;
					case 2:
						$message = JText::_( 'File size exceeds allowed by the html form');
						break;
					case 3:
						$message = JText::_( 'Partial upload error');
						break;
				endswitch;
				if($message != '') :
					return Error::raiseError(500,$message);
				endif;
			}
			else if($fileError == 4){
				if(isset($array['picture_hidden'])):;
					$array['picture'] = $array['picture_hidden'];
				endif;
			}
			else{

				//Replace any special characters in the filename
				$filename = explode('.',$file['name']);

				$filename[0] = preg_replace("/[^A-Za-z0-9]/i", "-", $filename[0]);

				//Add Timestamp MD5 to avoid overwriting
				$filename = md5(time()) . '-' . implode('.',$filename);
				$uploadPath = JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_questionnaire_nutrition'.DIRECTORY_SEPARATOR.'asset'.DIRECTORY_SEPARATOR.$filename;

				$fileTemp = $file['tmp_name'];

				if(!JFile::exists($uploadPath)):

					if (!JFile::upload($fileTemp, $uploadPath)):

						return JError::raiseError(500,'Error moving file');

					endif;

				endif;
				return $filename;
			}
		endif;	
	}
    
}