<?php

function age_from_dob($dob) {

    list($y,$m,$d) = explode('-', $dob);
   
    if (($m = (date('m') - $m)) < 0) {
        $y++;
    } elseif ($m == 0 && date('d') - $d < 0) {
        $y++;
    }
   
    return date('Y') - $y;   
}


function create_user_meals($loginUserId,$nutrition_program_start,$recipe_in,$eat_a_day_meal_snacks,$user_blood_type){
	$db = JFactory::getDbo(); 
	$app			= JFactory::getApplication();
	//check user meals are created for future dates
	$query="SELECT  COUNT(id) as usermeal_nubm FROM #__usermeals_level WHERE user_id=$loginUserId  AND start_date >= curdate()";
	$db->setQuery($query);

	if($db->loadObject()->usermeal_nubm==0){

		//Get the start date of nutrition , if last date from db is not exist then use questionnaire date start date 
		$query="SELECT max(start_date) as last_date  FROM #__usermeals_level WHERE user_id=$loginUserId";
		$db->setQuery($query);
		if(isset($db->loadObject()->last_date)){
			$nutrition_program_strtdate=date('Y-m-d',strtotime('+1 day',strtotime($db->loadObject()->last_date)));
		}else{
			$nutrition_program_strtdate=$nutrition_program_start;
		}
		
		$nutrition_program_startdate=$nutrition_program_strtdate;
		
		//Create one month nutrition program level
		$nutrition_program_enddate=date('Y-m-d',strtotime('+7 days',strtotime($nutrition_program_strtdate)));//+1 months

		while($nutrition_program_strtdate<$nutrition_program_enddate){
			$query="INSERT INTO #__usermeals_level(user_id,start_date) VALUES($loginUserId,'$nutrition_program_strtdate')";
			$db->setQuery($query);
			$db->query();
			$nutrition_program_strtdate=date('Y-m-d',strtotime("+1 day",strtotime($nutrition_program_strtdate)));
		}

		//Checks user blood type to search recipe.
		if(!empty($user_blood_type)){
			if($user_blood_type=='A'){
				$carbs_intake='55 and 65';
				$fat_intake='20 and 25';
				$protein_intake='20 and 25';	
			}elseif($user_blood_type=='B'){
				$carbs_intake='45 and 55';
				$fat_intake='20 and 30';
				$protein_intake='25 and 35';
			}elseif($user_blood_type=='AB'){
				$carbs_intake='50 and 60';
				$fat_intake='20 and 25';
				$protein_intake='25 and 35';
			}elseif($user_blood_type=='O'){
				$carbs_intake='40 and 50';
				$fat_intake='25 and 30';
				$protein_intake='25 and 35';	
			}
		}else{
				$carbs_intake='50 and 60';
				$fat_intake='20 and 25';
				$protein_intake='25 and 35';
		}
		
		
		$query="SELECT id FROM #__usermeals_level WHERE user_id=$loginUserId AND start_date >= $nutrition_program_startdate";//
		$db->setQuery($query);

		$food_item_limit1=rand(1,2);
		$food_item_limit2=rand(2,3);
		
		foreach($db->loadObjectList() as  $recipes_dateObj){
			
			$usermeals_level_id=$recipes_dateObj->id;
			$user_meal_number=(int)$eat_a_day_meal_snacks;
			
			//Create user meal like Meal 1, Meal 2,etc.
			for($j=1;$j<=$user_meal_number;$j++){
				$query="INSERT INTO	#__usermeals_meal(usermeals_level_id,meal_number)VALUES($usermeals_level_id,$j)";
				$db->setQuery($query);
				$db->query();
				$usermeals_meal_id=$db->insertid();
				
				//Query to search user recipes from recipe libraray based on user question/answers.
				$query="(SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 AND (id IN($recipe_in) AND (carbohydrates between $carbs_intake AND fat between $fat_intake AND protein between $protein_intake)) limit $food_item_limit1) UNION (SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 LIMIT $food_item_limit2)";
	
				$db->setQuery($query);
				//Create food items under each meals
				foreach($db->loadObjectList() as $fooddbList){
					$query="INSERT IGNORE INTO	#__usermeals(user_id,usermeals_meal_id,recipe_id,serv_size,carbohydrates,fat,protein,calories)
					VALUES(	$loginUserId,$usermeals_meal_id,$fooddbList->id,$fooddbList->serv_size,$fooddbList->carbohydrates,$fooddbList->fat,$fooddbList->protein,$fooddbList->calories)";
					$db->setQuery($query);
					$db->query();
				}
			}
		}
	}
}

function create_user_own_meals($loginUserId,$recipe_in,$eat_a_day_meal_snacks,$user_blood_type,&$usermeals_date_lvl_id){
	if(is_null($usermeals_date_lvl_id)){
		$query="INSERT INTO #__usermeals_level_own(user_id,start_date) VALUES($loginUserId,curdate())";
		$db->setQuery($query);
		$db->query();
		$usermeals_date_lvl_id=$db->insertid();
		
		//Checks user blood type to search recipe.
		if(!empty($user_blood_type)){
			if($user_blood_type=='A'){
				$carbs_intake='55 and 65';
				$fat_intake='20 and 25';
				$protein_intake='20 and 25';	
			}elseif($user_blood_type=='B'){
				$carbs_intake='45 and 55';
				$fat_intake='20 and 30';
				$protein_intake='25 and 35';
			}elseif($user_blood_type=='AB'){
				$carbs_intake='50 and 60';
				$fat_intake='20 and 25';
				$protein_intake='25 and 35';
			}elseif($user_blood_type=='O'){
				$carbs_intake='40 and 50';
				$fat_intake='25 and 30';
				$protein_intake='25 and 35';	
			}
		}else{
				$carbs_intake='50 and 60';
				$fat_intake='20 and 25';
				$protein_intake='25 and 35';
		}

		$user_meal_number=(int)$eat_a_day_meal_snacks;
		$food_item_limit1=rand(1,2);
		$food_item_limit2=rand(2,3);
		//Create user meal like Meal 1, Meal 2,etc.
		for($j=1;$j<=$user_meal_number;$j++){
			$query="INSERT INTO	#__usermeals_meal_own(usermeals_level_own_id,meal_number)VALUES($usermeals_date_lvl_id,$j)";
			$db->setQuery($query);
			$db->query();
			$usermeals_meal_own_id=$db->insertid();
			
			//Query to search user recipes from recipe libraray based on user question/answers.
			$query="(SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 AND (id IN($recipe_in) AND (carbohydrates between $carbs_intake AND fat between $fat_intake AND protein between $protein_intake)) limit $food_item_limit1) UNION (SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 LIMIT $food_item_limit2)";

			$db->setQuery($query);
			//Create food items under each meals
			foreach($db->loadObjectList() as $fooddbList){
				$query="INSERT IGNORE INTO	#__usermeals_own(user_id,usermeals_meal_own_id,recipe_id,serv_size,carbohydrates,fat,protein,calories)
				VALUES(	$loginUserId,$usermeals_meal_own_id,$fooddbList->id,$fooddbList->serv_size,$fooddbList->carbohydrates,$fooddbList->fat,$fooddbList->protein,$fooddbList->calories)";
				$db->setQuery($query);
				$db->query();
			}
		}
	}
}

function calculate_cal_intake(){
  $db = JFactory::getDbo(); 
  $loginUserId	= (int) JFactory::getUser()->get('id');	
 
   //query to get actucal calories intake
  $query="select sum(caloriesTotal)caloriesTotal ,sum(carbohydratesTotal)carbohydratesTotal ,sum(fatTotal)fatTotal ,sum(proteinTotal)proteinTotal 
   FROM(
   		(SELECT sum(A.calories) caloriesTotal,sum(A.carbohydrates) carbohydratesTotal,sum(A.fat) fatTotal,sum(A.protein) proteinTotal 
			FROM mejwq_usermeals A 
			JOIN mejwq_usermeals_meal B ON (A.usermeals_meal_id=B.id AND B.is_complete=1) 
			WHERE A.user_id=$loginUserId)
	UNION
		(SELECT sum(A.calories) caloriesTotal,sum(A.carbohydrates) carbohydratesTotal,sum(A.fat) fatTotal,sum(A.protein) proteinTotal
			FROM mejwq_usermeals_own A 
			JOIN mejwq_usermeals_meal_own B ON (A.usermeals_meal_own_id=B.id AND B.is_complete=1)
			WHERE A.user_id=$loginUserId)) as total_calories";
  
  $db->setQuery($query);

  $actualCalories=@$db->loadObject()->caloriesTotal;
  $actualCarbs=@$db->loadObject()->carbohydratesTotal;
  $actualFats=@$db->loadObject()->fatTotal;
  $actualProtein=@$db->loadObject()->proteinTotal;
   
  return array('actualCalories'=>$actualCalories,'actualCarbs'=>$actualCarbs,'actualFats'=>$actualFats,'actualProtein'=>$actualProtein);
}
?>