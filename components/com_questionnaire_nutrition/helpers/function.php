<?php
function age_from_dob($dob) {

    list($y,$m,$d) = explode('-', $dob);
   
    if (($m = (date('m') - $m)) < 0) {
        $y++;
    } elseif ($m == 0 && date('d') - $d < 0) {
        $y++;
    }
   
    return date('Y') - $y;   
}


function create_user_meals($loginUserId,$nutrition_program_dbstrtdate,$recipe_in,$eat_a_day_meal_snacks,$user_blood_type){
	$db = JFactory::getDbo(); 
	
	//Get the last food date
	$query="SELECT max(start_date) as last_date  FROM #__usermeals_level WHERE user_id=$loginUserId";
	$db->setQuery($query);
	if(isset($db->loadObject()->last_date)){
		$nutrition_program_dbstrtdate=date('Y-m-d',strtotime('+1 day',strtotime($db->loadObject()->last_date)));
	}
	
	$nutrition_program_strtdate= $nutrition_program_dbstrtdate;
	$nutrition_program_enddate=date('Y-m-d',strtotime('+1 month',strtotime($nutrition_program_dbstrtdate)));
	
	//
	//Checks user blood type to search recipe.
	if(!empty($user_blood_type)){
		if($user_blood_type=='A'){
			$carbs_intake='55 and 65';
			$fat_intake='20 and 25';
			$protein_intake='20 and 25';	
		}elseif($user_blood_type=='B'){
			$carbs_intake='45 and 55';
			$fat_intake='20 and 30';
			$protein_intake='25 and 35';
		}elseif($user_blood_type=='AB'){
			$carbs_intake='50 and 60';
			$fat_intake='20 and 25';
			$protein_intake='25 and 35';
		}elseif($user_blood_type=='O'){
			$carbs_intake='40 and 50';
			$fat_intake='25 and 30';
			$protein_intake='25 and 35';	
		}
	}else{
			$carbs_intake='50 and 60';
			$fat_intake='20 and 25';
			$protein_intake='25 and 35';
	}

	
	$food_item_limit1=rand(1,2);
	$food_item_limit2=rand(2,3);
	
	$query="INSERT INTO #__usermeals_meal_breakdowns_user(user_id) VALUES($loginUserId)";
	$db->setQuery($query);
	$db->query();
	$usermeals_meal_breakdowns_user_id=$db->insertid();     
	
	//Query to search user recipes from recipe libraray based on user question/answers.
	$query="(SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 AND (id IN($recipe_in) AND (carbohydrates between $carbs_intake AND fat between $fat_intake AND protein between $protein_intake)) limit $food_item_limit1) UNION (SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 LIMIT $food_item_limit2)";  
 
	$db->setQuery($query);
	
	//Create food items under each meals
	foreach($db->loadObjectList() as $fooddbList){
		$query="INSERT INTO #__usermeals_meal_breakdowns(usermeals_meal_breakdowns_user_id,food_id,serv_size,carbohydrates,fat,protein,calories) VALUES($usermeals_meal_breakdowns_user_id,$fooddbList->id,$fooddbList->serv_size,$fooddbList->carbohydrates,$fooddbList->fat,$fooddbList->protein,$fooddbList->calories)";
		$db->setQuery($query);
		$db->query();
	}  
	//
	$user_meal_number=(int)$eat_a_day_meal_snacks;
	while($nutrition_program_strtdate<=$nutrition_program_enddate){//usermeals_meal_breakdowns_id
		$query="INSERT INTO #__usermeals_level(user_id,start_date,usermeals_meal_breakdowns_user_id) VALUES($loginUserId,'$nutrition_program_strtdate',$usermeals_meal_breakdowns_user_id)";
		$db->setQuery($query);
		$db->query();
		$usermeals_date_lvl_id=$db->insertid();
		
		//Create user meal like Meal 1, Meal 2,etc.
		for($j=1;$j<=$user_meal_number;$j++){
			$query="INSERT INTO	#__usermeals_meal(usermeals_level_id,meal_number)VALUES($usermeals_date_lvl_id,$j)";
			$db->setQuery($query);
			$db->query();
			
			$usermeals_meal_id=$db->insertid();
				
			//Query to search user recipes from recipe libraray based on user question/answers.
			$query="SELECT * FROM #__usermeals_meal_breakdowns WHERE usermeals_meal_breakdowns_user_id=$usermeals_meal_breakdowns_user_id";

			$db->setQuery($query);
			
			//Create food items under each meals
			foreach($db->loadObjectList() as $fooddbList){
				$query="INSERT IGNORE INTO	#__usermeals(user_id,usermeals_meal_id,food_id,serv_size,carbohydrates,fat,protein,calories)
				VALUES(	$loginUserId,$usermeals_meal_id,$fooddbList->food_id,$fooddbList->serv_size,$fooddbList->carbohydrates,$fooddbList->fat,$fooddbList->protein,$fooddbList->calories)";
				$db->setQuery($query);
				$db->query();
			}
		}
		
		$nutrition_program_strtdate=date('Y-m-d',strtotime("+1 day",strtotime($nutrition_program_strtdate)));
	}	  
}

function create_user_own_meals($loginUserId,$eat_a_day_meal_snacks,&$usermeals_date_lvl_id,$current_date){
	if(is_null($usermeals_date_lvl_id)){
		$db = JFactory::getDbo(); 
		$query="INSERT INTO #__usermeals_level_own(user_id,start_date) VALUES($loginUserId,curdate())";
		$db->setQuery($query);
		$db->query();
		$usermeals_date_lvl_id=$db->insertid();
		
		$user_meal_number=(int)$eat_a_day_meal_snacks;
		
		//Create user meal like Meal 1, Meal 2,etc.
		for($j=1;$j<=$user_meal_number;$j++){
			$query="INSERT INTO	#__usermeals_meal_own(usermeals_level_own_id,meal_number)VALUES($usermeals_date_lvl_id,$j)";
			$db->setQuery($query);
			$db->query();
		}
	}
}

function calculate_cal_intake($loginUserId){
  $db = JFactory::getDbo(); 
 
   //query to get actucal calories intake from complete food items
  $query="select sum(caloriesTotal)caloriesTotal ,sum(carbohydratesTotal)carbohydratesTotal ,sum(fatTotal)fatTotal ,sum(proteinTotal)proteinTotal 
   FROM(
   		(SELECT sum(A.calories) caloriesTotal,sum(A.carbohydrates) carbohydratesTotal,sum(A.fat) fatTotal,sum(A.protein) proteinTotal 
			FROM #__usermeals A 
			JOIN #__usermeals_meal B ON (A.usermeals_meal_id=B.id AND B.is_complete=1) 
			WHERE A.user_id=$loginUserId)
	UNION
		(SELECT sum(A.calories) caloriesTotal,sum(A.carbohydrates) carbohydratesTotal,sum(A.fat) fatTotal,sum(A.protein) proteinTotal
			FROM #__usermeals_own A 
			JOIN #__usermeals_meal_own B ON (A.usermeals_meal_own_id=B.id AND B.is_complete=1)
			WHERE A.user_id=$loginUserId)
	UNION
		(SELECT sum(A.calories) caloriesTotal,sum(A.carbohydrates) carbohydratesTotal,sum(A.fat) fatTotal,sum(A.protein) proteinTotal
			FROM #__usermeals_recipe A 
			JOIN #__usermeals_meal B ON (A.usermeals_meal_id=B.id AND B.is_complete=1)
			WHERE A.user_id=$loginUserId)			
	UNION
		(SELECT sum(A.calories) caloriesTotal,sum(A.carbohydrates) carbohydratesTotal,sum(A.fat) fatTotal,sum(A.protein) proteinTotal
			FROM #__usermeals_own_recipe A 
			JOIN #__usermeals_meal_own B ON (A.usermeals_meal_own_id=B.id AND B.is_complete=1)
			WHERE A.user_id=$loginUserId)						
	) as total_calories";
  
  $db->setQuery($query);

  $actualCalories=@$db->loadObject()->caloriesTotal;
  $actualCarbs=@$db->loadObject()->carbohydratesTotal;
  $actualFats=@$db->loadObject()->fatTotal;
  $actualProtein=@$db->loadObject()->proteinTotal;
   
  return array('actualCalories'=>$actualCalories,'actualCarbs'=>$actualCarbs,'actualFats'=>$actualFats,'actualProtein'=>$actualProtein);
}

//Check next/previous date validation
function check_date_valid($loginUserId,$date_var){
	$db = JFactory::getDbo(); 
	$query="SELECT count('id') as is_date_ext  FROM #__usermeals_level WHERE user_id=$loginUserId AND start_date ='$date_var'";
	$db->setQuery($query);
	$is_ndate_exist= @$db->loadObject()->is_date_ext;
	
	if($is_ndate_exist==0){ 
		return false;
	}else{
		return true;
	}	
}

//get nutrition program details view programs
function meal_program_page_dates($loginUserId,$date,$meal_number){
	$db = JFactory::getDbo(); 
	$query="SELECT B.id FROM `#__usermeals_level` A JOIN #__usermeals_meal B ON A.id=B.usermeals_level_id WHERE A.user_id=$loginUserId AND A.start_date ='$date' AND B.meal_number=$meal_number";
	$db->setQuery($query);
	return @$usermeals_meal_id=$db->loadObject()->id;
}

function check_program_page_dates_valid($loginUserId,$date_meal_id){
	$db = JFactory::getDbo(); 
	$query="SELECT count('id') as is_date_ext  FROM #__usermeals_level A JOIN #__usermeals_meal B ON A.id=B.usermeals_level_id WHERE A.user_id=$loginUserId AND B.id ='$date_meal_id'";
	$db->setQuery($query);
	$is_ndate_exist= @$db->loadObject()->is_date_ext;
	if($is_ndate_exist==0){ 
		return false;
	}else{
		return true;
	}	
}

function meal_program_page_dates_own($loginUserId,$date,$meal_number){
	$db = JFactory::getDbo(); 
	$query="SELECT B.id FROM `#__usermeals_level_own` A JOIN #__usermeals_meal_own B ON A.id=B.usermeals_level_own_id WHERE A.user_id=$loginUserId AND A.start_date ='$date' AND B.meal_number=$meal_number";
	$db->setQuery($query); 
	return @$usermeals_meal_id=$db->loadObject()->id;
}

function check_program_page_dates_valid_own($loginUserId,$date_meal_id){
	$db = JFactory::getDbo(); 
	$query="SELECT count('id') as is_date_ext  FROM #__usermeals_level_own A JOIN #__usermeals_meal_own B ON A.id=B.usermeals_level_own_id WHERE A.user_id=$loginUserId AND B.id ='$date_meal_id'";
	$db->setQuery($query);
	$is_ndate_exist= @$db->loadObject()->is_date_ext;
	if($is_ndate_exist==0){ 
		return false;
	}else{
		return true;
	}	
}


?>