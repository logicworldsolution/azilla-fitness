<?php
/**
 * @version     1.0.0
 * @package     com_usermeals
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_usermeals', JPATH_ADMINISTRATOR);
?>

<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<style>
    .front-end-edit ul {
        padding: 0 !important;
    }
    .front-end-edit li {
        list-style: none;
        margin-bottom: 6px !important;
    }
    .front-end-edit label {
        margin-right: 10px;
        display: block;
        float: left;
        width: 200px !important;
    }
    .front-end-edit .radio label {
        float: none;
    }
    .front-end-edit .readonly {
        border: none !important;
        color: #666;
    }    
    .front-end-edit #editor-xtd-buttons {
        height: 50px;
        width: 600px;
        float: left;
    }
    .front-end-edit .toggle-editor {
        height: 50px;
        width: 120px;
        float: right;
    }

    #jform_rules-lbl{
        display:none;
    }

    #access-rules a:hover{
        background:#f5f5f5 url('../images/slider_minus.png') right  top no-repeat;
        color: #444;
    }

    fieldset.radio label{
        width: 50px !important;
    }
</style>
<script type="text/javascript">
    function getScript(url,success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
        done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState
                || this.readyState == 'loaded'
                || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }
    getScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',function() {
        js = jQuery.noConflict();
        js(document).ready(function(){
            js('#form-usermeal').submit(function(event){
                
				if(js('#jform_picture').val() != ''){
					js('#jform_picture_hidden').val(js('#jform_picture').val());
				} 
            }); 
        
            
        });
    });
    
</script>

<div class="usermeal-edit front-end-edit">
    <?php if (!empty($this->item->id)): ?>
        <h1>Edit <?php echo $this->item->id; ?></h1>
    <?php else: ?>
        <h1>Add</h1>
    <?php endif; ?>

    <form id="form-usermeal" action="<?php echo JRoute::_('index.php?option=com_usermeals&task=usermeal.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <ul>
            				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('food_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('food_id'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('recipe_type_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('recipe_type_id'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('recipe_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('recipe_id'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('user_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('user_id'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('date'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('date'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('brand_name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('brand_name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('meal_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('meal_id'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('serv_size'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('serv_size'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('calories'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('calories'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('carbohydrates'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('carbohydrates'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('protein'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('protein'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('total_carbohydrates'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('total_carbohydrates'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('total_fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('total_fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('total_protein'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('total_protein'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('saturated_fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('saturated_fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('trans_fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('trans_fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('cholesterol'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('cholesterol'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('sodium'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('sodium'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('dietary_fiber'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('dietary_fiber'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('sugars'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('sugars'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_a'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_a'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_c'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_c'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_d'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_d'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_k'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_k'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('folic_acid'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('folic_acid'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('calcium'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('calcium'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('potassium'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('potassium'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('iron'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('iron'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('blood_type'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('blood_type'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('notes'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('notes'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('picture'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('picture'); ?></div>
			</div>

				<?php if (!empty($this->item->picture)) : ?>
						<a href="<?php echo JRoute::_(JUri::base() . 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_usermeals' . DIRECTORY_SEPARATOR . 'asset' .DIRECTORY_SEPARATOR . $this->item->picture, false);?>"><?php echo JText::_("COM_USERMEALS_VIEW_FILE"); ?></a>
				<?php endif; ?>
				<input type="hidden" name="jform[picture]" id="jform_picture_hidden" value="<?php echo $this->item->picture ?>" />			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('ingredients'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('ingredients'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('cooking_directions'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('cooking_directions'); ?></div>
			</div>

        </ul>

        <div>
            <button type="submit" class="validate"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
            <?php echo JText::_('or'); ?>
            <a href="<?php echo JRoute::_('index.php?option=com_usermeals&task=usermeal.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>

            <input type="hidden" name="option" value="com_usermeals" />
            <input type="hidden" name="task" value="usermealform.save" />
            <?php echo JHtml::_('form.token'); ?>
        </div>
    </form>
</div>
