<?php
/**
 * @version     1.0.0
 * @package     com_usermeals
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_usermeals', JPATH_ADMINISTRATOR);
$canEdit = JFactory::getUser()->authorise('core.edit', 'com_usermeals');
if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_usermeals')) {
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>
<?php if ($this->item) : ?>

    <div class="item_fields">

        <ul class="fields_list">

            			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_ID'); ?>:
			<?php echo $this->item->id; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_ORDERING'); ?>:
			<?php echo $this->item->ordering; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_STATE'); ?>:
			<?php echo $this->item->state; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_CREATED_BY'); ?>:
			<?php echo $this->item->created_by; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_NAME'); ?>:
			<?php echo $this->item->name; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_FOOD_ID'); ?>:
			<?php echo $this->item->food_id; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_RECIPE_TYPE_ID'); ?>:
			<?php echo $this->item->recipe_type_id; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_RECIPE_ID'); ?>:
			<?php echo $this->item->recipe_id; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_USER_ID'); ?>:
			<?php echo $this->item->user_id; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_DATE'); ?>:
			<?php echo $this->item->date; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_BRAND_NAME'); ?>:
			<?php echo $this->item->brand_name; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_MEAL_ID'); ?>:
			<?php echo $this->item->meal_id; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_SERV_SIZE'); ?>:
			<?php echo $this->item->serv_size; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_CALORIES'); ?>:
			<?php echo $this->item->calories; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_CARBOHYDRATES'); ?>:
			<?php echo $this->item->carbohydrates; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_FAT'); ?>:
			<?php echo $this->item->fat; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_PROTEIN'); ?>:
			<?php echo $this->item->protein; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_TOTAL_CARBOHYDRATES'); ?>:
			<?php echo $this->item->total_carbohydrates; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_TOTAL_FAT'); ?>:
			<?php echo $this->item->total_fat; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_TOTAL_PROTEIN'); ?>:
			<?php echo $this->item->total_protein; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_SATURATED_FAT'); ?>:
			<?php echo $this->item->saturated_fat; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_TRANS_FAT'); ?>:
			<?php echo $this->item->trans_fat; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_CHOLESTEROL'); ?>:
			<?php echo $this->item->cholesterol; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_SODIUM'); ?>:
			<?php echo $this->item->sodium; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_DIETARY_FIBER'); ?>:
			<?php echo $this->item->dietary_fiber; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_SUGARS'); ?>:
			<?php echo $this->item->sugars; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_VITAMIN_A'); ?>:
			<?php echo $this->item->vitamin_a; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_VITAMIN_C'); ?>:
			<?php echo $this->item->vitamin_c; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_VITAMIN_D'); ?>:
			<?php echo $this->item->vitamin_d; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_VITAMIN_K'); ?>:
			<?php echo $this->item->vitamin_k; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_FOLIC_ACID'); ?>:
			<?php echo $this->item->folic_acid; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_CALCIUM'); ?>:
			<?php echo $this->item->calcium; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_POTASSIUM'); ?>:
			<?php echo $this->item->potassium; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_IRON'); ?>:
			<?php echo $this->item->iron; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_BLOOD_TYPE'); ?>:
			<?php echo $this->item->blood_type; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_NOTES'); ?>:
			<?php echo $this->item->notes; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_PICTURE'); ?>:

			<?php 
				$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_usermeals' . DIRECTORY_SEPARATOR . 'asset' . DIRECTORY_SEPARATOR . $this->item->picture;
			?>
			<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->picture; ?></a></li>			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_INGREDIENTS'); ?>:
			<?php echo $this->item->ingredients; ?></li>
			<li><?php echo JText::_('COM_USERMEALS_FORM_LBL_USERMEAL_COOKING_DIRECTIONS'); ?>:
			<?php echo $this->item->cooking_directions; ?></li>


        </ul>

    </div>
    <?php if($canEdit): ?>
		<a href="<?php echo JRoute::_('index.php?option=com_usermeals&task=usermeal.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_USERMEALS_EDIT_ITEM"); ?></a>
	<?php endif; ?>
								<?php if(JFactory::getUser()->authorise('core.delete','com_usermeals')):
								?>
									<a href="javascript:document.getElementById('form-usermeal-delete-<?php echo $this->item->id ?>').submit()"><?php echo JText::_("COM_USERMEALS_DELETE_ITEM"); ?></a>
									<form id="form-usermeal-delete-<?php echo $this->item->id; ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_usermeals&task=usermeal.remove'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
										<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
										<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
										<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
										<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />
										<input type="hidden" name="jform[name]" value="<?php echo $this->item->name; ?>" />
										<input type="hidden" name="jform[food_id]" value="<?php echo $this->item->food_id; ?>" />
										<input type="hidden" name="jform[recipe_type_id]" value="<?php echo $this->item->recipe_type_id; ?>" />
										<input type="hidden" name="jform[recipe_id]" value="<?php echo $this->item->recipe_id; ?>" />
										<input type="hidden" name="jform[user_id]" value="<?php echo $this->item->user_id; ?>" />
										<input type="hidden" name="jform[date]" value="<?php echo $this->item->date; ?>" />
										<input type="hidden" name="jform[brand_name]" value="<?php echo $this->item->brand_name; ?>" />
										<input type="hidden" name="jform[meal_id]" value="<?php echo $this->item->meal_id; ?>" />
										<input type="hidden" name="jform[serv_size]" value="<?php echo $this->item->serv_size; ?>" />
										<input type="hidden" name="jform[calories]" value="<?php echo $this->item->calories; ?>" />
										<input type="hidden" name="jform[carbohydrates]" value="<?php echo $this->item->carbohydrates; ?>" />
										<input type="hidden" name="jform[fat]" value="<?php echo $this->item->fat; ?>" />
										<input type="hidden" name="jform[protein]" value="<?php echo $this->item->protein; ?>" />
										<input type="hidden" name="jform[total_carbohydrates]" value="<?php echo $this->item->total_carbohydrates; ?>" />
										<input type="hidden" name="jform[total_fat]" value="<?php echo $this->item->total_fat; ?>" />
										<input type="hidden" name="jform[total_protein]" value="<?php echo $this->item->total_protein; ?>" />
										<input type="hidden" name="jform[saturated_fat]" value="<?php echo $this->item->saturated_fat; ?>" />
										<input type="hidden" name="jform[trans_fat]" value="<?php echo $this->item->trans_fat; ?>" />
										<input type="hidden" name="jform[cholesterol]" value="<?php echo $this->item->cholesterol; ?>" />
										<input type="hidden" name="jform[sodium]" value="<?php echo $this->item->sodium; ?>" />
										<input type="hidden" name="jform[dietary_fiber]" value="<?php echo $this->item->dietary_fiber; ?>" />
										<input type="hidden" name="jform[sugars]" value="<?php echo $this->item->sugars; ?>" />
										<input type="hidden" name="jform[vitamin_a]" value="<?php echo $this->item->vitamin_a; ?>" />
										<input type="hidden" name="jform[vitamin_c]" value="<?php echo $this->item->vitamin_c; ?>" />
										<input type="hidden" name="jform[vitamin_d]" value="<?php echo $this->item->vitamin_d; ?>" />
										<input type="hidden" name="jform[vitamin_k]" value="<?php echo $this->item->vitamin_k; ?>" />
										<input type="hidden" name="jform[folic_acid]" value="<?php echo $this->item->folic_acid; ?>" />
										<input type="hidden" name="jform[calcium]" value="<?php echo $this->item->calcium; ?>" />
										<input type="hidden" name="jform[potassium]" value="<?php echo $this->item->potassium; ?>" />
										<input type="hidden" name="jform[iron]" value="<?php echo $this->item->iron; ?>" />
										<input type="hidden" name="jform[blood_type]" value="<?php echo $this->item->blood_type; ?>" />
										<input type="hidden" name="jform[notes]" value="<?php echo $this->item->notes; ?>" />
										<input type="hidden" name="jform[picture]" value="<?php echo $this->item->picture; ?>" />
										<input type="hidden" name="jform[ingredients]" value="<?php echo $this->item->ingredients; ?>" />
										<input type="hidden" name="jform[cooking_directions]" value="<?php echo $this->item->cooking_directions; ?>" />
										<input type="hidden" name="option" value="com_usermeals" />
										<input type="hidden" name="task" value="usermeal.remove" />
										<?php echo JHtml::_('form.token'); ?>
									</form>
								<?php
								endif;
							?>
<?php
else:
    echo JText::_('COM_USERMEALS_ITEM_NOT_LOADED');
endif;
?>
