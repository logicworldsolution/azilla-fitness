<?php
/**
 * @version     1.0.0
 * @package     com_usermeals
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;
?>

<div class="items">
    <ul class="items_list">
<?php $show = false; ?>
        <?php foreach ($this->items as $item) : ?>

            
				<?php
					if($item->state == 1 || ($item->state == 0 && JFactory::getUser()->authorise('core.edit.own',' com_usermeals'))):
						$show = true;
						?>
							<li>
								<a href="<?php echo JRoute::_('index.php?option=com_usermeals&view=usermeal&id=' . (int)$item->id); ?>"><?php echo $item->name; ?></a>
								<?php
									if(JFactory::getUser()->authorise('core.edit.state','com_usermeals')):
									?>
										<a href="javascript:document.getElementById('form-usermeal-state-<?php echo $item->id; ?>').submit()"><?php if($item->state == 1): echo JText::_("COM_USERMEALS_UNPUBLISH_ITEM"); else: echo JText::_("COM_USERMEALS_PUBLISH_ITEM"); endif; ?></a>
										<form id="form-usermeal-state-<?php echo $item->id ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_usermeals&task=usermeal.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
											<input type="hidden" name="jform[id]" value="<?php echo $item->id; ?>" />
											<input type="hidden" name="jform[ordering]" value="<?php echo $item->ordering; ?>" />
											<input type="hidden" name="jform[state]" value="<?php echo (int)!((int)$item->state); ?>" />
											<input type="hidden" name="jform[name]" value="<?php echo $item->name; ?>" />
											<input type="hidden" name="jform[food_id]" value="<?php echo $item->food_id; ?>" />
											<input type="hidden" name="jform[recipe_type_id]" value="<?php echo $item->recipe_type_id; ?>" />
											<input type="hidden" name="jform[recipe_id]" value="<?php echo $item->recipe_id; ?>" />
											<input type="hidden" name="jform[user_id]" value="<?php echo $item->user_id; ?>" />
											<input type="hidden" name="jform[date]" value="<?php echo $item->date; ?>" />
											<input type="hidden" name="jform[brand_name]" value="<?php echo $item->brand_name; ?>" />
											<input type="hidden" name="jform[meal_id]" value="<?php echo $item->meal_id; ?>" />
											<input type="hidden" name="jform[serv_size]" value="<?php echo $item->serv_size; ?>" />
											<input type="hidden" name="jform[calories]" value="<?php echo $item->calories; ?>" />
											<input type="hidden" name="jform[carbohydrates]" value="<?php echo $item->carbohydrates; ?>" />
											<input type="hidden" name="jform[fat]" value="<?php echo $item->fat; ?>" />
											<input type="hidden" name="jform[protein]" value="<?php echo $item->protein; ?>" />
											<input type="hidden" name="jform[total_carbohydrates]" value="<?php echo $item->total_carbohydrates; ?>" />
											<input type="hidden" name="jform[total_fat]" value="<?php echo $item->total_fat; ?>" />
											<input type="hidden" name="jform[total_protein]" value="<?php echo $item->total_protein; ?>" />
											<input type="hidden" name="jform[saturated_fat]" value="<?php echo $item->saturated_fat; ?>" />
											<input type="hidden" name="jform[trans_fat]" value="<?php echo $item->trans_fat; ?>" />
											<input type="hidden" name="jform[cholesterol]" value="<?php echo $item->cholesterol; ?>" />
											<input type="hidden" name="jform[sodium]" value="<?php echo $item->sodium; ?>" />
											<input type="hidden" name="jform[dietary_fiber]" value="<?php echo $item->dietary_fiber; ?>" />
											<input type="hidden" name="jform[sugars]" value="<?php echo $item->sugars; ?>" />
											<input type="hidden" name="jform[vitamin_a]" value="<?php echo $item->vitamin_a; ?>" />
											<input type="hidden" name="jform[vitamin_c]" value="<?php echo $item->vitamin_c; ?>" />
											<input type="hidden" name="jform[vitamin_d]" value="<?php echo $item->vitamin_d; ?>" />
											<input type="hidden" name="jform[vitamin_k]" value="<?php echo $item->vitamin_k; ?>" />
											<input type="hidden" name="jform[folic_acid]" value="<?php echo $item->folic_acid; ?>" />
											<input type="hidden" name="jform[calcium]" value="<?php echo $item->calcium; ?>" />
											<input type="hidden" name="jform[potassium]" value="<?php echo $item->potassium; ?>" />
											<input type="hidden" name="jform[iron]" value="<?php echo $item->iron; ?>" />
											<input type="hidden" name="jform[blood_type]" value="<?php echo $item->blood_type; ?>" />
											<input type="hidden" name="jform[notes]" value="<?php echo $item->notes; ?>" />
											<input type="hidden" name="jform[picture]" value="<?php echo $item->picture; ?>" />
											<input type="hidden" name="jform[ingredients]" value="<?php echo $item->ingredients; ?>" />
											<input type="hidden" name="jform[cooking_directions]" value="<?php echo $item->cooking_directions; ?>" />
											<input type="hidden" name="option" value="com_usermeals" />
											<input type="hidden" name="task" value="usermeal.save" />
											<?php echo JHtml::_('form.token'); ?>
										</form>
									<?php
									endif;
									if(JFactory::getUser()->authorise('core.delete','com_usermeals')):
									?>
										<a href="javascript:document.getElementById('form-usermeal-delete-<?php echo $item->id; ?>').submit()"><?php echo JText::_("COM_USERMEALS_DELETE_ITEM"); ?></a>
										<form id="form-usermeal-delete-<?php echo $item->id; ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_usermeals&task=usermeal.remove'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
											<input type="hidden" name="jform[id]" value="<?php echo $item->id; ?>" />
											<input type="hidden" name="jform[ordering]" value="<?php echo $item->ordering; ?>" />
											<input type="hidden" name="jform[state]" value="<?php echo $item->state; ?>" />
											<input type="hidden" name="jform[created_by]" value="<?php echo $item->created_by; ?>" />
											<input type="hidden" name="jform[name]" value="<?php echo $item->name; ?>" />
											<input type="hidden" name="jform[food_id]" value="<?php echo $item->food_id; ?>" />
											<input type="hidden" name="jform[recipe_type_id]" value="<?php echo $item->recipe_type_id; ?>" />
											<input type="hidden" name="jform[recipe_id]" value="<?php echo $item->recipe_id; ?>" />
											<input type="hidden" name="jform[user_id]" value="<?php echo $item->user_id; ?>" />
											<input type="hidden" name="jform[date]" value="<?php echo $item->date; ?>" />
											<input type="hidden" name="jform[brand_name]" value="<?php echo $item->brand_name; ?>" />
											<input type="hidden" name="jform[meal_id]" value="<?php echo $item->meal_id; ?>" />
											<input type="hidden" name="jform[serv_size]" value="<?php echo $item->serv_size; ?>" />
											<input type="hidden" name="jform[calories]" value="<?php echo $item->calories; ?>" />
											<input type="hidden" name="jform[carbohydrates]" value="<?php echo $item->carbohydrates; ?>" />
											<input type="hidden" name="jform[fat]" value="<?php echo $item->fat; ?>" />
											<input type="hidden" name="jform[protein]" value="<?php echo $item->protein; ?>" />
											<input type="hidden" name="jform[total_carbohydrates]" value="<?php echo $item->total_carbohydrates; ?>" />
											<input type="hidden" name="jform[total_fat]" value="<?php echo $item->total_fat; ?>" />
											<input type="hidden" name="jform[total_protein]" value="<?php echo $item->total_protein; ?>" />
											<input type="hidden" name="jform[saturated_fat]" value="<?php echo $item->saturated_fat; ?>" />
											<input type="hidden" name="jform[trans_fat]" value="<?php echo $item->trans_fat; ?>" />
											<input type="hidden" name="jform[cholesterol]" value="<?php echo $item->cholesterol; ?>" />
											<input type="hidden" name="jform[sodium]" value="<?php echo $item->sodium; ?>" />
											<input type="hidden" name="jform[dietary_fiber]" value="<?php echo $item->dietary_fiber; ?>" />
											<input type="hidden" name="jform[sugars]" value="<?php echo $item->sugars; ?>" />
											<input type="hidden" name="jform[vitamin_a]" value="<?php echo $item->vitamin_a; ?>" />
											<input type="hidden" name="jform[vitamin_c]" value="<?php echo $item->vitamin_c; ?>" />
											<input type="hidden" name="jform[vitamin_d]" value="<?php echo $item->vitamin_d; ?>" />
											<input type="hidden" name="jform[vitamin_k]" value="<?php echo $item->vitamin_k; ?>" />
											<input type="hidden" name="jform[folic_acid]" value="<?php echo $item->folic_acid; ?>" />
											<input type="hidden" name="jform[calcium]" value="<?php echo $item->calcium; ?>" />
											<input type="hidden" name="jform[potassium]" value="<?php echo $item->potassium; ?>" />
											<input type="hidden" name="jform[iron]" value="<?php echo $item->iron; ?>" />
											<input type="hidden" name="jform[blood_type]" value="<?php echo $item->blood_type; ?>" />
											<input type="hidden" name="jform[notes]" value="<?php echo $item->notes; ?>" />
											<input type="hidden" name="jform[picture]" value="<?php echo $item->picture; ?>" />
											<input type="hidden" name="jform[ingredients]" value="<?php echo $item->ingredients; ?>" />
											<input type="hidden" name="jform[cooking_directions]" value="<?php echo $item->cooking_directions; ?>" />
											<input type="hidden" name="jform[additional_information]" value="<?php echo $item->additional_information; ?>" />
											<input type="hidden" name="option" value="com_usermeals" />
											<input type="hidden" name="task" value="usermeal.remove" />
											<?php echo JHtml::_('form.token'); ?>
										</form>
									<?php
									endif;
								?>
							</li>
						<?php endif; ?>

<?php endforeach; ?>
        <?php
        if (!$show):
            echo JText::_('COM_USERMEALS_NO_ITEMS');
        endif;
        ?>
    </ul>
</div>
<?php if ($show): ?>
    <div class="pagination">
        <p class="counter">
            <?php echo $this->pagination->getPagesCounter(); ?>
        </p>
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
<?php endif; ?>


									<?php if(JFactory::getUser()->authorise('core.create','com_usermeals')): ?><a href="<?php echo JRoute::_('index.php?option=com_usermeals&task=usermeal.edit&id=0'); ?>"><?php echo JText::_("COM_USERMEALS_ADD_ITEM"); ?></a>
	<?php endif; ?>