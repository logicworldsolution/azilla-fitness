<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Registration model class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class UsersModelRegistration extends JModelForm
{
	/**
	 * @var    object  The user registration data.
	 * @since  1.6
	 */
	protected $data;

	/**
	 * Method to activate a user account.
	 *
	 * @param   string  $token  The activation token.
	 *
	 * @return  mixed    False on failure, user object on success.
	 *
	 * @since   1.6
	 */
	 function PPHttpPost($methodName_, $nvpStr_) {  
     $environment = 'sandbox'; 
  
 $API_UserName = urlencode('simran-facilitator_api1.34interactive.com');  
 $API_Password = urlencode('1364191440');  
 $API_Signature = urlencode('AZLGlolaP4S-KgTmqfi6DzuaPuCKALydU0Lzo9TAVyFmlrhQ3W.K3Khd');  
 $API_Endpoint = "https://api-3t.paypal.com/nvp";  
   
 if("sandbox" === $environment || "beta-sandbox" === $environment) {  
  $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";  
 }  
 $version = urlencode('51.0');  
  
 // setting the curl parameters.  
 $ch = curl_init();  
 curl_setopt($ch, CURLOPT_URL, $API_Endpoint);  
 curl_setopt($ch, CURLOPT_VERBOSE, 1);  
  
 // turning off the server and peer verification(TrustManager Concept).  
 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
 curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  
  
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
 curl_setopt($ch, CURLOPT_POST, 1);  
  
 // NVPRequest for submitting to server  
 $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";  
  
 // setting the nvpreq as POST FIELD to curl  
 curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);  
  
 // getting response from server  
 $httpResponse = curl_exec($ch);  
  
 if(!$httpResponse) {  
  exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');  
 }  
  
 // Extract the RefundTransaction response details  
 $httpResponseAr = explode("&", $httpResponse);  
  
 $httpParsedResponseAr = array();  
 foreach ($httpResponseAr as $i => $value) {  
$tmpAr = explode("=", $value);  
if(sizeof($tmpAr) > 1) {  
$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];  
}  
}  
  
if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {  
exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");  
}  
  
return $httpParsedResponseAr;  
}  
	 
	public function activate($token)
	{
		$config = JFactory::getConfig();
		$userParams = JComponentHelper::getParams('com_users');
		$db = $this->getDbo();

		// Get the user id based on the token.
		$query = $db->getQuery(true);
		$query->select($db->quoteName('id'))
			->from($db->quoteName('#__users'))
			->where($db->quoteName('activation') . ' = ' . $db->quote($token))
			->where($db->quoteName('block') . ' = ' . 1)
			->where($db->quoteName('lastvisitDate') . ' = ' . $db->quote($db->getNullDate()));
		$db->setQuery($query);

		try
		{
			$userId = (int) $db->loadResult();
		}
		catch (RuntimeException $e)
		{
			$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
			return false;
		}

		// Check for a valid user id.
		if (!$userId)
		{
			$this->setError(JText::_('COM_USERS_ACTIVATION_TOKEN_NOT_FOUND'));
			return false;
		}

		// Load the users plugin group.
		JPluginHelper::importPlugin('user');

		// Activate the user.
		$user = JFactory::getUser($userId);

		// Admin activation is on and user is verifying their email
		if (($userParams->get('useractivation') == 2) && !$user->getParam('activate', 0))
		{
			$uri = JUri::getInstance();

			// Compile the admin notification mail values.
			$data = $user->getProperties();
			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
			$user->set('activation', $data['activation']);
			$data['siteurl'] = JUri::base();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$user->setParam('activate', 1);
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_SUBJECT',
				$data['name'],
				$data['sitename']
			);

			$emailBody = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_BODY',
				$data['sitename'],
				$data['name'],
				$data['email'],
				$data['username'],
				$data['activate']
			);

			// get all admin users
			$query->clear()
				->select($db->quoteName(array('name', 'email', 'sendEmail', 'id')))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('sendEmail') . ' = ' . 1);

			$db->setQuery($query);

			try
			{
				$rows = $db->loadObjectList();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
				return false;
			}

			// Send mail to all users with users creating permissions and receiving system emails
			foreach ($rows as $row)
			{
				$usercreator = JFactory::getUser($row->id);

				if ($usercreator->authorise('core.create', 'com_users'))
				{
					$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBody);

					// Check for an error.
					if ($return !== true)
					{
						$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
						return false;
					}
				}
			}
		}
		// Admin activation is on and admin is activating the account
		elseif (($userParams->get('useractivation') == 2) && $user->getParam('activate', 0))
		{
			$user->set('activation', '');
			$user->set('block', '0');

			// Compile the user activated notification mail values.
			$data = $user->getProperties();
			$user->setParam('activate', 0);
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$data['siteurl'] = JUri::base();
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_SUBJECT',
				$data['name'],
				$data['sitename']
			);

			$emailBody = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_BODY',
				$data['name'],
				$data['siteurl'],
				$data['username']
			);

			$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

			// Check for an error.
			if ($return !== true)
			{
				$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
				return false;
			}
		}
		else
		{
			$user->set('activation', '');
			$user->set('block', '0');
		}

		// Store the user object.
		if (!$user->save())
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_ACTIVATION_SAVE_FAILED', $user->getError()));
			return false;
		}

		return $user;
	}

	/**
	 * Method to get the registration form data.
	 *
	 * The base form data is loaded and then an event is fired
	 * for users plugins to extend the data.
	 *
	 * @return  mixed  Data object on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getData()
	{
		if ($this->data === null)
		{
			$this->data = new stdClass;
			$app = JFactory::getApplication();
			$params = JComponentHelper::getParams('com_users');

			// Override the base user data with any data in the session.
			$temp = (array) $app->getUserState('com_users.registration.data', array());
			foreach ($temp as $k => $v)
			{
				$this->data->$k = $v;
			}

			// Get the groups the user should be added to after registration.
			$this->data->groups = array();

			// Get the default new user group, Registered if not specified.
			$system = $params->get('new_usertype', 2);

			$this->data->groups[] = $system;

			// Unset the passwords.
			unset($this->data->password1);
			unset($this->data->password2);

			// Get the dispatcher and load the users plugins.
			$dispatcher = JEventDispatcher::getInstance();
			JPluginHelper::importPlugin('user');

			// Trigger the data preparation event.
			$results = $dispatcher->trigger('onContentPrepareData', array('com_users.registration', $this->data));

			// Check for errors encountered while preparing the data.
			if (count($results) && in_array(false, $results, true))
			{
				$this->setError($dispatcher->getError());
				$this->data = false;
			}
		}

		return $this->data;
	}

	/**
	 * Method to get the registration form.
	 *
	 * The base form is loaded from XML and then an event is fired
	 * for users plugins to extend the form with extra fields.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_users.registration', 'registration', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = $this->getData();

		$this->preprocessData('com_users.registration', $data);

		return $data;
	}

	/**
	 * Override preprocessForm to load the user plugin group instead of content.
	 *
	 * @param   JForm   $form   A JForm object.
	 * @param   mixed   $data   The data expected for the form.
	 * @param   string  $group  The name of the plugin group to import (defaults to "content").
	 *
	 * @return  void
	 *
	 * @since   1.6
	 * @throws  Exception if there is an error in the form event.
	 */
	protected function preprocessForm(JForm $form, $data, $group = 'user')
	{
		$userParams = JComponentHelper::getParams('com_users');

		//Add the choice for site language at registration time
		if ($userParams->get('site_language') == 1 && $userParams->get('frontend_userparams') == 1)
		{
			$form->loadFile('sitelang', false);
		}

		parent::preprocessForm($form, $data, $group);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		// Get the application object.
		$app = JFactory::getApplication();
		$params = $app->getParams('com_users');

		// Load the parameters.
		$this->setState('params', $params);
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $temp  The form data.
	 *
	 * @return  mixed  The user id on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function register($temp)
	{
		$params = JComponentHelper::getParams('com_users');

		// Initialise the table with JUser.
		$user = new JUser;
		$data = (array) $this->getData();

		// Merge in the registration data.
		foreach ($temp as $k => $v)
		{
			$data[$k] = $v;
		}

		// Prepare the data for the user object.
		$data['email'] = JStringPunycode::emailToPunycode($data['email1']);
		$data['password'] = $data['password1'];
		$useractivation = $params->get('useractivation');
		$sendpassword = $params->get('sendpassword', 1);

		// Check if the user needs to activate their account.
		if (($useractivation == 1) || ($useractivation == 2))
		{
			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
			$data['block'] = 1;
		}

		// Bind the data.
		if (!$user->bind($data))
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));
			return false;
		}

		// Load the users plugin group.
		JPluginHelper::importPlugin('user');

		// Store the data.
		if (!$user->save())
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));
			return false;
		}

		$config = JFactory::getConfig();
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Compile the notification mail values.
		$data = $user->getProperties();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$data['siteurl'] = JUri::root();

		// Handle account activation/confirmation emails.
		if ($useractivation == 2)
		{
			// Set the link to confirm the user email.
			$uri = JUri::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username']
				);
			}
		}
		elseif ($useractivation == 1)
		{
			// Set the link to activate the user account.
			$uri = JUri::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username']
				);
			}
		}
		else
		{

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_BODY',
					$data['name'],
					$data['sitename'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['siteurl']
				);
			}
		}

		// Send the registration email.
		$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

		// Send Notification mail to administrators
		if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1))
		{
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			$emailBodyAdmin = JText::sprintf(
				'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
				$data['name'],
				$data['username'],
				$data['siteurl']
			);

			// Get all admin users
			$query->clear()
				->select($db->quoteName(array('name', 'email', 'sendEmail')))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('sendEmail') . ' = ' . 1);

			$db->setQuery($query);

			try
			{
				$rows = $db->loadObjectList();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
				return false;
			}

			// Send mail to all superadministrators id
			foreach ($rows as $row)
			{
				$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);

				// Check for an error.
				if ($return !== true)
				{
					$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
					return false;
				}
			}
		}

		// Check for an error.
		/*if ($return !== true)
		{
			$this->setError(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));

			// Send a system message to administrators receiving system mails
			$db = JFactory::getDbo();
			$query->clear()
				->select($db->quoteName(array('name', 'email', 'sendEmail', 'id')))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('block') . ' = ' . (int) 0)
				->where($db->quoteName('sendEmail') . ' = ' . (int) 1);
			$db->setQuery($query);

			try
			{
				$sendEmail = $db->loadColumn();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
				return false;
			}

			if (count($sendEmail) > 0)
			{
				$jdate = new JDate;

				// Build the query to add the messages
				foreach ($sendEmail as $userid)
				{
					$values = array($db->quote($userid), $db->quote($userid), $db->quote($jdate->toSql()), $db->quote(JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT')), $db->quote(JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username'])));
					$query->clear()
						->insert($db->quoteName('#__messages'))
						->columns($db->quoteName(array('user_id_from', 'user_id_to', 'date_time', 'subject', 'message')))
						->values(implode(',', $values));
					$db->setQuery($query);

					try
					{
						$db->execute();
					}
					catch (RuntimeException $e)
					{
						$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
						return false;
					}
				}
			}
			return false;
		}*/

		if ($useractivation == 1)
		{
			return "useractivate,".$user->id;
		}
		elseif ($useractivation == 2)
		{
			return "adminactivate,".$user->id;
		}
		else
		{
			return "test,".$user->id;
		}
	}
	public function payment($data, $user_id)
	{
		$app	= JFactory::getApplication();
		//$model = $this->getModel('Membershiptype', 'UsermembershipModel');
		$db = JFactory::getDbo();	
		//$data = JRequest::get('post');
		$query = "select * from #__users where id='".$user_id."'";
		$db->setQuery($query);
		$row = $db->loadAssoc();
		$jref_code = $data['jref_code'];
		$cvv2 = $data['jcard_cvv']; 
		$environment = 'sandbox';
		$paymentType =	'Sale';
		$firstName =urlencode($row['name']);
		$email = urlencode($row['email']);
		$userid = urlencode($user_id);
		$sel = "SELECT * from #__users where id='".$user_id."'";
			$db->setQuery($sel);
			$row = $db->loadAssoc();
			$recipient = $row['email'];
			
		if(isset($data['jpaypal_id']))
		{
			$jpaypal_id = $data['jpaypal_id'];
		}
		else
		{
			$jpaypal_id = '';
		}	
		$creditCardType = urlencode($data['jcard_type']);
		$creditCardNumber = urlencode($data['jcard_number']);
		$expDateMonth = $data['jcard_month'];

		$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));

		$expDateYear = urlencode($data['jcard_year']);
		$cvv2Number = urlencode($cvv2);
		$country = 'US';	// US or other valid country code
		if($data['typeid'] == '1')
		{
			$amount = '100.00';	
		}
		else
		{
			$amount = '25.00';	
		}
		$currencyID = 'USD';// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
		if($data['typeid'] == '1')
		{
			$BILLINGPERIOD='Year';
			$BILLINGFREQUENCY=1; 
		}
		else
		{
			$BILLINGPERIOD='Month';
			$BILLINGFREQUENCY=1; 
		}
		$MAXFAILEDPAYMENTS=3; 
		$PROFILESTARTDATE = date('Y-m-d');
		$tbc = 0;// How many billing cycles. 0 for no expiration. This example is for 3 total months of billing.
		$nvpStr =	"&PAYMENTACTION=$paymentType&PROFILESTARTDATE=$PROFILESTARTDATE&AMT=$amount&BILLINGPERIOD=$BILLINGPERIOD&BILLINGFREQUENCY=$BILLINGFREQUENCY&MAXFAILEDPAYMENTS=$MAXFAILEDPAYMENTS&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&EMAIL=$email&COUNTRYCODE=$country&CURRENCYCODE=$currencyID&TOTALBILLINGCYCLES=$tbc";
		$methodName_ = 'DoDirectPayment';
		//$methodName_ = 'CreateRecurringPaymentsProfile';
		$nvpStr_ = $nvpStr;
		/*$API_UserName = urlencode('simran-facilitator_api1.34interactive.com');
		$API_Password = urlencode('1364191440');
		$API_Signature = urlencode('AZLGlolaP4S-KgTmqfi6DzuaPuCKALydU0Lzo9TAVyFmlrhQ3W.K3Khd');*/
		$API_UserName = urlencode('sunil.purple-facilitator_api1.purplecowwebsites.com');
		$API_Password = urlencode('1378981924');
		$API_Signature = urlencode('ASJjmbBXF9P5HtD4HPW-2oqmaYOiAvs.I.H5BP6ma8vSvm68FjQ3n5SE');
		$API_Endpoint = "https://api-3t.paypal.com/nvp";
		if("sandbox" === $environment || "beta-sandbox" === $environment) {
			$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
		}
		$version = urlencode('51.0');

		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

	
		//print_r($httpParsedResponseAr);
		if($data['typeid'] == 1)
		{	
			if($jref_code !='' )
			{
				$subjref_code = substr($jref_code, 3);
				$selquery1 = "select * from `#__usermembership_types` where ref_code='".$subjref_code."'";
				$db->setQuery($selquery1);
				$listcat1 = $db->loadObjectList();
				$count1 = count($listcat1);
				if($count1 > 0)
				{
				/**************************************************************************/
				
echo "testt1";
exit;

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
					// Turn off the server and peer verification (TrustManager Concept).
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
					// Set the request as a POST FIELD for curl.
					curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
					// Get response from the server.
					$httpResponse = curl_exec($ch);

					if(!$httpResponse) {
						exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
					}

					// Extract the response details.
					$httpResponseAr = explode("&", $httpResponse);

					$httpParsedResponseAr = array();
					foreach ($httpResponseAr as $i => $value) {
						$tmpAr = explode("=", $value);
						if(sizeof($tmpAr) > 1) {
							$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
						}
					}

					if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
						exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
					}
					//$httpParsedResponseAr = PPHttpPost('DoDirectPayment', $nvpStr, $environment);
					if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
		
					/**********************************
					
					
					*******************************/
					$selquery = "select max(ref_code) as MaxCode from `#__usermembership_types`";
					$db->setQuery($selquery);
					$listcat = $db->loadObjectList();
					$count = count($listcat);
					if($count > 0)
					{
						foreach($listcat as $rowcode)
						{
							$jcode = ($rowcode->MaxCode)+1;
							$code = sprintf("%06s", $jcode);
						}
					}
					else
					{
						$code = 100001;
					}
					$amount = urldecode($httpParsedResponseAr['AMT']);
					$expdate = date('Y-m-d', strtotime('+1 year'));
			$query = "INSERT INTO `#__usermembership_types` SET `user_id` = '".$user_id."', `ref_prefix` = 'AZI', `ref_code` = '".$code."', `membership_type` = '".$data['typeid']."', `paypal_id` = '".$jpaypal_id."', `transaction_id` = '".$httpParsedResponseAr['TRANSACTIONID']."', `date` = NOW(), `expdate` = '".$expdate."', `amount` = '".$amount."'";
                $db->setQuery($query);
                $db->query();
				$last = $db->insertid();
			$query2 = "INSERT INTO `#__user_affiliated_member` SET `user_id` = '".$user_id."', `mem_type_id` = '".$last."', `ref_code`='".$jref_code."', `payment` = '".$amount."',  `transaction_id` = '".$httpParsedResponseAr['TRANSACTIONID']."', `date` = NOW(), `exp_date` = '".$expdate."'";
				$db->setQuery($query2);
                $db->query();
				$mail =& JFactory::getMailer();
			$from = "sunil@purplecowwebsites.com";
			$fromname = "Azzilla";
			$subject = "Recieved payment from Azzilla";
			$body = "Hi Member, \n\n\tYour Payment has been recieved. Your Refferal No. is AZI".$code."\n\n\nThanks,\nTeam Azzilla.";
			$mail->setSender(array($from, $fromname));
			$mail->setSubject($subject);
			$mail->setBody($body);
	 
			// Are we sending the email as HTML?
			//if ( $mode ) {
				//	$mail->IsHTML(true);
			//}
	 
			$mail->addRecipient($recipient);
			//$mail->addCC($cc);
			//$mail->addBCC($bcc);
			//$mail->addAttachment($attachment);
	 
			// Take care of reply email addresses
			/*if( is_array( $replyto ) ) {
					$numReplyTo = count($replyto);
					for ( $i=0; $i < $numReplyTo; $i++){
							$mail->addReplyTo( array($replyto[$i], $replytoname[$i]) );
					}
			} elseif( isset( $replyto ) ) {
					$mail->addReplyTo( array( $replyto, $replytoname ) );
			}*/
	 
			 $mail->Send();
				$app->redirect(JRoute::_("index.php?option=com_usermembership&view=membershiptype&id=3&mem_id=".$last."&userid=".$user_id, false),"First step payment is successfully completed. Please Pay for monthly membership. Your referral code is <strong>AZI$code</strong>", 'notice');
			}
			else
			{
			/*************************************************/
			
			$app->redirect(JRoute::_("index.php?option=com_users&view=registration&id=".$data['typeid'], false),"There is a problem please try again.", 'notice');
			
			/*************************************************/
			}
			}
			else
			{
				$app->redirect(JRoute::_("index.php?option=com_users&view=registration&id=1", false),"Your referral number is wrong.", 'notice');
			}
			}
			else
			{
			echo "<pre>";
		
   $environment = 'sandbox'; 		
  
/*--------------------*/  
// Collect the payment info  
 
// Set request-specific fields.  
$firstName = urlencode($_POST['name']);  
$username = urlencode($_POST['username']);  
 $password1 = urlencode($_POST['password1']);  
 $email1 = urlencode($_POST['email1']);  
 $typeid = urlencode($_POST['typeid']);  
 $jref_code = urlencode($_POST['jref_code']);  
 $jpaypal_id = urlencode($_POST['jpaypal_id']);
 $jcard_name = urlencode($_POST['jcard_name']);
 $creditCardType = urlencode($_POST['jcard_type']);
$creditCardNumber = urlencode($_POST['jcard_number']);

 $expDateMonth = urlencode($_POST['jcard_month']); 
// Month must be padded with leading zero  
 $padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));  
  
 $expDateYear = urlencode($_POST['jcard_year']);  
 $cvv2Number = urlencode($_POST['jcard_cvv']);  
 $country = urlencode('US');    // US or other valid country code  
 $amount = urlencode('25');  
 $currencyID = urlencode('USD');     // or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')  
/*--------------------*/ 
  
 $firstName = urlencode($_POST['name']);  
 $username = urlencode($_POST['username']);   

$firstName = urlencode($_POST['name']);  
/*$lastName = urlencode('tmDDDww');  
$creditCardType = urlencode('Visa');  
$creditCardNumber = urlencode('4111111111111111');  
$expDateMonth = '11';  
// Month must be padded with leading zero  
$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));  
  
$expDateYear = urlencode('2017');  
$cvv2Number = urlencode('');  
$address1 = urlencode('address 1');  
$address2 = urlencode('address 2');  
$city = urlencode('United States ');  
$state = urlencode('New York');  
$zip = urlencode('13355');  
$country = urlencode('US');*/    // US or other valid country code  
$amount = urlencode('100');  
$currencyID = urlencode('USD');       // or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')  
/*--------------------*/  
  
  
$token = urlencode("");  
if($data['typeid'] == '1')
		{
		$paymentAmount = urlencode("100");  
		
			$billingPeriod = urlencode("Year");    // or "Day", "Week", "SemiMonth", "Year"  
$billingFreq = urlencode("1");      // combination of this and billingPeriod must be at most a year
		}
		else
		{
		$billingPeriod = urlencode("Month");    // or "Day", "Week", "SemiMonth", "Year"  
$billingFreq = urlencode("1");      // combination of this and billingPeriod must be at most a year
		}


$currencyID = urlencode("USD");      // or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')  
$startDate = urlencode("2013-12-12T0:0:0");     
  
$desc = urlencode('desc sample');  
$nvpStr="&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=$padDateMonth$expDateYear&FIRSTNAME=$firstName&LASTNAME=$lastName&AMT=$paymentAmount&CURRENCYCODE=$currencyID&PROFILESTARTDATE=$startDate";  
$nvpStr .= "&BILLINGPERIOD=$billingPeriod&BILLINGFREQUENCY=$billingFreq&DESC=$desc";  
    
$httpParsedResponseAr =$this->PPHttpPost('CreateRecurringPaymentsProfile', $nvpStr);  
echo "<pre>";  
if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {  
	/**********************************
		
		
		*******************************/
				$selquery = "select max(ref_code) as MaxCode from `#__usermembership_types`";
					$db->setQuery($selquery);
					$listcat = $db->loadObjectList();
					$count = count($listcat);
					if($count > 0)
					{
						foreach($listcat as $rowcode)
						{
							$jcode = ($rowcode->MaxCode)+1;
							$code = sprintf("%06s", $jcode);
						}
					}
					else
					{
						$code = 100001;
					}
					$amount = urldecode($httpParsedResponseAr['AMT']);
					$expdate = date('Y-m-d', strtotime('+1 year'));
			$query = "INSERT INTO `#__usermembership_types` SET `user_id` = '".$user_id."', `ref_prefix` = 'AZI', `ref_code` = '".$code."', `membership_type` = '".$data['typeid']."', `paypal_id` = '".$jpaypal_id."', `transaction_id` = '".$httpParsedResponseAr['TRANSACTIONID']."', `date` = NOW(), `expdate` = '".$expdate."', `amount` = '".$amount."'";
                $db->setQuery($query);
                $db->query();
				$last = $db->insertid();
			$query2 = "INSERT INTO `#__user_affiliated_member` SET `user_id` = '".$user_id."', `mem_type_id` = '".$last."', `ref_code`='".$jref_code."', `payment` = '".$amount."',  `transaction_id` = '".$httpParsedResponseAr['TRANSACTIONID']."', `date` = NOW(), `exp_date` = '".$expdate."'";
				$db->setQuery($query2);
                $db->query();
				$mail =& JFactory::getMailer();
			$from = "sunil@purplecowwebsites.com";
			$fromname = "Azzilla";
			$subject = "Recieved payment from Azzilla";
			$body = "Hi Member, \n\n\tYour Payment has been recieved. Your Refferal No. is AZI".$code."\n\n\nThanks,\nTeam Azzilla.";
			$mail->setSender(array($from, $fromname));
			$mail->setSubject($subject);
			$mail->setBody($body);
	 
			// Are we sending the email as HTML?
			//if ( $mode ) {
				//	$mail->IsHTML(true);
			//}
	 
			$mail->addRecipient($recipient);
			//$mail->addCC($cc);
			//$mail->addBCC($bcc);
			//$mail->addAttachment($attachment);
	 
			// Take care of reply email addresses
			/*if( is_array( $replyto ) ) {
					$numReplyTo = count($replyto);
					for ( $i=0; $i < $numReplyTo; $i++){
							$mail->addReplyTo( array($replyto[$i], $replytoname[$i]) );
					}
			} elseif( isset( $replyto ) ) {
					$mail->addReplyTo( array( $replyto, $replytoname ) );
			}*/
	 
			 $mail->Send();
				$app->redirect(JRoute::_("index.php?option=com_usermembership&view=membershiptype&id=3&mem_id=".$last."&userid=".$user_id, false),"First step payment is successfully completed. Please Pay for monthly membership. Your referral code is <strong>AZI$code</strong>", 'notice');
			}



		//$httpParsedResponseAr = PPHttpPost('DoDirectPayment', $nvpStr, $environment);

			else
			{
				$app->redirect(JRoute::_("index.php?option=com_users&view=registration&id=".$data['typeid'], false),"There is a problem please try again.", 'notice');
			}
			}
			}
			else 
			{
			
				 $environment = 'sandbox'; 		
  
/*--------------------*/  
// Collect the payment info  
 // print_r($_POST); 
// Set request-specific fields.  
 $firstName = urlencode($_POST['name']);  
 $username = urlencode($_POST['username']);  
 $password1 = urlencode($_POST['password1']);  
 $email1 = urlencode($_POST['email1']);  
 $typeid = urlencode($_POST['typeid']);  
  $jref_code = urlencode($_POST['jref_code']);  
 $jpaypal_id = urlencode($_POST['jpaypal_id']);
  $jcard_name = urlencode($_POST['jcard_name']);
 $creditCardType = urlencode($_POST['jcard_type']);
 $creditCardNumber = urlencode($_POST['jcard_number']);

 $expDateMonth = urlencode($_POST['jcard_month']); 
// Month must be padded with leading zero  
 $padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));  
  
$expDateYear = urlencode($_POST['jcard_year']);  
 $cvv2Number = urlencode($_POST['jcard_cvv']);  
 $country = urlencode('US');    // US or other valid country code  
 $amount = urlencode('25');  
 $currencyID = urlencode('USD');     // or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')  
/*--------------------*/ 
  

$firstName = urlencode($_POST['name']);  
/*$lastName = urlencode('tmDDDww');  
$creditCardType = urlencode('Visa');  
$creditCardNumber = urlencode('4111111111111111');  
$expDateMonth = '11';  
// Month must be padded with leading zero  
$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));  
  
$expDateYear = urlencode('2017');  
$cvv2Number = urlencode('');  
$address1 = urlencode('address 1');  
$address2 = urlencode('address 2');  
$city = urlencode('United States ');  
$state = urlencode('New York');  
$zip = urlencode('13355');  
$country = urlencode('US');*/    // US or other valid country code  
$amount = urlencode('25');  
$currencyID = urlencode('USD');       // or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')  
/*--------------------*/  
  
  
$token = urlencode("");  
if($data['typeid'] == '1')
		{
		$paymentAmount = urlencode("100");  
		
			$billingPeriod = urlencode("Year");    // or "Day", "Week", "SemiMonth", "Year"  
$billingFreq = urlencode("1");      // combination of this and billingPeriod must be at most a year
		}
		else
		{
		$paymentAmount = urlencode("25"); 
		$billingPeriod = urlencode("Month");    // or "Day", "Week", "SemiMonth", "Year"  
		$billingFreq = urlencode("1");      // combination of this and billingPeriod must be at most a year
		}


$currencyID = urlencode("USD");      // or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')  
$startDate = NOW();  
  
$desc = urlencode('MemberShip ');  
$nvpStr="&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=$padDateMonth$expDateYear&FIRSTNAME=$firstName&LASTNAME=$lastName&AMT=$paymentAmount&CURRENCYCODE=$currencyID&PROFILESTARTDATE=$startDate";  
$nvpStr .= "&BILLINGPERIOD=$billingPeriod&BILLINGFREQUENCY=$billingFreq&DESC=$desc";  
   
$httpParsedResponseAr =$this->PPHttpPost('CreateRecurringPaymentsProfile', $nvpStr);  
echo "<pre>";  
if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {  
exit('CreateRecurringPaymentsProfile Completed Successfully: '.print_r($httpParsedResponseAr, true));  
} else  {  
exit('CreateRecurringPaymentsProfile failed: ' . print_r($httpParsedResponseAr, true));  
}  
echo "</pre>";  

			exit;
	// Extract the response details.
	$httpResponseAr = explode("&", $httpResponse);

	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}

	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}
		//$httpParsedResponseAr = PPHttpPost('DoDirectPayment', $nvpStr, $environment);
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
		
		/**********************************
		
		
		*******************************/
				$amount = urldecode($httpParsedResponseAr['AMT']);
			$query = "INSERT INTO `#__usermembership_types` SET `user_id` = '".$user_id."', `membership_type` = '".$data['typeid']."', `paypal_id` = '".$jpaypal_id."', `transaction_id` = '".$httpParsedResponseAr['TRANSACTIONID']."', `date` = NOW(), `amount` = '".$amount."'";
                $db->setQuery($query);
                $db->query();
				$last = $db->insertid();
				$expdate = date('Y-m-d', strtotime('+1 month'));
			$query2 = "INSERT INTO `#__user_regular_member` SET `user_id` = '".$user_id."', `mem_type_id` = '".$last."', `payment` = '".$amount."', `transaction_id` = '".$httpParsedResponseAr['TRANSACTIONID']."', `date` = NOW(), `expdate` = '".$expdate."'";
				$db->setQuery($query2);
                $db->query();
				$mail =& JFactory::getMailer();
			$from = "sunil@purplecowwebsites.com";
			$fromname = "Azzilla";
			$subject = "Recieved payment from Azzilla";
			$body = "Hi Member, \n\n\tYour Payment has been recieved.";
			$mail->setSender(array($from, $fromname));
			$mail->setSubject($subject);
			$mail->setBody($body);
	 
			// Are we sending the email as HTML?
			//if ( $mode ) {
				//	$mail->IsHTML(true);
			//}
	 
			$mail->addRecipient($recipient);
			//$mail->addCC($cc);
			//$mail->addBCC($bcc);
			//$mail->addAttachment($attachment);
	 
			// Take care of reply email addresses
			/*if( is_array( $replyto ) ) {
					$numReplyTo = count($replyto);
					for ( $i=0; $i < $numReplyTo; $i++){
							$mail->addReplyTo( array($replyto[$i], $replytoname[$i]) );
					}
			} elseif( isset( $replyto ) ) {
					$mail->addReplyTo( array( $replyto, $replytoname ) );
			}*/
	 
			 $mail->Send();
				$app->redirect(JRoute::_("index.php?option=com_users&view=login", false),"Your Regular membership is successfully completed. Please Enter username and password for login", 'notice');
			}
			else
			{
			$app->redirect(JRoute::_("index.php?option=com_users&view=registration&id=".$data['typeid'], false),"There is a problem please try again.", 'notice');
			}
		} 
	}

}