<?php
/**
 * @version     1.0.0
 * @package     com_usermembership
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Membershiptype controller class.
 */
class UsermembershipControllerMembershiptype extends UsermembershipController
{

	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @since	1.6
	 */
	public function edit()
	{
		$app = JFactory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_usermembership.edit.membershiptype.id');
		$editId	= JFactory::getApplication()->input->getInt('id', null, 'array');

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_usermembership.edit.membershiptype.id', $editId);

		// Get the model.
		$model = $this->getModel('Membershiptype', 'UsermembershipModel');

		// Check out the item
		if ($editId) {
            $model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId) {
            $model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_usermembership&view=membershiptypeform&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	 function PPHttpPost($methodName_, $nvpStr_) {  
		
		$environment = 'sandbox'; 
		$API_UserName = urlencode('simran-facilitator_api1.34interactive.com');  
		$API_Password = urlencode('1364191440');  
		$API_Signature = urlencode('AZLGlolaP4S-KgTmqfi6DzuaPuCKALydU0Lzo9TAVyFmlrhQ3W.K3Khd');  
		$API_Endpoint = "https://api-3t.paypal.com/nvp";  
   
		 if("sandbox" === $environment || "beta-sandbox" === $environment) {  
		  $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";  
		 }  
		$version = urlencode('51.0');  
  
		// setting the curl parameters.  
		$ch = curl_init();  
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);  
		curl_setopt($ch, CURLOPT_VERBOSE, 1);  

		// turning off the server and peer verification(TrustManager Concept).  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);  

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
		curl_setopt($ch, CURLOPT_POST, 1);  							
  
		// NVPRequest for submitting to server  
		$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";  
  
		// setting the nvpreq as POST FIELD to curl  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);  
  
		// getting response from server  
		$httpResponse = curl_exec($ch);  
  
		if(!$httpResponse) {  
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');  
		}  
  
		// Extract the RefundTransaction response details  
		$httpResponseAr = explode("&", $httpResponse);  
  
		$httpParsedResponseAr = array();  
		foreach ($httpResponseAr as $i => $value) {  
			$tmpAr = explode("=", $value);  
			if(sizeof($tmpAr) > 1) {  
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];  
			}  
		}  
  
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {  
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");  
		}  
  
		return $httpParsedResponseAr;  
	}  
	 
	public function save()
	{
		$app	= JFactory::getApplication();
		$model = $this->getModel('Membershiptype', 'UsermembershipModel');
		$db = JFactory::getDbo();	
		$data = JRequest::get('post');
		
		if($data['typeid'] == 3)
		{
		$jref_code = $data['jref_code'];
		$cvv2 = $data['jcard_cvv']; 
		$environment = 'sandbox';
		$paymentType =	'Sale';
		$firstName =urlencode($data['firstname']);
		$email = urlencode($data['email']);
		$userid = urlencode($data['userid']);
		$creditCardType = urlencode($data['jcard_type']);
		$creditCardNumber = urlencode($data['jcard_number']);
		$expDateMonth = $data['jcard_month'];
		$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
		$expDateYear = urlencode($data['jcard_year']);
		$cvv2Number = urlencode($cvv2);
		$country = 'US';	// US or other valid country code
		$amount = '25.00';	
		$currencyID = 'USD';// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
		
/*--------------------*/  
  
$paymentAmount = urlencode("25");  
$PROFILESTARTDATE = urlencode(date('Y-m-d').' T00:00:00Z');
$billingPeriod = urlencode("Month");    // or "Day", "Week", "SemiMonth", "Year"  
$billingFreq = urlencode("1");      // combination of this and billingPeriod must be at most a year  
$desc = urlencode('Membership');  
$nvpStr="&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=$padDateMonth$expDateYear&FIRSTNAME=$firstName&LASTNAME=$lastName&EMAIL=$email&AMT=$paymentAmount&CURRENCYCODE=$currencyID&PROFILESTARTDATE=$PROFILESTARTDATE";  
$nvpStr .= "&BILLINGPERIOD=$billingPeriod&BILLINGFREQUENCY=$billingFreq&DESC=$desc";  
  
$httpParsedResponseAr =$this->PPHttpPost('CreateRecurringPaymentsProfile', $nvpStr);  

if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {   
			$amount = urldecode($httpParsedResponseAr['AMT']);
				$expdate = date('Y-m-d', strtotime('+1 month'));
				  $profileId = urldecode($httpParsedResponseAr['PROFILEID']);
				
			  $query2 = "INSERT INTO `#__user_regular_member` SET `user_id` = '".$data['userid']."', `mem_type_id` = '".$data['mem_id']."', `payment` = '".$paymentAmount."', `transaction_id` = '".$httpParsedResponseAr['TRANSACTIONID']."',`profile_id` = '".$profileId."', `date` = NOW(), `expdate` = '".$expdate."'";
				$db->setQuery($query2);
                $db->query();
				$sel = "SELECT * from #__users where id='".$data['userid']."'";
			$db->setQuery($sel);
			$row = $db->loadAssoc();
			$recipient = $row['email'];
			$mail =& JFactory::getMailer();
			$from = "sunil@purplecowwebsites.com";
			$fromname = "Azzilla";
			$subject = "Recieved payment from Azzilla";
			$body = "Hi Member, \n\n\tYour Payment has been recieved.\n\n\nThanks,\nTeam Azzilla.";
			$mail->setSender(array($from, $fromname));
			$mail->setSubject($subject);
			$mail->setBody($body);
	 
			// Are we sending the email as HTML?
			//if ( $mode ) {
				//	$mail->IsHTML(true);
			//}
	 
			$mail->addRecipient($recipient);
			//$mail->addCC($cc);
			//$mail->addBCC($bcc);
			//$mail->addAttachment($attachment);
	 
			// Take care of reply email addresses
			/*if( is_array( $replyto ) ) {
					$numReplyTo = count($replyto);
					for ( $i=0; $i < $numReplyTo; $i++){
							$mail->addReplyTo( array($replyto[$i], $replytoname[$i]) );
					}
			} elseif( isset( $replyto ) ) {
					$mail->addReplyTo( array( $replyto, $replytoname ) );
			}*/
	 
			 $mail->Send();
				$app->redirect(JRoute::_("index.php?option=com_users&view=login", false),"Thanks for registration. Please confirm your e-mail to login.", 'notice');
		}
		
		else
		{
			$app->redirect(JRoute::_("index.php?option=com_usermembership&view=membershiptype&id=".$data['typeid']."&mem_id=".$data['mem_id']."&userid=".$data['userid'], false),"There is a problem please try again.", 'notice');
		}
		}
		
			
		// Check for errors.
		/*if ($return === false) {
			// Save the data in the session.
			$app->setUserState('com_usermembership.edit.membershiptype.data', $data);

			// Redirect back to the edit screen.
			$id = (int)$app->getUserState('com_usermembership.edit.membershiptype.id');
			$this->setMessage(JText::sprintf('Save failed', $model->getError()), 'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_usermembership&view=membershiptype&layout=edit&id='.$id, false));
			return false;
		}
		}
            
        // Check in the profile.
       /* if ($return) {
            $model->checkin($return);
        }
        
        // Clear the profile id from the session.
        $app->setUserState('com_usermembership.edit.membershiptype.id', null);

        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_USERMEMBERSHIP_ITEM_SAVED_SUCCESSFULLY'));
        $menu = & JSite::getMenu();
        $item = $menu->getActive();
        $this->setRedirect(JRoute::_($item->link, false));

		// Flush the data from the session.
		$app->setUserState('com_usermembership.edit.membershiptype.data', null);*/
	}
    
    
    function cancel() {
		$menu = & JSite::getMenu();
        $item = $menu->getActive();
        $this->setRedirect(JRoute::_($item->link, false));
    }
    
	function remove()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$app	= JFactory::getApplication();
		$model = $this->getModel('Membershiptype', 'UsermembershipModel');

		// Get the user data.
		$data = JFactory::getApplication()->input->get('jform', array(), 'array');

		// Validate the posted data.
		$form = $model->getForm();
		if (!$form) {
			JError::raiseError(500, $model->getError());
			return false;
		}

		// Validate the posted data.
		$data = $model->validate($form, $data);

		// Check for errors.
		if ($data === false) {
			// Get the validation messages.
			$errors	= $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
				if ($errors[$i] instanceof Exception) {
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				} else {
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			// Save the data in the session.
			$app->setUserState('com_usermembership.edit.membershiptype.data', $data);

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_usermembership.edit.membershiptype.id');
			$this->setRedirect(JRoute::_('index.php?option=com_usermembership&view=membershiptype&layout=edit&id='.$id, false));
			return false;
		}

		// Attempt to save the data.
		$return	= $model->delete($data);

		// Check for errors.
		if ($return === false) {
			// Save the data in the session.
			$app->setUserState('com_usermembership.edit.membershiptype.data', $data);

			// Redirect back to the edit screen.
			$id = (int)$app->getUserState('com_usermembership.edit.membershiptype.id');
			$this->setMessage(JText::sprintf('Delete failed', $model->getError()), 'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_usermembership&view=membershiptype&layout=edit&id='.$id, false));
			return false;
		}

            
        // Check in the profile.
        if ($return) {
            $model->checkin($return);
        }
        
        // Clear the profile id from the session.
        $app->setUserState('com_usermembership.edit.membershiptype.id', null);

        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_USERMEMBERSHIP_ITEM_DELETED_SUCCESSFULLY'));
        $menu = & JSite::getMenu();
        $item = $menu->getActive();
        $this->setRedirect(JRoute::_($item->link, false));

		// Flush the data from the session.
		$app->setUserState('com_usermembership.edit.membershiptype.data', null);
	}
}