<?php
/**
 * @version     1.0.0
 * @package     com_usermembership
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_usermembership', JPATH_ADMINISTRATOR);
JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
$app	= JFactory::getApplication();
$db = JFactory::getDbo();
$userid = $_GET['userid'];
$query = "select * from #__users where id='".$userid."'";
$db->setQuery($query);
$row = $db->loadAssoc();
$email = $row['email'];
$name = $row['name'];

//4683075410516684
//AZI000001
?>
<div class="registration">
<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
	</div>
<?php endif; ?>

<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_usermembership&task=membershiptype.save'); ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
<?php
if(isset($_GET['id']))
{
if($_GET['id'] == 1)
{
?>
<fieldset>
	<legend>Affiliated Member Payment</legend>
	<input type="hidden" value="<?php echo $_GET['id']; ?>" name="typeid" />
	<input type="hidden" value="<?php echo $name; ?>" name="firstname" />
	<input type="hidden" value="<?php echo $email; ?>" name="email" />
	<input type="hidden" value="<?php echo $userid; ?>" name="userid" />
	<div class="control-group">
		<div class="control-label">
			<label title="Please Enter Referral Code" class="hasTooltip required" for="jref_code" id="jref_code-lbl">Referral Code:</label>										
		</div>
		<div class="controls">
			<input type="text" size="30" value="" id="jref_code" name="jref_code">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter paypal id" class="hasTooltip required" for="jpaypal_id" id="jpaypal_id-lbl">Paypal Id:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jpaypal_id" name="jpaypal_id">
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please select credit card" class="hasTooltip required" for="jcard_type" id="jcard_type-lbl">Credit Card Type:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" class="required" id="jcard_type" name="jcard_type">
				<option value="">Select Card</option>
				<option value="Visa">Visa</option>
				<option value="MasterCard">Master Card</option>
				<option value="Amex">American Standard</option>
				<option value="Discover">Discover</option>
			</select>
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Enter credit card number" class="hasTooltip required" for="jcard_number" id="jcard_number-lbl">Credit Card Number:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_number" name="jcard_number">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter Card holder name" class="hasTooltip required" for="jcard_name" id="jcard_name-lbl">Card Holder Name:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_name" name="jcard_name">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please select expiration date" class="hasTooltip required" for="jcard_month" id="jcard_month-lbl">Exp. Date:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" style="width:60px;" class="required" id="jcard_month" name="jcard_month">	
				<?php for($i=1; $i<10; $i++) { ?>
				<option value="0<?php echo $i; ?>">0<?php echo $i; ?></option>
				<?php } 
				for($i=10; $i<13; $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
			<select required="required" style="width:75px;" class="required" id="jcard_year" name="jcard_year">	
				<?php $date = date("Y"); for($i=$date; $i<($date+50); $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php }  ?>
			</select>			
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter CVV or CVV2" class="hasTooltip required" for="jcard_cvv" id="jcard_cvv-lbl">CVV/CVV2:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_cvv" name="jcard_cvv">					
		</div>
	</div>
	
</fieldset>
<?php
}
if($_GET['id'] == 2)
{
?>
<fieldset>
	<legend>Regular Member Payment</legend>
	<input type="hidden" value="<?php echo $_GET['id']; ?>" name="typeid" />
	<input type="hidden" value="<?php echo $name; ?>" name="firstname" />
	<input type="hidden" value="<?php echo $email; ?>" name="email" />
	<input type="hidden" value="<?php echo $userid; ?>" name="userid" />
	<div class="control-group">
		<div class="control-label">
			<label title="Please select credit card" class="hasTooltip required" for="jcard_type" id="jcard_type-lbl">Credit Card Type:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" class="required" id="jcard_type" name="jcard_type">
				<option value="">Select Card</option>
				<option value="Visa">Visa</option>
				<option value="MasterCard">Master Card</option>
				<option value="Amex">American Standard</option>
				<option value="Discover">Discover</option>
			</select>
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Enter credit card number" class="hasTooltip required" for="jcard_number" id="jcard_number-lbl">Credit Card Number:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_number" name="jcard_number">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter Card holder name" class="hasTooltip required" for="jcard_name" id="jcard_name-lbl">Card Holder Name:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_name" name="jcard_name">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please select expiration date" class="hasTooltip required" for="jcard_month" id="jcard_month-lbl">Exp. Date:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" style="width:60px;" class="required" id="jcard_month" name="jcard_month">	
				<?php for($i=1; $i<10; $i++) { ?>
				<option value="0<?php echo $i; ?>">0<?php echo $i; ?></option>
				<?php } 
				for($i=10; $i<13; $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
			<select required="required" style="width:75px;" class="required" id="jcard_year" name="jcard_year">	
				<?php $date = date("Y"); for($i=$date; $i<($date+50); $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php }  ?>
			</select>			
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter CVV or CVV2" class="hasTooltip required" for="jcard_cvv" id="jcard_cvv-lbl">CVV/CVV2:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_cvv" name="jcard_cvv">					
		</div>
	</div>
	
</fieldset>
<?php
}
if($_GET['id'] == 3)
{
?>
<fieldset>
	<legend>Monthly Payment for Affiliated Member</legend>
	<input type="hidden" value="<?php echo $_GET['id']; ?>" name="typeid" />
	<input type="hidden" value="<?php echo $name; ?>" name="firstname" />
	<input type="hidden" value="<?php echo $email; ?>" name="email" />
	<input type="hidden" value="<?php echo $userid; ?>" name="userid" />
	<input type="hidden" value="<?php echo $_GET['mem_id']; ?>" name="mem_id" />
	<div class="control-group">
		<div class="control-label">
			<label title="Please select credit card" class="hasTooltip required" for="jcard_type" id="jcard_type-lbl">Credit Card Type:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" class="required" id="jcard_type" name="jcard_type">
				<option value="">Select Card</option>
				<option value="Visa">Visa</option>
				<option value="MasterCard">Master Card</option>
				<option value="Amex">American Standard</option>
				<option value="Discover">Discover</option>
			</select>
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Enter credit card number" class="hasTooltip required" for="jcard_number" id="jcard_number-lbl">Credit Card Number:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_number" name="jcard_number">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter Card holder name" class="hasTooltip required" for="jcard_name" id="jcard_name-lbl">Card Holder Name:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_name" name="jcard_name">					
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please select expiration date" class="hasTooltip required" for="jcard_month" id="jcard_month-lbl">Exp. Date:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<select required="required" style="width:60px;" class="required" id="jcard_month" name="jcard_month">	
				<?php for($i=1; $i<10; $i++) { ?>
				<option value="0<?php echo $i; ?>">0<?php echo $i; ?></option>
				<?php } 
				for($i=10; $i<13; $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
			<select required="required" style="width:75px;" class="required" id="jcard_year" name="jcard_year">	
				<?php $date = date("Y"); for($i=$date; $i<($date+50); $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php }  ?>
			</select>			
		</div>
	</div>
	
	<div class="control-group">
		<div class="control-label">
			<label title="Please enter CVV or CVV2" class="hasTooltip required" for="jcard_cvv" id="jcard_cvv-lbl">CVV/CVV2:<span class="star">&nbsp;*</span></label>
		</div>
		<div class="controls">
			<input type="text" aria-required="true" required="required" size="30" class="required" value="" id="jcard_cvv" name="jcard_cvv">					
		</div>
	</div>
	
</fieldset>
<?php
}
}
?>
<input type="submit" value="Submit">
</form>
</div>

