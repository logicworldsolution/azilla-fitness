<?php
/**
 * @version     1.0.0
 * @package     com_usermembership
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;
?>
<script type="text/javascript">
    function deleteItem(item_id){
        if(confirm("<?php echo JText::_('COM_USERMEMBERSHIP_DELETE_MESSAGE'); ?>")){
            document.getElementById('form-membershiptype-delete-' + item_id).submit();
        }
    }
</script>

<div class="items">
    <ul class="items_list membership_type">
	<li>
	<h2>Affiliated Member</h2>
	<p>Affiliated Charges : <span>$100 p.a</span></p>
	<p>Monthly Charges : <span>$25 p.m.</span></p>
	<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration&id=1'); ?>" class="getmembership">Click here to be Affiliated Member</a>
	</li>
	<li>
	<h2>Regular Member</h2>
	<p>Monthly Charges : <span>$25 p.m.</span></p>
	<p> &nbsp;</p>
	<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration&id=2'); ?>" class="getmembership">Click here to be regular Member</a>
	</li>
	</ul>
	
<?php /*$show = false; ?>
        <?php foreach ($this->items as $item) : ?>

            
				<?php
					if($item->state == 1 || ($item->state == 0 && JFactory::getUser()->authorise('core.edit.own',' com_usermembership'))):
						$show = true;
						?>
							<li>
								<a href="<?php echo JRoute::_('index.php?option=com_usermembership&view=membershiptype&id=' . (int)$item->id); ?>"><?php echo $item->ref_prefix; ?></a>
							</li>
						<?php endif; ?>

<?php endforeach; ?>
        <?php
        if (!$show):
            echo JText::_('COM_USERMEMBERSHIP_NO_ITEMS');
        endif;
        ?>
    </ul>
</div>
<?php if ($show): ?>
    <div class="pagination">
        <p class="counter">
            <?php echo $this->pagination->getPagesCounter(); ?>
        </p>
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
<?php endif; */ ?>

