<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

require(JPATH_SITE.'/components/com_questionnaire_exercise/helpers/function.php');	 

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);

$app	= JFactory::getApplication();
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');

$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_exercise/views/fitness/tmpl/css/fitness.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_exercise/views/fitness/tmpl/js/fitness.js');

$db = JFactory::getDbo();	
$loginUserId	= (int) JFactory::getUser()->get('id');

$jinput = JFactory::getApplication()->input;

$exercise_id = $jinput->get('exercise_id','','filter');
$swap_exercise_id= $jinput->get('swap_exercise_id','','filter');
$own_exercise=$jinput->get('own_exercise',0,'filter');

//Get user level.
$query="SELECT experience_lvl_working_out FROM #__questionnaire_question WHERE user_id=$loginUserId";
$db->setQuery($query);
$user_explevel=$db->loadObject()->experience_lvl_working_out;

if(isset($exercise_id) && is_numeric($exercise_id)){
	$query	= "SELECT *	FROM " . $db->quoteName( '#__questionnaire_exercise_name' ) . " WHERE id =$exercise_id ";
	$db->setQuery( $query );
	
	$user_exercise_details	= $db->loadObject();
	$routine=@json_decode($user_exercise_details->routine,'array');
	$equipments=@json_decode($user_exercise_details->equipments,'array');
}

//Check own fitness exercise already assign.
//And Check the exercise id cardiovascular exercise by id 14
$query="SELECT C.id FROM #__questionnaire_exercise_name A JOIN  #__questionnaire_exercise_category B ON A.category_id=B.id JOIN #__questionnaire_exercise_maincategory C ON  C.id=B.category WHERE A.id=$exercise_id";
$db->setQuery($query);	


$query="SELECT id as user_exercise_chk FROM #__userworkouts WHERE exercise_id=$exercise_id and own_fitness_program=$own_exercise";
$db->setQuery($query);
$user_exercise_chk=isset($db->loadObject()->user_exercise_chk)?1:NULL;

$query="SELECT DISTINCT exercise_id  FROM #__userworkouts WHERE  user_id=$loginUserId and own_fitness_program=$own_exercise";
$db->setQuery($query);
$user_exercise_ids=array();
foreach($db->loadObjectList() as $exercise_id_val){
	$user_exercise_ids[]=$exercise_id_val->exercise_id;
}

//check own fitness exercise already assign.
$query="SELECT count(id) ownprogram_cnt FROM #__userworkouts WHERE exercise_id=$exercise_id and own_fitness_program=1";
$db->setQuery($query);
$own_exercise_assign=$db->loadObject()->ownprogram_cnt;

//most popular exercises
$query	= "SELECT c.id	FROM #__questionnaire_exercise_maincategory c 
JOIN #__questionnaire_exercise_category b ON c.id=b.category 
JOIN #__questionnaire_exercise_name a ON a.category_id=b.id
WHERE a.id =$user_exercise_details->id";
$db->setQuery( $query );

if(isset($db->loadObject()->id) and !empty($db->loadObject()->id)){
	$query="SELECT a.id,a.video1,a.video2,a.video3,a.name ,
	(SELECT count(aa.id) occurance from #__userworkouts aa WHERE  a.id=aa.exercise_id) occurance 
	FROM #__questionnaire_exercise_name a 
	JOIN #__questionnaire_exercise_category b ON  a.category_id=b.id
	JOIN #__questionnaire_exercise_maincategory c ON b.category=c.id 
	where c.id={$db->loadObject()->id} order by occurance desc";

	$db->setQuery( $query );
	$popularExercises_list=$db->loadObjectList();
}
?>
<h2 class=" page_head">My Fitness Library
  <?php //echo $user_exercise_details->id; ?>
</h2>
<div class="mini-layout fluid height_auto" id="hidden_element">
  <div class="mini-layout-body fitness_progrm2">
    <div class="navbar azilla_hidden">
      <div class="nav-collapse collapse">
        <ul class="nav azilla_nav azilla_nav_fitness bgcolor_53c4ca">
          <li><a href="#directions" data-lightbox='on' >Directions</a></li>
          <li><a href="#purpose" data-lightbox='on' >Purpose </a></li>
          <li><a href="#warmup" data-lightbox='on' >Warm-up </a></li>
          <li><a href="#recovery" data-lightbox='on' >Recovery</a></li>
          <li class="last_nav"><a href="#tips" data-lightbox='on' >Tips</a></li>
        </ul>
      </div>
    </div>
    <section class="azilla02_top">
      <div class="video_gallery azilla_video">
        <div id="exercisevideo"> <?php echo @$user_exercise_details->video1; ?> </div>
        <ul class="azilla_video_nav">
          <li class="bgcolor_1a8bad border_bottom_0a416a"> <a href="javascript:;" video_embed_code='<?php echo @$user_exercise_details->video1; ?>' class="youtube_video_lkn" > 1</a></li>
          <li class="bgcolor_bfda73 border_bottom_0a416a"> <a href="javascript:;" video_embed_code='<?php echo @$user_exercise_details->video2; ?>' class="youtube_video_lkn" > 2</a></li>
          <li class="bgcolor_1da4a8"> <a href="javascript:;" video_embed_code='<?php echo @$user_exercise_details->video3; ?>' class="youtube_video_lkn" > 3</a></li>
        </ul>
      </div>
    </section>
    <?php 
	if(!empty($user_exercise_details->progression)):
		$progression_exercise_id=wherein_remove_lastcomma($user_exercise_details->progression);
		$nuber_of_items= count(@explode(',',$progression_exercise_id));

		$query	= "SELECT video1 FROM " . $db->quoteName( '#__questionnaire_exercise_name' ) . " WHERE id IN ($progression_exercise_id)";
		$db->setQuery( $query );
		$items=$db->loadObjectList();
		if(count($items)>0):
	?>
    <section class="azilla_02_bottom">
      <h3 class="progression_modification"> Progressions and Modifications </h3>
      <div id="myCarousel" class="carousel slide progression_modification_container">
        <div class="carousel-inner">
          <?php 
				$videocontainer=0;
				$active_video='active';
				foreach($db->loadObjectList() as $progressionVal){
					if($videocontainer%3==0){
						echo '<div class="'.$active_video.' item">';	$active_video='';
						echo '<div class="row-fluid show-grid span4_az_m marginbot0">';
					}
						echo '<div class="span4 fit_progrm1 video_bottom_img span4_width"><a href="javascript:;" video_embed_code=\''.$progressionVal->video1.'\' class="youtube_video_lkn" >'.getYoutube_embed_code($progressionVal->video1).'</a></div>';
						$videocontainer++;
					if($videocontainer%3==0 or $videocontainer==$nuber_of_items){	
						echo '</div>';
						echo '</div>';
					}
				 }; 
			?>
        </div>
        <?php if($nuber_of_items>3): ?>
        <a class="carousel-control left progression_modification_left" href="#myCarousel" data-slide="prev">&lsaquo;</a> <a class="carousel-control right progression_modification_right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        <?php endif; ?>
      </div>
    </section>
    <?php endif; endif; ?>
    <section class="azilla_02_bottom bottom_nav_back bottom_nav_back_02" >
      <table class="meal_chart_table" >
        <tr>
          <th class="borderleft_53C4CA">sets</th>
          <th>reps</th>
          <th>weight</th>
          <th>tempo</th>
          <th class="borderright-none">T.B.S.</th>
        </tr>
        <tr class="borderccc">
          <td><?php echo @$routine[$user_explevel]['sets']; ?></td>
          <td><?php echo @$routine[$user_explevel]['reps']; ?></td>
          <td><?php echo @$routine[$user_explevel]['weight']; ?></td>
          <td><?php echo @$routine[$user_explevel]['tempo']; ?></td>
          <td class="border-none"><?php echo @$routine[$user_explevel]['tbs']; ?></td>
        </tr>
      </table>
    </section>
    <section class="azilla_02_bottom bottom_nav_back bottom_nav_back_02" >
      <table class="meal_chart_table" >
        <tr>
          <th class="borderleft_53C4CA">T.U.T.</th>
          <th>time</th>
          <th>Intensity</th>
          <th>T.h.r.</th>
          <th class="borderright-none">Distance</th>
        </tr>
        <tr class="borderccc">
          <td><?php echo @$routine[$user_explevel]['tut']; ?></td>
          <td><?php echo @$routine[$user_explevel]['time']; ?></td>
          <td><?php echo @$routine[$user_explevel]['intensity']; ?></td>
          <td><?php echo @$routine[$user_explevel]['thr']; ?></td>
          <td class="border-none"><?php echo @$routine[$user_explevel]['distance']; ?></td>
        </tr>
      </table>
    </section>
  </div>
  <div class="mini-layout-sidebar fitness_progrm2_aside">
  	<?php if($own_exercise==1 and $own_exercise_assign==0 and empty($swap_exercise_id)): ?>
    <h4 class="aside_title_swap add_exercise"><a href="<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&task=questionform.add_own_exercise&exercise_id=$exercise_id"); ?>" > Add this exercise </a></h4>
  	<?php endif; 
		 if($user_exercise_chk!=NULL and !in_array($swap_exercise_id,$user_exercise_ids) and empty($swap_exercise_id)): ?>
     <h4 class="aside_title_swap"><a href="#swap_exercise" data-lightbox='on' > Swap this exercise </a></h4>
    <?php elseif(!empty($swap_exercise_id) and !in_array($exercise_id,$user_exercise_ids)): ?>
     <h4 class="aside_title_swap"><a href="<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&task=questionform.swap_exercise&exercise_id=$exercise_id&swap_exercise_id=$swap_exercise_id&own_exercise=$own_exercise"); ?>" > Swap out </a></h4>
    <?php endif; ?>
    <section class="aside_listing">
      <h4 class="aside_title"> Equipment </h4>
      <ul>
        <?php
		if(!empty($equipments)){
			$equipment_ids=@implode(',',$equipments[$user_explevel]);
			if(!empty($equipment_ids)){
				$query	= "SELECT name FROM #__questionnaire_equipment WHERE id IN($equipment_ids)";
				$db->setQuery( $query );
				$class_alt=1;
				foreach($db->loadObjectList() as $equipment_listval){
					$alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
					echo "<li class='$alter_class'>".$equipment_listval->name.'</li>';
				}
			}
		 }
		?>
      </ul>
    </section>
    <section class="aside_listing">
      <h4 class="aside_title"> Primary Muscles </h4>
      <ul>
        <?php
		if(!empty($user_exercise_details->primary_muscles)){
			$primary_muscles=wherein_remove_lastcomma($user_exercise_details->primary_muscles);
			$query	= "SELECT name FROM #__questionnaire_exercise_muscles WHERE category=1 and id IN($primary_muscles)";
			$db->setQuery( $query );
			$class_alt=1;
			foreach($db->loadObjectList() as $primary_muscles_list_objval){
				$alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
				echo "<li class='$alter_class'>".$primary_muscles_list_objval->name.'</li>';
			  }
		}	  
		?>
      </ul>
    </section>
    <section class="aside_listing">
      <h4 class="aside_title"> Secondary Muscles </h4>
      <ul>
        <?php
		if(!empty($user_exercise_details->secondary_muscles)){
			$primary_muscles=wherein_remove_lastcomma($user_exercise_details->secondary_muscles);
			$query	= "SELECT name FROM #__questionnaire_exercise_muscles WHERE category=2 and id IN($primary_muscles)";
			$db->setQuery( $query );
			$class_alt=1;
			foreach($db->loadObjectList() as $secondary_muscles_list_objval){
				 $alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
				 echo "<li class='$alter_class'>".$secondary_muscles_list_objval->name.'</li>';
			}
		 }
		?>
      </ul>
    </section>
    <section class="aside_listing aside_bottom">
      <h4 class="aside_title"> My Fitness Program </h4>
      <ul  class="myfitnessprogram_minheight">
        <?php 
		if($own_exercise==1){		//List My Own Fitness Program
			$query	= "SELECT A.id,A.name,B.exercise_type FROM " . $db->quoteName( '#__questionnaire_exercise_name' ) . " A JOIN " . $db->quoteName( '#__userworkouts' ) ." B ON A.id=B.exercise_id  WHERE B.user_id = $loginUserId AND B.own_fitness_program=1  ORDER BY A.name ASC";
			$db->setQuery( $query );

			foreach($db->loadObjectList() as $user_workout_exercisesval){
				$alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
				$userexercise_id=$user_workout_exercisesval->id;
				echo "<li class='$alter_class'><a href='".jRoute::_("index.php?option=com_questionnaire_exercise&view=fitness&Itemid=247&exercise_id=$userexercise_id&own_exercise=1")."'>".$user_workout_exercisesval->name.'</a></li>';
			}	
		}else{		//List My Fitness Program
			$fitness_date=date('Y-m-d',strtotime("now"));
			$own_exerciselnk='';
			$query	= "SELECT A.id,A.name,B.exercise_type FROM " . $db->quoteName( '#__questionnaire_exercise_name' ) . " A JOIN " . $db->quoteName( '#__userworkouts' ) ." B ON A.id=B.exercise_id JOIN #__userworkouts_level C ON B.userworkouts_level_id=C.id WHERE C.user_id = $loginUserId AND B.own_fitness_program=0  AND C.start_date='$fitness_date' ORDER BY A.name ASC";
			$db->setQuery( $query );		
			
			foreach($db->loadObjectList() as $user_workout_exercisesval){
				$alter_class=($class_alt%2==0)?"colone":"coltwo";$class_alt++;
				$userexercise_id=$user_workout_exercisesval->id;
				echo "<li class='$alter_class'><a href='".jRoute::_("index.php?option=com_questionnaire_exercise&view=fitness&Itemid=247&exercise_id=$userexercise_id")."'>".$user_workout_exercisesval->name.'</a></li>';
			}
		}		
		?>
      </ul>
    </section>
  </div>
  <section class="most_popular">
    <h3 class="progression_modification"> Most Popular </h3>
    <div id="myCarousel2" class="carousel slide progression_modification_container">
      <div class="carousel-inner">
        <?php 
$popupal_exercise_count=count($popularExercises_list);
$videocontainer=0;
$active_video='active';
foreach($popularExercises_list as $popularExercises_listVal):
if($videocontainer%2==0){
echo '<div class="'.$active_video.' item"><div class="row-fluid show-grid">';	$active_video='';
}
 ?>
        <div class='fit_progrm1 youtube_img_container'>
          <div class="youtube_img_container_inner">
            <h4 class='popular_video_title'><?php echo $popularExercises_listVal->name; ?></h4>
            <div class="popular_videos"> <?php echo getYoutube_embed_code($popularExercises_listVal->video1); ?> </div>
            <ul class="popular_videos_nav">
              <li class="bgcolor_1a8bad border_bottom_0a416a_1px"> <a href="javascript:;" video_embed_code='<?php echo getYoutube_embed_code_path($popularExercises_listVal->video1); ?>' class="youtube_img_lkn" > 1</a></li>
              <li class="bgcolor_bfda73 border_bottom_0a416a_1px"> <a href="javascript:;" video_embed_code='<?php echo getYoutube_embed_code_path($popularExercises_listVal->video2); ?>' class="youtube_img_lkn" > 2</a></li>
              <li class="bgcolor_1da4a8"> <a href="javascript:;" video_embed_code='<?php echo getYoutube_embed_code_path($popularExercises_listVal->video3); ?>' class="youtube_img_lkn" > 3</a></li>
            </ul>
          </div>
        </div>
        <?php 
$videocontainer++;
if($videocontainer%2==0 or $videocontainer==$popupal_exercise_count){	
	echo '</div></div>';
}

endforeach; ?>
      </div>
      <?php if($popupal_exercise_count>2): ?>
      <a class="carousel-control left most_popular_left" href="#myCarousel2" data-slide="prev">&lsaquo;</a>
      <a class="carousel-control right most_popular_right" href="#myCarousel2" data-slide="next">&rsaquo;</a> 
      <?php endif; ?>
      </div>
  </section>
</div>
<div class="hide">
  <div id="swap_exercise" style="width:400px; height:80px">
    <h4 class="center">Are you sure you want to change your fitness program?</h4>
    <div class="center">
      <table align="center">
        <tr>
          <td><button class="validate button btn btn-large submit_method" onclick="window.location.href='<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&view=library&Itemid=248&swap_exercise_id=$exercise_id&own_exercise=$own_exercise"); ?>'" ><span>Yes</span></button></td>
          <td>&nbsp;</td>
          <td><button class="validate button btn btn-large submit_method" onclick="jQuery('#lightbox-close').trigger('click')" ><span>No</span></button></td>
        </tr>
      </table>
    </div>
  </div>
  <div id="directions" style="width:400px; min-height:80px">
    <div class="center">
      <?php echo $user_exercise_details->directions; ?> 
    </div>
  </div>
  <div id="purpose" style="width:400px; min-height:80px">
    <div class="center">
      <?php echo $user_exercise_details->purpose; ?> 
    </div>
  </div> 
  <div id="warmup" style="width:400px; min-height:80px">
    <div class="center">
      <?php echo $user_exercise_details->warmup; ?> 
    </div>
  </div>
  <div id="recovery" style="width:400px; min-height:80px">
    <div class="center">
      <?php echo $user_exercise_details->recovery; ?> 
    </div>
  </div>
  <div id="tips" style="width:400px; min-height:80px">
    <div class="center">
      <?php echo $user_exercise_details->tips; ?> 
    </div>
  </div>
</div>
