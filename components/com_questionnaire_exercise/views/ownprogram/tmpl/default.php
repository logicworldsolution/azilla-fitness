<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

require(JPATH_SITE.'/components/com_questionnaire_exercise/helpers/function.php');	 

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);

$app	= JFactory::getApplication();
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');

$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_exercise/views/program/tmpl/css/default.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire_exercise/views/program/tmpl/js/default.js');
$db = JFactory::getDbo();	
$loginUserId	= (int) JFactory::getUser()->get('id');

//Get user experiance
$query	= 'SELECT birthday,overallgoal,experience_lvl_working_out FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '
. "WHERE id= $loginUserId";

$db->setQuery( $query );
$user_explevelObj	= $db->loadObject();
$user_explevel=$user_explevelObj->experience_lvl_working_out;

$query	= "SELECT A.id,A.name,A.routine,A.equipments,IF(B.notes='',A.notes,B.notes) as notes,B.id as w_id,B.exercise_type FROM " . $db->quoteName( '#__questionnaire_exercise_name' ) . " A JOIN " . $db->quoteName( '#__userworkouts' ) ." B ON A.id=B.exercise_id  WHERE B.user_id = $loginUserId AND B.own_fitness_program=1  ORDER BY A.name ASC";
$db->setQuery( $query );
$user_exercise_list=$db->loadObjectList();
?>
<h2 class="page_head padbot0" style="float:left">Create My Own Fitness Program</h2>
<a class="add_exercise" href="<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&view=library&Itemid=248&own_exercise=1"); ?>" > <button type="button" class="btn btn-success modify_button" >Add an exercise</button>  </a>
<div class="myworkout create_workout" >
<table border="1" width="100%" cellpadding="0" cellspacing="0">
  	<tr>
    	<td>
        	<div class="accordion bottom_border_bebe miles_snakes_accordion" >
                <div class="accordion-group acc_group_marginnone">
                  <div class="accordion-heading acc_top_border"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent=".miles_snakes_accordion" href="#collapseFormRoll"><span class="plus_icon"> Foam Roll</span> <span class="up_click"></span>  </a> </div>
                  <div id="collapseFormRoll" class="accordion-body collapse">
                    <div class="accordion-inner bakround_none">
                    <table border="0" class="group_exercise_inner">
          <tbody><tr>
            <th class="exercise_name">CHEST</th>
            <th>SETS</th>
            <th>REPS.</th>
            <th>WEIGHT</th>
            <th>TEMPO.</th>
            <th>T.B.S.</th>
            <th>T.U.T.</th>
          </tr>
          <?php
		  $i=0;
		  $w=0;
		  $workout_notes=array();
		  foreach($user_exercise_list as $user_exercise_listVal):
		  	if($user_exercise_listVal->exercise_type==1):				
				$exercise_id=$user_exercise_listVal->id;
				$routine=@json_decode($user_exercise_listVal->routine,'array');
				
				$workout_notes[$w]['name']=$user_exercise_listVal->name;
				$workout_notes[$w]['notes']=$user_exercise_listVal->notes;
				$workout_notes[$w]['w_id']=$user_exercise_listVal->w_id;									
				$w++;				
		   ?>
              <tr class="<?php echo ($i==0)?'':'odd_tr';  ?>">
                <td class="exercise_list"><a href="<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&view=fitness&Itemid=247&exercise_id=$exercise_id&own_exercise=1"); ?>" ><?php echo $user_exercise_listVal->name; ?></a></td>
                <td><?php echo @$routine[$user_explevel]['sets']; ?></td>
                <td><?php echo @$routine[$user_explevel]['reps']; ?></td>
                <td><?php echo @$routine[$user_explevel]['weight']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tempo']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tbs']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tut']; ?></td>
              </tr>
            <?php if($i==0){$i=1;}else{$i=0;} endif; endforeach; ?>
        </tbody></table>
                    </div>
                  </div>
                </div>
           </div>   
        </td>
    </tr>
    <tr>
    	<td>
           <div class='hide'>
               <div id='foam_roll_notes' class="notes_style">
               <?php foreach($workout_notes as $workout_notesVal){
                    echo "<strong>{$workout_notesVal['name']}<a class='editNotes clk' w_id='".$workout_notesVal['w_id']."' >Edit Note</a></strong><p>{$workout_notesVal['notes']}</p>";
               }?>
               </div>
           </div>
           <a href="#foam_roll_notes" data-lightbox='on' ><button type="button" class="btn btn-success fitness_note" >NOTES</button></a>
       </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
    	<td>
        	<div class="accordion bottom_border_bebe miles_snakes_accordion" >
                <div class="accordion-group acc_group_marginnone">
                  <div class="accordion-heading acc_top_border"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent=".miles_snakes_accordion" href="#collapseStretch"><span class="plus_icon"> Stretch</span> <span class="up_click"></span>  </a> </div>
                  <div id="collapseStretch" class="accordion-body collapse">
                    <div class="accordion-inner bakround_none">
                    <table border="0" class="group_exercise_inner">
          <tbody><tr>
            <th class="exercise_name">CHEST</th>
            <th>SETS</th>
            <th>REPS.</th>
            <th>WEIGHT</th>
            <th>TEMPO.</th>
            <th>T.B.S.</th>
            <th>T.U.T.</th>
          </tr>
           <?php
		  $i=0;
  		  $w=0;
		  $workout_notes=array();
		  foreach($user_exercise_list as $user_exercise_listVal):
		  	if($user_exercise_listVal->exercise_type==2):				
				$exercise_id=$user_exercise_listVal->id;
				$routine=@json_decode($user_exercise_listVal->routine,'array');
				
				$workout_notes[$w]['name']=$user_exercise_listVal->name;
				$workout_notes[$w]['notes']=$user_exercise_listVal->notes;
				$workout_notes[$w]['w_id']=$user_exercise_listVal->w_id;					
				$w++;				
		   ?>
              <tr class="<?php echo ($i==0)?'':'odd_tr';  ?>">
                <td class="exercise_list"><a href="<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&view=fitness&Itemid=247&exercise_id=$exercise_id&own_exercise=1"); ?>" ><?php echo $user_exercise_listVal->name; ?></a></td>
                <td><?php echo @$routine[$user_explevel]['sets']; ?></td>
                <td><?php echo @$routine[$user_explevel]['reps']; ?></td>
                <td><?php echo @$routine[$user_explevel]['weight']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tempo']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tbs']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tut']; ?></td>
              </tr>
            <?php if($i==0){$i=1;}else{$i=0;} endif; endforeach; ?>                    
        </tbody></table>
                    </div>
                  </div>
                </div>
           </div>   
        </td>
    </tr>
    <tr>
    	<td>
           <div class='hide'>
               <div id='stretch_notes' class="notes_style">
               <?php foreach($workout_notes as $workout_notesVal){
                    echo "<strong>{$workout_notesVal['name']}<a class='editNotes clk' w_id='".$workout_notesVal['w_id']."' >Edit Note</a></strong><p>{$workout_notesVal['notes']}</p>";
               }?>
               </div>
           </div>
           <a href="#stretch_notes" data-lightbox='on' ><button type="button" class="btn btn-success fitness_note" >NOTES</button></a>
        </td>
    </tr>
    <tr>
    	<td>&nbsp; </td>
    </tr>
    <tr>
    	<td>
        	<div class="accordion bottom_border_bebe miles_snakes_accordion" >
                <div class="accordion-group acc_group_marginnone">
                  <div class="accordion-heading acc_top_border"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent=".miles_snakes_accordion" href="#collapseWarmUp"><span class="plus_icon"> Warm Up Exercises</span> <span class="up_click"></span>  </a> </div>
                  <div id="collapseWarmUp" class="accordion-body collapse">
                    <div class="accordion-inner bakround_none"> 
                    <table border="0" class="group_exercise_inner">
                      <tbody><tr>
                        <th class="exercise_name">CHEST</th>
                        <th>SETS</th>
                        <th>REPS.</th>
                        <th>WEIGHT</th>
                        <th>TEMPO.</th>
                        <th>T.B.S.</th>
                        <th>T.U.T.</th>
                      </tr>
                       <?php
                      $i=0;
					  $w=0;
					  $workout_notes=array();					  
                      foreach($user_exercise_list as $user_exercise_listVal):
                        if($user_exercise_listVal->exercise_type==3):				
                            $exercise_id=$user_exercise_listVal->id;
                            $routine=@json_decode($user_exercise_listVal->routine,'array');
										
							$workout_notes[$w]['name']=$user_exercise_listVal->name;
							$workout_notes[$w]['notes']=$user_exercise_listVal->notes;
							$workout_notes[$w]['w_id']=$user_exercise_listVal->w_id;					
							$w++;							
                       ?>
                          <tr class="<?php echo ($i==0)?'':'odd_tr';  ?>">
                            <td class="exercise_list"><a href="<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&view=fitness&Itemid=247&exercise_id=$exercise_id&own_exercise=1"); ?>" ><?php echo $user_exercise_listVal->name; ?></a></td>
                            <td><?php echo @$routine[$user_explevel]['sets']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['reps']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['weight']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['tempo']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['tbs']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['tut']; ?></td>
                          </tr>
                        <?php if($i==0){$i=1;}else{$i=0;} endif; endforeach; ?>                    
                    </tbody></table>
                    
                    </div>
                  </div>
                </div>
           </div>   
        </td>
    </tr>
    <tr>
    	<td>
           <div class='hide'>
               <div id='warm_up_notes' class="notes_style">
               <?php foreach($workout_notes as $workout_notesVal){
                   echo "<strong>{$workout_notesVal['name']}<a class='editNotes clk' w_id='".$workout_notesVal['w_id']."' >Edit Note</a></strong><p>{$workout_notesVal['notes']}</p>";
               }?>
               </div>
           </div>
           <a href="#warm_up_notes" data-lightbox='on' ><button type="button" class="btn btn-success fitness_note" >NOTES</button></a>
        </td>
    </tr>
    <tr>
    	<td>&nbsp; </td>
    </tr>
      	<tr>
    	<td>
        	<div class="accordion bottom_border_bebe miles_snakes_accordion" >
                <div class="accordion-group acc_group_marginnone">
                  <div class="accordion-heading acc_top_border"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent=".miles_snakes_accordion" href="#collapseCardiovascular"><span class="plus_icon"> Cardiovascular Exercise</span> <span class="up_click"></span>  </a> </div>
                  <div id="collapseCardiovascular" class="accordion-body collapse">
                    <div class="accordion-inner bakround_none">
                    <table border="0" class="group_exercise_inner">
                      <tbody><tr>
                        <th class="exercise_name">CHEST</th>
                        <th>SETS</th>
                        <th>REPS.</th>
                        <th>WEIGHT</th>
                        <th>TEMPO.</th>
                        <th>T.B.S.</th>
                        <th>T.U.T.</th>
                      </tr>
                       <?php
                      $i=0;
					  $w=0;
					  $workout_notes=array();
                      foreach($user_exercise_list as $user_exercise_listVal):
                        if($user_exercise_listVal->exercise_type==5):				
                            $exercise_id=$user_exercise_listVal->id;
                            $routine=@json_decode($user_exercise_listVal->routine,'array');
										
							$workout_notes[$w]['name']=$user_exercise_listVal->name;
							$workout_notes[$w]['notes']=$user_exercise_listVal->notes;
							$workout_notes[$w]['w_id']=$user_exercise_listVal->w_id;					
							$w++;							
                       ?>
                          <tr class="<?php echo ($i==0)?'':'odd_tr';  ?>">
                            <td class="exercise_list"><a href="<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&view=fitness&Itemid=247&exercise_id=$exercise_id&own_exercise=1"); ?>" ><?php echo $user_exercise_listVal->name; ?></a></td>
                            <td><?php echo @$routine[$user_explevel]['sets']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['reps']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['weight']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['tempo']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['tbs']; ?></td>
                            <td><?php echo @$routine[$user_explevel]['tut']; ?></td>
                          </tr>
                        <?php if($i==0){$i=1;}else{$i=0;} endif; endforeach; ?>                    
                    </tbody></table>                    
                    </div>
                  </div>
                </div>
           </div>   
        </td>
    </tr>
    <tr>
    	<td>
	       <div class='hide'>
               <div id='cardiovascular_notes' class="notes_style">
               <?php foreach($workout_notes as $workout_notesVal){
                   echo "<strong>{$workout_notesVal['name']}<a class='editNotes clk' w_id='".$workout_notesVal['w_id']."' >Edit Note</a></strong><p>{$workout_notesVal['notes']}</p>";
               }?>
               </div>
           </div>
           <a href="#cardiovascular_notes" data-lightbox='on' ><button type="button" class="btn btn-success fitness_note" >NOTES</button></a>
        </td>
    </tr>
    <tr>
    	<td>&nbsp; </td>
    </tr>
      	<tr>
    	<td>
        	<div class="accordion bottom_border_bebe miles_snakes_accordion" >
                <div class="accordion-group acc_group_marginnone">
                  <div class="accordion-heading acc_top_border"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent=".miles_snakes_accordion" href="#collapsePrimaryWorkout"><span class="plus_icon"> Primary Workout</span> <span class="up_click"></span>  </a> </div>
                  <div id="collapsePrimaryWorkout" class="accordion-body collapse">
                    <div class="accordion-inner bakround_none">
                    <table border="0" class="group_exercise_inner">
          <tbody><tr>
            <th class="exercise_name">CHEST</th>
            <th>SETS</th>
            <th>REPS.</th>
            <th>WEIGHT</th>
            <th>TEMPO.</th>
            <th>T.B.S.</th>
            <th>T.U.T.</th>
          </tr>
           <?php
		  $i=0;
   		  $w=0;
		  $workout_notes=array();
		  foreach($user_exercise_list as $user_exercise_listVal):
		  	if($user_exercise_listVal->exercise_type==6):				
				$exercise_id=$user_exercise_listVal->id;
				$routine=@json_decode($user_exercise_listVal->routine,'array');
				
				$workout_notes[$w]['name']=$user_exercise_listVal->name;
				$workout_notes[$w]['notes']=$user_exercise_listVal->notes;
				$workout_notes[$w]['w_id']=$user_exercise_listVal->w_id;					
				$w++;
		   ?>
              <tr class="<?php echo ($i==0)?'':'odd_tr';  ?>">
                <td class="exercise_list"><a href="<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&view=fitness&Itemid=247&exercise_id=$exercise_id&own_exercise=1"); ?>" ><?php echo $user_exercise_listVal->name; ?></a></td>
                <td><?php echo @$routine[$user_explevel]['sets']; ?></td>
                <td><?php echo @$routine[$user_explevel]['reps']; ?></td>
                <td><?php echo @$routine[$user_explevel]['weight']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tempo']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tbs']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tut']; ?></td>
              </tr>
            <?php if($i==0){$i=1;}else{$i=0;} endif; endforeach; ?>                    
        </tbody></table>
                    
                    </div>
                  </div>
                </div>
           </div>   
        </td>
    </tr>
    <tr>
     	<td>
           <div class='hide'>
               <div id='primary_notes' class="notes_style">
               <?php foreach($workout_notes as $workout_notesVal){
                   echo "<strong>{$workout_notesVal['name']}<a class='editNotes clk' w_id='".$workout_notesVal['w_id']."' >Edit Note</a></strong><p>{$workout_notesVal['notes']}</p>";
               }?>
               </div>
           </div>
           <a href="#primary_notes" data-lightbox='on' ><button type="button" class="btn btn-success fitness_note" >NOTES</button></a>
        </td>
    </tr>
    <tr>
    	<td>&nbsp; </td>
    </tr>
      	<tr>
    	<td>
        	<div class="accordion bottom_border_bebe miles_snakes_accordion" >
                <div class="accordion-group acc_group_marginnone">
                  <div class="accordion-heading acc_top_border"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent=".miles_snakes_accordion" href="#collapseCoolDown"><span class="plus_icon"> Cool Down Exercises</span> <span class="up_click"></span>  </a> </div>
                  <div id="collapseCoolDown" class="accordion-body collapse">
                    <div class="accordion-inner bakround_none">
                    <table border="0" class="group_exercise_inner">
          <tbody><tr>
            <th class="exercise_name">CHEST</th>
            <th>SETS</th>
            <th>REPS.</th>
            <th>WEIGHT</th>
            <th>TEMPO.</th>
            <th>T.B.S.</th>
            <th>T.U.T.</th>
          </tr>
           <?php
		  $i=0;
   		  $w=0;
		  $workout_notes=array();
		  foreach($user_exercise_list as $user_exercise_listVal):
		  	if($user_exercise_listVal->exercise_type==4):				
				$exercise_id=$user_exercise_listVal->id;
				$routine=@json_decode($user_exercise_listVal->routine,'array');
				
				$workout_notes[$w]['name']=$user_exercise_listVal->name;
				$workout_notes[$w]['notes']=$user_exercise_listVal->notes;
				$workout_notes[$w]['w_id']=$user_exercise_listVal->w_id;					
				$w++;				
		   ?>
              <tr class="<?php echo ($i==0)?'':'odd_tr';  ?>">
                <td class="exercise_list"><a href="<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&view=fitness&Itemid=247&exercise_id=$exercise_id&own_exercise=1"); ?>" ><?php echo $user_exercise_listVal->name; ?></a></td>
                <td><?php echo @$routine[$user_explevel]['sets']; ?></td>
                <td><?php echo @$routine[$user_explevel]['reps']; ?></td>
                <td><?php echo @$routine[$user_explevel]['weight']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tempo']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tbs']; ?></td>
                <td><?php echo @$routine[$user_explevel]['tut']; ?></td>
              </tr>
            <?php if($i==0){$i=1;}else{$i=0;} endif; endforeach; ?>                    
        </tbody></table>
                    
                    </div>
                  </div>
                </div>
           </div>   
        </td>
    </tr>
    <tr>
    	<td>
		   <div class='hide'>
               <div id='cool_down_notes' class="notes_style">
               <?php foreach($workout_notes as $workout_notesVal){
                    echo "<strong>{$workout_notesVal['name']}<a class='editNotes clk' w_id='".$workout_notesVal['w_id']."' >Edit Note</a></strong><p>{$workout_notesVal['notes']}</p>";
               }?>
               </div>
           </div>
           <a href="#cool_down_notes" data-lightbox='on' ><button type="button" class="btn btn-success fitness_note" >NOTES</button></a>
        </td>
    </tr>
</table>		        
</div>