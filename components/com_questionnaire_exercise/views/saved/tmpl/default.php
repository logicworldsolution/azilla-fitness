<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);

$app	= JFactory::getApplication();
$doc = JFactory::getDocument();
JHtml::_('bootstrap.framework');

$db = JFactory::getDbo();	
$loginUserId	= (int) JFactory::getUser()->get('id');

$jinput = JFactory::getApplication()->input;

//$exercise_id = $jinput->get('exercise_id','','filter');
?>
<h2 class="page_head">Saved Fitness Programs</h2>
<div class="mini-layout fluid height_auto" id="hidden_element">

</div>