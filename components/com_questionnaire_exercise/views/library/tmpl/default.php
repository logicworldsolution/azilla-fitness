<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);

$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire_exercise/views/library/tmpl/css/library.css', $type = 'text/css');

$db = JFactory::getDbo();	

$loginuser	= JFactory::getUser();
$loginUserId	= (int) $loginuser->get('id');

$jinput = JFactory::getApplication()->input;
$open_fid = $jinput->get('open_fid', '', 'filter');
$swap_exercise_id= $jinput->get('swap_exercise_id', '', 'filter');
$own_exercise= $jinput->get('own_exercise',0, 'filter');

if(!empty($swap_exercise_id)){
	$swap_exercise_id="&swap_exercise_id=$swap_exercise_id";
}

$query="SELECT id,name FROM #__questionnaire_exercise_maincategory WHERE state=1 ORDER BY name ASC";
$db->setQuery($query);
?>
<h2 class="page_head">My Fitness Library</h2>
<div id="library_container">
  <div class="accordion" id="accordionMain">
<?php  foreach($db->loadObjectList() as $main_category): ?>
    <div class="accordion-group">
      <div class="accordion-heading library_main"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionMain" href="#collapsemain<?php echo $main_category->id;  ?>"> <?php echo $main_category->name; ?> </a> </div>
      <div id="collapsemain<?php echo $main_category->id;  ?>" class="accordion-body <?php if($main_category->id==$open_fid) echo 'in';  ?> collapse">
        <div class="accordion-inner">
          <div class="accordion" id="accordionSub<?php echo $main_category->id; ?>">
<?php $query="SELECT id,name FROM #__questionnaire_exercise_category WHERE state=1 and category=$main_category->id ORDER BY name ASC";
	  $db->setQuery($query);
	  foreach($db->loadObjectList() as $main_sub_category):?>		  
            <div class="accordion-group">
              <div class="accordion-heading librarie_subcat"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionSub<?php echo $main_category->id; ?>" href="#accordionSubcat<?php echo $main_sub_category->id; ?>"> <?php echo $main_sub_category->name  ?> </a> </div>
              <div id="accordionSubcat<?php echo $main_sub_category->id; ?>" class="accordion-body collapse">
                <?php
				$query="SELECT id,name FROM #__questionnaire_exercise_name WHERE state=1 and category_id=$main_sub_category->id ORDER BY name ASC";
				$db->setQuery($query);
				$childItems=count($db->loadObjectList());
				if($childItems==0){
						echo "<div  class='accordion-inner'></div>";
				}else{
				foreach($db->loadObjectList() as $exercise_obj):
				$exercise_id=$exercise_obj->id;	?>
				<div class='accordion-inner exercise_names_container'>
                	<div class='exercise_names'>
                    	<a href='<?php echo jRoute::_("index.php?option=com_questionnaire_exercise&view=fitness&Itemid=247&exercise_id=$exercise_id$swap_exercise_id&own_exercise=$own_exercise"); ?>'><?php echo $exercise_obj->name; ?></a>
                    </div>
                </div>
			  <?php	endforeach; } ?>   
              </div>
            </div>            
<?php endforeach;  ?>
		  </div>
        </div>
      </div>
    </div>
<?php  endforeach; ?>
  </div>
</div>