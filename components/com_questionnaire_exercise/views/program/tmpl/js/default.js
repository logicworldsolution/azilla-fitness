jQuery().ready(function($){		
		$('.miles_snakes_accordion .accordion-toggle').click(function(){																	  
			if($(this).hasClass('collapsed')){
				$('.miles_snakes_accordion .accordion-toggle').addClass('collapsed');
				$(this).removeClass('collapsed');
			}															  
		});	
		
		$('.editNotes.clk').live('click',function(){
			var current_paragraph=$(this).parent().next();	
			current_paragraph.hide();

			current_paragraph.after("<textarea class='notetext' >"+current_paragraph.html()+"</textarea>");
			
			$(this).before("<a class='cancelNotes'> Cancel</a>");
			$(this).text('Save').removeClass('clk').addClass('save');
		});
		
		$('.editNotes.save').live('click',function(){
			var current_paragraph=$(this).parent().next();
			var w_id=$(this).attr('w_id');

			$.ajax({
				 url: "index.php?option=com_questionnaire_exercise&task=questionform.ajax_add_note",
				 type: "POST",
				 data:'w_id='+w_id+'&note='+current_paragraph.next().val(),
				 dataType:'html',
				 success:function(){
					 	current_paragraph.next().hide();
					 	current_paragraph.html(current_paragraph.next().val()).show();
						current_paragraph.prev().find('a.save').text('Edit Note').removeClass('save').addClass('clk');
						current_paragraph.prev().find('a.cancelNotes').remove();
						current_paragraph.next().remove();
				 }
			});								  
		});	
		
		$('.cancelNotes').live('click',function(){
			var current_lnk=$(this);	
			current_lnk.next().text('Edit Note').removeClass('save').addClass('clk');
			current_lnk.parent().next().show().next().remove();
			current_lnk.remove();
 	    });
});