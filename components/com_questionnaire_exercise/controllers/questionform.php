<?php

/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> 
 */

// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Questionnaire_exerciseController controller class.
 */
class Questionnaire_exerciseControllerQuestionForm extends Questionnaire_exerciseController
{

	function swap_exercise(){
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		$loginUserId			= JFactory::getUser()->get('id');
		
		$exercise_id=$app->input->get('exercise_id');
		$swap_exercise_id=$app->input->get('swap_exercise_id');
		$own_exercise=$app->input->get('own_exercise',0);
		
		//Check the exercise type
		$query="SELECT C.id,B.id as subCat FROM #__questionnaire_exercise_name A JOIN  #__questionnaire_exercise_category B ON A.category_id=B.id JOIN #__questionnaire_exercise_maincategory C ON  C.id=B.category WHERE A.id=$exercise_id";
		$db->setQuery($query);
		
		$exercise_cat=$db->loadObject()->id;

		if($exercise_cat==16){
			$type=1;
		}elseif($exercise_cat==11){
			$type=2;
		}elseif($exercise_cat==14){
			$type=5;
		}elseif($exercise_cat==18){
			if($db->loadObject()->subCat==45){
				$type=3;
			}elseif($db->loadObject()->subCat==46){
				$type=4;
			}
		}else{
			$type=6;
		}
		
		//swap out cardio exercise
		$query="UPDATE #__userworkouts SET exercise_id=$exercise_id,exercise_type=$type WHERE exercise_id=$swap_exercise_id and user_id=$loginUserId and own_fitness_program=$own_exercise";

		$db->setQuery($query);
		$db->query();
		
		$own_exercise_view=($own_exercise==1)?'ownprogram':'program';
		
		if($db->getAffectedRows()>0){
				$this->setMessage("Exercise swap successfully.", 'notice');
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_exercise&view=$own_exercise_view&Itemid=247", false));				
		}else{		
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_exercise&view=$own_exercise_view&Itemid=247", false));
		}
	}
	
	function add_own_exercise(){
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		$loginUserId	= (int)  JFactory::getUser()->get('id');
		$exercise_id=$app->input->get('exercise_id');
		
		//Check the exercise type
		$query="SELECT C.id,B.id as subCat FROM #__questionnaire_exercise_name A JOIN  #__questionnaire_exercise_category B ON A.category_id=B.id JOIN #__questionnaire_exercise_maincategory C ON  C.id=B.category WHERE A.id=$exercise_id";
		$db->setQuery($query);	
		$exercise_cat=$db->loadObject()->id;

		if($exercise_cat==16){
			$type=1;
		}elseif($exercise_cat==11){
			$type=2;
		}elseif($exercise_cat==14){
			$type=5;
		}elseif($exercise_cat==18){
			if($db->loadObject()->subCat==45){
				$type=3;
			}elseif($db->loadObject()->subCat==46){
				$type=4;
			}
		}else{
			$type=6;
		}
		$query="INSERT IGNORE INTO #__userworkouts(user_id,exercise_id,exercise_type,own_fitness_program) VALUES($loginUserId,$exercise_id,$type,1)";	

		$db->setQuery($query);
		$db->query();
		$insertId= $db->insertid();
		if($insertId>0){
			$this->setMessage("Exercise added successfully.", 'notice');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_exercise&view=ownprogram&Itemid=250", false));		
		}else{
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire_exercise&view=ownprogram&Itemid=250", false));	
		}
	
	}
	
	function ajax_add_note(){ 
	
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		$loginUserId	= (int)  JFactory::getUser()->get('id');
		$w_id=$app->input->getPost('w_id');
		$note=$app->input->getPost('note');
		
		$query="UPDATE #__userworkouts SET notes='$note' WHERE id=$w_id and user_id=$loginUserId";
		$db->setQuery($query);
		$db->query();		
		die;	
	}
	
    function cancel() { 
		$menu = & JSite::getMenu();
        $item = $menu->getActive();
        $this->setRedirect(JRoute::_('index.php?option=com_questionnaire_nutrition', false));
    }    
}