<?php
function wherein_remove_lastcomma($data){
	return preg_replace('/,$/','',$data);
}
function getYoutube_embed_code($data){
	preg_match('/.*embed\/([-_\w ]*)\".*/', $data, $matches);
	$temp= @$matches[1];
	if(!empty($temp))
	return "<img alt='Progressions exercise' alt='Progressions exercise' src='http://img.youtube.com/vi/$temp/0.jpg' width='auto' height='auto'>";
}

function getYoutube_embed_code_path($data){
	preg_match('/.*embed\/([-_\w ]*)\".*/', $data, $matches);
	$temp= @$matches[1];
	if(!empty($temp))
	return "http://img.youtube.com/vi/$temp/0.jpg";
}

function age_from_dob($dob) {

    list($y,$m,$d) = explode('-', $dob);
   
    if (($m = (date('m') - $m)) < 0) {
        $y++;
    } elseif ($m == 0 && date('d') - $d < 0) {
        $y++;
    }
   
    return date('Y') - $y;   
}

function get_user_fitness_day($cur_date,$user_fitness_days_arr){
	if(in_array(date('l',strtotime($cur_date)),$user_fitness_days_arr)){
		return $cur_date;
	}else{
		$cur_date=date('Y-m-d',strtotime('+1 day',strtotime($cur_date)));
		return get_user_fitness_day($cur_date,$user_fitness_days_arr);
	}
}

function get_user_cardio_exercise($cardio_exercise_count,$cardio_exercise_subcat){
	$db = JFactory::getDbo();	
	$loginuser	= (int)JFactory::getUser()->get('id');
	
	$cardio_rand_exercise_id= rand(1,$cardio_exercise_count-1);
	$cardio_rend_id= $cardio_exercise_subcat[$cardio_rand_exercise_id]->id;

	$query="SELECT count(id) cardCount FROM  #__questionnaire_exercise_name  WHERE category_id=$cardio_rend_id and id NOT IN (SELECT exercise_id FROM #__userworkouts WHERE user_id=$loginuser)";
	$db->setQuery($query);
	if($db->loadObject()->cardCount==0){
		$cardio_rand_exercise_id= rand(1,$cardio_exercise_count-1);
		$cardio_rend_id= $cardio_exercise_subcat[$cardio_rand_exercise_id]->id;
	
		return get_user_cardio_exercise($cardio_exercise_count,$cardio_exercise_subcat);
	}
	else{
	
	}
}


function create_user_workouts($loginUserId){
	$db = JFactory::getDbo();
	$app			= JFactory::getApplication();
	
	//Query to check if the user has fill all the fitness related questions.
	$query	= 'SELECT A.birthday, A.overallgoal,A.gender, A.experience_lvl_working_out,A.day_start_your_fitness_cardiovascular_program,A.you_train_days,B.experience_sublvl_working_out,B.workout_end_date FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' A LEFT JOIN #__userworkouts_info B ON A.user_id=B.user_id '."WHERE A.id= $loginUserId";
	
	$db->setQuery( $query );
	$user_explevelObj	= $db->loadObject();
	
	$user_explevel=$user_explevelObj->experience_lvl_working_out;
	//$user_explevelsub=$user_explevelObj->experience_sublvl_working_out;
	$user_gender=$user_explevelObj->gender;
	
	$user_fitness_days=$user_explevelObj->you_train_days;
	$user_fitness_days_arr=explode(',',$user_fitness_days);
	
	$birthday=$user_explevelObj->birthday;
	$overallgoal=wherein_remove_lastcomma($user_explevelObj->overallgoal);
	
	//Ensure user has enter all information in questionnaire if not then redirect to fitness questionnaire page. 
	if(empty($user_explevel)){
		$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=questionform&layout=fitness&Itemid=249", false),"Please enter your experience level.", 'notice');
	}
	elseif(empty($birthday) or $birthday=='0000-00-00'){
		$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=questionform&layout=stats&Itemid=234", false),"Please enter your birthdays.", 'notice');
	}
	elseif(empty($overallgoal)){
		$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=questionform&layout=goal&Itemid=310", false),"Please enter your overallgoal.", 'notice');
	}
	
	$user_age= age_from_dob($birthday);
	$overallgoal=str_replace(',','|',$overallgoal);
	
	//Query to check if user has assigned workouts for comming days.
	$query="SELECT COUNT(id) as userworkouts_level_nubm FROM #__userworkouts_level WHERE user_id=$loginUserId AND level_id=$user_explevel AND start_date >= curdate()";	
	$db->setQuery($query);

	$user_fitness_date_start=$user_explevelObj->day_start_your_fitness_cardiovascular_program;
	$user_fitness_date_end=$user_explevelObj->workout_end_date;
	
	if(isset($user_fitness_date_end) and $user_fitness_date_end!='0000-00-00'){
		$user_fitness_date=date('Y-m-d',strtotime("+1 day",strtotime($user_fitness_date_end)));
	}else{
		$user_fitness_date=$user_fitness_date_start;
	}

	$j=1;
	
	if(isset($db->loadObject()->userworkouts_level_nubm) and $db->loadObject()->userworkouts_level_nubm==0){
		
		// Get user's top experience level  
		$query="SELECT level_id FROM #__userworkouts_level where user_id=$loginUserId ORDER BY start_date DESC limit 1";
		$db->setQuery($query);
	
		if(isset($db->loadObject()->level_id)){
			$level_id=$db->loadObject()->level_id;
			$user_explevel=$level_id+1;
			//Update the user level upon completing the old workout program			
			$query="UPDATE  #__questionnaire_question SET experience_lvl_working_out=$user_explevel WHERE id=$loginUserId";
			$db->setQuery($query);
			$db->query();
	    }
	
		//Get number of exercises under each user level.
		$query="SELECT concat (level_1,',',level_2,',',level_3,',',level_4,',',level_5) workout_levels  FROM #__userlevel WHERE id=$user_explevel";
		$db->setQuery($query);
		$sublvl_workout_data=explode(',',$db->loadObject()->workout_levels);
	
		foreach($sublvl_workout_data as $sublvl_workout_dataKey=>$sublvl_workout_dataVal){
			$sublevel_keyVal=$sublvl_workout_dataKey+1;
	
			for($i=1;$i<=$sublvl_workout_dataVal;$i++){
	
				$user_fitness_date= get_user_fitness_day($user_fitness_date,$user_fitness_days_arr);
			
				$query="INSERT INTO #__userworkouts_level(user_id,level_id,sublevel_id,start_date) VALUES($loginUserId,$user_explevel,$sublevel_keyVal,'$user_fitness_date')";
				
				$db->setQuery($query);
				$db->query();
				
				$user_fitness_date=date('Y-m-d',strtotime("+1 day",strtotime($user_fitness_date)));
			}
		}
		
		//My fitness Program workout categories list
		$exercise_type_list=array(1=>'FoamRoll',2=>'Stretch',3=>'WarmUp',4=>'CoolDown',5=>'Cardiovascular',6=>'Primary');
		//Get user workouts and assign exercises to them
		$query="SELECT * FROM #__userworkouts_level WHERE user_id=$loginUserId and level_id=$user_explevel";
		$db->setQuery($query);
	
		foreach($db->loadObjectList() as $user_workout_levels){
			$userworkouts_level_id=$user_workout_levels->id;
			$rand_exercise=rand(1,2);
			//AND (A.exercise_type REGEXP '$overallgoal')
			//List Foam Roll Workout exercises
			$query= "SELECT A.id,1 as type FROM #__questionnaire_exercise_name A JOIN  #__questionnaire_exercise_category B ON A.category_id=B.id JOIN #__questionnaire_exercise_maincategory C ON B.category=C.id WHERE C.id=16 AND gender IN($user_gender,3) and ($user_age>A.age1 and $user_age<A.age2) ORDER BY RAND() LIMIT $rand_exercise";
		
			$db->setQuery( $query );
			
			$user_foamroll_exercise_list=$db->loadAssocList();
			
			//List Stretch Workout exercises
			$query= "SELECT A.id,2 as type FROM #__questionnaire_exercise_name A JOIN  #__questionnaire_exercise_category B ON A.category_id=B.id JOIN #__questionnaire_exercise_maincategory C ON B.category=C.id  WHERE C.id=11 AND gender IN($user_gender,3) AND ($user_age>A.age1 and $user_age<A.age2)  ORDER BY RAND() LIMIT $rand_exercise";	
			
			$db->setQuery( $query );
		
			$user_stretch_exercise_list=$db->loadAssocList();
			
			//List Warm Up Workout exercises
			$query= "SELECT A.id,3 as type FROM #__questionnaire_exercise_name A JOIN  #__questionnaire_exercise_category B ON A.category_id=B.id WHERE B.id=45 AND gender IN($user_gender,3) AND ($user_age>A.age1 and $user_age<A.age2)  ORDER BY RAND() LIMIT $rand_exercise";	
			
			$db->setQuery( $query );
		
			$user_warmup_exercise_list=$db->loadAssocList();
		
		
			//List Cool Down Workout exercises
			$query= "SELECT A.id,4 as type FROM #__questionnaire_exercise_name A JOIN  #__questionnaire_exercise_category B ON A.category_id=B.id WHERE B.id=46 AND gender IN($user_gender,3) AND ($user_age>A.age1 and $user_age<A.age2)  ORDER BY RAND() LIMIT $rand_exercise";	
			
			$db->setQuery( $query );
		
			$user_cooldown_exercise_list=$db->loadAssocList();
			
			//List Cardiovascular Workout exercises
			$query= "SELECT A.id,5 as type FROM #__questionnaire_exercise_name A JOIN  #__questionnaire_exercise_category B ON A.category_id=B.id JOIN #__questionnaire_exercise_maincategory C ON B.category=C.id  WHERE C.id=14 AND gender IN($user_gender,3) AND ($user_age>A.age1 and $user_age<A.age2)  ORDER BY RAND() LIMIT $rand_exercise";		
			
			$db->setQuery( $query );
		
			$user_cardio_exercise_list=$db->loadAssocList();
			
		
			$rand_exercise=rand(2,4);
			//List Primary Workout exercises
			$query= "SELECT A.id,6 as type FROM #__questionnaire_exercise_name A JOIN  #__questionnaire_exercise_category B ON A.category_id=B.id JOIN #__questionnaire_exercise_maincategory C ON B.category=C.id  WHERE C.id NOT IN (11,16,18) AND gender IN($user_gender,3) AND ($user_age>A.age1 and $user_age<A.age2)  ORDER BY RAND() LIMIT $rand_exercise";	
		
			
			$db->setQuery( $query );
		
			$user_primary_exercise_list=$db->loadAssocList();
			
			//Merge all exercises into one array
			$user_all_exercises=array_merge($user_foamroll_exercise_list,$user_stretch_exercise_list,$user_warmup_exercise_list,$user_cooldown_exercise_list,$user_cardio_exercise_list,$user_primary_exercise_list);
		
			foreach($user_all_exercises as $user_all_exercisesVal){
		
				$user_exercise_id=$user_all_exercisesVal['id'];
				$exercise_type=$user_all_exercisesVal['type'];		
		
				//Assign exercises to workouts
				$query= "INSERT INTO #__userworkouts
				(user_id,userworkouts_level_id,exercise_id,exercise_type) 
				VALUES($loginUserId,$userworkouts_level_id,$user_exercise_id,$exercise_type)";
				$db->setQuery( $query );
				$db->query();
				
			}
		}	
	}
}
?>