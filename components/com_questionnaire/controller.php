<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */
 
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class QuestionnaireController extends JControllerLegacy
{
	 function __construct() {
	    	parent::__construct();
			$user = JFactory::getUser();
			
			if($user->guest==1){
			
				$this->setMessage("Please login", 'notice');
				$this->setRedirect(JRoute::_("index.php?option=com_users&view=login", false));
			
			}
	 }
	 
	public function display($cachable = false, $urlparams = false)
	{
		// Get the document object.
		$document = JFactory::getDocument();

		// Set the default view name and format from the Request.
		$vName = $this->input->get('view', 'questionform');
		$this->input->set('view', $vName);
		
		$vName = $this->input->get('layout', 'new_member');
		$this->input->set('layout', $vName);

		$user = JFactory::getUser();

		parent::display($cachable);

		return $this;
	}
}