<?php

/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * QuestionnaireController controller class.
 */
class QuestionnaireControllerQuestionForm extends QuestionnaireController
{

	 public function save(){
	
		$app			= JFactory::getApplication();
		$user			= JFactory::getUser();
		$loginUserId	= (int) $user->get('id');
		
		$data = $app->input->post->get('jform', array(), 'array');

		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$data['id']=$data['user_id'] = $loginUserId;

		$model	= $this->getModel('QuestionForm');
		
		$return	= $model->save($data);
		
		$Itemid	= $app->input->post->get('Itemid');
		$item_id="&Itemid=$Itemid";
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=$action$item_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message",'success');
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=$action$item_id", false));
		}
	} 
	
	function savefitness(){

		$app			= JFactory::getApplication();
		$user			= JFactory::getUser();
		$loginUserId	= (int) $user->get('id');
		$db			= JFactory::getDBO();
		
		$data = $app->input->post->get('jform', array(), 'array');
		
		if(isset($data['workout_path'])){
			$data['workout_path']=implode(',',$data['workout_path']);
		}else{
			$data['workout_path']='';
		}
		
		if(isset($data['you_train_days'])){
			$data['you_train_days']=implode(',',$data['you_train_days']);
		}else{
			$data['you_train_days']='';
		}
		
		if($data['have_workout_partner']==0){
			$data['workout_partner_days']='';
		}else{
			$data['workout_partner_days']=implode(',',$data['workout_partner_days']);
		}

		if(isset($data['equipment_available'])){
				$data['equipment_available']= json_encode($data['equipment_available']);
		}else{
			$data['equipment_available']='';
		}
		
		if(isset($data['where_you_train_day'])){
				$data['where_you_train_day']= json_encode($data['where_you_train_day']);
		}else{
			$data['where_you_train_day']='';
		}
		
		if(isset($data['cardiovascular_exercise_goal'])){
			$data['cardiovascular_exercise_goal']=implode(',',$data['cardiovascular_exercise_goal']);
		}else{
			$data['cardiovascular_exercise_goal']='';
		}
		
		if(isset($data['equipment_or_amenities_you_have_available'])){
			$data['equipment_or_amenities_you_have_available']= json_encode($data['equipment_or_amenities_you_have_available']);
		}else{
			$data['equipment_or_amenities_you_have_available']='';
		}
		
		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$data['id']=$data['user_id'] = $loginUserId;

		$model	= $this->getModel('QuestionForm');
		
		$return	= $model->save($data);
		
		$Itemid	= $app->input->post->get('Itemid');
		$item_id="&Itemid=$Itemid";
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=$action$item_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message",'success');
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=$action$item_id", false));
		}
	}
	
	function savenutrition(){
		$app			= JFactory::getApplication();
		$user			= JFactory::getUser();
		$loginUserId	= (int) $user->get('id');
		
		$data = $app->input->post->get('jform', array(), 'array');
		
		if(isset($data['scenario_program'])){
			$data['scenario_program']= json_encode($data['scenario_program']);
		}else{
			$data['scenario_program']='';
		}
		
		if(isset($data['question_answers'])){
			$data['question_answers']= json_encode($data['question_answers']);
		}else{
			$data['question_answers']='';
		}	
		
		if(isset($data['foods_condiments_like'])){
			$data['foods_condiments_like']= json_encode($data['foods_condiments_like']);
		}else{
			$data['foods_condiments_like']='';
		}			
		

		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$data['id']=$data['user_id'] = $loginUserId;

		$model	= $this->getModel('QuestionForm');
		
		$return	= $model->save($data);
		
		$Itemid	= $app->input->post->get('Itemid');
		$item_id="&Itemid=$Itemid";		
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=$action$item_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message",'success');
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=$action$item_id", false));
		}
	}
	
	function my_all_questions(){
		$app			= JFactory::getApplication();
		$user			= JFactory::getUser();
		$loginUserId	= (int) $user->get('id');
		
		$data = $app->input->post->get('jform', array(), 'array');

		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$data['id']=$data['user_id'] = $loginUserId;

		$model	= $this->getModel('QuestionForm');
		
		if(isset($data['measuremts'])){
			$data['measuremts']= json_encode($data['measuremts']);
		}else{
			$data['measuremts']='';
		}
		
		if(isset($data['overallgoal'])){
				$data['overallgoal']=implode(',',$data['overallgoal']);
		}else{
			$data['overallgoal']='';
		}	
		
		if(isset($data['workout_path'])){
			$data['workout_path']=implode(',',$data['workout_path']);
		}else{
			$data['workout_path']='';
		}
		
		if(isset($data['you_train_days'])){
			$data['you_train_days']=implode(',',$data['you_train_days']);
		}else{
			$data['you_train_days']='';
		}
		
		if($data['have_workout_partner']==0){
			$data['workout_partner_days']='';
		}else{
			$data['workout_partner_days']=implode(',',$data['workout_partner_days']);
		}

		if(isset($data['equipment_available'])){
				$data['equipment_available']= json_encode($data['equipment_available']);
		}else{
			$data['equipment_available']='';
		}
		
		if(isset($data['where_you_train_day'])){
				$data['where_you_train_day']= json_encode($data['where_you_train_day']);
		}else{
			$data['where_you_train_day']='';
		}

		if(isset($data['cardiovascular_exercise_goal'])){
			$data['cardiovascular_exercise_goal']=implode(',',$data['cardiovascular_exercise_goal']);
		}else{
			$data['cardiovascular_exercise_goal']='';
		}
		
		if(isset($data['equipment_or_amenities_you_have_available'])){
			$data['equipment_or_amenities_you_have_available']= json_encode($data['equipment_or_amenities_you_have_available']);
		}else{
			$data['equipment_or_amenities_you_have_available']='';
		}
		
		if(isset($data['scenario_program'])){
			$data['scenario_program']= json_encode($data['scenario_program']);
		}else{
			$data['scenario_program']='';
		}
		
		if(isset($data['question_answers'])){
			$data['question_answers']= json_encode($data['question_answers']);
		}else{
			$data['question_answers']='';
		}	
		
		if(isset($data['foods_condiments_like'])){
			$data['foods_condiments_like']= json_encode($data['foods_condiments_like']);
		}else{
			$data['foods_condiments_like']='';
		}			
		
		$return	= $model->save($data);
		
		$Itemid	= $app->input->post->get('Itemid');
		$item_id="&Itemid=$Itemid";
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=$action$item_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message",'success');
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=$action$item_id", false));
		}
	}
	
    function cancel() {
		$menu = & JSite::getMenu();
        $item = $menu->getActive();
        $this->setRedirect(JRoute::_('index.php?option=com_questionnaire', false));
    }
}

?>