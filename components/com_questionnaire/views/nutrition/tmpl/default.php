<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
//JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);

$db			= JFactory::getDBO();
$user			= JFactory::getUser();
$loginUserId	= (int) $user->get('id');

$query	= 'SELECT scenario_program,question_answers,foods_condiments_like FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '
		. "WHERE id= $loginUserId";

$db->setQuery( $query );
$user_obj	= $db->loadObject();

$scenario_program=array();
if(!empty($user_obj->scenario_program)){
	$scenario_program=@json_decode($user_obj->scenario_program,'array');
}

$question_answers=array();
if(!empty($user_obj->question_answers)){
	$question_answers=@json_decode($user_obj->question_answers,'array');
}

$foods_condiments_like=array();
if(!empty($user_obj->foods_condiments_like)){
	$foods_condiments_like=@json_decode($user_obj->foods_condiments_like,'array');
}
?>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<script type="text/javascript">
jQuery().ready(function($){
<?php if($this->form->getValue('nutrition_program_start')!='0000-00-00'): ?>
	$('#jform_nutrition_program_start').attr('disabled','disabled');
	$('#jform_nutrition_program_start_img').remove();
<?php endif; ?>	
});
</script>
<div class="questionform_content">
<div class="row-fluid">
    <div class="question-edit front-end-edit">
      <h1 class="nw_member_info">My Nutrition Path</h1>
      <form id="form-question" action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=questionform.savenutrition'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <div class="menber_lable">
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label   for="jform_eat_a_day_meal_snacks" >How many times do you usually eat a day including meals and snacks?</label>
            </div>
            <div class="controls calender_option1"> <?php echo $this->form->getInput('eat_a_day_meal_snacks'); ?> </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label   for="jform_nutrition_program_start" >Select what day you would like your Nutrition Program to start</label>
            </div>
            <div class="controls wokout_lable2"> <?php echo $this->form->getInput('nutrition_program_start'); ?> </div>
          </div>
          <fieldset class="fild_form1">
          <legend>Which scenario best describes your day to day lifestyle regarding meal preparation</legend>
          <div class="control-group something_right1">
            <div class="control-label">
              <label for="jform_scenario_program_break_lunch_dinner" >Can prepare/cook breakfast, lunch and dinner</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[scenario_program][break_lunch_dinner]" id="jform_scenario_program_break_lunch_dinner">
                <option value="Always"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right1">
            <div class="control-label">
              <label for="jform_scenario_program_break_dinner" >Can prepare/cook breakfast and dinner</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[scenario_program][break_dinner]" id="jform_scenario_program_break_dinner">
                <option value="Always"  <?php   echo (@$scenario_program['break_dinner']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$scenario_program['break_dinner']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$scenario_program['break_dinner']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$scenario_program['break_dinner']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$scenario_program['break_dinner']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right1">
            <div class="control-label">
              <label  for="jform_scenario_program_dinner" > Can only prepare/cook dinner</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[scenario_program][dinner]" id="jform_scenario_program_dinner">
                <option value="Always"  <?php   echo (@$scenario_program['dinner']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$scenario_program['dinner']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$scenario_program['dinner']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$scenario_program['dinner']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$scenario_program['dinner']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right1">
            <div class="control-label">
              <label  for="jform_night_sleep_hours" >How many hours of sleep do you usually average a night?</label>
            </div>
            <div class="controls calender_option1"> <?php echo $this->form->getInput('night_sleep_hours'); ?> </div>
          </div>
          </fieldset>
          <fieldset class="fild_form1">
          <legend>Please answer the following questions as honestly as possible</legend>
          <div class="control-group something_right1">
            <div class="control-label">
              <label   for="jform_eat_sweet_fruit_candy" >If I eat something sweet such as fruit or candy I pick right up</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[question_answers][eat_sweet_fruit_candy]" id="jform_eat_sweet_fruit_candy">
                <option value="Always"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right">
            <div class="control-label">
              <label   for="jform_eat_meat_daily" >I could eat red meat daily</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[question_answers][eat_meat_daily]" id="jform_eat_meat_daily">
                <option value="Always"  <?php   echo (@$question_answers['eat_meat_daily']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$question_answers['eat_meat_daily']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$question_answers['eat_meat_daily']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$question_answers['eat_meat_daily']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$question_answers['eat_meat_daily']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right">
            <div class="control-label">
              <label   for="jform_eat_steamed_vegetables" >I could eat steamed or raw vegetables daily</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[question_answers][eat_steamed_vegetables]" id="jform_eat_steamed_vegetables">
                <option value="Always"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right">
            <div class="control-label">
              <label   for="jform_find_myself_snacking" >I often find myself snacking throughout the day</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[question_answers][find_myself_snacking]" id="jform_find_myself_snacking">
                <option value="Always"  <?php   echo (@$question_answers['find_myself_snacking']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$question_answers['find_myself_snacking']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$question_answers['find_myself_snacking']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$question_answers['find_myself_snacking']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$question_answers['find_myself_snacking']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right">
            <div class="control-label">
              <label   for="jform_eat_meat_feel_right" >If I eat meat I feel like I it picks me right up and gives me energy </label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[question_answers][eat_meat_feel_right]" id="jform_eat_meat_feel_right">
                <option value="Always"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right">
            <div class="control-label">
              <label   for="jform_crave_sweets" >I often crave sweets</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[question_answers][crave_sweets]" id="jform_crave_sweets">
                <option value="Always"  <?php   echo (@$question_answers['crave_sweets']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$question_answers['crave_sweets']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$question_answers['crave_sweets']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$question_answers['crave_sweets']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$question_answers['crave_sweets']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right">
            <div class="control-label">
              <label   for="jform_feel_sluggish_not_eat_meat" >I often feel sluggish if I don't eat every two or three hours</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[question_answers][feel_sluggish_not_eat_meat]" id="jform_feel_sluggish_not_eat_meat">
                <option value="Always"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right">
            <div class="control-label">
              <label   for="jform_drinking_meal_replacements" >I often find myself drinking Meal Replacements or Protein Sakes</label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[question_answers][drinking_meal_replacements]" id="jform_drinking_meal_replacements">
                <option value="Always"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          <div class="control-group something_right">
            <div class="control-label">
              <label   for="jform_tend_get_constipated" >I tend to get constipated </label>
            </div>
            <div class="controls calender_option1">
              <select name="jform[question_answers][tend_get_constipated]" id="jform_tend_get_constipated">
                <option value="Always"  <?php   echo (@$question_answers['tend_get_constipated']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                <option value="Very Often"  <?php   echo (@$question_answers['tend_get_constipated']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                <option value="Sometimes"  <?php   echo (@$question_answers['tend_get_constipated']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                <option value="Rarely"  <?php   echo (@$question_answers['tend_get_constipated']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                <option value="Never"  <?php   echo (@$question_answers['tend_get_constipated']=='Never')? "selected='selected'" : '';  ?> >Never</option>
              </select>
            </div>
          </div>
          </fieldset>
          <div class="restricted_area" style="float:left">
            <fieldset class="food_condiments">
            <legend>Please choose which foods and condiments you like in each category</legend>
            <?php
			$query	= 'SELECT id,name FROM ' . $db->quoteName( '#__questionnaire_nutrition_category' ).'  WHERE state=1 and id!=20 order by name  ASC';
			
			$db->setQuery( $query );
			  
			foreach($db->loadObjectList() as $nutritions_cat):			
			?>
            <table class="food_condiments_table">
              <tr>
                <th><?php echo $nutritions_cat->name; ?></th>
              </tr>
              <tr>
                <td><table>
                    <?php  
						 $nutrition_cat_id=$nutritions_cat->id;

						 $query	= "SELECT id,name FROM " . $db->quoteName( '#__questionnaire_nutrition_food' ) . " WHERE category_id=$nutrition_cat_id and state=1 order by  name ASC";
						 $db->setQuery( $query );
						 $nutrition_lst=0;
						 foreach($db->loadObjectList() as $nutrition_items):
						 $nutrition_id = $nutrition_items->id;
						 $nutrition_name = $nutrition_items->name;		
						 $car_nut_labl_suffix=$nutrition_cat_id.'_'.$nutrition_id;				 
						  ?>
                    <?php if($nutrition_lst%3==0): ?>
                    <tr>
                      <?php endif;  ?>
                      <td><input type="checkbox" <?php   echo (@$foods_condiments_like[$nutrition_cat_id][$nutrition_id]==$nutrition_id)? "checked='checked'" : '';  ?> value="<?php echo $nutrition_id; ?>"   name="jform[foods_condiments_like][<?php echo $nutrition_cat_id; ?>][<?php echo $nutrition_id;?>]" id="jform_cardiovascular_exercise_<?php echo $car_nut_labl_suffix; ?>"></td>
                      <td><label  for="jform_cardiovascular_exercise_<?php echo $car_nut_labl_suffix; ?>"><?php echo $nutrition_name; ?></label></td>
                      <?php if(($nutrition_lst+1)%3==0): ?>
                    </tr>
                    <?php endif;  ?>
                    <?php $nutrition_lst++; endforeach; ?>
                  </table></td>
              </tr>
            </table>
            <?php
			endforeach;			
			?>
            </fieldset>
          </div>
          <div class="control-group">
            <div class="control-label"> </div>
            <div class="controls wokout_button">
              <div class="submit_member1">
                <button type="submit" class="validate button btn btn-large submit_method"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
                <input type="hidden" name="action" value="nutrition" />
                <input type="hidden" name="action_success_message" value="My_nutrition_information_saved_successfully" />
                <input type="hidden" name="action_fail_message" value="Error_saving_my_nutrition_informationn" />
                <input type="hidden" name="option" value="com_questionnaire" />
                <input type="hidden" name="task" value="questionform.savenutrition" />
                <input type="hidden" name="Itemid" value="<?php echo JRequest::getint( 'Itemid' ); ?>" />
                <?php echo JHtml::_('form.token'); ?> </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
