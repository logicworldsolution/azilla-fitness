<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
//JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('bootstrap.framework');	

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);

$doc = JFactory::getDocument();
$doc->addScript(JURI::base().'components/com_questionnaire/views/fitness/tmpl/js/fitness.js');

$db			= JFactory::getDBO();
$user			= JFactory::getUser();
$loginUserId	= (int) $user->get('id');

$query	= 'SELECT equipment_available,where_you_train_day,equipment_or_amenities_you_have_available FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '
		. "WHERE id= $loginUserId";

$db->setQuery( $query );
$user_obj	= $db->loadObject();

$user_equipments=array();
if(!empty($user_obj->equipment_available)){
	$user_equipments=json_decode($user_obj->equipment_available,'array');
}

$user_where_you_train_day=array();
if(!empty($user_obj->where_you_train_day)){
	$user_where_you_train_day=json_decode($user_obj->where_you_train_day,'array');
}

$user_amenities_obj	= $user_obj;
$user_amenities=array();
if(!empty($user_amenities_obj->equipment_or_amenities_you_have_available)){
	$user_amenities=json_decode($user_amenities_obj->equipment_or_amenities_you_have_available,'array');
}
?>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<script type="text/javascript">
jQuery().ready(function($){
<?php if($this->form->getValue('experience_lvl_working_out')!='0'): ?>
	$('#jform_experience_lvl_working_out').attr('disabled','disabled');
<?php endif; ?>	
<?php if($this->form->getValue('day_start_your_fitness_cardiovascular_program')!='0000-00-00'): ?>
	$('#jform_day_start_your_fitness_cardiovascular_program').attr('disabled','disabled');
	$('#jform_day_start_your_fitness_cardiovascular_program_img').remove();
<?php endif; ?>	
});
</script>
<div class="questionform_content">
  <div class="row-fluid">
    <div class="question-edit front-end-edit">
      <h1 class="nw_member_info">My Fitness Path</h1>
      <form id="form-question" action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=questionform.savefitness'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <div class="menber_lable">
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label  for="jform_workout_path" >Choose a Workout Path</label>
            </div>
            <div class="controls fild_form payment_space"> <?php echo $this->form->getInput('workout_path'); ?> </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label avai_equipment wokout_lable1">
              <label  for="fitness_business_code" >Please choose which equipment you have available to you. (Choose whether equipment is for Home (H) Park (P) Gym (G) or All (A))</label>
            </div>
            <div class="controls payment_space">
              <ul id="equipment_id" class="equipment_product">
                <?php 
			  $query	= 'SELECT A.id,A.name FROM ' . $db->quoteName( '#__questionnaire_equipment' )
				. ' AS A JOIN '.$db->quoteName( '#__questionnaire_equipment_category' ).' AS B ON  A.category_id=B.id WHERE  B.id=1 and (A.state=1 and B.state=1) order by A.name asc';
	
			  $db->setQuery( $query );
			  foreach($db->loadObjectList() as $equipments):?>
                <li>
                  <?php
						$equipment_checked='';
							if(array_key_exists($equipments->id,$user_equipments)){
								$equipment_checked="checked='checked'";
							}
								
							echo "<input type='checkbox' class='equipment_class gap' name='jform[equipment_available][".$equipments->id."][id]' value='".$equipments->id."' $equipment_checked >".$equipments->name;
							
							if(!empty($equipment_checked)){								
								$current_equipments=$user_equipments[$equipments->id];
								if(!empty($current_equipments['days'])){

									$is_h_checked=(@$current_equipments['days']['H'])? "checked='checked'" : '';
									$is_g_checked=(@$current_equipments['days']['G'])? "checked='checked'" : '';
									$is_p_checked=(@$current_equipments['days']['P'])? "checked='checked'" : '';
									$is_a_checked=(@$current_equipments['days']['A'])? "checked='checked'" : '';
									
									echo "<span class='equipment_days".$equipments->id."' ><span class='hgpa_main'>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_available][$equipments->id][days][H]' $is_h_checked >H</span>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_available][$equipments->id][days][G]' $is_g_checked >G</span>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_available][$equipments->id][days][P]' $is_p_checked >P</span>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_available][$equipments->id][days][A]' $is_a_checked >A</span>";							
									echo "</span></span>";
								}else{
								echo "<span class='equipment_days".$equipments->id."'></span>";
								}
							
							}
							else{
								echo "<span class='equipment_days".$equipments->id."'></span>";
							}
					 ?>
                </li>
                <?php endforeach;			  
			  ?>
              </ul>
            </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label  for="jform_experience_lvl_working_out" >What is your experience level when it comes to working out?</label>
            </div>
            <div class="controls payment_space"> <?php echo $this->form->getInput('experience_lvl_working_out'); ?> </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label  for="fitness_business_code" >How many days a week can you train and on what days? <br>(Please choose 3 to 6 days,recommended days are 5 to 6)</label>
            </div>
            <div class="controls fild_form payment_space"> <?php echo $this->form->getInput('you_train_days'); ?> </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label fild_form wokout_lable1">
              <label   >Do you have a workout partner and if so what days?</label>
            </div>
            <div class="controls fild_form payment_space"> <?php echo $this->form->getInput('have_workout_partner'); ?>
              <div id="workout_partner_days_sh" style="display:none"><?php echo $this->form->getInput('workout_partner_days'); ?></div>
            </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label  for="fitness_business_code" >Where will you be training and on what days? (Choose whether location is Home (H) Park (P) Gym (G) or All (A))</label>
            </div>
            <div class="controls payment_space">
              <?php $where_you_train_day=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'); ?>
              <ul id="where_you_train_day_id" class="equipment_product">
                <?php 	 foreach($where_you_train_day as $where_you_train_day_val): ?>
                <li>
                  <?php $where_you_train_day_checked=(array_key_exists($where_you_train_day_val,$user_where_you_train_day))? "checked='checked'" : ''; ?>
                  <input type="checkbox" value="<?php echo $where_you_train_day_val; ?>" name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][dayname]" class="where_you_train_day_class gap" <?php echo $where_you_train_day_checked; ?> >
                  <?php echo $where_you_train_day_val; ?>
                  <?php if(!empty($where_you_train_day_checked)): ?>
                  <span class="where_you_train_day_<?php echo $where_you_train_day_val; ?>">
                  <?php  
									$is_h_checked=(@$user_where_you_train_day[$where_you_train_day_val]['days']['H'])? "checked='checked'" : '';
									$is_g_checked=(@$user_where_you_train_day[$where_you_train_day_val]['days']['G'])? "checked='checked'" : '';
									$is_p_checked=(@$user_where_you_train_day[$where_you_train_day_val]['days']['P'])? "checked='checked'" : '';
									$is_a_checked=(@$user_where_you_train_day[$where_you_train_day_val]['days']['A'])? "checked='checked'" : '';
							 ?>
                  <span class="hgpa_main"> <span class="hgpa">
                  <input type="checkbox" <?php echo $is_h_checked; ?> name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][days][H]" value="1">
                  H </span> <span class="hgpa">
                  <input type="checkbox" <?php echo $is_g_checked; ?> name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][days][G]" value="1">
                  G</span> <span class="hgpa">
                  <input type="checkbox" <?php echo $is_p_checked; ?> name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][days][P]" value="1">
                  P</span> <span class="hgpa">
                  <input type="checkbox" <?php echo $is_a_checked; ?> name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][days][A]" value="1">
                  A</span> </span> </span>
                  <?php else: ?>
                  <span class="where_you_train_day_<?php echo $where_you_train_day_val; ?>"></span>
                  <?php endif; ?>
                </li>
                <?php endforeach ?>
              </ul>
            </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label   for="jform_resting_heart" >What is your resting heart? (Take your pulse for 1 minute or 15 seconds and times it by 4)</label>
            </div>
            <div class="controls fild_form11"> <?php echo $this->form->getInput('resting_heart'); ?> </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label   for="jform_cardiovascular_exercise_goal" >What is your primary goal(s) when it comes to cardiovascular exercise?</label>
            </div>
            <div class="controls wokout_lable"> <?php echo $this->form->getInput('cardiovascular_exercise_goal'); ?> </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label   for="jform_cardiovascular_exercise_experience_lvl" >What is your experience level when it comes to cardiovascular exercise?</label>
            </div>
            <div class="controls calender_option1"> <?php echo $this->form->getInput('cardiovascular_exercise_experience_lvl'); ?> </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label  >Please choose which equipment or amenities you have available to you. (Choose whether equipment or amenities are for Home (H) Park (P) Gym (G) or All (A))</label>
            </div>
            <div class="controls amenities_checking">
              <ul>
                <?php 
			 $query	= 'SELECT A.id,A.name FROM ' . $db->quoteName( '#__questionnaire_equipment' )
				. ' AS A JOIN '.$db->quoteName( '#__questionnaire_equipment_category' ).' AS B ON  A.category_id=B.id WHERE  B.id=2 and (A.state=1 and B.state=1) order by A.ordering';
	
			  $db->setQuery( $query );
			  foreach($db->loadObjectList() as $equipments):?>
                <li>
                  <?php
						$equipment_checked='';
							if(array_key_exists($equipments->id,$user_amenities)){
								$equipment_checked="checked='checked'";
							}
								
							echo "<input type='checkbox' class='equipmentcard_class gap' name='jform[equipment_or_amenities_you_have_available][".$equipments->id."][id]' value='".$equipments->id."' $equipment_checked >".$equipments->name;
							
							if(!empty($equipment_checked)){								
								$current_equipments=$user_amenities[$equipments->id];
								if(!empty($current_equipments['days'])){

									$is_h_checked=(@$current_equipments['days']['H'])? "checked='checked'" : '';
									$is_g_checked=(@$current_equipments['days']['G'])? "checked='checked'" : '';
									$is_p_checked=(@$current_equipments['days']['P'])? "checked='checked'" : '';
									$is_a_checked=(@$current_equipments['days']['A'])? "checked='checked'" : '';
									
									echo "<span class='equipment_days".$equipments->id."' ><span class='hgpa_main'>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][H]' $is_h_checked >H</span>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][G]' $is_g_checked >G</span>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][P]' $is_p_checked >P</span>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][A]' $is_a_checked >A</span>";							
									echo "</span></span>";
								}else{
								echo "<span class='equipment_days".$equipments->id."'></span>";
								}
							
							}
							else{
								echo "<span class='equipment_days".$equipments->id."'></span>";
							}
					 ?>
                </li>
                <?php endforeach;	?>
              </ul>
            </div>
          </div>
          <div class="control-group restricted_area">
            <div class="control-label wokout_lable1">
              <label   for="jform_day_start_your_fitness_cardiovascular_program" >Please select what day you would like to start your Fitness and Cardiovascular Program</label>
            </div>
            <div class="controls wokout_lable2"> <?php echo $this->form->getInput('day_start_your_fitness_cardiovascular_program'); ?> </div>
          </div>          
          <div class="control-group">
            <div class="control-label"> </div>
            <div class="controls">
              <div class="submit_member11">
                <button type="submit" class="validate button btn btn-large submit_method"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
                <input type="hidden" name="action" value="fitness" />
                <input type="hidden" name="action_success_message" value="Fitness_information_saved_successfully" />
                <input type="hidden" name="action_fail_message" value="Error_saving_fitness_information" />
                <input type="hidden" name="option" value="com_questionnaire" />
                <input type="hidden" name="task" value="questionform.savefitness" />
                <input type="hidden" name="Itemid" value="<?php echo JRequest::getint( 'Itemid' ); ?>" />
                <?php echo JHtml::_('form.token'); ?> </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
