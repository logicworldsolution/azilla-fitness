jQuery().ready(function(){
		jQuery('#jform_have_workout_partner0').click(function(){
			jQuery('#workout_partner_days_sh').show();
		});
		jQuery('#jform_have_workout_partner1').click(function(){
			jQuery('#workout_partner_days_sh').hide();
		});
		
		if(jQuery('#jform_have_workout_partner0').is(':checked')){
			jQuery('#workout_partner_days_sh').show()
		}
		
		//code to add/remove HGPA from checkbox
			jQuery('.hgpa_main input').live('click',function(){
				if(jQuery(this).attr('checked')!='checked'){
					if(jQuery(this).parents('.hgpa_main').find('input:checked').length==0){
						jQuery(this).parents('li').find('input').removeAttr('checked');
						jQuery(this).parents('.hgpa_main').remove();
					}		
				}
			});	
			
			//Please choose which equipment you have available to you? 
			//Where will you be training and on what days? 
			//Please choose which equipment or amenities you have available to you
			jQuery('.equipment_class').click(function(){
				var equp_indx=jQuery(this).val();
				if(jQuery(this).attr('checked')=='checked'){
					jQuery('.equipment_days'+jQuery(this).val()).html("<span class='hgpa_main'><span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_available]["+equp_indx+"][days][H]'  >H</span><span class='hgpa'><input type='checkbox' name='jform[equipment_available]["+equp_indx+"][days][G]' value='1'   >G</span><span class='hgpa'><input type='checkbox' name='jform[equipment_available]["+equp_indx+"][days][P]' value='1'  >P</span><span class='hgpa'><input type='checkbox' name='jform[equipment_available]["+equp_indx+"][days][A]' value='1'  >A</span></span>");
				}else{
					jQuery('.equipment_days'+jQuery(this).val()).find('.hgpa_main').remove();
				}
			});
			
			
			jQuery('.where_you_train_day_class').click(function(){
				var equp_indx=jQuery(this).val();

				if(jQuery(this).attr('checked')=='checked'){
					jQuery('.where_you_train_day_'+jQuery(this).val()).html("<span class='hgpa_main'><span class='hgpa'><input type='checkbox' value='1' name='jform[where_you_train_day]["+equp_indx+"][days][H]'  >H</span><span class='hgpa'><input type='checkbox' name='jform[where_you_train_day]["+equp_indx+"][days][G]' value='1'   >G</span><span class='hgpa'><input type='checkbox' name='jform[where_you_train_day]["+equp_indx+"][days][P]' value='1'  >P</span><span class='hgpa'><input type='checkbox' name='jform[where_you_train_day]["+equp_indx+"][days][A]' value='1'  >A</span></span>");
				}else{
					jQuery('.where_you_train_day_'+jQuery(this).val()).find('.hgpa_main').remove();
				}
			});		
			
			jQuery('.equipmentcard_class').click(function(){
				var equp_indx=jQuery(this).val();
				if(jQuery(this).attr('checked')=='checked'){
					jQuery('.equipment_days'+jQuery(this).val()).html("<span class='hgpa_main'><span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available]["+equp_indx+"][days][H]'  >H</span><span class='hgpa'><input type='checkbox' name='jform[equipment_or_amenities_you_have_available]["+equp_indx+"][days][G]' value='1'   >G</span><span class='hgpa'><input type='checkbox' name='jform[equipment_or_amenities_you_have_available]["+equp_indx+"][days][P]' value='1'  >P</span><span class='hgpa'><input type='checkbox' name='jform[equipment_or_amenities_you_have_available]["+equp_indx+"][days][A]' value='1'  >A</span></span>");
				}else{
					jQuery('.equipment_days'+jQuery(this).val()).find('.hgpa_main').remove();
				}
			});
});