<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;
//error_reporting(0);
JHtml::_('behavior.keepalive');
//JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('bootstrap.framework');	

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);

$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/questionform/tmpl/css/question.css', $type = 'text/css');
$doc->addScript(JURI::base().'components/com_questionnaire/views/questionform/tmpl/js/question.js');

$db			= JFactory::getDBO();
$user			= JFactory::getUser();
$loginUserId	= (int) $user->get('id');

$query	= 'SELECT * FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '
		. "WHERE id= $loginUserId";

$db->setQuery( $query );
$user_obj	= $db->loadObject();

$measuremts_data=@json_decode($user_obj->measuremts,'Array');

$user_amenities=array();
if(!empty($user_obj->equipment_or_amenities_you_have_available)){
	$user_amenities=json_decode($user_obj->equipment_or_amenities_you_have_available,'array');
}


$user_equipments=array();
if(!empty($user_obj->equipment_available)){
	$user_equipments=json_decode($user_obj->equipment_available,'array');
}

$user_where_you_train_day=array();
if(!empty($user_obj->where_you_train_day)){
	$user_where_you_train_day=json_decode($user_obj->where_you_train_day,'array');
}

$scenario_program=array();
if(!empty($user_obj->scenario_program)){
	$scenario_program=@json_decode($user_obj->scenario_program,'array');
}

$question_answers=array();
if(!empty($user_obj->question_answers)){
	$question_answers=@json_decode($user_obj->question_answers,'array');
}

$foods_condiments_like=array();
if(!empty($user_obj->foods_condiments_like)){
	$foods_condiments_like=@json_decode($user_obj->foods_condiments_like,'array');
}

?>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<script type="text/javascript">
jQuery().ready(function($){
<?php if($this->form->getValue('experience_lvl_working_out')!='0'): ?>
	$('#jform_experience_lvl_working_out').attr('disabled','disabled');
<?php endif; ?>	
<?php if($this->form->getValue('day_start_your_fitness_cardiovascular_program')!='0000-00-00'): ?>
	$('#jform_day_start_your_fitness_cardiovascular_program').attr('disabled','disabled');
	$('#jform_day_start_your_fitness_cardiovascular_program_img').remove();
<?php endif; ?>	
<?php if($this->form->getValue('nutrition_program_start')!='0000-00-00'): ?>
	$('#jform_nutrition_program_start').attr('disabled','disabled');
	$('#jform_nutrition_program_start_img').remove();
<?php endif; ?>	
});
</script>
<div id="my_questions" class="questionform_content">
<div class="row-fluid">
    <div class="question-edit front-end-edit" >
      <h2 class="page_head">Edit My Questionnaire</h2>
      <form id="form-question" action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=questionform.my_all_questions'); ?>" method="post" class="form-validate" enctype="multipart/form-data" >
    <div class="menber_lable" >
        
      <div class="accordion" id="miles_snakes_accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#new_member">
                New Member Information
                </a>
            </div>
            <div id="new_member" class="accordion-body collapse">
                <div class="accordion-inner">
                  <div class="control-group payment_space">
                    <div class="control-label">
                      <label title=""  for="jform_promotional_code" >If you have a Promotional Code please enter here</label>
                    </div>
                    <div class="controls promotion_input"> <?php echo $this->form->getInput('promotional_code'); ?> </div>
                  </div>
                  <div class="control-group payment_space">
                    <div class="control-label">
                      <label title=""  for="jform_fitness_business_code" >If you have a Fitness Business Code please enter here</label>
                    </div>
                    <div class="controls promotion_input"> <?php echo $this->form->getInput('fitness_business_code'); ?> </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#address">
               Address Information
                </a>
            </div>
            <div id="address" class="accordion-body collapse">
                <div class="accordion-inner">
                  <div class="control-group">
                    <div class="control-label">
                      <label title=""  for="jform_address1" >Address 1</label>
                    </div>
                    <div class="controls promotion_input"> <?php echo $this->form->getInput('address1'); ?> </div>
                  </div>
                  <div class="control-group">
                    <div class="control-label">
                      <label title=""  for="jform_address2" >Address 2</label>
                    </div>
                    <div class="controls promotion_input"> <?php echo $this->form->getInput('address2'); ?> </div>
                  </div>
                  <div class="control-group">
                    <div class="control-label">
                      <label title=""  for="jform_city" >City</label>
                    </div>
                    <div class="controls promotion_input"> <?php echo $this->form->getInput('city'); ?> </div>
                  </div>
                  <div class="control-group">
                    <div class="control-label">
                      <label title=""  for="jform_state" >State</label>
                    </div>
                    <div class="controls promotion_input"> <?php echo $this->form->getInput('state'); ?> </div>
                  </div>
                  <div class="control-group">
                    <div class="control-label">
                      <label title=""  for="jform_zip" >Zip</label>
                    </div>
                    <div class="controls promotion_input"> <?php echo $this->form->getInput('zip'); ?> </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#payment">
                Payment and Credit Card Information
                </a>
            </div>
            <div id="payment" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="control-group payment_space">
                      <div class="control-label">
                        <label title=""  for="jform_fitness_membership" >Fitness Membership</label>
                      </div>
                      <div class="controls promotion_input"> <?php echo $this->form->getInput('fitness_membership'); ?> </div>
                    </div>
                    <div class="control-group payment_space">
                      <div class="control-label">
                        <label title=""  for="jform_fitness_affiliate_membership" >Fitness Affiliate Membership</label>
                      </div>
                      <div class="controls promotion_input"> <?php echo $this->form->getInput('fitness_affiliate_membership'); ?> </div>
                    </div>
                    <div class="control-group payment_space">
                      <div class="control-label">
                        <label title=""  for="jform_bill_address1" >Billing Address 1</label>
                      </div>
                      <div class="controls promotion_input"> <?php echo $this->form->getInput('bill_address1'); ?> </div>
                    </div>
                    <div class="control-group payment_space">
                      <div class="control-label">
                        <label title=""  for="jform_bill_address2" >Billing Address 2</label>
                      </div>
                      <div class="controls promotion_input"> <?php echo $this->form->getInput('bill_address2'); ?> </div>
                    </div>
                    <div class="control-group payment_space">
                      <div class="control-label">
                        <label title=""  for="jform_bill_city" >City</label>
                      </div>
                      <div class="controls promotion_input"> <?php echo $this->form->getInput('bill_city'); ?> </div>
                    </div>
                    <div class="control-group payment_space">
                      <div class="control-label">
                        <label title=""  for="jform_bill_state" >State</label>
                      </div>
                      <div class="controls promotion_input"> <?php echo $this->form->getInput('bill_state'); ?> </div>
                    </div>
                    <div class="control-group payment_space">
                      <div class="control-label">
                        <label title=""  for="jform_bill_zip" >Zip Code</label>
                      </div>
                      <div class="controls promotion_input"> <?php echo $this->form->getInput('bill_zip'); ?> </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#mystats">
                My Stats and Information
                </a>
            </div>
            <div id="mystats" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="control-group payment_space">
                        <div class="control-label">
                          <label title=""  for="jform_gender" >What is your gender?</label>
                        </div>
                        <div class="controls calender_option"> <?php echo $this->form->getInput('gender'); ?> </div>
                      </div>
                      <div class="control-group payment_space">
                        <div class="control-label">
                          <label title=""  for="jform_height" >What is your height (in cm.)?</label>
                        </div>
                        <div class="controls promotion_input"> <?php echo $this->form->getInput('height'); ?> </div>
                      </div>
                      <div class="control-group payment_space">
                        <div class="control-label">
                          <label title=""  for="jform_weight" >What is your weight (in kg.)?</label>
                        </div>
                        <div class="controls promotion_input"> <?php echo $this->form->getInput('weight'); ?> </div>
                      </div>
                      <div class="control-group payment_space">
                        <div class="control-label">
                          <label title=""  for="jform_fat" >What is your body fat %?</label>
                        </div>
                        <div class="controls promotion_input"> <?php echo $this->form->getInput('fat'); ?> </div>
                      </div>
                      <div class="control-group payment_space">
                        <div class="control-label">
                          <label title=""  for="jform_goal_body_fat" >What is your goal body fat %?</label>
                        </div>
                        <div class="controls promotion_input"> <?php echo $this->form->getInput('goal_body_fat'); ?> </div>
                      </div>
                      <div class="control-group payment_space">
                        <div class="control-label">
                          <label title=""  for="jform_blood_type" >What is your blood type?</label>
                        </div>
                        <div class="controls calender_option"> <?php echo $this->form->getInput('blood_type'); ?> </div>
                      </div>
                      <div class="control-group payment_space">
                        <div class="control-label">
                          <label title=""  for="jform_birthday" >What is your birthday?</label>
                        </div>
                        <div class="controls"> <?php echo $this->form->getInput('birthday'); ?> </div>
                      </div>
                      <div class="restricted_area">
                        <h4 class="commen_heading  payment_info"> What are your measuremts? </h4>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_neck" >Neck</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['neck'];  ?>" id="jform_neck" name="jform[measuremts][neck]">
                          </div>
                        </div>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_chestline" >Chest Line</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['chestline'];  ?>" id="jform_chestline" name="jform[measuremts][chestline]">
                          </div>
                        </div>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_shoulders" >Shoulders</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['shoulders'];  ?>" id="jform_shoulders" name="jform[measuremts][shoulders]">
                          </div>
                        </div>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_upperarms" >Upper Arms</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['upperarms'];  ?>" id="jform_upperarms" name="jform[measuremts][upperarms]">
                          </div>
                        </div>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_forearms" >Forearms</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['forearms'];  ?>" id="jform_forearms" name="jform[measuremts][forearms]">
                          </div>
                        </div>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_waist" >Waist</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['waist'];  ?>" id="jform_waist" name="jform[measuremts][waist]">
                          </div>
                        </div>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_wrist" >Wrist</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['wrist'];  ?>" id="jform_wrist" name="jform[measuremts][wrist]">
                          </div>
                        </div>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_hips" >Hips</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['hips'];  ?>" id="jform_hips" name="jform[measuremts][hips]">
                          </div>
                        </div>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_midthighs" >Mid-Thighs</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['midthighs'];  ?>" id="jform_midthighs" name="jform[measuremts][midthighs]">
                          </div>
                        </div>
                        <div class="control-group payment_space">
                          <div class="control-label">
                            <label title=""  for="jform_calves" >Calves</label>
                          </div>
                          <div class="controls promotion_input">
                            <input type="text" size="32" value="<?php echo @$measuremts_data['calves'];  ?>" id="jform_calves" name="jform[measuremts][calves]">
                          </div>
                        </div>
            
                      <div class="control-group payment_space">
                        <div class="control-label">
                          <label title=""  for="jform_weekly_activity_lvl" >What is your weekly activity level on and off the job?</label>
                        </div>
                        <div class="controls calender_option"> <?php echo $this->form->getInput('weekly_activity_lvl'); ?> </div>
                      </div>
                </div>
            </div>
          </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#goal">
                Primary and Overall Goal(s)
                </a>
            </div>
            <div id="goal" class="accordion-body collapse">
                <div class="accordion-inner">
                 <div class="control-group">
                    <div class="control-label">
                      <label for="jform_primary_goal" >What is your Primary Goal?</label>
                    </div>
                    <div class="controls calender_option"> <?php echo $this->form->getInput('primary_goal'); ?> </div>
                  </div>
                  <div class="control-group">
                    <div class="control-label">
                      <label >What is your overall Goal(s)?</label>
                    </div>
                    <div class="controls goal_height"> <?php echo $this->form->getInput('overallgoal'); ?> </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#myfitnesspath">
               	My Fitness Path
                </a>
            </div>
            <div id="myfitnesspath" class="accordion-body collapse">
                <div class="accordion-inner">
                 <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label>Choose a Workout Path</label>
                    </div>
                    <div class="controls fild_form payment_space"> <?php echo $this->form->getInput('workout_path'); ?> </div>
                  </div>                  
                  <div class="control-group restricted_area">
                    <div class="control-label avai_equipment wokout_lable1">
                      <label>Please choose which equipment you have available to you. (Choose whether equipment is for Home (H) Park (P) Gym (G) or All (A))</label>
                    </div>
                    <div class="controls payment_space">
                      <ul id="equipment_id" class="equipment_product">
                        <?php 
                      $query	= 'SELECT A.id,A.name FROM ' . $db->quoteName( '#__questionnaire_equipment' )
                        . ' AS A JOIN '.$db->quoteName( '#__questionnaire_equipment_category' ).' AS B ON  A.category_id=B.id WHERE  B.id=1 and (A.state=1 and B.state=1) order by A.name asc';
            
                      $db->setQuery( $query );
                      foreach($db->loadObjectList() as $equipments):?>
                        <li>
                          <?php
                                $equipment_checked='';
                                    if(array_key_exists($equipments->id,$user_equipments)){
                                        $equipment_checked="checked='checked'";
                                    }
                                        
                                    echo "<input type='checkbox' class='equipment_class gap' name='jform[equipment_available][".$equipments->id."][id]' value='".$equipments->id."' $equipment_checked >".$equipments->name;
                                    
                                    if(!empty($equipment_checked)){								
                                        $current_equipments=$user_equipments[$equipments->id];
                                        if(!empty($current_equipments['days'])){
        
                                            $is_h_checked=(@$current_equipments['days']['H'])? "checked='checked'" : '';
                                            $is_g_checked=(@$current_equipments['days']['G'])? "checked='checked'" : '';
                                            $is_p_checked=(@$current_equipments['days']['P'])? "checked='checked'" : '';
                                            $is_a_checked=(@$current_equipments['days']['A'])? "checked='checked'" : '';
                                            
                                            echo "<span class='equipment_days".$equipments->id."' ><span class='hgpa_main'>";
                                            echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_available][$equipments->id][days][H]' $is_h_checked >H</span>";
                                            echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_available][$equipments->id][days][G]' $is_g_checked >G</span>";
                                            echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_available][$equipments->id][days][P]' $is_p_checked >P</span>";
                                            echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_available][$equipments->id][days][A]' $is_a_checked >A</span>";							
                                            echo "</span></span>";
                                        }else{
                                        echo "<span class='equipment_days".$equipments->id."'></span>";
                                        }
                                    
                                    }
                                    else{
                                        echo "<span class='equipment_days".$equipments->id."'></span>";
                                    }
                             ?>
                        </li>
                        <?php endforeach;			  
                      ?>
                      </ul>
                    </div>
                  </div>
                  <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label   for="jform_experience_lvl_working_out" >What is your experience level when it comes to working out?</label>
                    </div>
                    <div class="controls payment_space"> <?php echo $this->form->getInput('experience_lvl_working_out'); ?> </div>
                  </div>
                  <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label>How many days a week can you train and on what days? <br>(Please choose 3 to 6 days,recommended days are 5 to 6)</label>
                    </div>
                    <div class="controls fild_form payment_space"> <?php echo $this->form->getInput('you_train_days'); ?> </div>
                  </div>
                  <div class="control-group restricted_area">
                    <div class="control-label fild_form wokout_lable1">
                      <label    >Do you have a workout partner and if so what days?</label>
                    </div>
                    <div class="controls fild_form payment_space"> <?php echo $this->form->getInput('have_workout_partner'); ?>
                      <div id="workout_partner_days_sh" style="display:none"><?php echo $this->form->getInput('workout_partner_days'); ?></div>
                    </div>
                  </div>
                  <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label >Where will you be training and on what days? (Choose whether location is Home (H) Park (P) Gym (G) or All (A))</label>
                    </div>
                    <div class="controls payment_space">
                      <?php $where_you_train_day=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'); ?>
                      <ul id="where_you_train_day_id" class="equipment_product">
                        <?php 	 foreach($where_you_train_day as $where_you_train_day_val): ?>
                        <li>
                          <?php $where_you_train_day_checked=(array_key_exists($where_you_train_day_val,$user_where_you_train_day))? "checked='checked'" : ''; ?>
                          <input type="checkbox" value="<?php echo $where_you_train_day_val; ?>" name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][dayname]" class="where_you_train_day_class gap" <?php echo $where_you_train_day_checked; ?> >
                          <?php echo $where_you_train_day_val; ?>
                          <?php if(!empty($where_you_train_day_checked)): ?>
                          <span class="where_you_train_day_<?php echo $where_you_train_day_val; ?>">
                          <?php  
                                            $is_h_checked=(@$user_where_you_train_day[$where_you_train_day_val]['days']['H'])? "checked='checked'" : '';
                                            $is_g_checked=(@$user_where_you_train_day[$where_you_train_day_val]['days']['G'])? "checked='checked'" : '';
                                            $is_p_checked=(@$user_where_you_train_day[$where_you_train_day_val]['days']['P'])? "checked='checked'" : '';
                                            $is_a_checked=(@$user_where_you_train_day[$where_you_train_day_val]['days']['A'])? "checked='checked'" : '';
                                     ?>
                          <span class="hgpa_main"> <span class="hgpa">
                          <input type="checkbox" <?php echo $is_h_checked; ?> name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][days][H]" value="1">
                          H </span> <span class="hgpa">
                          <input type="checkbox" <?php echo $is_g_checked; ?> name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][days][G]" value="1">
                          G</span> <span class="hgpa">
                          <input type="checkbox" <?php echo $is_p_checked; ?> name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][days][P]" value="1">
                          P</span> <span class="hgpa">
                          <input type="checkbox" <?php echo $is_a_checked; ?> name="jform[where_you_train_day][<?php echo $where_you_train_day_val; ?>][days][A]" value="1">
                          A</span> </span> </span>
                          <?php else: ?>
                          <span class="where_you_train_day_<?php echo $where_you_train_day_val; ?>"></span>
                          <?php endif; ?>
                        </li>
                        <?php endforeach ?>
                      </ul>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#cardiovascular">
                My Cardiovascular Path
                </a>
            </div>
            <div id="cardiovascular" class="accordion-body collapse">
                <div class="accordion-inner">
                   <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label   for="jform_resting_heart" >What is your resting heart? (Take your pulse for 1 minute or 15 seconds and times it by 4)</label>
                    </div>
                    <div class="controls fild_form11"> <?php echo $this->form->getInput('resting_heart'); ?> </div>
                  </div>
                  <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label   for="jform_cardiovascular_exercise_goal" >What is your primary goal(s) when it comes to cardiovascular exercise?</label>
                    </div>
                    <div class="controls wokout_lable"> <?php echo $this->form->getInput('cardiovascular_exercise_goal'); ?> </div>
                  </div>
                  <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label   for="jform_cardiovascular_exercise_experience_lvl" >What is your experience level when it comes to cardiovascular exercise?</label>
                    </div>
                    <div class="controls calender_option1"> <?php echo $this->form->getInput('cardiovascular_exercise_experience_lvl'); ?> </div>
                  </div>
                  <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label  >Please choose which equipment or amenities you have available to you. (Choose whether equipment or amenities are for Home (H) Park (P) Gym (G) or All (A))</label>
                    </div>
                    <div class="controls amenities_checking">
                      <ul>
                        <?php 
                     $query	= 'SELECT A.id,A.name FROM ' . $db->quoteName( '#__questionnaire_equipment' )
                        . ' AS A JOIN '.$db->quoteName( '#__questionnaire_equipment_category' ).' AS B ON  A.category_id=B.id WHERE  B.id=2 and (A.state=1 and B.state=1) order by A.ordering';
            
                      $db->setQuery( $query );
                      foreach($db->loadObjectList() as $equipments):?>
                        <li>
                          <?php
                                $equipment_checked='';
                                    if(array_key_exists($equipments->id,$user_amenities)){
                                        $equipment_checked="checked='checked'";
                                    }
                                        
                                    echo "<input type='checkbox' class='equipmentcard_class gap' name='jform[equipment_or_amenities_you_have_available][".$equipments->id."][id]' value='".$equipments->id."' $equipment_checked >".$equipments->name;
                                    
                                    if(!empty($equipment_checked)){								
                                        $current_equipments=$user_amenities[$equipments->id];
                                        if(!empty($current_equipments['days'])){
        
                                            $is_h_checked=(@$current_equipments['days']['H'])? "checked='checked'" : '';
                                            $is_g_checked=(@$current_equipments['days']['G'])? "checked='checked'" : '';
                                            $is_p_checked=(@$current_equipments['days']['P'])? "checked='checked'" : '';
                                            $is_a_checked=(@$current_equipments['days']['A'])? "checked='checked'" : '';
                                            
                                            echo "<span class='equipment_days".$equipments->id."' ><span class='hgpa_main'>";
                                            echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][H]' $is_h_checked />H</span>";
                                            echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][G]' $is_g_checked />G</span>";
                                            echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][P]' $is_p_checked />P</span>";
                                            echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][A]' $is_a_checked />A</span>";							
                                            echo "</span></span>";
                                        }else{
                                        echo "<span class='equipment_days".$equipments->id."'></span>";
                                        }
                                    
                                    }
                                    else{
                                        echo "<span class='equipment_days".$equipments->id."'></span>";
                                    }
                             ?>
                        </li>
                        <?php endforeach;	?>
                      </ul>
                    </div>
                  </div>
                  <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label   for="jform_day_start_your_fitness_cardiovascular_program" >Please select what day you would like to start your Fitness and Cardiovascular Program</label>
                    </div>
                    <div class="controls wokout_lable2"> <?php echo $this->form->getInput('day_start_your_fitness_cardiovascular_program'); ?> </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#miles_snakes_accordion" href="#nutrition">
                My Nutrition Path
                </a>
            </div>
            <div id="nutrition" class="accordion-body collapse">
                <div class="accordion-inner">
                 <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label   for="jform_eat_a_day_meal_snacks" >How many times do you usually eat a day including meals and snacks?</label>
                    </div>
                    <div class="controls calender_option1"> <?php echo $this->form->getInput('eat_a_day_meal_snacks'); ?> </div>
                  </div>
                  <div class="control-group restricted_area">
                    <div class="control-label wokout_lable1">
                      <label   for="jform_nutrition_program_start" >Select what day you would like your Nutrition Program to start</label>
                    </div>
                    <div class="controls wokout_lable2"> <?php echo $this->form->getInput('nutrition_program_start'); ?> </div>
                  </div>
                  <fieldset class="fild_form1">
                  <legend>Which scenario best describes your day to day lifestyle regarding meal preparation</legend>
                  <div class="control-group something_right1">
                    <div class="control-label">
                      <label for="jform_scenario_program_break_lunch_dinner" >Can prepare/cook breakfast, lunch and dinner</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[scenario_program][break_lunch_dinner]" id="jform_scenario_program_break_lunch_dinner">
                        <option value="Always"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$scenario_program['break_lunch_dinner']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right1">
                    <div class="control-label">
                      <label for="jform_scenario_program_break_dinner" >Can prepare/cook breakfast and dinner</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[scenario_program][break_dinner]" id="jform_scenario_program_break_dinner">
                        <option value="Always"  <?php   echo (@$scenario_program['break_dinner']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$scenario_program['break_dinner']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$scenario_program['break_dinner']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$scenario_program['break_dinner']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$scenario_program['break_dinner']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right1">
                    <div class="control-label">
                      <label  for="jform_scenario_program_dinner" > Can only prepare/cook dinner</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[scenario_program][dinner]" id="jform_scenario_program_dinner">
                        <option value="Always"  <?php   echo (@$scenario_program['dinner']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$scenario_program['dinner']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$scenario_program['dinner']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$scenario_program['dinner']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$scenario_program['dinner']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right1">
                    <div class="control-label">
                      <label  for="jform_night_sleep_hours" >How many hours of sleep do you usually average a night?</label>
                    </div>
                    <div class="controls calender_option1"> <?php echo $this->form->getInput('night_sleep_hours'); ?> </div>
                  </div>
                  </fieldset>
                  <fieldset class="fild_form1">
                  <legend>Please answer the following questions as honestly as possible</legend>
                  <div class="control-group something_right1">
                    <div class="control-label">
                      <label   for="jform_eat_sweet_fruit_candy" >If I eat something sweet such as fruit or candy I pick right up</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[question_answers][eat_sweet_fruit_candy]" id="jform_eat_sweet_fruit_candy">
                        <option value="Always"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$question_answers['eat_sweet_fruit_candy']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right">
                    <div class="control-label">
                      <label   for="jform_eat_meat_daily" >I could eat red meat daily</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[question_answers][eat_meat_daily]" id="jform_eat_meat_daily">
                        <option value="Always"  <?php   echo (@$question_answers['eat_meat_daily']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$question_answers['eat_meat_daily']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$question_answers['eat_meat_daily']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$question_answers['eat_meat_daily']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$question_answers['eat_meat_daily']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right">
                    <div class="control-label">
                      <label   for="jform_eat_steamed_vegetables" >I could eat steamed or raw vegetables daily</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[question_answers][eat_steamed_vegetables]" id="jform_eat_steamed_vegetables">
                        <option value="Always"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$question_answers['eat_steamed_vegetables']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right">
                    <div class="control-label">
                      <label   for="jform_find_myself_snacking" >I often find myself snacking throughout the day</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[question_answers][find_myself_snacking]" id="jform_find_myself_snacking">
                        <option value="Always"  <?php   echo (@$question_answers['find_myself_snacking']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$question_answers['find_myself_snacking']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$question_answers['find_myself_snacking']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$question_answers['find_myself_snacking']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$question_answers['find_myself_snacking']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right">
                    <div class="control-label">
                      <label   for="jform_eat_meat_feel_right" >If I eat meat I feel like I it picks me right up and gives me energy </label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[question_answers][eat_meat_feel_right]" id="jform_eat_meat_feel_right">
                        <option value="Always"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$question_answers['eat_meat_feel_right']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right">
                    <div class="control-label">
                      <label   for="jform_crave_sweets" >I often crave sweets</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[question_answers][crave_sweets]" id="jform_crave_sweets">
                        <option value="Always"  <?php   echo (@$question_answers['crave_sweets']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$question_answers['crave_sweets']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$question_answers['crave_sweets']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$question_answers['crave_sweets']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$question_answers['crave_sweets']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right">
                    <div class="control-label">
                      <label   for="jform_feel_sluggish_not_eat_meat" >I often feel sluggish if I don't eat every two or three hours</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[question_answers][feel_sluggish_not_eat_meat]" id="jform_feel_sluggish_not_eat_meat">
                        <option value="Always"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$question_answers['feel_sluggish_not_eat_meat']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right">
                    <div class="control-label">
                      <label   for="jform_drinking_meal_replacements" >I often find myself drinking Meal Replacements or Protein Sakes</label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[question_answers][drinking_meal_replacements]" id="jform_drinking_meal_replacements">
                        <option value="Always"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$question_answers['drinking_meal_replacements']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group something_right">
                    <div class="control-label">
                      <label   for="jform_tend_get_constipated" >I tend to get constipated </label>
                    </div>
                    <div class="controls calender_option1">
                      <select name="jform[question_answers][tend_get_constipated]" id="jform_tend_get_constipated">
                        <option value="Always"  <?php   echo (@$question_answers['tend_get_constipated']=='Always')? "selected='selected'" : '';   ?> >Always</option>
                        <option value="Very Often"  <?php   echo (@$question_answers['tend_get_constipated']=='Very Often')? "selected='selected'" : ''; ?> >Very Often</option>
                        <option value="Sometimes"  <?php   echo (@$question_answers['tend_get_constipated']=='Sometimes')? "selected='selected'" : ''; ?> >Sometimes</option>
                        <option value="Rarely"  <?php   echo (@$question_answers['tend_get_constipated']=='Rarely')? "selected='selected'" : ''; ?> >Rarely</option>
                        <option value="Never"  <?php   echo (@$question_answers['tend_get_constipated']=='Never')? "selected='selected'" : '';  ?> >Never</option>
                      </select>
                    </div>
                  </div>
                  </fieldset>
                  <div class="restricted_area" style="float:left">
                    <fieldset class="food_condiments">
                    <legend>Please choose which foods and condiments you like in each category</legend>
                    <?php
                    $query	= 'SELECT id,name FROM ' . $db->quoteName( '#__questionnaire_nutrition_category' ).'  WHERE state=1 and id!=20 order by name ASC';
                    
                    $db->setQuery( $query );
                      
                    foreach($db->loadObjectList() as $nutritions_cat):			
                    ?>
                    <table class="food_condiments_table">
                      <tr>
                        <th><?php echo $nutritions_cat->name; ?></th>
                      </tr>
                      <tr>
                        <td><table>
                            <?php  
                                 $nutrition_cat_id=$nutritions_cat->id;
        
                                 $query	= "SELECT id,name FROM " . $db->quoteName( '#__questionnaire_nutrition_food' ) . " WHERE category_id=$nutrition_cat_id and state=1 order by  name ASC";
                                 $db->setQuery( $query );
                                 $nutrition_lst=0;
                                 foreach($db->loadObjectList() as $nutrition_items):
                                 $nutrition_id = $nutrition_items->id;
                                 $nutrition_name = $nutrition_items->name;		
                                 $car_nut_labl_suffix=$nutrition_cat_id.'_'.$nutrition_id;				 
                                  ?>
                            <?php if($nutrition_lst%3==0): ?>
                            <tr>
                              <?php endif;  ?>
                              <td><input type="checkbox" <?php   echo (@$foods_condiments_like[$nutrition_cat_id][$nutrition_id]==$nutrition_id)? "checked='checked'" : '';  ?> value="<?php echo $nutrition_id; ?>"   name="jform[foods_condiments_like][<?php echo $nutrition_cat_id; ?>][<?php echo $nutrition_id;?>]" id="jform_cardiovascular_exercise_<?php echo $car_nut_labl_suffix; ?>"></td>
                              <td><label  for="jform_cardiovascular_exercise_<?php echo $car_nut_labl_suffix; ?>"><?php echo $nutrition_name; ?></label></td>
                              <?php if(($nutrition_lst+1)%3==0): ?>
                            </tr>
                            <?php endif;  ?>
                            <?php $nutrition_lst++; endforeach; ?>
                          </table></td>
                      </tr>
                    </table>
                    <?php
                    endforeach;			
                    ?>
                    </fieldset>
                  </div>
                </div>
            </div>
        </div>
	  </div>
      <div class="control-group">
        <div class="control-label"> </div>
        <div class="controls">
          <div class="submit_member11">
            <button type="submit" class="validate button btn btn-success all_answers_submit btn-large submit_method"><span>Update</span></button>
            <input type="hidden" name="action" value="questionform" />
            <input type="hidden" name="action_success_message" value="My_questionnaire_saved_successfully" />
            <input type="hidden" name="action_fail_message" value="Error_saving_my_questionnaire_information" />
            <input type="hidden" name="option" value="com_questionnaire" />
            <input type="hidden" name="task" value="questionform.my_all_questions" />
            <input type="hidden" name="Itemid" value="<?php echo JRequest::getint( 'Itemid' ); ?>" />
            <?php echo JHtml::_('form.token'); ?> </div>
        </div>
      </div>
        </div>
      </form>
    </div>
  </div>
</div>