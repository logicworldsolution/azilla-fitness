<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
//JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);


?>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<div class="questionform_content">
<div class="row-fluid">
    <div class="question-edit front-end-edit" >
      <h1 class="nw_member_info">New Member Information</h1>
      <form id="form-question" action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=questionform.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <div class="menber_lable">
          <div class="control-group">
            <div class="control-label">
              <label title="" class="hasTip" for="jform_promotional_code" >If you have a Promotional Code please enter here</label>
            </div>
            <div class="controls promotion_input"> <?php echo $this->form->getInput('promotional_code'); ?> </div>
          </div>
          <div class="control-group">
            <div class="control-label">
              <label title="" class="hasTip" for="jform_fitness_business_code" >If you have a Fitness Business Code please enter here</label>
            </div>
            <div class="controls promotion_input"> <?php echo $this->form->getInput('fitness_business_code'); ?> </div>
          </div>
          <div class="control-group">
            <div class="control-label"> </div>
            <div class="controls">
              <div class="submit_member">
                <button type="submit" class="validate button btn btn-large submit_method"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
                <input type="hidden" name="option" value="com_questionnaire" />
                <input type="hidden" name="task" value="questionform.save" />
                <input type="hidden" name="action" value="new_member" />
                <input type="hidden" name="action_success_message" value="Member_information_saved_successfully" />
                <input type="hidden" name="action_fail_message" value="Error_saving_member_information" />
                <input type="hidden" name="Itemid" value="<?php echo JRequest::getint( 'Itemid' ); ?>" />
                <?php echo JHtml::_('form.token'); ?> </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>