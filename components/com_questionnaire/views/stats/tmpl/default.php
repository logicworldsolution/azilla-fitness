<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

require(JPATH_SITE.'/components/com_questionnaire/helpers/function.php');	 

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);
$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/stats/tmpl/css/stats.css', $type = 'text/css');
$db = JFactory::getDbo();	

$loginuser	= JFactory::getUser();
$loginUserId	= (int) $loginuser->get('id');

$query	= 'SELECT id,gender,height,weight,fat,goal_body_fat,blood_type,birthday,measuremts,resting_heart,weekly_activity_lvl,primary_goal,you_train_days,night_sleep_hours,question_answers FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '
. "WHERE id= $loginUserId";

$db->setQuery( $query );

$user_fitness_obj	= $db->loadObject();

$measuremts=@json_decode($user_fitness_obj->measuremts);

$weight_pound=@$user_fitness_obj->weight*2.20462;
$height_inch=@$user_fitness_obj->height*0.393701;
$bmi= round(703*$weight_pound/($height_inch*$height_inch),2);
?>
<script type="text/javascript">
jQuery().ready(function($){
	$("#datepicker_stat").datepicker({
		dateFormat:'m-d-yy',
		beforeShowDay: disableAllTheseDays,
		showOtherMonths: true,
		onSelect: function(dateText, inst) {
			if($.inArray(dateText,disabledDays)>=0){
				window.location = 'index.php?option=com_questionnaire_exercise&view=program&Itemid=247&caldate='+dateText; 
			}
		}
	});
});
</script>
<h2 class="page_head">My Profile</h2>
<div class="bs-docs-grid">
  <div class="mystatscontainer" >
    <div class="control-group restricted_area stats-parent" >
      <div class="my-stat">
        <label class="stats-parent-label" >My Stats</label>
      </div>
      <table class="stat_table">
        <tr>
          <td class="border_right">Age</td>
          <td><?php 
			if($user_fitness_obj->birthday!='0000-00-00'){
				$d1 = new DateTime($user_fitness_obj->birthday);
				$d2 = new DateTime(date('Y-m-d'));

				$diff = $d2->diff($d1);

				echo $user_age=$diff->y;
			}
			?>
          </td>
        </tr>
        <tr>
          <td class="border_right">Name</td>
          <td><?php echo @$loginuser->name; ?></td>
        </tr>
        <tr>
          <td class="border_right">Gender</td>
          <td><?php
			$gender=array(1=>'Male',2=>'Female');
			$user_gender=@$user_fitness_obj->gender;
			echo @$gender[@$user_fitness_obj->gender]; ?>
          </td>
        </tr>
        <tr>
          <td class="border_right">Weight</td>
          <td><?php echo $user_weight= @$user_fitness_obj->weight;		  
		   ?></td>
        </tr>
        <tr>
          <td class="border_right">Height</td>
          <td><?php echo $user_height= @$user_fitness_obj->height; ?></td>
        </tr>
        <tr>
          <td class="border_right">Body fat(%)</td>
          <td><?php echo @$user_fitness_obj->fat; ?></td>
        </tr>
        <tr>
          <td class="border_right">Goal body fat(%)</td>
          <td><?php echo @$user_fitness_obj->goal_body_fat; ?></td>
        </tr>
        <tr>
          <td class="border_right">Blood type</td>
          <td><?php echo @$user_fitness_obj->blood_type; ?></td>
        </tr>
      </table>
    </div>
    <div class="datepicker_stat_container" >
      <div id="datepicker_stat"></div>
      <div class="chkmail" >
      	<button type="button" class="btn btn-success save-this-meal">Check My Email</button>
      </div>
    </div>
  </div>
  <div class="measurementcontainer" >
    <div class="control-group restricted_area stats-parent" >
      <div class="my-stat">
        <label class="stats-parent-label textaligncenter">My Measurements</label>
      </div>
      <table class="stat_table">
        <tbody><tr>
          <td class="border_right">Neck</td>
          <td><?php echo @$measuremts->neck; ?></td>
        </tr>
        <tr>
          <td class="border_right">Chest Line</td>
          <td><?php echo @$measuremts->chestline; ?></td>
        </tr>
        <tr>
          <td class="border_right">Shoulders</td>
          <td><?php echo @$measuremts->shoulders; ?></td>
        </tr>
        <tr>
          <td class="border_right">Upper Arms</td>
          <td><?php echo @$measuremts->upperarms; ?> </td>
        </tr>
        <tr>
          <td class="border_right">Forearms</td>
          <td><?php echo @$measuremts->forearms; ?></td>
        </tr>
        <tr>
          <td class="border_right">Waist</td>
          <td><?php echo @$measuremts->waist; ?></td>
        </tr>
        <tr>
          <td class="border_right">Wrist</td>
          <td><?php echo @$measuremts->wrist; ?></td>
        </tr>
        <tr>
          <td class="border_right">Hips</td>
          <td><?php echo @$measuremts->hips; ?> </td>
        </tr>
        <tr>
          <td class="border_right">Mid-Thighs</td>
          <td><?php echo @$measuremts->midthighs; ?></td>
        </tr>
        <tr>
          <td class="border_right">Calves</td>
          <td><?php echo @$measuremts->calves; ?></td>
        </tr>
      </tbody></table>
    </div>
    <div class="datepicker_stat_container bordercac marginbot_30px" >
      <div class="my-stat">
        <label class="stats-parent-label textaligncenter">My Target Heart Rate</label>
      </div>
      <table class="stat_table">
        <tbody>
        <tr>
          <td class="border_right width64pc">Maintain</td>
          <td><?php 
		 $maintain= (((220 - $user_age)-@$user_fitness_obj->resting_heart)*0.63)+@$user_fitness_obj->resting_heart;		
		 echo round($maintain,2);
		   ?></td>
        </tr>
        <tr>
          <td class="border_right">Fitness/Fat Burn</td>
          <td><?php 
		 $fat_burn= (((220 - $user_age)-@$user_fitness_obj->resting_heart)*0.63)+@$user_fitness_obj->resting_heart;		
		 echo round($fat_burn,2);
		   ?></td>
        </tr> 
        <tr>
          <td class="border_right">Aerobic/Cardio Endurance</td>
          <td><?php 
		 $aerobic= (((220 - $user_age)-@$user_fitness_obj->resting_heart)*0.73)+@$user_fitness_obj->resting_heart;
		  echo round($aerobic,2);		
		   ?></td>
        </tr>
        <tr>
          <td class="border_right">Anaerobic</td>
          <td><?php 
		 $anaerobic=(((220 - $user_age)-@$user_fitness_obj->resting_heart)*0.83)+@$user_fitness_obj->resting_heart;		
		 echo round($anaerobic,2);
		   ?></td>
        </tr>      
      </tbody></table>
    </div>
    <div class="datepicker_stat_container bordercac" >
      <div class="my-stat">
        <label class="stats-parent-label textaligncenter">Other Stats</label>
      </div>
      <table class="stat_table">
        <tbody><tr>
          <td class="border_right width64pc">BMR</td>
          <td><?php 
		  if($bmi>=30){
				$bmr=370 + (9.79759519 * $bmi);
				echo round($bmr,2);
		  }else{
			  if($user_gender==1){
				 $bmr=66 + (13.7 * $user_weight) + (5 * $user_height) - (6.8 * $user_age);
			  }elseif($user_gender==2){
				 $bmr=655 + (9.6 * $user_weight) + (1.8 * $user_height) - (4.7 * $user_age);
			  } 
			  echo round($bmr,2);
		  }
		  ?></td>
        </tr>
        <tr>
          <td class="border_right">Lean Body Mass/Fat Mass</td>
          <td><?php 
		  if(isset($user_fitness_obj->fat) and !empty($user_fitness_obj->fat) and !empty($user_weight)){
			$lean_body_mass=(100-$user_fitness_obj->fat)/100*$user_weight;			
		  }else{
			 if($user_gender==1){
				 $factor1=($user_weight*1.082) + 94.42;
				 $factor2=@$measuremts->waist*4.15;
				 $lean_body_mass=$factor1-$factor2;
			  }elseif($user_gender==2){
				 $factor1=($user_weight*0.732) + 8.987;
				 $factor2=@$measuremts->wrist/3.140;
				 $factor3=@$measuremts->waist*0.157;
				 $factor4=@$measuremts->hips*0.249;
				 $factor5=@$measuremts->forearms*0.434;
				 $lean_body_mass=$factor1+$factor2-$factor3-$factor4+$factor5;
			  }
		  }
		  echo round($lean_body_mass,2);
		   ?></td>
        </tr>
        <tr>
          <td class="border_right">Estimated Body Fat (%)</td>
          <td><?php 
		    /*$gender_factor=($user_gender==1)? 1 :0 ;
			$bfp = (1.20 * $bmi) + (0.23 * $user_age) - (10.8 * $gender_factor) - 5.4;
			$bfp=100-(100*$lean_body_mass/$user_weight);*/
			
			$body_fat_weight=$user_weight-$lean_body_mass;
			$body_fat_percentage=$body_fat_weight*100/$user_weight;
			echo $body_fat_percentage;
		   ?></td>
        </tr>
         <tr>
          <td class="border_right">RHR</td>
          <td><?php 
		 echo @$user_fitness_obj->resting_heart;		
		   ?></td>
        </tr>
         <tr>
          <td class="border_right">BMI</td>
          <td><?php  echo $bmi; ?></td>
        </tr>
        <tr>
          <td class="border_right">Daily Water Intake</td>
          <td><?php echo round(@$user_fitness_obj->weight*0.657,2);  ?></td>
        </tr>
      </tbody></table>
    </div>    
  </div>
</div>
<?php
$tdee="";
if($user_fitness_obj->weekly_activity_lvl=='Sedentary'){
	$tdee=$bmr*1.2;
}elseif($user_fitness_obj->weekly_activity_lvl=='Lightly active'){
	$tdee=$bmr*1.375 ;
}elseif($user_fitness_obj->weekly_activity_lvl=='Moderately active'){
	$tdee=$bmr*1.55;
}elseif($user_fitness_obj->weekly_activity_lvl=='Very active'){
	$tdee=$bmr*1.725;
}elseif($user_fitness_obj->weekly_activity_lvl=='Extra active'){
	$tdee=$bmr*1.9;
}

if(!empty($user_fitness_obj->blood_type)){
	if($user_fitness_obj->blood_type=='A'){
		$carbs_intake=60;
		$fat_intake=20;
		$protein_intake=20;	
	}elseif($user_fitness_obj->blood_type=='B'){
		$carbs_intake=50;
		$fat_intake=20;
		$protein_intake=30;	
	}elseif($user_fitness_obj->blood_type=='AB'){
		$carbs_intake=55;
		$fat_intake=20;
		$protein_intake=25;	
	}elseif($user_fitness_obj->blood_type=='O'){
		$carbs_intake=40;
		$fat_intake=30;
		$protein_intake=30;	
	}
}else{
	$carbs_intake=55;
	$fat_intake=20;
	$protein_intake=25;	
}

$calories_intake=$bmr;

if($user_fitness_obj->primary_goal=='Muscle Gain'){
	$calories_temp=($user_gender==1)? 250 :150;
	$calories_intake=$calories_intake+$calories_temp;
}elseif($user_fitness_obj->primary_goal=='Fat Loss'){
	$calories_temp=($user_gender==1)? 250 :150;
	$calories_intake=$calories_intake-$calories_temp;
}elseif($user_fitness_obj->primary_goal=='Maintenance'){
	//Do nothing
}
if(!empty($user_fitness_obj->you_train_days)){
	$you_train_days_Arr=explode(',',$user_fitness_obj->you_train_days);
	$you_train_days_count= count($you_train_days_Arr);
	if($you_train_days_count==6){
		$calories_intake=$calories_intake+350;
	}
	elseif($you_train_days_count==5){
		$calories_intake=$calories_intake+250;
	}
	elseif($you_train_days_count==4 or $you_train_days_count==3){
		$calories_intake=$calories_intake-250;
	}
}

if(!empty($user_fitness_obj->night_sleep_hours)){
	if($user_fitness_obj->night_sleep_hours==5){
		$calories_intake=$calories_intake+50;
	}elseif($user_fitness_obj->night_sleep_hours==4){
		$calories_intake=$calories_intake+100;
	}elseif($user_fitness_obj->night_sleep_hours==3){
		$calories_intake=$calories_intake+150;
	}elseif($user_fitness_obj->night_sleep_hours<3){
		$calories_intake=$calories_intake+200;
	}
}


$question_answers=@json_decode($user_fitness_obj->question_answers);

foreach($question_answers as $question_answers_val){
	if($question_answers_val=='Always'){
		$carbs_intake=$carbs_intake+3;
		$fat_intake=$fat_intake-2;
		$protein_intake=$protein_intake-1;		
	}elseif($question_answers_val=='Very Often'){
		$carbs_intake=$carbs_intake+2;
		$fat_intake=$fat_intake-1;
		$protein_intake=$protein_intake-1;
	}elseif($question_answers_val=='Sometimes'){
		$carbs_intake=$carbs_intake+1;
		$fat_intake=$fat_intake-1;
		//$protein_intake=$protein_intake;
	}elseif($question_answers_val=='Rarely'){
/*		$carbs_intake=$carbs_intake;
		$fat_intake=$fat_intake;
		$protein_intake=$protein_intake;*/
	}elseif($question_answers_val=='Never'){
		$carbs_intake=$carbs_intake-2;
		$fat_intake=$fat_intake+1;
		$protein_intake=$protein_intake+2;
	}
}

if($calories_intake<1250){
	$calories_intake=1250;
}

$sql = "SELECT max(start_date) end_date FROM `#__userworkouts_level` WHERE user_id=$loginUserId";
$db->setQuery($sql);
if($db->loadObject()->end_date!=NULL){
	$workout_end_date= $db->loadObject()->end_date;
}
$user_train_days=$user_fitness_obj->you_train_days;

$sql="SELECT * FROM #__userworkouts_info WHERE user_id=$loginUserId";
$db->setQuery($sql);
$get_user_data=$db->loadObject();

$user_fitness_curdate=date('Y-m-d');
//Update the user workout levels if user update exercise dates
if(!empty($get_user_data->train_days) and !empty($user_train_days) and $get_user_data->train_days!=$user_train_days){

	$query="SELECT * FROM #__userworkouts_level WHERE user_id=$loginUserId ORDER BY start_date ASC";
	$db->setQuery($query);
	$user_workout_level=$db->loadObjectList();
	$update_dates=false;
	if(isset($user_workout_level) and !empty($user_workout_level)){
		foreach($user_workout_level as $user_workout_levelData){
			if($user_workout_levelData->start_date>=$user_fitness_curdate or $update_dates){
				
				$user_fitness_curdate= get_user_fitness_day($user_fitness_curdate,$you_train_days_Arr);
				
				$user_current_fitness_date_id=$user_workout_levelData->id;
	
				$query="UPDATE #__userworkouts_level SET start_date='$user_fitness_curdate' WHERE user_id=$loginUserId and id=$user_current_fitness_date_id";
				$db->setQuery($query);
				$db->query();
				$user_fitness_curdate=date('Y-m-d',strtotime("+1 day",strtotime($user_fitness_curdate)));
				$update_dates=true;
			}
		}	
	}
}

$sql = "SELECT sublevel_id FROM `#__userworkouts_level` WHERE user_id=$loginUserId  AND start_date<=curdate( ) ORDER BY start_date DESC limit 1";
$db->setQuery($sql);

if(isset($db->loadObject()->sublevel_id)){
	$user_sublevel_id=@$db->loadObject()->sublevel_id;
}else{
	$user_sublevel_id=1;
}

$query="INSERT INTO #__userworkouts_info(user_id,workout_end_date, 	train_days,bmr,bmi,tdee,lean_body_mass,body_fat_weight,body_fat_percentage,carbs_intake,fat_intake,protein_intake,calories_intake,experience_sublvl_working_out,updated_date) VALUES($loginUserId,'$workout_end_date','$user_train_days','$bmr','$bmi','$tdee','$lean_body_mass','$body_fat_weight','$body_fat_percentage','$carbs_intake','$fat_intake','$protein_intake','$calories_intake',$user_sublevel_id,'curdate()') ON DUPLICATE KEY UPDATE bmr='$bmr',bmi='$bmi',tdee='$tdee',lean_body_mass='$lean_body_mass',body_fat_weight='$body_fat_weight',body_fat_percentage='$body_fat_percentage',carbs_intake='$carbs_intake',fat_intake='$fat_intake',protein_intake='$protein_intake',calories_intake='$calories_intake',workout_end_date='$workout_end_date',train_days='$user_train_days',experience_sublvl_working_out=$user_sublevel_id,updated_date=curdate()";
$db->setQuery($query);
$db->query();
?>