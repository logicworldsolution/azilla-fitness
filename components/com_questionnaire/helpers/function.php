<?php

function get_user_fitness_day($cur_date,$user_fitness_days_arr){
	if(in_array(date('l',strtotime($cur_date)),$user_fitness_days_arr)){
		return $cur_date;
	}else{
		$cur_date=date('Y-m-d',strtotime('+1 day',strtotime($cur_date)));
		return get_user_fitness_day($cur_date,$user_fitness_days_arr);
	}
}


?>