<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$db = JFactory::getDbo();	

$loginUserId	= (int)JFactory::getUser()->get('id');

$matched_days_str='date.getDay() == 0 || date.getDay() == 1 || date.getDay() == 2 || date.getDay() == 3 || date.getDay() == 4 || date.getDay() == 5 || date.getDay() == 6';

$query="SELECT DATE_FORMAT(start_date, '%c-%e-%Y') start_date FROM #__userworkouts_level WHERE user_id=$loginUserId" ; 
$db->setQuery($query);

$user_workout_days_arr=array();
foreach($db->loadAssocList() as $user_workout_days){
	$user_workout_days_arr[]=$user_workout_days['start_date'];
}
$date_rng_json_encode=json_encode($user_workout_days_arr);
?>
<script type="text/javascript">
var disabledDays =<?php echo $date_rng_json_encode; ?>;

function disableAllTheseDays(date) {
	var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
	for (i = 0; i < disabledDays.length; i++) {
		if(jQuery.inArray((m+1) + '-' + d + '-' + y,disabledDays) != -1) {
			return [<?php echo $matched_days_str; ?>, "dp-highlight"];
		}
	}
	return [true];
}

jQuery(document).ready(function($) {	
	$("#datepicker_left").datepicker({
		dateFormat:'m-d-yy',
		beforeShowDay: disableAllTheseDays,
		showOtherMonths: true,
		onSelect: function(dateText, inst) {
			if($.inArray(dateText,disabledDays)>=0){
				window.location = 'index.php?option=com_questionnaire_exercise&view=program&Itemid=247&caldate='+dateText; 
			}
		}
	});
});
</script>
<div id="datepicker_left" ></div>