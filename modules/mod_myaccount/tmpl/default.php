<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$user = JFactory::getUser();
?>
<?php if($user->guest==1): ?>
<h1>Join our growing community</h1>
<ul>
	<li><a class="login" href="<?php echo JRoute::_('index.php?option=com_users&view=login'); ?>">Login</a></li>
	<li><a class="register" href="<?php echo JRoute::_('index.php?option=com_usermembership&view=membershiptypes'); ?>">Register</a></li>
	<!--<li><a class="register" href="<?php //echo JRoute::_('index.php?option=com_users&view=registration'); ?>">Register</a></li>-->
</ul>
<?php else: ?>
<ul class="margin0">
	 <div class="btn-group" >
		<div class="btn btn-large dropdown-toggle" onclick="document.logoutform.submit()" data-toggle="dropdown">
        	<form class='margin0' name='logoutform' action="<?php echo JRoute::_('index.php?option=com_users&task=user.logout'); ?>" method="post" class="form-horizontal">
				<a href="javascript:;" onclick="document.logoutform.submit()" >Logout</a>
				<?php echo JHtml::_('form.token'); ?>
			</form>
        </div>
	  </div>
</ul>
<?php endif; ?>