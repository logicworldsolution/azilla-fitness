<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$db = JFactory::getDbo();	

$jinput = JFactory::getApplication()->input;
$Itemid = $jinput->get('Itemid', '', 'filter');

$fitness_active='';
if(in_array($Itemid,array(247,248,249,250,251,311))){
	$fitness_active='active';
}
$nutrition_active='';
if(in_array($Itemid,array(240,241,242,243,244,245))){
	$nutrition_active='active';
}

$open_nidval = $jinput->get('open_nid', '', 'filter');

$open_fidval = $jinput->get('open_fid', '', 'filter');
?>
<script type='text/javascript' >
jQuery().ready(function(){
	jQuery('#myCarouseltool').carousel('pause');
});
</script>
<div>  
  <div id="myCarouseltool" class="carousel slide">
    <!-- Carousel items -->
    <div class="carousel-inner">
      <div class="<?php echo $fitness_active; ?> item">
      <h2 class="mytoolboxscroll"> <a href="#myCarouseltool" data-slide="prev" class="left_arrow"></a>My Fitness<a href="#myCarouseltool"  data-slide="next" class="right_arrow"></a></h2>
        <ul class="nav-child unstyled small toolbox_list mytoollst">
        <?php 
		$query="SELECT id,name FROM #__questionnaire_exercise_maincategory WHERE state=1 order by name asc";
		$db->setQuery($query);
		foreach($db->loadObjectList() as $fitness_categories){
			$open_fid=$fitness_categories->id;
			if($open_fidval==$fitness_categories->id){$acvite='class="active"';}else{$acvite='';}
		echo "<li $acvite ><a href='".JRoute::_('index.php?option=com_questionnaire_exercise&view=library&Itemid=248&open_fid='.$open_fid)."'>".$fitness_categories->name."</a></li>";
		
		} ?>	
        </ul>
      </div>
      <div class="<?php echo $nutrition_active; ?> item">
      <h2 class="mytoolboxscroll"> <a href="#myCarouseltool" data-slide="prev" class="left_arrow"></a>My Nutrition<a href="#myCarouseltool"  data-slide="next" class="right_arrow"></a></h2>
        <ul class="nav-child unstyled small toolbox_list">
        <?php 
		$query="SELECT id,name FROM #__questionnaire_nutrition_category WHERE state=1 order by name asc";
		$db->setQuery($query);
		$acvite='';
		foreach($db->loadObjectList() as $nutrition_categories){
				$open_nid=$nutrition_categories->id;
				if($open_nidval==$nutrition_categories->id){$acvite='class="active"';}else{$acvite='';}
			echo "<li $acvite ><a href='".JRoute::_('index.php?option=com_questionnaire_nutrition&view=library&Itemid=242&open_nid='.$open_nid)."'>".$nutrition_categories->name."</a></li>";
		
		} ?>	
        </ul>
      </div>
      <div class="item">
      <h2 class="mytoolboxscroll"> <a href="#myCarouseltool" data-slide="prev" class="left_arrow"></a>My Business<a href="#myCarouseltool"  data-slide="next" class="right_arrow"></a></h2>
        <ul class="nav-child unstyled small toolbox_list">
          <li><a href="#">DEMO Business1</a></li>
          <li><a href="#">DEMO Business2</a></li>
          <li><a href="#">DEMO Business3</a></li>
          <li><a href="#">DEMO Business4</a></li>
        </ul>
      </div>
    </div>
    <!-- Carousel nav -->
 </div>
</div>