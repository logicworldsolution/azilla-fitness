<?php
/**
 * @package    Joomla.Site
 *
 * @copyright  Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

if (version_compare(PHP_VERSION, '5.3.1', '<'))
{
	die('Your host needs to use PHP 5.3.1 or higher to run this version of Joomla!');
}

/**
 * Constant that is checked in included files to prevent direct access.
 * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
 */
define('_JEXEC', 1);

if (file_exists(__DIR__ . '/defines.php'))
{
	include_once __DIR__ . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	define('JPATH_BASE', __DIR__);
	require_once JPATH_BASE . '/includes/defines.php';
}

function pr($data){
	print_r($data);
	die;
}

require_once JPATH_BASE . '/includes/framework.php';
$db = JFactory::getDbo(); 

$query="SELECT A.eat_a_day_meal_snacks,A.foods_condiments_like,A.blood_type,A.nutrition_program_start,A.user_id FROM #__questionnaire_question A JOIN #__userworkouts_info B ON A.id=B.user_id";

$db->setQuery($query);

foreach($db->loadObjectList() as $user_nutrition_obj){
	$eat_a_day_meal_snacks=$user_nutrition_obj->eat_a_day_meal_snacks;
	$loginUserId=$user_nutrition_obj->user_id;
	$user_blood_type=$user_nutrition_obj->blood_type;

	//Get the user food items like in questions
	$recipe_list=array();
	if(!empty($user_nutrition_obj->foods_condiments_like)){
	$userQuestioFood=json_decode($user_nutrition_obj->foods_condiments_like,'array');
		foreach($userQuestioFood as $recipr_cat=>$userQuestioFood_items){ 
			foreach($userQuestioFood_items as $userQuestioFood_itemsVal){
				$recipe_list[]=$userQuestioFood_itemsVal;
			}
		}
	}
	$recipe_in=implode(',',$recipe_list);
	

	$nutrition_program_strtdate= $nutrition_program_dbstrtdate =$user_nutrition_obj->nutrition_program_start;
	$nutrition_program_enddate=date('Y-m-d',strtotime('+1 month ',strtotime($user_nutrition_obj->nutrition_program_start)));//+1 months

	//Create Meal items
	while($nutrition_program_strtdate<=$nutrition_program_enddate){
		/*$query="INSERT INTO #__usermeals_level(user_id,start_date) VALUES($loginUserId,'$nutrition_program_strtdate')";
		$db->setQuery($query);
		$db->query();
		$usermeals_date_lvl_id=$db->insertid();


		//Checks user blood type to search recipe.
		if(!empty($user_blood_type)){
			if($user_blood_type=='A'){
				$carbs_intake='55 and 65';
				$fat_intake='20 and 25';
				$protein_intake='20 and 25';	
			}elseif($user_blood_type=='B'){
				$carbs_intake='45 and 55';
				$fat_intake='20 and 30';
				$protein_intake='25 and 35';
			}elseif($user_blood_type=='AB'){
				$carbs_intake='50 and 60';
				$fat_intake='20 and 25';
				$protein_intake='25 and 35';
			}elseif($user_blood_type=='O'){
				$carbs_intake='40 and 50';
				$fat_intake='25 and 30';
				$protein_intake='25 and 35';	
			}
		}else{
				$carbs_intake='50 and 60';
				$fat_intake='20 and 25';
				$protein_intake='25 and 35';
		}

		$user_meal_number=(int)$eat_a_day_meal_snacks;
		$food_item_limit1=rand(1,2);
		$food_item_limit2=rand(2,3);
		
		//Create user meal like Meal 1, Meal 2,etc.
		for($j=1;$j<=$user_meal_number;$j++){
			$query="INSERT INTO	#__usermeals_meal(usermeals_level_id,meal_number)VALUES($usermeals_date_lvl_id,$j)";
			$db->setQuery($query);
			$db->query();
			
			$usermeals_meal_id=$db->insertid();
				
			//Query to search user recipes from recipe libraray based on user question/answers.
			$query="(SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 AND (id IN($recipe_in) AND (carbohydrates between $carbs_intake AND fat between $fat_intake AND protein between $protein_intake)) limit $food_item_limit1) UNION (SELECT id,serv_size,carbohydrates,fat,protein,calories FROM #__questionnaire_nutrition_food WHERE state=1 LIMIT $food_item_limit2)";

			$db->setQuery($query);
			
			//Create food items under each meals
			foreach($db->loadObjectList() as $fooddbList){
				$query="INSERT IGNORE INTO	#__usermeals(user_id,usermeals_meal_id,food_id,serv_size,carbohydrates,fat,protein,calories)
				VALUES(	$loginUserId,$usermeals_meal_id,$fooddbList->id,$fooddbList->serv_size,$fooddbList->carbohydrates,$fooddbList->fat,$fooddbList->protein,$fooddbList->calories)";
				$db->setQuery($query);
				$db->query();
			}
		}
*/

//
		//Check the 30 days food items are created ?
/*		$querychk="SELECT  COUNT(id) as usermeal_nubm FROM #__usermeals_level WHERE user_id=$loginUserId  AND start_date BETWEEN '$nutrition_program_dbstrtdate' AND '$nutrition_program_enddate'";
		$db->setQuery($querychk);
		if($db->loadObject()->usermeal_nubm>=0){
*/		
		echo $nutrition_program_strtdate.'<br>';
		$nutrition_program_strtdate=date('Y-m-d',strtotime("+1 day",strtotime($nutrition_program_strtdate)));
	}

}
?>