<?php
/**
 * @package    Joomla.Administrator
 *
 * @copyright  Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

if (version_compare(PHP_VERSION, '5.3.1', '<'))
{
	die('Your host needs to use PHP 5.3.1 or higher to run this version of Joomla!');
}

/**
 * Constant that is checked in included files to prevent direct access.
 * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
 */
define('_JEXEC', 1);

if (file_exists(__DIR__ . '/defines.php'))
{
	include_once __DIR__ . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	define('JPATH_BASE', __DIR__);
	require_once JPATH_BASE.'/includes/defines.php';
}

require_once JPATH_BASE.'/includes/framework.php';
require_once JPATH_BASE.'/includes/helper.php';
require_once JPATH_BASE.'/includes/toolbar.php';

// Mark afterLoad in the profiler.
JDEBUG ? $_PROFILER->mark('afterLoad') : null;

// Instantiate the application.
$app = JFactory::getApplication('administrator');

// Initialise the application.
$app->initialise(
	array('language' => $app->getUserState('application.lang'))
);
?>
<script>
jQuery(document).ready(function($){
	$('.closepopup').click(function(e){
		e.preventDefault();
		$('.sentpayment').fadeOut(300);
		$('div').remove('.mask');
		$(".sentpayment").html();
		$('.sentpayment').delay(3000).removeAttr("style");
	});
	$('.mask').click(function(){
		$('.sentpayment').fadeOut(300);
		$('div').remove('.mask');
		$(".sentpayment").html();
		$('.sentpayment').delay(3000).removeAttr("style");
	});
});
</script>
<style>
.paybtnadmin{ background: #0088CC; border: medium none;  border-radius: 5px 5px 5px 5px;
color: #FFFFFF; font-size: 18px; padding: 5px 35px;}
.paybtnadmin:hover{background: #077ab4;}
</style>

<a href="#" class="closepopup"></a>
<form method="POST" action="<?php echo JRoute::_('index.php?option=com_usermembership&task=payment'); ?>" enctype="multipart/form-data" >
			<div id="request_form">
				<table class="params" style="border:solid 1px #ddd;">
		<tr style="background:#0088cc; color:#fff;">
			<th style="border:solid 1px #ddd;" class="param_name">Mail</th>
			<th style="border:solid 1px #ddd;" class="param_name">UserID</th>
			<th style="border:solid 1px #ddd;" class="param_name">Amount</th>
			<th style="border:solid 1px #ddd;" class="param_name">Currency Code</th>
		</tr>
<?php
$id = $_POST['id'];
$db = JFactory::getDBO();
$select = "SELECT * from #__usermembership_types where id='".$id."' and payment_status = '0'";
$db->setQuery($select);
$memid = $db->loadAssoc();
$sel = "SELECT *,#__user_affiliated_member.ref_code as referal from #__users inner join #__user_affiliated_member on #__user_affiliated_member.user_id = #__users.id where #__user_affiliated_member.id='".$id."'";
$db->setQuery($sel);
$affi_mem = $db->loadAssoc();
if($affi_mem['referal'] != '')
{
?>

<?php
$refrence1 = substr($affi_mem['referal'],3);
$sec_sel = "SELECT *,#__user_affiliated_member.ref_code as referal from #__users inner join (#__usermembership_types, #__user_affiliated_member) on (#__user_affiliated_member.user_id = #__users.id and #__usermembership_types.id = #__user_affiliated_member.mem_type_id) where #__usermembership_types.ref_code='".$refrence1."'";
$db->setQuery($sec_sel);
$affi_mem1 = $db->loadAssoc();
?>
<tr>
<td style="border-right:solid 1px #ddd;"  class="param_name"><input type="text" name="mail[]" value="<?php echo $affi_mem1['paypal_id']; ?>" size="25" maxlength="260" readonly /></td>
<td  style="border-right:solid 1px #ddd;" class="param_name"><input type="text" name="id[]" value="<?php echo $affi_mem1['username']; ?>"  size="25" maxlength="260" readonly /></td>
<td style="border-right:solid 1px #ddd;" class="param_name"><input type="text" name="amount[]" value="10"  size="25" maxlength="260" readonly /></td>
<td  class="param_name"><input type="text" name="currencyCode[]" value="USD"  size="25" maxlength="260" readonly />
<input type="hidden" value="<?php echo $id; ?>" name="thisid" />
<input type="hidden" value="<?php echo $affi_mem1['mem_type_id']; ?>" name="memberid[]" />
</td>
</tr>
<?php
if($affi_mem1['referal'] != '')
{

$refrence = substr($affi_mem1['referal'],3);
$thrd_sel = "SELECT *,#__user_affiliated_member.ref_code as referal from #__users inner join (#__usermembership_types, #__user_affiliated_member) on (#__user_affiliated_member.user_id = #__users.id and #__usermembership_types.id = #__user_affiliated_member.mem_type_id) where #__usermembership_types.ref_code='".$refrence."'";
$db->setQuery($thrd_sel);
$affi_mem2 = $db->loadAssoc();
?>
<tr>  
<td style="border-right:solid 1px #ddd;" class="param_name"><input type="text" name="mail[]" value="<?php echo $affi_mem2['paypal_id']; ?>" size="25" maxlength="260" readonly /></td>
<td style="border-right:solid 1px #ddd;" class="param_name"><input type="text" name="id[]" value="<?php echo $affi_mem2['username']; ?>"  size="25" maxlength="260" readonly /></td>
<td style="border-right:solid 1px #ddd;" class="param_name"><input type="text" name="amount[]" value="15"  size="25" maxlength="260" readonly /></td>
<td class="param_name"><input type="text" name="currencyCode[]" value="USD"  size="25" maxlength="260" readonly />
<input type="hidden" value="<?php echo $affi_mem2['mem_type_id']; ?>" name="memberid[]" /></td>
</tr>
<?php
if($affi_mem2['referal'] != '')
{
$refrence3 = substr($affi_mem2['referal'],3);
$fourth_sel = "SELECT *,#__user_affiliated_member.ref_code as referal from #__users inner join (#__usermembership_types, #__user_affiliated_member) on (#__user_affiliated_member.user_id = #__users.id and #__usermembership_types.id = #__user_affiliated_member.mem_type_id) where #__usermembership_types.ref_code='".$refrence3."'";
$db->setQuery($fourth_sel);
$affi_mem3 = $db->loadAssoc();
?>
<tr>
<td style="border-right:solid 1px #ddd;"  class="param_name"><input type="text" name="mail[]" value="<?php echo $affi_mem3['paypal_id']; ?>" size="25" maxlength="260" readonly /></td>
<td style="border-right:solid 1px #ddd;"  class="param_name"><input type="text" name="id[]" value="<?php echo $affi_mem3['username']; ?>"  size="25" maxlength="260" readonly /></td>
<td style="border-right:solid 1px #ddd;" class="param_name"><input type="text" name="amount[]" value="15"  size="25" maxlength="260" readonly /></td>
<td class="param_name"><input type="text" name="currencyCode[]" value="USD"  size="25" maxlength="260" readonly />
<input type="hidden" value="<?php echo $affi_mem3['mem_type_id']; ?>" name="memberid[]" /></td>
</tr>
<?php
}
}
?>
</table>
<?php include('Permissions/Permission.html.php');?>
<input class="paybtnadmin" type="submit" name="MassPayBtn" value="Pay Now" />
</div>
</form>
<?php
}
else
{
?>
<tr>
<td colspan="4">Sorry, No refferal number found.</td>
</tr>
</table>
<?php
//include('Permissions/Permission.html.php');
?>
<!--<input type="submit" name="MassPayBtn" value="MassPay" />-->
</div>
</form>
<?php
}
?>
