<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_questionnaire_nutrition/assets/css/questionnaire_nutrition.css');
?>
<style>
.vitamin_min_class{
	margin-left:5px;
}
.vitamin_min_row{
	margin-bottom:10px;
}
</style>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){
        
	
	js('#sbox-content a').live('click',function(){
		var jform_addvitamins_name=js.trim(js('#sbox-content input.jform_addvitamins_name').val());
		var jform_addvitamins_value=js.trim(js('#sbox-content input.jform_addvitamins_value').val());
		
		var boxnumber=js('#jform_addvitamins .vitamin_min_row').length+1;
		
		if(jform_addvitamins_name==''){
			alert('Please enter vitamins and minerals name.');
			return false;
		}
		else if(jform_addvitamins_value==''){
			alert('Please enter vitamins and minerals value.');
			return false;
		}else{
			js('#jform_addvitamins').append("<div class='vitamin_min_row'><input type='text' name='jform[additional_vitamins_and_minerals]["+boxnumber+"][name]' value="+jform_addvitamins_name+" >&nbsp;:&nbsp;"+"<input type='text' name='jform[additional_vitamins_and_minerals]["+boxnumber+"][value]' class='vitamin_min_class' value="+jform_addvitamins_value+" ><a class='remove_vam' href='javascript:;' >X</a></div>");
		}
	});
	
	js('#jform_addvitamins a.remove_vam').live('click',function(){
		js(this).parent().remove();	
	});	
		
	js('input:hidden.category_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('category_idhidden')){
			js('#jform_category_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_category_id").trigger("liszt:updated");
	

    });
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'nutritionfood.cancel'){
            Joomla.submitform(task, document.getElementById('nutritionfood-form'));
        }
        else{
            
				js = jQuery.noConflict();
				if(js('#jform_picture').val() != ''){
					js('#jform_picture_hidden').val(js('#jform_picture').val());
				}
            if (task != 'nutritionfood.cancel' && document.formvalidator.isValid(document.id('nutritionfood-form'))) {
                
                Joomla.submitform(task, document.getElementById('nutritionfood-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="nutritionfood-form" class="form-validate">
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset class="adminform">

                				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('state'); ?></div>
			</div>

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('category_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('category_id'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('group_name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('group_name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('brand_name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('brand_name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('serv_size'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('serv_size'); ?></div>
			</div>
  			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('unit_of_measurement'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('unit_of_measurement'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('calories'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('calories'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('carbohydrates'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('carbohydrates'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('protein'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('protein'); ?></div>
			</div>			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('saturated_fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('saturated_fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('trans_fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('trans_fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('cholesterol'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('cholesterol'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('sodium'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('sodium'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('dietary_fiber'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('dietary_fiber'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('sugars'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('sugars'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_a'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_a'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_c'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_c'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_d'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_d'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_k'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_k'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('folic_acid'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('folic_acid'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('calcium'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('calcium'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('potassium'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('potassium'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('iron'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('iron'); ?></div>
			</div>
   			<div class="control-group">
				<div class="control-label"><label title="" class="hasTooltip" for="jform_addvitamins" id="jform_addvitamins-lbl" data-original-title="Additional Vitamins and Minerals">Additional Vitamins and <br /> Minerals</label> </div>
				<div class="controls">
                <div id="jform_addvitamins">
                <?php
				$i=1;
				$additional_vitamins_and_minerals_arr=json_decode(json_decode($this->item->additional_vitamins_and_minerals),true);
				if(!empty($additional_vitamins_and_minerals_arr)){
					foreach($additional_vitamins_and_minerals_arr as $additional_vitamins_and_minerals_arrVal){
						echo "<div class='vitamin_min_row'><input type='text' name='jform[additional_vitamins_and_minerals][".$i."][name]' value=".$additional_vitamins_and_minerals_arrVal['name']." >&nbsp;:&nbsp;<input type='text' name='jform[additional_vitamins_and_minerals][".$i."][value]' class='vitamin_min_class' value=".$additional_vitamins_and_minerals_arrVal['value']." >&nbsp;<a href='javascript:;' class='remove_vam'>X</a></div>";
						$i++;				
					}
				}				
				 ?>
                </div>
				<a href="#additional_vm" rel="{size: {x: 300, y: 180}}" class="btn modal-button" onclick="IeCursorFix(); return false;">Add</a>
                    <div style="display:none">
                        <table id="additional_vm">
                        	<tr>
                            	<th colspan="2" align="center"><h3>Additional Vitamins and Minerals</h3></th>
                            </tr>   
                            <tr>
                            	 <td><div style="float:left; padding-bottom:11px; font-weight:bold">Name:</div></td><td><input type="text" size="40" class="inputbox jform_addvitamins_name" value="" ></td>
                             </tr>
                             <tr>
                           		 <td><div style="float:left; padding-bottom:11px; font-weight:bold">Value:</div></td><td><input type="text" size="40" class="inputbox jform_addvitamins_value" value="" ></td>
                            </tr>
                            <tr>
                            	<td>&nbsp;</td>
                            	<td>
                                	<a href="javascript:;"  id="jform_addvitamins_submit" class="btn" >Submit</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('blood_type'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('blood_type'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('notes'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('notes'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('picture'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('picture'); ?></div>
			</div>

				<?php if (!empty($this->item->picture)) : ?>
                       <img style="margin-bottom:20px;" src="<?php echo JRoute::_(JUri::base() . 'components/com_questionnaire_nutrition/asset/' . $this->item->picture, false);?>" width="250" />
				<?php endif; ?>
				<input type="hidden" name="jform[picture]" id="jform_picture_hidden" value="<?php echo $this->item->picture ?>" />	

            </fieldset>
        </div>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>