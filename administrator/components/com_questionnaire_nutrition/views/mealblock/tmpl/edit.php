<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      malwinder <malwinder@34interactive.com> - http://
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$db			= JFactory::getDBO();	
$session = JFactory::getSession();	
$session->clear('food_id');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_questionnaire_nutrition/assets/css/questionnaire_nutrition.css');

$group_name=array(1=>"Vegetables",2=>"Fat Free / Very Low-Fat Milk",3=>"Very Lean Protein",4=>"Lean Protein",5=>"Medium-Fat Protein",6=>"Fruits",7=>"Starches",8=>"Fats");
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){	
		js('.chzn-done').change(function(){
			var cur_group_name=js(this).parents('tr');
			var meal_id=js('#meal_id').val();
			var group_id=js(this).val();
			
			if(js(this).val()!=''){
				js.ajax({
					url:"<?php echo jRoute::_("index.php?option=com_questionnaire_nutrition&task=mealblock.get_food_items",false); ?>",
					data:'group_id='+group_id+"&meal_id="+meal_id,
					type:'post',
					dataType:'JSON',
					success:function(response){
						cur_group_name.find('td .group_id').val(group_id);
						cur_group_name.find('td .food_item_id').val(response.id);	
						cur_group_name.find('td .food_items').val(response.name);
						cur_group_name.find('td .calories_items').val(response.calories);
						cur_group_name.find('td .serv_size_items').val(response.serv_size);
						cur_group_name.find('td .carbs_items').val(response.carbohydrates);
						cur_group_name.find('td .fat_items').val(response.fat);
						cur_group_name.find('td .protein_items').val(response.protein);
					}				
				});
			}else{
				cur_group_name.find('td .group_id').val('');
				cur_group_name.find('td .food_item_id').val('');	
				cur_group_name.find('td .food_items').val('');
				cur_group_name.find('td .calories_items').val('');
				cur_group_name.find('td .serv_size_items').val('');
				cur_group_name.find('td .carbs_items').val('');
				cur_group_name.find('td .fat_items').val('');
				cur_group_name.find('td .protein_items').val('');	
			}		
		});
    });
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'mealblock.cancel'){
            Joomla.submitform(task, document.getElementById('mealblock-form'));
        }
        else{
            
            if (task != 'mealblock.cancel' && document.formvalidator.isValid(document.id('mealblock-form'))) {
                
                Joomla.submitform(task, document.getElementById('mealblock-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_questionnaire_nutrition&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="mealblock-form" class="form-validate">
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset class="adminform">

                <input type="hidden" id="meal_id" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('primary_food_tag'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('primary_food_tag'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('secondary_food_tag'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('secondary_food_tag'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('rating_number'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('rating_number'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('calorie_count'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('calorie_count'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('food_items'); ?></div>
				<div class="controls">
				<?php  $food_items_db=json_decode(json_decode($this->item->food_items),'Array'); ?>
                	<table border="" width="100%" class="table table-striped food_items">
                    	<tr>
                        	<th class="center">Group Name</th><th class="center">Food Item Name</th><th class="center">Calories</th><th class="center">Serving Size</th><th class="center">Carbs</th><th class="center">Fat</th><th class="center">Protein</th>
                        </tr>
                        <?php for($i=1;$i<=8;$i++): ?>
                        <tr>
                        	<td class="center">
                            <select>
                            	<option value="">Select</option>
                            <?php foreach($group_name as $group_name_key=>$group_name_val){
									if($food_items_db[$i]['group_id']==$group_name_key){
										$selected="selected='selected'";
									}else{
										$selected='';
									}
								
								echo "<option value='$group_name_key' $selected >$group_name_val</option>";
							}							
							?>
                            </select>
                            <?php
							if(!empty($food_items_db[$i]['food_item_id'])){
								$food_id_db= $food_items_db[$i]['food_item_id'];
								$query="SELECT id,name,calories,CONCAT(serv_size,' ',unit_of_measurement) serv_size,carbohydrates,fat,protein FROM #__questionnaire_nutrition_food WHERE id=$food_id_db" ;			
								$db->setQuery($query);
								$db_food_items=$db->loadObject();

								$food_id[]=$db_food_items->id;
								$food_id=array_unique($food_id);
								$session->set('food_id',$food_id);
							}
							else{
								unset($db_food_items);
							}
							?>
                            </td>
                            <td class="center">
                            <input type="hidden" readonly="readonly" name="jform[food_items][<?php echo $i; ?>][group_id]" value="<?php echo @$food_items_db[$i]['group_id']; ?>" class="cssinput group_id" />
                            <input type="hidden" readonly="readonly" name="jform[food_items][<?php echo $i; ?>][food_item_id]" value="<?php echo @$food_items_db[$i]['food_item_id']; ?>" class="cssinput food_item_id" />
                            <input type="text" readonly="readonly" value="<?php echo @$db_food_items->name; ?>" class="cssinput cssinputwidth120 food_items" /></td>
                            <td class="center"><input type="text" readonly="readonly" value="<?php echo @$db_food_items->calories; ?>" class="cssinput calories_items" /></td>
                            <td class="center"><input type="text" readonly="readonly" value="<?php echo @$db_food_items->serv_size; ?>" class="cssinput cssinputwidth120 serv_size_items" /></td>
                            <td class="center"><input type="text" readonly="readonly" value="<?php echo @$db_food_items->carbohydrates; ?>" class="cssinput carbs_items" /></td>
                            <td class="center"><input type="text" readonly="readonly" value="<?php echo @$db_food_items->fat; ?>" class="cssinput fat_items" /></td>
                            <td class="center"><input type="text" readonly="readonly" value="<?php echo @$db_food_items->protein; ?>" class="cssinput protein_items" /></td>
                        </tr>
                        <?php endfor; ?>
                    </table>                
                </div>
			</div>

            </fieldset>
        </div>
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
<div id="bottom_space"></div>