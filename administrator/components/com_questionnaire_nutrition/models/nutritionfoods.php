<?php

/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Questionnaire_nutrition records.
 */
class Questionnaire_nutritionModelnutritionfoods extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'name', 'a.name',
                'category_id', 'a.category_id',
                'group_name', 'a.group_name',
                'brand_name', 'a.brand_name',
                'serv_size', 'a.serv_size',
				'unit_of_measurement', 'a.unit_of_measurement',
                'calories', 'a.calories',
                'carbohydrates', 'a.carbohydrates',
                'fat', 'a.fat',
                'protein', 'a.protein',
                'total_carbohydrates', 'a.total_carbohydrates',
                'total_fat', 'a.total_fat',
                'total_protein', 'a.total_protein',
                'saturated_fat', 'a.saturated_fat',
                'trans_fat', 'a.trans_fat',
                'cholesterol', 'a.cholesterol',
                'sodium', 'a.sodium',
                'dietary_fiber', 'a.dietary_fiber',
                'sugars', 'a.sugars',
                'vitamin_a', 'a.vitamin_a',
                'vitamin_c', 'a.vitamin_c',
                'vitamin_d', 'a.vitamin_d',
                'vitamin_k', 'a.vitamin_k',
                'folic_acid', 'a.folic_acid',
                'calcium', 'a.calcium',
                'potassium', 'a.potassium',
                'iron', 'a.iron',
				'additional_vitamins_and_minerals','a.additional_vitamins_and_minerals',
                'blood_type', 'a.blood_type',
                'notes', 'a.notes',
                'picture', 'a.picture',

            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     */
    protected function populateState($ordering = null, $direction = null) {
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
        $this->setState('filter.state', $published);

        
		//Filtering category_id
		$this->setState('filter.category_id', $app->getUserStateFromRequest($this->context.'.filter.category_id', 'filter_category_id', '', 'string'));


        // Load the parameters.
        $params = JComponentHelper::getParams('com_questionnaire_nutrition');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.name', 'asc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param	string		$id	A prefix for the store id.
     * @return	string		A store id.
     * @since	1.6
     */
    protected function getStoreId($id = '') {
        // Compile the store id.
        $id.= ':' . $this->getState('filter.search');
        $id.= ':' . $this->getState('filter.state');

        return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'a.*'
                )
        );
        $query->from('`#__questionnaire_nutrition_food` AS a');

        
		// Join over the user field 'created_by'
		$query->select('created_by.name AS created_by');
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');
		// Join over the foreign key 'category_id'
		$query->select('#__questionnaire_nutrition_category_637239.name AS nutritionscategories_name_637239');
		$query->join('LEFT', '#__questionnaire_nutrition_category AS #__questionnaire_nutrition_category_637239 ON #__questionnaire_nutrition_category_637239.id = a.category_id');
		// Join over the foreign key 'recipe_type_id'
		$query->select('#__questionnaire_nutrition_recipe_type_637170.name AS nutritionrecipetypes_name_637170');
		$query->join('LEFT', '#__questionnaire_nutrition_recipe_type AS #__questionnaire_nutrition_recipe_type_637170 ON #__questionnaire_nutrition_recipe_type_637170.id = a.recipe_type_id');

        
    // Filter by published state
    $published = $this->getState('filter.state');
    if (is_numeric($published)) {
        $query->where('a.state = '.(int) $published);
    } else if ($published === '') {
        $query->where('(a.state IN (0, 1))');
    }
    

        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                $query->where('( a.name LIKE '.$search.' )');
            }
        }

        

		//Filtering category_id
		$filter_category_id = $this->state->get("filter.category_id");
		if ($filter_category_id) {
			$query->where("a.category_id = '".$db->escape($filter_category_id)."'");
		}


        // Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }

        return $query;
    }

    public function getItems() {
        $items = parent::getItems();
        
		foreach ($items as $oneItem) {

			if (isset($oneItem->category_id)) {
				$values = explode(',', $oneItem->category_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('name')
							->from('`#__questionnaire_nutrition_category`')
							->where('id = ' .$value);
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->category_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->category_id;

			}

			if (isset($oneItem->recipe_type_id)) {
				$values = explode(',', $oneItem->recipe_type_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('name')
							->from('`#__questionnaire_nutrition_recipe_type`')
							->where('id = ' .$value);
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->recipe_type_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->recipe_type_id;

			}
		}
        return $items;
    }

}
