CREATE TABLE IF NOT EXISTS `#__questionnaire_nutrition_recipes` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(64)  NOT NULL ,
`food_id` INT NOT NULL ,
`recipe_type_id` TEXT NOT NULL ,
`brand_name` VARCHAR(64)  NOT NULL ,
`serv_size` VARCHAR(64)  NOT NULL ,
`calories` INT(4)  NOT NULL ,
`carbohydrates` INT(4)  NOT NULL ,
`fat` INT(4)  NOT NULL ,
`protein` INT(4)  NOT NULL ,
`total_carbohydrates` INT(4)  NOT NULL ,
`total_fat` INT(4)  NOT NULL ,
`total_protein` INT(4)  NOT NULL ,
`saturated_fat` INT(4)  NOT NULL ,
`trans_fat` INT(4)  NOT NULL ,
`cholesterol` INT(4)  NOT NULL ,
`sodium` INT(4)  NOT NULL ,
`dietary_fiber` INT(4)  NOT NULL ,
`sugars` INT(4)  NOT NULL ,
`vitamin_a` INT(4)  NOT NULL ,
`vitamin_c` INT(4)  NOT NULL ,
`vitamin_d` INT(4)  NOT NULL ,
`vitamin_k` INT(4)  NOT NULL ,
`folic_acid` INT(4)  NOT NULL ,
`calcium` INT(4)  NOT NULL ,
`potassium` INT(4)  NOT NULL ,
`iron` INT(4)  NOT NULL ,
`blood_type` VARCHAR(255)  NOT NULL ,
`notes` VARCHAR(255)  NOT NULL ,
`picture` VARCHAR(255)  NOT NULL ,
`ingredients` TEXT NOT NULL ,
`cooking_directions` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__questionnaire_nutrition_food` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(64)  NOT NULL ,
`category_id` INT NOT NULL ,
`recipe_type_id` TEXT NOT NULL ,
`brand_name` VARCHAR(64)  NOT NULL ,
`serv_size` VARCHAR(64)  NOT NULL ,
`calories` INT(4)  NOT NULL ,
`carbohydrates` INT(4)  NOT NULL ,
`fat` INT(4)  NOT NULL ,
`protein` INT(4)  NOT NULL ,
`total_carbohydrates` INT(4)  NOT NULL ,
`total_fat` INT(4)  NOT NULL ,
`total_protein` INT(4)  NOT NULL ,
`saturated_fat` INT(4)  NOT NULL ,
`trans_fat` INT(4)  NOT NULL ,
`cholesterol` INT(4)  NOT NULL ,
`sodium` INT(4)  NOT NULL ,
`dietary_fiber` INT(4)  NOT NULL ,
`sugars` INT(4)  NOT NULL ,
`vitamin_a` INT(4)  NOT NULL ,
`vitamin_c` INT(4)  NOT NULL ,
`vitamin_d` INT(4)  NOT NULL ,
`vitamin_k` INT(4)  NOT NULL ,
`folic_acid` INT(4)  NOT NULL ,
`calcium` INT(4)  NOT NULL ,
`potassium` INT(4)  NOT NULL ,
`iron` INT(4)  NOT NULL ,
`blood_type` VARCHAR(255)  NOT NULL ,
`notes` VARCHAR(255)  NOT NULL ,
`picture` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__questionnaire_nutrition_category` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`name` VARCHAR(64)  NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__questionnaire_nutrition_taskbar` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(64)  NOT NULL ,
`recipe_id` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__questionnaire_nutrition_recipe_type` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(64)  NOT NULL ,
`parent_id` INT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

