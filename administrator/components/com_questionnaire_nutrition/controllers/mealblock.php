<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      malwinder <malwinder@34interactive.com> - http://
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Mealblock controller class.
 */
class Questionnaire_nutritionControllerMealblock extends JControllerForm
{

    function __construct() {
        $this->view_list = 'mealblocks';
        parent::__construct();
    }
	
	function get_food_items(){
		$app			= JFactory::getApplication();
		
		$db			= JFactory::getDBO();
		
		$session = JFactory::getSession();	

		$group_id = $app->input->post->get('group_id','');
		
		$food_id = $session->get('food_id', array());

		$food_id=array_filter($food_id);

		$is_food_exist='';
		if(!empty($food_id)){
			$is_food_exist="AND id NOT IN(".implode(',',$food_id).")";
		}
		
		$query="SELECT id,name,concat(serv_size,' ',unit_of_measurement) serv_size,calories,carbohydrates,fat,protein FROM #__questionnaire_nutrition_food WHERE  group_name=$group_id  $is_food_exist limit 1";

		$db->setQuery($query);

		$food_id[]=$db->loadObject()->id;
		$food_id=array_unique($food_id);
		$session->set('food_id',$food_id);
		
		echo json_encode($db->loadObject());
		die;
	}
}