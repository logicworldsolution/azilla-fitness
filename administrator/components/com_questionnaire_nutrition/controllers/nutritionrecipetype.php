<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Nutritionrecipetype controller class.
 */
class Questionnaire_nutritionControllerNutritionrecipetype extends JControllerForm
{

    function __construct() {
        $this->view_list = 'nutritionrecipetypes';
        parent::__construct();
    }

}