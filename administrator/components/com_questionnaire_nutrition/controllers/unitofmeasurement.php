<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      malwinder <malwinder@34interactive.com> - http://
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Unitofmeasurement controller class.
 */
class Questionnaire_nutritionControllerUnitofmeasurement extends JControllerForm
{

    function __construct() {
        $this->view_list = 'unitofmeasurements';
        parent::__construct();
    }

}