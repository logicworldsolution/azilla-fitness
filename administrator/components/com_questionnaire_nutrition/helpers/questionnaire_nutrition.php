<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_nutrition
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Questionnaire_nutrition helper.
 */
class Questionnaire_nutritionHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_NUTRITION_TITLE_NUTRITIONRECIPES'),
			'index.php?option=com_questionnaire_nutrition&view=nutritionrecipes',
			$vName == 'nutritionrecipes'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_NUTRITION_TITLE_NUTRITIONFOODS'),
			'index.php?option=com_questionnaire_nutrition&view=nutritionfoods',
			$vName == 'nutritionfoods'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_NUTRITION_TITLE_NUTRITIONSCATEGORIES'),
			'index.php?option=com_questionnaire_nutrition&view=nutritionscategories',
			$vName == 'nutritionscategories'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_NUTRITION_TITLE_NUTRITIONTASKBARITEMS'),
			'index.php?option=com_questionnaire_nutrition&view=nutritiontaskbaritems',
			$vName == 'nutritiontaskbaritems'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_NUTRITION_TITLE_NUTRITIONRECIPETYPES'),
			'index.php?option=com_questionnaire_nutrition&view=nutritionrecipetypes',
			$vName == 'nutritionrecipetypes'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_NUTRITION_TITLE_MEALBLOCKS'),
			'index.php?option=com_questionnaire_nutrition&view=mealblocks',
			$vName == 'mealblocks'
		);		
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_NUTRITION_TITLE_UNITOFMEASUREMENTS'),
			'index.php?option=com_questionnaire_nutrition&view=unitofmeasurements',
			$vName == 'unitofmeasurements'
		);
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_questionnaire_nutrition';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
