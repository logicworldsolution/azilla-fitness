<?php
/**
 * @version     1.0.0
 * @package     com_grocerylist
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class GrocerylistViewGrocerylist extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->item->id == 0);

		$app			= JFactory::getApplication();
		$layout=$app->input->get('layout');
		
		if($layout=='edit'){
			JToolBarHelper::title('Grocery Data', 'grocerylist.png');
			JToolBarHelper::apply('grocerylist.savegrocery', 'JTOOLBAR_APPLY');
			JToolBarHelper::cancel('grocerylist.canceluser', 'JTOOLBAR_CLOSE');
		}else{		
			JToolBarHelper::title(JText::_('COM_GROCERYLIST_TITLE_GROCERYLIST'), 'grocerylist.png');
			JToolBarHelper::cancel('grocerylist.cancel', 'JTOOLBAR_CLOSE');
		}

	}
}
