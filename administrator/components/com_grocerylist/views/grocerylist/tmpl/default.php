<?php
/**
 * @version     1.0.0
 * @package     com_grocerylist
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_grocerylist/assets/css/grocerylist.css');

$db=JFactory::getDBO();
$app			= JFactory::getApplication();
$user_id=$app->input->get('user_id');

$query="SELECT id,start_date,end_date FROM #__usergrocerylist WHERE user_id=$user_id order by id DESC";
$db->setQuery($query);
$usergrocerylist=$db->loadObjectList();

?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){
        
    });
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'grocerylist.cancel'){
            Joomla.submitform(task, document.getElementById('grocerylist-form'));
        }
        else{
            
            if (task != 'grocerylist.cancel' && document.formvalidator.isValid(document.id('grocerylist-form'))) {
                
                Joomla.submitform(task, document.getElementById('grocerylist-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_grocerylist&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="grocerylist-form" class="form-validate">
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
    <div class="row-fluid">
        <table class="table table-striped" id="usermealList">
			<thead>
				<tr>
					<th width="1%" class="nowrap center">Id</th>
					<th width="1%" class="nowrap center">Start Date -  End Date</th>
					<th width="1%" class="nowrap center">Action</th>
				</tr>
			</thead>
			<tfoot>
                <?php 
/*                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }*/
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php //echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php $i=0; foreach ($usergrocerylist as $usergrocerylistVal) :
				?>
				<tr class="row<?php echo $i++ % 2; ?>">
					<td class="center"><?php echo $usergrocerylistVal->id; ?></td>
					<td class="center"><?php echo date('d/m/Y',strtotime($usergrocerylistVal->start_date)); ?> - <?php echo date('d/m/Y',strtotime($usergrocerylistVal->end_date)); ?></td>
					<td class="center"><a href="<?php echo JRoute::_("index.php?option=com_grocerylist&view=grocerylist&layout=edit&id=".$usergrocerylistVal->id); ?>">View</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>