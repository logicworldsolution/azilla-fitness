<?php
/**
 * @version     1.0.0
 * @package     com_grocerylist
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_grocerylist/assets/css/grocerylist.css');
$db=JFactory::getDBO();
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){
        
    });
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'grocerylist.cancel'){
            Joomla.submitform(task, document.getElementById('grocerylist-form'));
        }
        else{
            
            if (task != 'grocerylist.cancel' && document.formvalidator.isValid(document.id('grocerylist-form'))) {
                
                Joomla.submitform(task, document.getElementById('grocerylist-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>
<style>
ul{ list-style-type:none; padding-top:5px;}
.textstrike{ text-decoration:line-through;}
</style>

<form action="<?php echo JRoute::_('index.php?option=com_grocerylist&task=grocerylist.savegrocery&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="grocerylist-form" class="form-validate">
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
    <input type="hidden" name="jform[user_id]" value="<?php echo $this->form->getValue('user_id'); ?>" />
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset class="adminform">			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('start_date'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('start_date'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('end_date'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('end_date'); ?></div>
			</div>
            <div class="control-group">
				<div class="control-label">Grocery List</div>
				<div class="controls"><?php 
				$query="SELECT B.name,A.food_serv_size,A.state FROM #__usergrocery_meals A JOIN #__questionnaire_nutrition_food B ON A.food_id=B.id WHERE usergrocerylist_id=".$this->item->id." ORDER BY B.name ";
				$db->setQuery($query);
				 ?>
                 <ul>
                 <?php foreach($db->loadObjectList() as $groceryList){
				 	if($groceryList->state==0){					
						echo "<li class='textstrike'>{$groceryList->name}</li>";
					}else{
				 		echo "<li>{$groceryList->name}</li>";
					}
				 }?>                 
                 </ul>
                 </div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('notes'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('notes'); ?></div>
			</div>
            </fieldset>
        </div>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>