CREATE TABLE IF NOT EXISTS `#__usergrocerylist` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`state` TINYINT(1)  NOT NULL DEFAULT '1',
`created_by` INT(11)  NOT NULL ,
`user_id` INT(11)  NOT NULL ,
`start_date` DATE NOT NULL DEFAULT '0000-00-00',
`end_date` DATE NOT NULL DEFAULT '0000-00-00',
`notes` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__usergrocery_meals` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`created_by` INT(11)  NOT NULL ,
`usergrocery_id` INT(11)  NOT NULL ,
`usermeal_id` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

