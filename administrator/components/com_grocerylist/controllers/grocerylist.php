<?php
/**
 * @version     1.0.0
 * @package     com_grocerylist
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Grocerylist controller class.
 */
class GrocerylistControllerGrocerylist extends JControllerForm
{

    function __construct() {
        $this->view_list = 'grocerylists';
        parent::__construct();
    }
	
	function canceluser(){
		$app			= JFactory::getApplication();

		$data = $app->input->post->get('jform', array(), 'array');
		
		$user_id=$data['user_id'];
		
		$this->setRedirect(JRoute::_("index.php?option=com_grocerylist&view=grocerylist&layout=default&user_id=$user_id", false));
	}
	
	function savegrocery(){
		$app			= JFactory::getApplication();

		$data = $app->input->post->get('jform', array(), 'array');

		$model	= $this->getModel('Grocerylist');
		$return	= $model->save($data);
		$user_id=$data['user_id'];			
		if ($return === false) {
			$this->setMessage("Fail to updated recipe", 'warning');
			
			$this->setRedirect(JRoute::_("index.php?option=com_grocerylist&view=grocerylist&layout=default&user_id=$user_id", false));
			return false;
		}else{
				$this->setMessage("Recipe updated successfully");
				$this->setRedirect(JRoute::_("index.php?option=com_grocerylist&view=grocerylist&layout=default&user_id=$user_id", false));
		}
	}

}