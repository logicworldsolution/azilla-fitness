<?php
/**
 * @version     1.0.0
 * @package     com_userworkouts
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      malwinder <malwinder@34interactive.com> - http://
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Userworkout controller class.
 */
class UserworkoutsControllerUserworkout extends JControllerForm
{

    function __construct() {
        $this->view_list = 'userworkouts';
        parent::__construct();
    }
	
	function cancelexercise($key = NULL){
		$app			= JFactory::getApplication();

		$user_id = $app->input->post->get('user_id');

		$this->setRedirect(JRoute::_("index.php?option=com_userworkouts&view=userworkout&layout=edit&user_id=$user_id", false));		
	}

}