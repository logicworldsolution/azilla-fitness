CREATE TABLE IF NOT EXISTS `#__userworkouts` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`user_id` INT(11)  NOT NULL ,
`experience_sublvl_working_out` TINYINT(4)  NOT NULL DEFAULT '1',
`exercise_id` INT(11)  NOT NULL ,
`cardio_exercise_id` INT(11)  NOT NULL ,
`exercise_days` INT(4)  NOT NULL ,
`exercise_date` DATE NOT NULL DEFAULT '0000-00-00',
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

