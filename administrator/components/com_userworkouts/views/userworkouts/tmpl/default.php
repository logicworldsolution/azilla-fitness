<?php
/**
 * @version     1.0.0
 * @package     com_userworkouts
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      malwinder <malwinder@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_userworkouts/assets/css/userworkouts.css');
?>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_userworkouts&view=userworkouts'); ?>" method="post" name="adminForm" id="adminForm">
	<div id="j-main-container">
		<div id="filter-bar" class="btn-toolbar">
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
		</div>        
		<div class="clearfix"> </div>
		<table class="table table-striped" id="userworkoutList">
			<thead>
				<tr>
					<th width="1%" class="nowrap center">Id</th>
					<th width="1%" class="nowrap center">User Name</th>
					<th width="1%" class="nowrap center">Experience Level</th>
					<th width="1%" class="nowrap center">Experience Sub Level</th>
					<th width="1%" class="nowrap center">Action</th>
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
				?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center"><?php echo $item->id; ?></td>
					<td class="center"><?php echo $item->name; ?></td>
					<td class="center"><?php echo $item->experiance_name; ?></td>
					<td class="center"><?php echo $item->experience_sublvl_working_out; ?></td>
					<td class="center"><a href="<?php echo JRoute::_("index.php?option=com_userworkouts&view=userworkout&layout=edit&user_id=$item->id"); ?>">View</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>        

		
