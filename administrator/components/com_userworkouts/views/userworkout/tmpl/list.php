<?php
/**
 * @version     1.0.0
 * @package     com_userworkouts
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      malwinder <malwinder@34interactive.com> - http://
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_userworkouts/assets/css/userworkouts.css');

$db=JFactory::getDBO();
$app			= JFactory::getApplication();
$user_id=$app->input->get('user_id');

$workout_id = $app->input->get('id');

$query	= "SELECT A.name FROM #__questionnaire_exercise_name A JOIN #__userworkouts B ON A.id=B.exercise_id WHERE B.userworkouts_level_id = $workout_id";
$db->setQuery( $query );
$user_exercise_list=$db->loadObjectList();
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){
        
    });
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'userworkout.cancelexercise'){
            Joomla.submitform(task, document.getElementById('userworkout-form'));
        }
        else{
            
            if (task != 'userworkout.cancel' && document.formvalidator.isValid(document.id('userworkout-form'))) {
                
                Joomla.submitform(task, document.getElementById('userworkout-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>
<style>
.controls{ padding-top:5px;}
.exerciselist{
	margin:0px;
}
.exerciselist li{
	list-style:none;
}
</style>
<form action="<?php echo JRoute::_('index.php?option=com_userworkouts&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="userworkout-form" class="form-validate">
    <div class="row-fluid">
         <table class="table table-striped" id="usermealList">
			<thead>
				<tr>
					<th width="1%" class="nowrap center">Exercise Name</th>
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php //echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php $i=0; foreach ($user_exercise_list as $user_exercise_listVal) :
				?>
				<tr class="row<?php echo $i++ % 2; ?>">
					<td class="center"><?php echo $user_exercise_listVal->name ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
        <input type="hidden" name="task" value="" />
		<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>