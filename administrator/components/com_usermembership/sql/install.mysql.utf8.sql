CREATE TABLE IF NOT EXISTS `#__usermembership_types` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`user_id` INT(11)  NOT NULL ,
`ref_prefix` VARCHAR(255)  NOT NULL ,
`ref_code` INT(11)  NOT NULL ,
`membership_type` TINYINT(4)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

