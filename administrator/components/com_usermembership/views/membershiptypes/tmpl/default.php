<?php
/**
 * @version     1.0.0
 * @package     com_usermembership
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_usermembership/assets/css/usermembership.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_usermembership');
$saveOrder	= $listOrder == 'a.ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_usermembership&task=membershiptypes.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'membershiptypeList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<script>
jQuery(document).ready(function($){
	$('.payment').click(function(e){
		e.preventDefault();
		$(".sentpayment").html();
		var value = $(this).attr('id');
		$.ajax({
			type:"POST",
			url: "ajaxid.php",
			data: {id:value},
			success: function(data){
				$('.sentpayment').html(data);
				var width1 = $('.sentpayment').width();
				var height1 = $('.sentpayment').height();
				var width = parseInt(width1);
				var height = parseInt(height1);
				var leftcen = parseInt(width)/2; 
				var topcen =  parseInt(height)/2;
				$('.sentpayment').css({"height":height+"px", "width":width+"px", "margin-top":-topcen+"px", "margin-left": -leftcen+"px"});
				$('.sentpayment').fadeIn(1000);
				var mask = "<div class='mask'></div>";
				$('body').append(mask);
			}
		});
	});
	$('.closepopup').click(function(e){
		e.preventDefault();
		$('.sentpayment').fadeOut(300);
		$('div').remove('.mask');
		$(".sentpayment").html();
		$('.sentpayment').delay(3000).removeAttr("style");
	});
	$('.mask').click(function(){
		$('.sentpayment').fadeOut(300);
		$('div').remove('.mask');
		$(".sentpayment").html();
		$('.sentpayment').delay(3000).removeAttr("style");
	});
});
</script>
<style>
.sentpayment{position:fixed; z-index:9; top:50%; left:50%; background:#fff; padding:20px; border-radius:5px 5px 5px 5px; }
.param_name input {margin:0px;  border: medium none; margin: 0; background:#fff; cursor: default;}
.param_name input:focus{box-shadow:none!important;}
#permissions{ float:left; margin:2px 5px 0 0;}
.closepopup{background: url("images/close.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
 display: block; height: 34px; position: absolute; right: -17px; top: -17px; width: 34px;}
 .mask{position:fixed; width:100%; height:100%; top:0; left:0; opacity:0.75; z-index:8; background:#000;}
</style>
<div class="sentpayment" style="display:none;">
</div>
<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) {
    $this->sidebar .= $this->extra_sidebar;
}
?>

<form action="<?php echo JRoute::_('index.php?option=com_usermembership&view=membershiptypes'); ?>" method="post" name="adminForm" id="adminForm">
<?php if(!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
    
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				<label for="filter_search" class="element-invisible"><?php echo JText::_('JSEARCH_FILTER');?></label>
				<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('JSEARCH_FILTER'); ?>" />
			</div>
			<div class="btn-group pull-left">
				<button class="btn hasTooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC');?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC');?></option>
					<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING');?></option>
					<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');?></option>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY');?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder);?>
				</select>
			</div>
		</div>        
		<div class="clearfix"> </div>
		<table class="table table-striped" id="membershiptypeList">
			<thead>
				<tr>
                <?php if (isset($this->items[0]->ordering)): ?>
					<th width="1%" class="nowrap center hidden-phone">
						<?php  echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
					</th>
                <?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
               
                    
				<th class='left'>
					<?php echo "Member Name"; //JHtml::_('grid.sort',  'User Name', 'a.user_id', $listDirn, $listOrder); ?>
				</th>
				
				<th class='left'>
					<?php echo "Member username"; //JHtml::_('grid.sort',  'User Name', 'a.user_id', $listDirn, $listOrder); ?>
				</th>
				
				<th class='left'>
					<?php echo "Member Email"; //JHtml::_('grid.sort',  'User Name', 'a.user_id', $listDirn, $listOrder); ?>
				</th>
			
				<th class="left">
					<?php echo JHtml::_('grid.sort', 'Refferal No.', 'a.ref_code', $listDirn, $listOrder); ?>
				</th>
            
				<th class='left'>
					<?php echo "Payment Status"; //JHtml::_('grid.sort',  'User Name', 'a.user_id', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
					<?php echo "Member Status"; //JHtml::_('grid.sort',  'User Name', 'a.user_id', $listDirn, $listOrder); ?>
				</th>
				
				                    
                <?php if (isset($this->items[0]->id)): ?>
					<th width="1%" class="nowrap center hidden-phone">
						<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
					</th>
                <?php endif; ?>
				
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
				$ordering   = ($listOrder == 'a.ordering');
                $canCreate	= $user->authorise('core.create',		'com_usermembership');
                $canEdit	= $user->authorise('core.edit',			'com_usermembership');
                $canCheckin	= $user->authorise('core.manage',		'com_usermembership');
                $canChange	= $user->authorise('core.edit.state',	'com_usermembership');
				$db = JFactory::getDBO();
				$sel = "SELECT * from #__users where id='".$item->user_id."'";
				$db->setQuery($sel);
				$username = $db->loadAssoc();
				?>
				<tr class="row<?php echo $i % 2; ?>">
                    
                <?php if (isset($this->items[0]->ordering)): ?>
					<td class="order nowrap center hidden-phone">
					<?php if ($canChange) :
						$disableClassName = '';
						$disabledLabel	  = '';
						if (!$saveOrder) :
							$disabledLabel    = JText::_('JORDERINGDISABLED');
							$disableClassName = 'inactive tip-top';
						endif; ?>
						<span class="sortable-handler hasTooltip <?php echo $disableClassName?>" title="<?php echo $disabledLabel?>">
							<i class="icon-menu"></i>
						</span>
						<input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering;?>" class="width-20 text-area-order " />
					<?php else : ?>
						<span class="sortable-handler inactive" >
							<i class="icon-menu"></i>
						</span>
					<?php endif; ?>
					</td>
                <?php endif; ?>
					<td class="center hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
					</td>
                
                    
				<td>

					<?php echo $username['name']; ?>
				</td>
				<td>

					<?php echo $username['username']; ?>
				</td>
				<td>
					<?php echo $username['email']; ?>
				</td>
				<td>
					<?php echo $item->ref_prefix.$item->ref_code; ?>
				</td>
				<td>
				<?php 
					$db = JFactory::getDBO();
					$sel = "SELECT * from #__user_affiliated_member where mem_type_id='".$item->id."'";
					$db->setQuery($sel);
					$rowmem = $db->loadAssoc();
					if($rowmem['ref_code'] == '')
					{
					}
					else
					{
						if($rowmem['status'] == 0){
					?>
					<a href="#" id="<?php echo  $rowmem['id']; ?>" class="payment">Payment</a>
					<?php } else { ?>
					Already Paid
					<?php } } ?>
				</td>
				<td>
				<?php  $acti=$username['block'];
					 if($acti=='1')
						 {
						 echo 'InActive';
						 }
						 else
						 {
						 echo "Active";
						 }

				?>
				</td>
				
                <?php if (isset($this->items[0]->id)): ?>
					<td class="center hidden-phone">
						<?php echo (int) $item->id; ?>
					</td>
                <?php endif; ?>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php  echo JHtml::_('form.token'); ?>
	</div>
</form>        

		