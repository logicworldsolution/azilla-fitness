<?php
/**
 * @version     1.0.0
 * @package     com_usermembership
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */


// No direct access
defined('_JEXEC') or die;

class UsermembershipController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param	boolean			$cachable	If true, the view output will be cached
	 * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		require_once JPATH_COMPONENT.'/helpers/usermembership.php';

		$view		= JFactory::getApplication()->input->getCmd('view', 'membershiptypes');
        JFactory::getApplication()->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
	public function payment()
	{
	//print_r($_POST);
	require_once('PPBootStrap.php');
require_once('Constants.php');
/*
 *  # MassPay API
The MassPay API operation makes a payment to one or more PayPal account
holders.
This sample code uses Merchant PHP SDK to make API call
*/
$massPayRequest = new MassPayRequestType();
$massPayRequest->MassPayItem = array();
//$count = count($_POST['mail']); die;
//$_REQUEST['mail']=array_fill(0,250,'simran@34interactive.com');
//print_r($_REQUEST['mail']);die;
$_REQUEST['receiverInfoCode']='EmailAddress';
$_POST['accessToken']=$_POST['tokenSecret']='';

for($i=0; $i<count($_REQUEST['mail']); $i++) {
	$masspayItem = new MassPayRequestItemType();
	/*
	 *  `Amount` for the payment which contains

	* `Currency Code`
	* `Amount`
	*/
//	$masspayItem->Amount = new BasicAmountType($_REQUEST['currencyCode'][$i], $_REQUEST['amount'][$i]);
	$masspayItem->Amount = new BasicAmountType('USD', $_POST['amount'][$i]);
	if($_REQUEST['receiverInfoCode'] == 'EmailAddress') {
		/*
		 *  (Optional) How you identify the recipients of payments in this call to MassPay. It is one of the following values:
		EmailAddress
		UserID
		PhoneNumber
		*/
		$masspayItem->ReceiverEmail = $_REQUEST['mail'][$i];
	} /*elseif ($_REQUEST['receiverInfoCode'] == 'UserID') {
		$masspayItem->ReceiverID = $_REQUEST['id'][$i];
	} elseif ($_REQUEST['receiverInfoCode'] == 'PhoneNumber') {
		$masspayItem->ReceiverPhone = $_REQUEST['phone'][$i];
	}*/
	$massPayRequest->MassPayItem[] = $masspayItem;
}

/*
 *  ## MassPayReq
Details of each payment.
`Note:
A single MassPayRequest can include up to 250 MassPayItems.`
*/
$massPayReq = new MassPayReq();
$massPayReq->MassPayRequest = $massPayRequest;

/*
 * 	 ## Creating service wrapper object
Creating service wrapper object to make API call and loading
Configuration::getAcctAndConfig() returns array that contains credential and config parameters
*/
$paypalService = new PayPalAPIInterfaceServiceService(Configuration::getAcctAndConfig());

// required in third party permissioning
if(($_POST['accessToken']!= null) && ($_POST['tokenSecret'] != null)) {
		$cred = new PPSignatureCredential(USERNAME, PASSWORD, SIGNATURE);
	    $cred->setThirdPartyAuthorization(new PPTokenAuthorization($_POST['accessToken'], $_POST['tokenSecret']));
}

try {
	/* wrap API method calls on the service object with a try catch */
	if(($_POST['accessToken']!= null) && ($_POST['tokenSecret'] != null)) {
		$massPayResponse = $paypalService->MassPay($massPayReq, $cred);
	}
	else{
		$massPayResponse = $paypalService->MassPay($massPayReq);
	}
} catch (Exception $ex) {
	include_once("Error.php");
	exit;
}
if(isset($massPayResponse)) {
	if($massPayResponse->Ack == 'Success' )
	{
		$count1 = count($_POST['memberid']);
		for($i = 0; $i<$count1; $i++)
		{
			$memberid = $_POST['memberid'][$i];
			$id = $_POST['thisid'];
			$db =& JFactory::getDBO();
			$app = JFactory::getApplication('administrator');
			$sel = "SELECT *,#__user_affiliated_member.ref_code as referal from #__users inner join #__user_affiliated_member on #__user_affiliated_member.user_id = #__users.id where #__user_affiliated_member.mem_type_id='".$memberid."'";
			$db->setQuery($sel);
			$row = $db->loadAssoc();
			$recipient = $row['email'];
			$query = "UPDATE #__user_affiliated_member SET status = '1' WHERE id=".$id ;
			$db->setQuery( $query );
			$db->query();
			
			$mail =& JFactory::getMailer();
			$from = "azzilla@azzilla.com";
			$fromname = "Azzilla";
			$subject = "Recieved payment from Azzilla";
			$body = "Hi Member, \n\n\tYou have recieved payment for referring a user to be affiliated member. Please check your paypal account.\n\n\nThanks,\nTeam Azzilla.";
			$mail->setSender(array($from, $fromname));
			$mail->setSubject($subject);
			$mail->setBody($body);
	 
			// Are we sending the email as HTML?
			//if ( $mode ) {
				//	$mail->IsHTML(true);
			//}
	 
			$mail->addRecipient($recipient);
			//$mail->addCC($cc);
			//$mail->addBCC($bcc);
			//$mail->addAttachment($attachment);
	 
			// Take care of reply email addresses
			/*if( is_array( $replyto ) ) {
					$numReplyTo = count($replyto);
					for ( $i=0; $i < $numReplyTo; $i++){
							$mail->addReplyTo( array($replyto[$i], $replytoname[$i]) );
					}
			} elseif( isset( $replyto ) ) {
					$mail->addReplyTo( array( $replyto, $replytoname ) );
			}*/
	 
			 $mail->Send();
			$app->redirect(JRoute::_("index.php?option=com_usermembership&view=membershiptypes", false),"Mass Payment is completed successfully.", 'notice');
		}
	}
	else
	{
		$app->redirect(JRoute::_("index.php?option=com_usermembership&view=membershiptypes", false),"There is a problem please try again.", 'notice');
	}
	/*echo "<table>";
	echo "<tr><td>Ack :</td><td><div id='Ack'>$massPayResponse->Ack</div> </td></tr>";
	echo "</table>";

	echo "<pre>";
	print_r($massPayResponse);
	echo "</pre>";*/
}
//require_once 'Response.php';

	
	}
}