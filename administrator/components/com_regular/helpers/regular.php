<?php
/*------------------------------------------------------------------------
# regular.php - Regularmember Component
# ------------------------------------------------------------------------
# author    Azilla
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   azzilla.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Regular component helper.
 */
abstract class RegularHelper
{
	/**
	 *	Configure the Linkbar.
	 */
	public static function addSubmenu($submenu) 
	{
		JHtmlSidebar::addEntry(
			JText::_('Affiliated Member'),
			'index.php?option=com_usermembership&view=membershiptypes',
			$vName == 'membershiptypes'
		);
		JHtmlSidebar::addEntry(JText::_('Regular Member'), 'index.php?option=com_regular&view=regular', $submenu == 'regular');
	}

	/**
	 *	Get the actions
	 */
	public static function getActions($Id = 0)
	{
		jimport('joomla.access.access');

		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($Id)){
			$assetName = 'com_regular';
		} else {
			$assetName = 'com_regular.message.'.(int) $Id;
		};

		$actions = JAccess::getActions('com_regular', 'component');

		foreach ($actions as $action){
			$result->set($action->name, $user->authorise($action->name, $assetName));
		};

		return $result;
	}
}
?>