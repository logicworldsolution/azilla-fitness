<?php
/*------------------------------------------------------------------------
# default.php - Regularmember Component
# ------------------------------------------------------------------------
# author    Azilla
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   azzilla.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// load tooltip behavior
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
?>
<form action="<?php echo JRoute::_('index.php?option=com_regular&view=regular'); ?>" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)){ ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php } else { ?>
	<div id="j-main-container">
<?php }; ?>
		<div>
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<?php echo JHtml::_('form.token'); ?>
			<table class="table table-striped" id="membershiptypeList">
			<thead>
				<tr>
                <?php if (isset($this->items[0]->ordering)): ?>
					<th width="1%" class="nowrap center hidden-phone">
						<?php  echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
					</th>
                <?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
               
                    
				<th class='left'>
					<?php echo "Member Name"; //JHtml::_('grid.sort',  'User Name', 'a.user_id', $listDirn, $listOrder); ?>
				</th>
				
				<th class='left'>
					<?php echo "Member Username"; //JHtml::_('grid.sort',  'User Name', 'a.user_id', $listDirn, $listOrder); ?>
				</th>
				
				<th class='left'>
					<?php echo "Member Email"; //JHtml::_('grid.sort',  'User Name', 'a.user_id', $listDirn, $listOrder); ?>
				</th>
				
				<th class='left'>
					<?php echo "Member Status"; //JHtml::_('grid.sort',  'User Name', 'a.user_id', $listDirn, $listOrder); ?>
				</th>
			<?php
			
?>
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php //echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php 
				$db = JFactory::getDBO();
			 	$sel = "SELECT * from mejwq_usermembership_types where 	membership_type='2'";
				$db->setQuery($sel);
				//$row = $db->loadObjectList($sel);
				//print_r($row);
				$username = $db->loadAssocList();
				  foreach($username as $userrow)
				  {
				  $userid=$userrow['user_id'];
				  $sel = "SELECT * from #__users where id='$userid'";
				  $db->setQuery($sel);
				 $member = $db->loadAssoc();
				/*foreach($rowmem as $member)
				{*/
				?>
				<tr class="row<?php echo $i % 2; ?>">
                    
               
					<td class="center hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
					</td>
                
                    <td>

					<?php echo $member['name']; ?>
				</td>
				<td>

					<?php echo $member['username']; ?>
				</td>
				<td>
					<?php echo $member['email']; ?>
				</td>
				<td>
					<?php $acti=$member['block'];
                         if($acti=='1')
						 {
						 echo 'InActive';
						 }
						 else
						 {
						 echo "Active";
						 }
						 


					?>
				</td>
				</tr>
				<?php }  ?>
			</tbody>
		</table>

		</div>
	</div>
</form>