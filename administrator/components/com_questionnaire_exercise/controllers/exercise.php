<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_exercise
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Exercise controller class.
 */
class Questionnaire_exerciseControllerExercise extends JControllerForm
{

    function __construct() {	
		$app = JFactory::getApplication();
	
		$data = $app->input->post->get('jform', array(), 'array');

		if(isset($data['equipments']) and  !empty($data['equipments'])){
			$data['equipments']=json_encode($data['equipments']);
			$app->input->post->set('jform',$data);
		}
		if(isset($data['routine']) and  !empty($data['routine'])){
			$data['routine']=json_encode($data['routine']);
			$app->input->post->set('jform',$data);
		}
		
        $this->view_list = 'exercises';
        parent::__construct();
    }

}