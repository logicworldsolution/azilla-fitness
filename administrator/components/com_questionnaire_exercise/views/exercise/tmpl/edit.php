<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_exercise
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_questionnaire_exercise/assets/css/questionnaire_exercise.css');


$db = JFactory::getDBO();

//Get all user levels
$db->setQuery('SELECT id,name FROM #__userlevel WHERE state=1 order by ordering ASC');
$userLvlresult = $db->loadAssocList();
$userLvlList=array();

foreach($userLvlresult as $userLvlresultVal){
	$userLvlList[$userLvlresultVal['id']]=$userLvlresultVal['name'];
}

//Get all equipments
$db->setQuery('SELECT id,name FROM #__questionnaire_equipment WHERE state=1 and category_id=1 order by ordering ASC');
$equipmentresult = $db->loadAssocList();
$equipmentList=array();
foreach($equipmentresult as $equipmentresultVal){
	$equipmentList[$equipmentresultVal['id']]=$equipmentresultVal['name'];
}

//Get modification exercises type
$db->setQuery("SELECT id,name FROM #__questionnaire_exercise_overallgoals WHERE state=1 order by ordering ASC");
$modificationresult = $db->loadAssocList();
$modificationList=array();

foreach($modificationresult as $modificationresultVal){
	$modificationList[$modificationresultVal['id']]=$modificationresultVal['name'];
}

?>
<style>
.padright { padding:5px 10px 5px 5px;}
.padleft { padding:5px 5px 5px 10px;}
.routinetable tr th , .routinetable tr td{ width:60px; text-align:center}
.routinetable tr td input{ width:25px}
.innertable td{ vertical-align:top; padding-right:5px;}
#jform_age1 , #jform_age2 { width:90px;}
.age { width:85px;}
</style>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){
        
	js('input:hidden.category_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('category_idhidden')){
			js('#jform_category_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_category_id").trigger("liszt:updated");
	js('input:hidden.exercise_type').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('exercise_typehidden')){
			js('#jform_exercise_type option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_exercise_type").trigger("liszt:updated");
	js('input:hidden.primary_muscles').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('primary_muscleshidden')){
			js('#jform_primary_muscles option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_primary_muscles").trigger("liszt:updated");
	js('input:hidden.secondary_muscles').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('secondary_muscleshidden')){
			js('#jform_secondary_muscles option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_secondary_muscles").trigger("liszt:updated");
	js('input:hidden.progression').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('progressionhidden')){
			js('#jform_progression option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_progression").trigger("liszt:updated");

	js('input:hidden.modification').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('modificationhidden')){
			js('#jform_modification option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_modification").trigger("liszt:updated");
    
	});	
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'exercise.cancel'){
            Joomla.submitform(task, document.getElementById('exercise-form'));
        }
        else{
            
            if (task != 'exercise.cancel' && document.formvalidator.isValid(document.id('exercise-form'))) {
                
	if(js('#jform_exercise_type option:selected').length == 0){
		js("#jform_exercise_type option[value=0]").attr('selected','selected');
	}
	if(js('#jform_primary_muscles option:selected').length == 0){
		js("#jform_primary_muscles option[value=0]").attr('selected','selected');
	}
	if(js('#jform_secondary_muscles option:selected').length == 0){
		js("#jform_secondary_muscles option[value=0]").attr('selected','selected');
	}
	if(js('#jform_progression option:selected').length == 0){
		js("#jform_progression option[value=0]").attr('selected','selected');
	}
	if(js('#jform_modification option:selected').length == 0){
		js("#jform_modification option[value=0]").attr('selected','selected');
	}

	
                Joomla.submitform(task, document.getElementById('exercise-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_questionnaire_exercise&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="exercise-form" class="form-validate">
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset class="adminform">

                				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('state'); ?></div>
			</div>

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('category_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('category_id'); ?></div>
			</div>

			<?php
				foreach((array)$this->item->category_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="category_id" name="jform[category_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>	
 			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('gender'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('age1'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('age1'); ?>&nbsp;-&nbsp;<?php echo $this->form->getInput('age2'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('exercise_type'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('exercise_type'); ?></div>
			</div>

			<?php
				foreach((array)$this->item->exercise_type as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="exercise_type" name="jform[exercise_typehidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('primary_muscles'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('primary_muscles'); ?></div>
			</div>

			<?php
				foreach((array)$this->item->primary_muscles as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="primary_muscles" name="jform[primary_muscleshidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('secondary_muscles'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('secondary_muscles'); ?></div>
			</div>

			<?php
				foreach((array)$this->item->secondary_muscles as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="secondary_muscles" name="jform[secondary_muscleshidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>			
            <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('video1'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('video1'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('video2'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('video2'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('video3'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('video3'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('levels_available'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('levels_available'); ?></div>
			</div>     
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('equipments'); ?></div>
				<div class="controls"><?php //echo $this->form->getInput('equipments'); 
				$equipmentsdb_data=@json_decode($this->item->equipments,'array');
				?>
					<table class="innertable">				
					<?php
					foreach($userLvlList as $userLvlListKey=>$userLvlListVal){
						$tagId=str_replace(array('/',' '),'_',$userLvlListVal);
						echo '<tr><td class="padright">'.$userLvlListVal.'</td><td class="padleft">';
						echo "<select multiple='multiple' class='inputbox'  type='text' name='jform[equipments][$userLvlListKey][]' id='equipment$tagId' >";
						echo "<option value='0'>- Select Equipment Type -</option>";
						foreach($equipmentList as $equipmentList_key=>$equipmentList_val){
							if(in_array($equipmentList_key,$equipmentsdb_data[$userLvlListKey])){
								echo "<option selected='selected' value=".$equipmentList_key.">".$equipmentList_val."</option>";
							}else{
								echo "<option value=".$equipmentList_key.">".$equipmentList_val."</option>";
							}
						}
						echo "</select>";						
						echo '</td></tr>';
					}
					?>			
					</table>
				</div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('routine'); ?></div>
				<div class="controls"><?php //echo $this->form->getInput('routine'); ?>
					<table>			
						<tr>
							<th class="padright">Phases</th>
							<td class="padleft">					
								<table class="routinetable">
									<tr>
										<th>Sets</th>					
										<th>Reps</th>		
										<th>Weight</th>	
										<th>Tempo</th>		
										<th>T.B.S.</th>	
										<th>T.U.T.</th>	
										<th>Time</th>	
										<th>Intensity</th>	
										<th>T.H.R.</th>											
										<th>Distance</th>		
									</tr>
								</table>
							</td>
						</tr>					
					<?php
					$routineDbList= @json_decode($this->item->routine,'array');
					foreach($userLvlList as $userLvlListKey=>$userLvlListVal): ?>	
						<tr>
							<td class="padright"><?php echo $userLvlListVal; ?></td>
							<td class="padleft">					
								<table class="routinetable">
									<tr>
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][sets]" value="<?php echo @$routineDbList[$userLvlListKey]['sets'] ?>"></td>					
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][reps]" value="<?php echo @$routineDbList[$userLvlListKey]['reps'] ?>"></td>		
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][weight]" value="<?php echo @$routineDbList[$userLvlListKey]['weight'] ?>"></td>		
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][tempo]" value="<?php echo @$routineDbList[$userLvlListKey]['tempo'] ?>"></td>		
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][tbs]" value="<?php echo @$routineDbList[$userLvlListKey]['tbs'] ?>"></td>		
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][tut]" value="<?php echo @$routineDbList[$userLvlListKey]['tut'] ?>"></td>		
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][time]" value="<?php echo @$routineDbList[$userLvlListKey]['time'] ?>"></td>		
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][intensity]" value="<?php echo @$routineDbList[$userLvlListKey]['intensity'] ?>"></td>		
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][thr]" value="<?php echo @$routineDbList[$userLvlListKey]['thr'] ?>"></td>		
										<td><input type="text" name="jform[routine][<?php echo $userLvlListKey; ?>][distance]" value="<?php echo @$routineDbList[$userLvlListKey]['distance'] ?>"></td>		
									</tr>
								</table>
							</td>
						</tr>
					<?php	endforeach;	?>				
					</table>	
				</div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('chance'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('chance'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('progression'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('progression'); ?></div>
			</div>
			<?php
				foreach((array)$this->item->progression as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="progression" name="jform[progressionhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('modification'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('modification'); ?></div>
			</div>

			<?php
				foreach((array)$this->item->modification as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="modification" name="jform[modificationhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>	
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('directions'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('directions'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('purpose'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('purpose'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('warmup'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('warmup'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('recovery'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('recovery'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('tips'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('tips'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('notes'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('notes'); ?></div>
			</div>
            </fieldset>
        </div>

        

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>