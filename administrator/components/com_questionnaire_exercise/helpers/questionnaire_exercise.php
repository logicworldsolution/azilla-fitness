<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire_exercise
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Questionnaire_exercise helper.
 */
class Questionnaire_exerciseHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_EXERCISE_TITLE_EXERCISES'),
			'index.php?option=com_questionnaire_exercise&view=exercises',
			$vName == 'exercises'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_EXERCISE_TITLE_CATEGORIES'),
			'index.php?option=com_questionnaire_exercise&view=categories',
			$vName == 'categories'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_EXERCISE_TITLE_MAINCATEGORIES'),
			'index.php?option=com_questionnaire_exercise&view=maincategories',
			$vName == 'maincategories'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_EXERCISE_TITLE_MUSCLES'),
			'index.php?option=com_questionnaire_exercise&view=muscles',
			$vName == 'muscles'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_EXERCISE_TITLE_OVERALLGOALS'),
			'index.php?option=com_questionnaire_exercise&view=overallgoals',
			$vName == 'overallgoals'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_EXERCISE_TITLE_EQUIPMENTS'),
			'index.php?option=com_questionnaire_exercise&view=equipments',
			$vName == 'equipments'
		);
	/*
		JHtmlSidebar::addEntry(
			JText::_('COM_QUESTIONNAIRE_EXERCISE_TITLE_EQUIPMENTCATEGORIES'),
			'index.php?option=com_questionnaire_exercise&view=equipmentcategories',
			$vName == 'equipmentcategories'
		);
	*/
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_questionnaire_exercise';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
