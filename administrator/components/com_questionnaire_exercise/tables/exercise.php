<?php

/**
 * @version     1.0.0
 * @package     com_questionnaire_exercise
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// No direct access
defined('_JEXEC') or die;

/**
 * exercise Table class
 */
class Questionnaire_exerciseTableexercise extends JTable {

    /**
     * Constructor
     *
     * @param JDatabase A database connector object
     */
    public function __construct(&$db) {
        parent::__construct('#__questionnaire_exercise_name', 'id', $db);
    }

    /**
     * Overloaded bind function to pre-process the params.
     *
     * @param	array		Named array
     * @return	null|string	null is operation was satisfactory, otherwise returns an error
     * @see		JTable:bind
     * @since	1.5
     */
    public function bind($array, $ignore = '') {

        
		$input = JFactory::getApplication()->input;
		$task = $input->getString('task', '');
		if(($task == 'save' || $task == 'apply') && (!JFactory::getUser()->authorise('core.edit.state','com_questionnaire_exercise') && $array['state'] == 1)){
			$array['state'] = 0;
		}

		//Support for multiple or not foreign key field: category_id
			if(isset($array['category_id'])){
				if(is_array($array['category_id'])){
					$array['category_id'] = implode(',',$array['category_id']);
				}
				else if(strrpos($array['category_id'], ',') != false){
					$array['category_id'] = explode(',',$array['category_id']);
				}
				else if(empty($array['category_id'])) {
					$array['category_id'] = '';
				}
			}

		//Support for multiple or not foreign key field: exercise_type
			if(isset($array['exercise_type'])){
				if(is_array($array['exercise_type'])){
					$array['exercise_type'] = implode(',',$array['exercise_type']);
				}
				else if(strrpos($array['exercise_type'], ',') != false){
					$array['exercise_type'] = explode(',',$array['exercise_type']);
				}
				else if(empty($array['exercise_type'])) {
					$array['exercise_type'] = '';
				}
			}

		//Support for multiple or not foreign key field: primary_muscles
			if(isset($array['primary_muscles'])){
				if(is_array($array['primary_muscles'])){
					$array['primary_muscles'] = implode(',',$array['primary_muscles']);
				}
				else if(strrpos($array['primary_muscles'], ',') != false){
					$array['primary_muscles'] = explode(',',$array['primary_muscles']);
				}
				else if(empty($array['primary_muscles'])) {
					$array['primary_muscles'] = '';
				}
			}

		//Support for multiple or not foreign key field: secondary_muscles
			if(isset($array['secondary_muscles'])){
				if(is_array($array['secondary_muscles'])){
					$array['secondary_muscles'] = implode(',',$array['secondary_muscles']);
				}
				else if(strrpos($array['secondary_muscles'], ',') != false){
					$array['secondary_muscles'] = explode(',',$array['secondary_muscles']);
				}
				else if(empty($array['secondary_muscles'])) {
					$array['secondary_muscles'] = '';
				}
			}

		//Support for multiple or not foreign key field: progression
			if(isset($array['progression'])){
				if(is_array($array['progression'])){
					$array['progression'] = implode(',',$array['progression']);
				}
				else if(strrpos($array['progression'], ',') != false){
					$array['progression'] = explode(',',$array['progression']);
				}
				else if(empty($array['progression'])) {
					$array['progression'] = '';
				}
			}

		//Support for multiple or not foreign key field: modification
			if(isset($array['modification'])){
				if(is_array($array['modification'])){
					$array['modification'] = implode(',',$array['modification']);
				}
				else if(strrpos($array['modification'], ',') != false){
					$array['modification'] = explode(',',$array['modification']);
				}
				else if(empty($array['modification'])) {
					$array['modification'] = '';
				}
			}
		//Support for multiple field: levels_available
			if (is_array($array['levels_available'])){
				$array['levels_available'] = json_encode($array['levels_available']);
			}	
			
        if (isset($array['params']) && is_array($array['params'])) {
            $registry = new JRegistry();
            $registry->loadArray($array['params']);
            $array['params'] = (string) $registry;
        }

        if (isset($array['metadata']) && is_array($array['metadata'])) {
            $registry = new JRegistry();
            $registry->loadArray($array['metadata']);
            $array['metadata'] = (string) $registry;
        }
        if(!JFactory::getUser()->authorise('core.admin', 'com_questionnaire_exercise.exercise.'.$array['id'])){
            $actions = JFactory::getACL()->getActions('com_questionnaire_exercise','exercise');
            $default_actions = JFactory::getACL()->getAssetRules('com_questionnaire_exercise.exercise.'.$array['id'])->getData();
            $array_jaccess = array();
            foreach($actions as $action){
                $array_jaccess[$action->name] = $default_actions[$action->name];
            }
            $array['rules'] = $this->JAccessRulestoArray($array_jaccess);
        }
        //Bind the rules for ACL where supported.
		if (isset($array['rules']) && is_array($array['rules'])) {
			$this->setRules($array['rules']);
		}

        return parent::bind($array, $ignore);
    }
    
    /**
     * This function convert an array of JAccessRule objects into an rules array.
     * @param type $jaccessrules an arrao of JAccessRule objects.
     */
    private function JAccessRulestoArray($jaccessrules){
        $rules = array();
        foreach($jaccessrules as $action => $jaccess){
            $actions = array();
            foreach($jaccess->getData() as $group => $allow){
                $actions[$group] = ((bool)$allow);
            }
            $rules[$action] = $actions;
        }
        return $rules;
    }

    /**
     * Overloaded check function
     */
    public function check() {

        //If there is an ordering column and this is a new row then get the next ordering value
        if (property_exists($this, 'ordering') && $this->id == 0) {
            $this->ordering = self::getNextOrder();
        }

        return parent::check();
    }

    /**
     * Method to set the publishing state for a row or list of rows in the database
     * table.  The method respects checked out rows by other users and will attempt
     * to checkin rows that it can after adjustments are made.
     *
     * @param    mixed    An optional array of primary key values to update.  If not
     *                    set the instance property value is used.
     * @param    integer The publishing state. eg. [0 = unpublished, 1 = published]
     * @param    integer The user id of the user performing the operation.
     * @return    boolean    True on success.
     * @since    1.0.4
     */
    public function publish($pks = null, $state = 1, $userId = 0) {
        // Initialise variables.
        $k = $this->_tbl_key;

        // Sanitize input.
        JArrayHelper::toInteger($pks);
        $userId = (int) $userId;
        $state = (int) $state;

        // If there are no primary keys set check to see if the instance key is set.
        if (empty($pks)) {
            if ($this->$k) {
                $pks = array($this->$k);
            }
            // Nothing to set publishing state on, return false.
            else {
                $this->setError(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
                return false;
            }
        }

        // Build the WHERE clause for the primary keys.
        $where = $k . '=' . implode(' OR ' . $k . '=', $pks);

        // Determine if there is checkin support for the table.
        if (!property_exists($this, 'checked_out') || !property_exists($this, 'checked_out_time')) {
            $checkin = '';
        } else {
            $checkin = ' AND (checked_out = 0 OR checked_out = ' . (int) $userId . ')';
        }

        // Update the publishing state for rows with the given primary keys.
        $this->_db->setQuery(
                'UPDATE `' . $this->_tbl . '`' .
                ' SET `state` = ' . (int) $state .
                ' WHERE (' . $where . ')' .
                $checkin
        );
        $this->_db->query();

        // Check for a database error.
        if ($this->_db->getErrorNum()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        // If checkin is supported and all rows were adjusted, check them in.
        if ($checkin && (count($pks) == $this->_db->getAffectedRows())) {
            // Checkin each row.
            foreach ($pks as $pk) {
                $this->checkin($pk);
            }
        }

        // If the JTable instance value is in the list of primary keys that were set, set the instance.
        if (in_array($this->$k, $pks)) {
            $this->state = $state;
        }

        $this->setError('');
        return true;
    }
    
    /**
      * Define a namespaced asset name for inclusion in the #__assets table
      * @return string The asset name 
      *
      * @see JTable::_getAssetName 
    */
    protected function _getAssetName() {
        $k = $this->_tbl_key;
        return 'com_questionnaire_exercise.exercise.' . (int) $this->$k;
    }
 
    /**
      * Returns the parrent asset's id. If you have a tree structure, retrieve the parent's id using the external key field
      *
      * @see JTable::_getAssetParentId 
    */
    protected function _getAssetParentId($table = null, $id = null){
        // We will retrieve the parent-asset from the Asset-table
        $assetParent = JTable::getInstance('Asset');
        // Default: if no asset-parent can be found we take the global asset
        $assetParentId = $assetParent->getRootId();
        // The item has the component as asset-parent
        $assetParent->loadByName('com_questionnaire_exercise');
        // Return the found asset-parent-id
        if ($assetParent->id){
            $assetParentId=$assetParent->id;
        }
        return $assetParentId;
    }
    
    

}
