DROP TABLE IF EXISTS `#__questionnaire_exercise_name`;
DROP TABLE IF EXISTS `#__questionnaire_exercise_category`;
DROP TABLE IF EXISTS `#__questionnaire_exercise_maincategory`;
DROP TABLE IF EXISTS `#__questionnaire_exercise_muscles`;
DROP TABLE IF EXISTS `#__questionnaire_exercise_overallgoals`;
DROP TABLE IF EXISTS `#__questionnaire_equipment`;
DROP TABLE IF EXISTS `#__questionnaire_equipment_category`;
