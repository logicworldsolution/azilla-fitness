<?php

/**
 * @version     1.0.0
 * @package     com_questionnaire_exercise
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Questionnaire_exercise records.
 */
class Questionnaire_exerciseModelexercises extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'name', 'a.name',
                'category_id', 'a.category_id',
                'age1', 'a.age1',
				'age2', 'a.age2',				
                'exercise_type', 'a.exercise_type',
                'primary_muscles', 'a.primary_muscles',
                'secondary_muscles', 'a.secondary_muscles',
                'video1', 'a.video1',
                'video2', 'a.video2',
                'video3', 'a.video3',
                'routine', 'a.routine',
                'equipments', 'a.equipments',
                'chance', 'a.chance',
                'progression', 'a.progression',
                'modification', 'a.modification',
				'levels_available','a.levels_available',
				'directions','a.directions',
				'purpose','a.purpose',
				'warmup','a.warmup',
				'recovery','a.recovery',
				'tips','a.tips',
            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     */
    protected function populateState($ordering = null, $direction = null) {
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
        $this->setState('filter.state', $published);

        
		//Filtering category_id
		$this->setState('filter.category_id', $app->getUserStateFromRequest($this->context.'.filter.category_id', 'filter_category_id', '', 'string'));

		//Filtering exercise_type
		$this->setState('filter.exercise_type', $app->getUserStateFromRequest($this->context.'.filter.exercise_type', 'filter_exercise_type', '', 'string'));


        // Load the parameters.
        $params = JComponentHelper::getParams('com_questionnaire_exercise');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.name', 'asc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param	string		$id	A prefix for the store id.
     * @return	string		A store id.
     * @since	1.6
     */
    protected function getStoreId($id = '') {
        // Compile the store id.
        $id.= ':' . $this->getState('filter.search');
        $id.= ':' . $this->getState('filter.state');

        return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'a.*'
                )
        );
        $query->from('`#__questionnaire_exercise_name` AS a');

        
		// Join over the user field 'created_by'
		$query->select('created_by.name AS created_by');
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');
		// Join over the foreign key 'category_id'
		$query->select('#__questionnaire_exercise_category_655577.name AS categories_name_655577');
		$query->join('LEFT', '#__questionnaire_exercise_category AS #__questionnaire_exercise_category_655577 ON #__questionnaire_exercise_category_655577.id = a.category_id');
		// Join over the foreign key 'exercise_type'
		$query->select('#__questionnaire_exercise_overallgoals_655579.name AS overallgoals_name_655579');
		$query->join('LEFT', '#__questionnaire_exercise_overallgoals AS #__questionnaire_exercise_overallgoals_655579 ON #__questionnaire_exercise_overallgoals_655579.id = a.exercise_type');
		// Join over the foreign key 'primary_muscles'
		$query->select('#__questionnaire_exercise_muscles_655580.name AS muscles_name_655580');
		$query->join('LEFT', '#__questionnaire_exercise_muscles AS #__questionnaire_exercise_muscles_655580 ON #__questionnaire_exercise_muscles_655580.id = a.primary_muscles');
		// Join over the foreign key 'secondary_muscles'
		$query->select('#__questionnaire_exercise_muscles_655581.name AS muscles_name_655581');
		$query->join('LEFT', '#__questionnaire_exercise_muscles AS #__questionnaire_exercise_muscles_655581 ON #__questionnaire_exercise_muscles_655581.id = a.secondary_muscles');
		// Join over the foreign key 'progression'
		$query->select('#__questionnaire_exercise_name_655588.name AS exercises_name_655588');
		$query->join('LEFT', '#__questionnaire_exercise_name AS #__questionnaire_exercise_name_655588 ON #__questionnaire_exercise_name_655588.id = a.progression');

        
    // Filter by published state
    $published = $this->getState('filter.state');
    if (is_numeric($published)) {
        $query->where('a.state = '.(int) $published);
    } else if ($published === '') {
        $query->where('(a.state IN (0, 1))');
    }
    

        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                $query->where('( a.name LIKE '.$search.' )');
            }
        }

        

		//Filtering category_id
		$filter_category_id = $this->state->get("filter.category_id");
		if ($filter_category_id) {
			$query->where("a.category_id = '".$db->escape($filter_category_id)."'");
		}

		//Filtering exercise_type
		$filter_exercise_type = $this->state->get("filter.exercise_type");
		if ($filter_exercise_type) {
			$query->where("a.exercise_type REGEXP ',?".$db->escape($filter_exercise_type).",?'");
		}


        // Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }

        return $query;
    }

    public function getItems() {
        $items = parent::getItems();
        
		foreach ($items as $oneItem) {

			if (isset($oneItem->category_id)) {
				$values = explode(',', $oneItem->category_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('name')
							->from('`#__questionnaire_exercise_category`')
							->where('id = ' .$value);
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->category_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->category_id;

			}
			/*
			if (isset($oneItem->exercise_type)) {
				$values = explode(',', $oneItem->exercise_type);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('name')
							->from('`#__questionnaire_exercise_overallgoals`')
							->where('id = ' .$value);
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->exercise_type = !empty($textValue) ? implode(', ', $textValue) : $oneItem->exercise_type;

			}

			if (isset($oneItem->primary_muscles)) {
				$values = explode(',', $oneItem->primary_muscles);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('name')
							->from('`#__questionnaire_exercise_muscles`')
							->where('id = ' .$value);
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->primary_muscles = !empty($textValue) ? implode(', ', $textValue) : $oneItem->primary_muscles;

			}

			if (isset($oneItem->secondary_muscles)) {
				$values = explode(',', $oneItem->secondary_muscles);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('name')
							->from('`#__questionnaire_exercise_muscles`')
							->where('id = ' .$value);
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->secondary_muscles = !empty($textValue) ? implode(', ', $textValue) : $oneItem->secondary_muscles;

			}

			if (isset($oneItem->progression)) {
				$values = explode(',', $oneItem->progression);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('name')
							->from('`#__questionnaire_exercise_name`')
							->where('id = ' .$value);
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->progression = !empty($textValue) ? implode(', ', $textValue) : $oneItem->progression;

			}
			*/
		}
        return $items;
    }

}
