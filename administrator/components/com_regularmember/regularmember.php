<?php
/*------------------------------------------------------------------------
# regularmember.php - Regularmember Component
# ------------------------------------------------------------------------
# author    Azilla
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   azzilla.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_regularmember')){
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
};

// require helper files
JLoader::register('RegularmemberHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'regularmember.php');

// import joomla controller library
jimport('joomla.application.component.controller');

// Add CSS file for all pages
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_regularmember/assets/css/regularmember.css');
$document->addScript('components/com_regularmember/assets/js/regularmember.js');

// Get an instance of the controller prefixed by Regularmember
$controller = JController::getInstance('Regularmember');

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();

?>