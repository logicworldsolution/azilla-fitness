<?php

/**
 * @version     1.0.0
 * @package     com_usermeals
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class UsermealsViewUsermeal extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->item->id == 0);

		$canDo		= UsermealsHelper::getActions();

		JToolBarHelper::title(JText::_('COM_USERMEALS_TITLE_USERMEAL'), 'usermeal.png');
		$id=$this->item->id;
		
		$query="SELECT count(id) totalCount  FROM #__questionnaire_nutrition_recipes WHERE user_recipe_id=$id";
		
		$db			= JFactory::getDBO();
		$db->setQuery($query);
		
		// If not checked out, can save the item.
		if (($canDo->get('core.edit')||($canDo->get('core.create'))))
		{

			JToolBarHelper::apply('usermeal.apply', 'JTOOLBAR_APPLY');
			
			if($db->loadObject()->totalCount==0){
				JToolBarHelper::save('usermeal.systemRecipe', 'Make System Recipe');
			}
			
		}
		
		
		JToolBarHelper::cancel('usermeal.cancelusermeal', 'JTOOLBAR_CLOSE');

	}
}
