<?php
/**
 * @version     1.0.0
 * @package     com_usermeals
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_usermeals/assets/css/usermeals.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){

	js('input:hidden.recipe_type_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('recipe_type_idhidden')){
			js('#jform_recipe_type_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_recipe_type_id").trigger("liszt:updated");
        
    });
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'usermeal.cancel'){
            Joomla.submitform(task, document.getElementById('usermeal-form'));
        }
        else{
            
				js = jQuery.noConflict();
				if(js('#jform_picture').val() != ''){
					js('#jform_picture_hidden').val(js('#jform_picture').val());
				}
            if (task != 'usermeal.cancel' && document.formvalidator.isValid(document.id('usermeal-form'))) {
                	if(js('#jform_recipe_type_id option:selected').length == 0){
						js("#jform_recipe_type_id option[value=0]").attr('selected','selected');
					}
				
                Joomla.submitform(task, document.getElementById('usermeal-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_usermeals&view=usermeal&layout=default&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="usermeal-form" class="form-validate">
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset class="adminform">

             	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
			<input type="hidden" name="jform[user_id]" value="<?php echo $this->form->getValue('user_id'); ?>" />

 			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('food_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('food_id'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('recipe_type_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('recipe_type_id'); ?></div>
			</div>	<?php
				foreach((array)$this->item->recipe_type_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="recipe_type_id" name="jform[recipe_type_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>		
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('brand_name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('brand_name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('serv_size'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('serv_size'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('calories'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('calories'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('carbohydrates'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('carbohydrates'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('protein'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('protein'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('total_carbohydrates'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('total_carbohydrates'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('total_fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('total_fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('total_protein'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('total_protein'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('saturated_fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('saturated_fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('trans_fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('trans_fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('cholesterol'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('cholesterol'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('sodium'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('sodium'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('dietary_fiber'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('dietary_fiber'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('sugars'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('sugars'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_a'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_a'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_c'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_c'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_d'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_d'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('vitamin_k'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('vitamin_k'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('folic_acid'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('folic_acid'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('calcium'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('calcium'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('potassium'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('potassium'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('iron'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('iron'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('blood_type'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('blood_type'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('notes'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('notes'); ?></div>
			</div>
             <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('ingredients'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('ingredients'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('cooking_directions'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('cooking_directions'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('picture'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('picture'); ?></div>
			</div>
				<?php if (!empty($this->item->picture)) : ?>

                        <img style="margin-bottom:20px;" src="<?php echo JRoute::_(JUri::base() . 'components/com_questionnaire_nutrition/asset/' . $this->item->picture, false);?>" width="250" />
				<?php endif; ?>
				<input type="hidden" name="jform[picture]" id="jform_picture_hidden" value="<?php echo $this->item->picture ?>" />

            </fieldset>
        </div>

        

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>