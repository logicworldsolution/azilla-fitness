<?php
/**
 * @version     1.0.0
 * @package     com_usermeals
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_usermeals/assets/css/usermeals.css');

$db=JFactory::getDBO();
$app			= JFactory::getApplication();
$user_id=$app->input->get('user_id');

$query="SELECT * FROM #__usermeals WHERE user_id=$user_id";
$db->setQuery($query);
$user_meals=$db->loadObjectList();
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){
        
    });
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'usermeal.cancel'){
            Joomla.submitform(task, document.getElementById('usermeal-form'));
        }
        else{
            
				js = jQuery.noConflict();
				if(js('#jform_picture').val() != ''){
					js('#jform_picture_hidden').val(js('#jform_picture').val());
				}
            if (task != 'usermeal.cancel' && document.formvalidator.isValid(document.id('usermeal-form'))) {
                
                Joomla.submitform(task, document.getElementById('usermeal-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>
<form action="<?php echo JRoute::_('index.php?option=com_usermeals'); ?>" method="post" name="adminForm" id="usermeal-form" class="form-validate">
    <div class="row-fluid">
       <table class="table table-striped" id="usermealList">
			<thead>
				<tr>
					<th width="1%" class="nowrap center">Id</th>
					<th width="1%" class="nowrap center">Recipe Name</th>
					<th width="1%" class="nowrap center">Meal</th>
					<th width="1%" class="nowrap center">Action</th>
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php //echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php $i=0; foreach ($user_meals as $user_mealsVal) :
				?>
				<tr class="row<?php echo $i++ % 2; ?>">
					<td class="center"><?php echo $user_mealsVal->id; ?></td>
					<td class="center"><?php echo $user_mealsVal->name; ?></td>
					<td class="center">Meal <?php echo $user_mealsVal->meal_id; ?></td>
					<td class="center"><a href="<?php echo JRoute::_("index.php?option=com_usermeals&task=usermeal.mealdetails&id=".$user_mealsVal->id); ?>">View</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
    </div>
      <input type="hidden" name="task" value="" />
      <?php echo JHtml::_('form.token'); ?>
</form>