<?php
/**
 * @version     1.0.0
 * @package     com_usermeals
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Sunil <info@34interactive.com> - http://thirtyfour.in
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Usermeal controller class.
 */
class UsermealsControllerUsermeal extends JControllerForm
{

    function __construct() {
        $this->view_list = 'usermeals';
        parent::__construct();
    }
	
	function mealdetails(){
		$app			= JFactory::getApplication();
		
		$data = $app->input->get('id', array(), 'array');

		$id=$data[0];
		
		$this->setRedirect(JRoute::_("index.php?option=com_usermeals&view=usermeal&id=$id", false));		
	}
	
	function cancelusermeal($key = NULL){
		$app			= JFactory::getApplication();

		$data = $app->input->post->get('jform', array(), 'array');
		
		$user_id=$data['user_id'];
		
		$this->setRedirect(JRoute::_("index.php?option=com_usermeals&view=usermeallist&user_id=$user_id", false));		
	}
	
	 public function save($key = NULL, $urlVar = NULL){
		$app			= JFactory::getApplication();

		$data = $app->input->post->get('jform', array(), 'array');

		$model	= $this->getModel('Usermeal');
		$return	= $model->save($data);
		$user_id=$data['user_id'];			
		if ($return === false) {
			$this->setMessage("Fail to updated recipe", 'warning');
			
			$this->setRedirect(JRoute::_("index.php?option=com_usermeals&view=usermeallist&user_id=$user_id", false));
			return false;
		}else{
				$this->setMessage("Recipe updated successfully");
				$this->setRedirect(JRoute::_("index.php?option=com_usermeals&view=usermeallist&user_id=$user_id", false));
		}
	} 
	
	function systemRecipe(){
		$app			= JFactory::getApplication();
		$db			= JFactory::getDBO();
		
		$data = $app->input->post->get('jform', array(), 'array');
		
		$data['recipe_type_id']=implode(',',$data['recipe_type_id']);
		
		$query="INSERT INTO #__questionnaire_nutrition_recipes(created_by,name,food_id,recipe_type_id,brand_name,serv_size,calories,carbohydrates,fat,protein,total_carbohydrates 	,total_fat,total_protein,saturated_fat,trans_fat,cholesterol,sodium,dietary_fiber,sugars,vitamin_a,vitamin_c,vitamin_d,vitamin_k,folic_acid,calcium,potassium,iron,blood_type,notes,picture,ingredients,cooking_directions,user_recipe_id)VALUES({$data['user_id']},'{$data['name']}',{$data['food_id']},'{$data['recipe_type_id']}','{$data['brand_name']}','{$data['serv_size']}',{$data['calories']},{$data['carbohydrates']},{$data['fat']},{$data['protein']},{$data['total_carbohydrates']},{$data['total_fat']},{$data['total_protein']},{$data['saturated_fat']},{$data['trans_fat']},{$data['cholesterol']},{$data['sodium']},{$data['dietary_fiber']},{$data['sugars']},{$data['vitamin_a']},{$data['vitamin_c']},{$data['vitamin_d']},{$data['vitamin_k']},{$data['folic_acid']},{$data['calcium']},{$data['potassium']},{$data['iron']},'{$data['blood_type']}','{$data['notes']}','{$data['picture']}','{$data['ingredients']}','{$data['cooking_directions']}',{$data['id']})";

		$db->setQuery($query);
		
		$db->query();
		if($db->insertid()>0){
			$recipe_id=$db->insertid();
			$this->setMessage("Recipe added successfully.");
			$this->setRedirect(JRoute::_('index.php?option=com_usermeals&view=usermeallist&user_id='.$data['user_id'], false));
		}else{
			$this->setMessage("Error added successfully.");
			$this->setRedirect(JRoute::_("index.php?option=com_usermeals&view=usermeal&layout=default&id={$data['id']}", false));		
		}
	}	
}