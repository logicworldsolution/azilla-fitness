<?php
/**
 * @version     1.0.0
 * @package     com_userlevel
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      malwinder <malwinder@34interactive.com> - http://
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Userlevel controller class.
 */
class UserlevelControllerUserlevel extends JControllerForm
{

    function __construct() {
        $this->view_list = 'userlevels';
		
		$app = JFactory::getApplication();
		
		$data = $app->input->post->get('jform', array(), 'array');

		if(isset($data['workout_days']) and  !empty($data['workout_days'])){
			$data['workout_days']=json_encode($data['workout_days']);
			$app->input->post->set('jform',$data);
		}
		
        parent::__construct();
    }

}