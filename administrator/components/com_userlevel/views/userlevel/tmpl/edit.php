<?php
/**
 * @version     1.0.0
 * @package     com_userlevel
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      malwinder <malwinder@34interactive.com> - http://
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_userlevel/assets/css/userlevel.css');
?>
<script type="text/javascript">
    Joomla.submitbutton = function(task)
    {
        if(task == 'userlevel.cancel'){
            Joomla.submitform(task, document.getElementById('userlevel-form'));
        }
        else{
            
            if (task != 'userlevel.cancel' && document.formvalidator.isValid(document.id('userlevel-form'))) {
                
                Joomla.submitform(task, document.getElementById('userlevel-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
	var sublvl_workout_sum=0;
	
	function cal_workout_progression(){	 
			sublvl_workout_sum=0;
			jQuery('.sublvl_workout').each(function(){
				if(jQuery.isNumeric(jQuery(this).val())){ 
					sublvl_workout_sum=sublvl_workout_sum+parseInt(jQuery(this).val());
				}
			});	
			jQuery('.workouts').val(sublvl_workout_sum);
			jQuery('.daysToCompleteWorkout').each(function(){
				if(jQuery.isNumeric(jQuery(this).val())){
					var workouts_nextlvl_sum=parseInt(sublvl_workout_sum);
					var daysToCompleteWorkout=parseInt(jQuery(this).val());
					var workouts_progression=workouts_nextlvl_sum/daysToCompleteWorkout*100;
					workouts_progression=workouts_progression.toFixed(1);
					if(jQuery.isNumeric(workouts_progression)){
						jQuery(this).parents('tr').find('.progression').val(workouts_progression);			
					}
				}
			});		
	}		
	
	jQuery().ready(function(){
		cal_workout_progression();
				
		jQuery('.sublvl_workout').change(function(){
			cal_workout_progression();
		});	
	});	
</script>
<style>
.sublvl_workout { width:90px; text-align:center}
.workout_days th{ padding:5px 10px; text-align:center;}
.workout_days td{ text-align:center;}
.workout_days td input{ width:90px; text-align:center}
</style>
<form action="<?php echo JRoute::_('index.php?option=com_userlevel&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="userlevel-form" class="form-validate" >
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset class="adminform">

                				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('state'); ?></div>
			</div>
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('level_1'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('level_1'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('level_2'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('level_2'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('level_3'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('level_3'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('level_4'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('level_4'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('level_5'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('level_5'); ?></div>
			</div>
            <hr />
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('workout_days'); ?></div>
				<div class="controls">
               		 <table class="workout_days">
						<tr>
							<th>Workouts for next level</th><th>Number of days user workouting out</th><th>Days to complete level</th><th>Progression</th>
						</tr>
						<tr> 
							<td><input type="text" readonly class="workouts" name='jform[workout_days][day3][workouts]'  value="<?php echo @json_decode($this->item->workout_days)->day3->workouts; ?>" ></td>
							<td><input type="text" readonly name='jform[workout_days][day3][userDays]'  value="3" ></td>
							<td><input type="text" readonly class="daysToCompleteWorkout" name='jform[workout_days][day3][daysToCompleteWorkout]'  value="<?php echo @json_decode($this->item->workout_days)->day3->daysToCompleteWorkout; ?>" ></td>
							<td><input type="text" readonly class="progression" name='jform[workout_days][day3][progression]'  value="<?php echo @json_decode($this->item->workout_days)->day3->progression; ?>" ></td>
						</tr>						
						<tr>
							<td><input type="text" readonly class="workouts" name='jform[workout_days][day4][workouts]'  value="<?php echo @json_decode($this->item->workout_days)->day4->workouts; ?>" ></td>
							<td><input type="text" readonly name='jform[workout_days][day4][userDays]'  value="4" ></td>
							<td><input type="text" readonly class="daysToCompleteWorkout" name='jform[workout_days][day4][daysToCompleteWorkout]'  value="<?php echo @json_decode($this->item->workout_days)->day4->daysToCompleteWorkout; ?>" ></td>
							<td><input type="text" readonly class="progression" name='jform[workout_days][day4][progression]'  value="<?php echo @json_decode($this->item->workout_days)->day4->progression; ?>" ></td>
						</tr>						
						<tr>
							<td><input type="text" readonly class="workouts" name='jform[workout_days][day5][workouts]'  value="<?php echo @json_decode($this->item->workout_days)->day5->workouts; ?>" ></td>
							<td><input type="text" readonly name='jform[workout_days][day5][userDays]'  value="5" ></td>
							<td><input type="text" readonly class="daysToCompleteWorkout" name='jform[workout_days][day5][daysToCompleteWorkout]'  value="<?php echo @json_decode($this->item->workout_days)->day5->daysToCompleteWorkout; ?>" ></td>
							<td><input type="text" readonly class="progression" name='jform[workout_days][day5][progression]'  value="<?php echo @json_decode($this->item->workout_days)->day5->progression; ?>" ></td>
						</tr>						
						<tr>
							<td><input type="text" readonly class="workouts" name='jform[workout_days][day6][workouts]'  value="<?php echo @json_decode($this->item->workout_days)->day6->workouts; ?>" ></td>
							<td><input type="text" readonly name='jform[workout_days][day6][userDays]'  value="6" ></td>
							<td><input type="text" readonly class="daysToCompleteWorkout" name='jform[workout_days][day6][daysToCompleteWorkout]'  value="<?php echo @json_decode($this->item->workout_days)->day6->daysToCompleteWorkout; ?>" ></td>
							<td><input type="text" readonly class="progression" name='jform[workout_days][day6][progression]'  value="<?php echo @json_decode($this->item->workout_days)->day6->progression; ?>" ></td>
						</tr>						
					</table>
                </div>
			</div>


            </fieldset>
        </div>

        

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
<br /><br /><br /><br />