<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Questionnaire records.
 */
class QuestionnaireModelquestionnaires extends JModelList
{

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                                'id', 'a.id',
                'address1', 'a.address1',
                'address2', 'a.address2',
                'city', 'a.city',
                'state', 'a.state',
                'zip', 'a.zip',
                'bill_address1', 'a.bill_address1',
                'bill_address2', 'a.bill_address2',
                'bill_city', 'a.bill_city',
                'bill_state', 'a.bill_state',
                'bill_zip', 'a.bill_zip',
                'gender', 'a.gender',
                'height', 'a.height',
                'weight', 'a.weight',
                'fat', 'a.fat',
                'blood_type', 'a.blood_type',
                'birthday', 'a.birthday',
                'measurements', 'a.measurements',
                'weekly_activity_lvl', 'a.weekly_activity_lvl',
                'primary_goal', 'a.primary_goal',
                'overallgoal', 'a.overallgoal',
                'workout_path', 'a.workout_path',
                'equipment_available', 'a.equipment_available',
                'experience_lvl_working_out', 'a.experience_lvl_working_out',
                'you_train_days', 'a.you_train_days',
                'workout_partner', 'a.workout_partner',
                'where_you_train_day', 'a.where_you_train_day',
                'resting_heart', 'a.resting_heart',
                'cardiovascular_exercise_goal', 'a.cardiovascular_exercise_goal',
                'cardiovascular_exercise_experience_lvl', 'a.cardiovascular_exercise_experience_lvl',
                'equipment_or_amenities_you_have_available', 'a.equipment_or_amenities_you_have_available',
                'day_start_your_fitness_cardiovascular_program', 'a.day_start_your_fitness_cardiovascular_program',
                'eat_a_day_meal_snacks', 'a.eat_a_day_meal_snacks',
                'nutrition_program_start', 'a.nutrition_program_start',
                'scenario_program', 'a.scenario_program',
                'night_sleep_hours', 'a.night_sleep_hours',
                'question_answers', 'a.question_answers',
                'user_id', 'a.user_id',
                'promotional_code', 'a.promotional_code',
                'fitness_business_code', 'a.fitness_business_code',
                'fitness_membership', 'a.fitness_membership',
                'fitness_affiliate_membership', 'a.fitness_affiliate_membership',

            );
        }

        parent::__construct($config);
    }


	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
        
        
        
		// Load the parameters.
		$params = JComponentHelper::getParams('com_questionnaire');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 * @return	string		A store id.
	 * @since	1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id.= ':' . $this->getState('filter.search');
		$id.= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.*'
			)
		);
		$query->from('`#__questionnaire_question` AS a');

    // Filter by published state
   /* $published = $this->getState('filter.state');
    if (is_numeric($published)) {
        $query->where('a.state = '.(int) $published);
    } else if ($published === '') {
        $query->where('(a.state IN (0, 1))');
    }*/
    

		// Filter by search in title
/*		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
                
			}
		}*/
        
        
        
        
		// Add the list ordering clause.
/*        $orderCol	= $this->state->get('list.ordering');
        $orderDirn	= $this->state->get('list.direction');
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol.' '.$orderDirn));
        }
*/
		return $query;
	}
}
