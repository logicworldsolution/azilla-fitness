<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of categories
 */
class JFormFieldOverallgoal extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'overallgoal';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		$html = array();

		// Initialize some field attributes.
		$class = $this->element['class'] ? ' class="checkboxes ' . (string) $this->element['class'] . '"' : ' class="checkboxes"';
		$checkedOptions = explode(',', (string) $this->element['checked']);

		// Start the checkbox field output.
		$html[] = '<fieldset id="' . $this->id . '"' . $class . '>';

		$query = (string) $this->element['query'];

		// Get the database object.
		$db = JFactory::getDBO();

		// Set the query and get the result list.
		$db->setQuery($query);
		$items = $db->loadObjectlist();
		
		// Get the field options.		
		$userId=$_GET['id'];
		$user_list='';
		$user_goals_list_ids='';

		if(!empty($userId)){
			$user_exercise_type="SELECT overallgoal FROM #__questionnaire_question WHERE id=$userId";
			$db->setQuery($user_exercise_type);
			$user_list = $db->loadObject();
			
			if(!empty($user_list->overallgoal)){
				$user_goals_items=$user_list->overallgoal;
				$user_checked_goals_query="SELECT id,name FROM #__questionnaire_exercise_overallgoals WHERE id IN ($user_goals_items)";
				$db->setQuery($user_checked_goals_query);
				$user_goals_list_ids = $db->loadObjectList();				
			}
		}

		$html[] = '<ul>';
		foreach ($items as $i => $option)
		{
			$checked='';
			// Initialize some option attributes.
			if(!empty($user_goals_list_ids)){
				foreach($user_goals_list_ids as $user_goals_list_ids_val){
					if($option->id==$user_goals_list_ids_val->id){
						$checked = ' checked="checked" ';
						break;
					}
				}
			}
			
			$html[] = '<li>';
			$html[] = '<input type="checkbox" id="' . $this->id . $i . '" name="' . $this->name . '[]"' . ' value="'
				. htmlspecialchars($option->id, ENT_COMPAT, 'UTF-8') . '"' . $checked  . '/>';

			$html[] = '<label for="' . $this->id . $i . '"' . $class . '>' . JText::_($option->name) . '</label>';
			$html[] = '</li>';
		}
		$html[] = '</ul>';

		// End the checkbox field output.
		$html[] = '</fieldset>';

		return implode($html);
	}

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$options = array();

		foreach ($this->element->children() as $option)
		{

			// Only add <option /> elements.
			if ($option->getName() != 'option')
			{
				continue;
			}

			// Create a new option object based on the <option /> element.
			$tmp = JHtml::_(
				'select.option', (string) $option['value'], trim((string) $option), 'value', 'text',
				((string) $option['disabled'] == 'true')
			);

			// Set some option attributes.
			$tmp->class = (string) $option['class'];

			// Set some JavaScript option attributes.
			$tmp->onclick = (string) $option['onclick'];

			// Add the option object to the result set.
			$options[] = $tmp;
		}

		reset($options);

		return $options;
	}
}