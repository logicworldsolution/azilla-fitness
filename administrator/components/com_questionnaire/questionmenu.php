<?php 
$usermenu_sessionObj =JFactory::getSession();

if(!empty($_GET['id'])){
	$usermenu_sessionObj->set( 'user_menu_id', $_GET['id'] );
	$user_menu_id=$_GET['id'];
}else{
	$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
}
 ?>
<div id="sidebar">
	<div class="sidebar-nav">
        <ul class="nav nav-list member-info" id="submenu" >
            <li <?php echo (@$_GET['layout']=='new_member')? "class='active'" : '';  ?> ><a href="<?php echo JRoute::_("index.php?option=com_questionnaire&view=question&layout=new_member&id=$user_menu_id");?>">New Member Information</a></li>
            <li <?php echo (@$_GET['layout']=='address')? "class='active'" : '';  ?> ><a href="<?php echo JRoute::_("index.php?option=com_questionnaire&view=question&layout=address&id=$user_menu_id");?>">Address Information</a></li>
            <li <?php echo (@$_GET['layout']=='payment')? "class='active'" : '';  ?> ><a href="<?php echo JRoute::_("index.php?option=com_questionnaire&view=question&layout=payment&id=$user_menu_id");?>">Payment and Credit Card Information</a></li>
            <li <?php echo (@$_GET['layout']=='stats')? "class='active'" : '';  ?> ><a href="<?php echo JRoute::_("index.php?option=com_questionnaire&view=question&layout=stats&id=$user_menu_id");?>">My Stats and Information</a></li>
            <li <?php echo (@$_GET['layout']=='goal')? "class='active'" : '';  ?> ><a href="<?php echo JRoute::_("index.php?option=com_questionnaire&view=question&layout=goal&id=$user_menu_id");?>">Primary and Overall Goal(s)</a></li>
            <li <?php echo (@$_GET['layout']=='fitness')? "class='active'" : '';  ?> ><a href="<?php echo JRoute::_("index.php?option=com_questionnaire&view=question&layout=fitness&id=$user_menu_id");?>">My Fitness Path</a></li>
            <li <?php echo (@$_GET['layout']=='cardiovascular')? "class='active'" : '';  ?> ><a href="<?php echo JRoute::_("index.php?option=com_questionnaire&view=question&layout=cardiovascular&id=$user_menu_id");?>">My Cardiovascular Path</a></li>
            <li <?php echo (@$_GET['layout']=='nutrition')? "class='active'" : '';  ?> ><a href="<?php echo JRoute::_("index.php?option=com_questionnaire&view=question&layout=nutrition&id=$user_menu_id");?>">My Nutrition Path</a></li>
        </ul>
    </div>
</div>        