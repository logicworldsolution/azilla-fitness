<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Question controller class.
 */
class QuestionnaireControllerQuestion extends JControllerForm
{

    function __construct() {
        $this->view_list = 'questionnaires';
        parent::__construct();
    }

/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 * @since	1.6
	 */
	 
	 public function save(){
		$app			= JFactory::getApplication();
	 	$usermenu_sessionObj =JFactory::getSession();
		$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
		$loginUserId	= (int) $user_menu_id;
		
		$data = $app->input->post->get('jform', array(), 'array');

		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$data['id']=$data['user_id'] = $loginUserId;

		$model	= $this->getModel('Question');
		
		$return	= $model->save($data);
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message");
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
		}
	} 
	
		 
	 public function savestats(){
	
		$app			= JFactory::getApplication();
		$usermenu_sessionObj =JFactory::getSession();
		$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
		$loginUserId	= (int) $user_menu_id;

		$db			= JFactory::getDBO();
		
		$data = $app->input->post->get('jform', array(), 'array');

		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$user_id=$data['id']=$data['user_id'] = $loginUserId;
		
		if(isset($data['measuremts'])){
			$data['measuremts']= json_encode($data['measuremts']);
		}else{
			$data['measuremts']='';
		}	

		$model	= $this->getModel('Question');
		
		$return	= $model->save($data);
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message");
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
		}
	} 
	
	 public function savegoal(){
		$app			= JFactory::getApplication();
		$usermenu_sessionObj =JFactory::getSession();
		$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
		$loginUserId	= (int) $user_menu_id;
		$db			= JFactory::getDBO();
		
		$data = $app->input->post->get('jform', array(), 'array');
		
		if(isset($data['overallgoal'])){
				$data['overallgoal']=implode(',',$data['overallgoal']);
		}else{
			$data['overallgoal']='';
		}	


		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$data['id']=$data['user_id'] = $loginUserId;

		$model	= $this->getModel('Question');
		
		$return	= $model->save($data);
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message");
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
		}
	} 
	
	function savefitness(){

		$app			= JFactory::getApplication();
		$usermenu_sessionObj =JFactory::getSession();
		$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
		$loginUserId	= (int) $user_menu_id;
		
		$data = $app->input->post->get('jform', array(), 'array');

		if(isset($data['workout_path'])){
			$data['workout_path']=implode(',',$data['workout_path']);
		}else{
			$data['workout_path']='';
		}
		
		if(isset($data['you_train_days'])){
			$data['you_train_days']=implode(',',$data['you_train_days']);
		}else{
			$data['you_train_days']='';
		}
		
		if($data['have_workout_partner']==0){
			$data['workout_partner_days']='';
		}else{
			$data['workout_partner_days']=implode(',',$data['workout_partner_days']);
		}

		if(isset($data['equipment_available'])){
				$data['equipment_available']= json_encode($data['equipment_available']);
		}else{
			$data['equipment_available']='';
		}
		
		if(isset($data['where_you_train_day'])){
				$data['where_you_train_day']= json_encode($data['where_you_train_day']);
		}else{
			$data['where_you_train_day']='';
		}
		
		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$data['id']=$data['user_id'] = $loginUserId;

		$model	= $this->getModel('Question');
		
		$return	= $model->save($data);
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message");
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
		}
	}
	
	function  savecardiovascular(){
		$app			= JFactory::getApplication();
		$usermenu_sessionObj =JFactory::getSession();
		$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
		$loginUserId	= (int) $user_menu_id;
		
		$data = $app->input->post->get('jform', array(), 'array');

		if(isset($data['cardiovascular_exercise_goal'])){
			$data['cardiovascular_exercise_goal']=implode(',',$data['cardiovascular_exercise_goal']);
		}else{
			$data['cardiovascular_exercise_goal']='';
		}
		
		if(isset($data['equipment_or_amenities_you_have_available'])){
			$data['equipment_or_amenities_you_have_available']= json_encode($data['equipment_or_amenities_you_have_available']);
		}else{
			$data['equipment_or_amenities_you_have_available']='';
		}
		
		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$data['id']=$data['user_id'] = $loginUserId;

		$model	= $this->getModel('Question');
		
		$return	= $model->save($data);
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message");
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
		}
	}

	function savenutrition(){
		$app			= JFactory::getApplication();
		
		$usermenu_sessionObj =JFactory::getSession();
		$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
		$loginUserId	= (int) $user_menu_id;
		
		$data = $app->input->post->get('jform', array(), 'array');
		
		if(isset($data['scenario_program'])){
			$data['scenario_program']= json_encode($data['scenario_program']);
		}else{
			$data['scenario_program']='';
		}
		
		if(isset($data['question_answers'])){
			$data['question_answers']= json_encode($data['question_answers']);
		}else{
			$data['question_answers']='';
		}	
		
		if(isset($data['foods_condiments_like'])){
			$data['foods_condiments_like']= json_encode($data['foods_condiments_like']);
		}else{
			$data['foods_condiments_like']='';
		}		

		$action=$app->input->post->get('action');
		$action_fail_message=$app->input->post->get('action_fail_message');
		$action_fail_message=str_replace('_',' ',$action_fail_message);
		$action_success_message=$app->input->post->get('action_success_message');
		$action_success_message=str_replace('_',' ',$action_success_message);

		$data['id']=$data['user_id'] = $loginUserId;

		$model	= $this->getModel('Question');

		$return	= $model->save($data);
		
		if ($return === false) {
			$this->setMessage("$action_fail_message", 'warning');
			$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
			return false;
		}else{
				$this->setMessage("$action_success_message");
				$this->setRedirect(JRoute::_("index.php?option=com_questionnaire&view=question&layout=$action&id=$user_menu_id", false));
		}
	}
}