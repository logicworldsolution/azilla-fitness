<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_questionnaire/assets/css/questionnaire.css');

$user	= JFactory::getUser();
?>
<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) {
    $this->sidebar .= $this->extra_sidebar;
}
?>

<form action="<?php echo JRoute::_('index.php?option=com_questionnaire&view=questionnaires'); ?>" method="post" name="adminForm" id="adminForm">
	<div id="j-main-container" >
		<div class="clearfix"> </div>
		<table class="table table-striped" id="questionList" >
			<thead>
				<tr>
					<th width="10%" class="left">
                   	User Id
                    </th>
					<th width="80%" class="left">
					User Name
					</th>
                    <th width="10%" class="left" style="padding-left:20px">
                   	Action
                    </th>
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php
			 foreach ($this->items as $i => $item) :
                $canEdit	= $user->authorise('core.edit',			'com_questionnaire');
                $canCheckin	= $user->authorise('core.manage',		'com_questionnaire');
				?>
				<tr class="row<?php echo $i % 2; ?>">
                    
					<td  width="10%" class="left">
						<?php echo $item->id; ?>
					</td>
                <?php if (isset($this->items[0]->id)): ?>
					<td  width="80%" class="left">
						<?php
						$user->load($item->id);
						echo $user->get('name');
						?>
					</td>
                    <td  width="10%" class="left">
                    <a href="<?php echo JRoute::_('index.php?option=com_questionnaire&view=questiona&layout=new_member&id='.$item->id); ?>">View</a>&nbsp;&nbsp;&nbsp;
                    	<a href="<?php echo JRoute::_('index.php?option=com_questionnaire&view=question&layout=new_member&id='.$item->id); ?>">Edit</a>
                    </td>
                <?php endif; ?>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>        

		
