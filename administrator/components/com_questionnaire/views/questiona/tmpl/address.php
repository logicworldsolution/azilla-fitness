<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$db			= JFactory::getDBO();	

$usermenu_sessionObj =JFactory::getSession();
$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
$loginUserId	= (int) $user_menu_id;

$query	= 'SELECT address1,address2,city,state,zip FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '. "WHERE id= $loginUserId";

$db->setQuery( $query );
$question_obj	= $db->loadObject();

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/questiona/tmpl/css/style.css');

?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel') {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
	}
</script>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<style>
.anskey{
	font-weight:bold;
}
</style>
<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit" >
          <h1 class="nw_member_info">Address Information</h1>
        
         <form  action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.cancel'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
           <div class="menber_lable">
              <div class="control-group">
                <div class="control-label address_one">
                <label > <span> Address 1 :</span>  <span class="anskey"><?php echo $question_obj->address1; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label address_one">
                <label > <span> Address 2 :</span>  <span class="anskey"><?php echo $question_obj->address2; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label address_one">
                <label > <span> City : </span>  <span class="anskey"><?php echo $question_obj->city; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label address_one">
                <label> <span> State :</span>  <span class="anskey"><?php echo str_replace('0','',$question_obj->state); ?></span></label></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label address_one">
                <label> <span> Zip: </span> <span class="anskey"><?php echo $question_obj->zip; ?></span></label></label>
              </div>
            </div>
         </div>
          <input type="hidden" name="task" value="" />
           <?php echo JHtml::_('form.token'); ?> 
        </form>
     	</div>
      </div>
      <div class="span4 respon_span4"> <?php require_once JPATH_COMPONENT.'/questionmenua.php'; ?>
     </div>
    </div>
  </div>
</div>
