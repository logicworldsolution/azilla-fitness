<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$db			= JFactory::getDBO();	

$usermenu_sessionObj =JFactory::getSession();
$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
$loginUserId	= (int) $user_menu_id;

$query	= 'SELECT promotional_code,fitness_business_code FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '. "WHERE id= $loginUserId";

$db->setQuery( $query );
$question_obj	= $db->loadObject();
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/question/tmpl/css/style.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel') {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
	}
</script>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->



<div class="container">
<div class="row-fluid">
<div class="span12" id="content">



  <div class="span8 respon_span8"> 
  <div class="question-edit front-end-edit" >
        <h1 class="nw_member_info">New Member Information</h1>
         <form  action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.cancel'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
           <div class="menber_lable">
            <div class="control-group">
              <div class="control-label promotional_code_label">
                <label  for="jform_promotional_code" style="width:100%; padding:0 0 20px 0; font-size:13px" >Promotional Code :<span class="anskey" style="font-weight:bold;"><?php echo $question_obj->promotional_code; ?></span></label>
              </div>
            </div>
            <div class="control-group promotion_input">
              <div class="control-label">
                <label  for="jform_fitness_business_code" style="width:100%; padding:0 0 20px 0; font-size:13px" >Fitness Business Code : <span class="anskey" style="font-weight:bold;"><?php echo $question_obj->fitness_business_code; ?></span></label>
             <div class="control-group promotion_input">
              <div class="control-label"> </div>
              <div class="controls">
                <div class="submit_member">
          <input type="hidden" name="task" value="" />
           <?php echo JHtml::_('form.token'); ?>
            </div>
            </div>
          </div> 
        </form>
     </div></div>
     </div>
     </div>
     </div>
     
     
     
    <div class="span4 respon_span4"> <?php require_once JPATH_COMPONENT.'/questionmenua.php'; ?>
     </div>
 


</div></div></div>