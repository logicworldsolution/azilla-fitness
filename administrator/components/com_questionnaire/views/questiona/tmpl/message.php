<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_questionnaire', JPATH_ADMINISTRATOR);


?>

<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<style>
    .front-end-edit ul {
        padding: 0 !important;
    }
    .front-end-edit li {
        list-style: none;
        margin-bottom: 6px !important;
    }
    .front-end-edit label {
        margin-right: 10px;
        display: block;
        float: left;
        width: 200px !important;
    }
    .front-end-edit .radio label {
        float: none;
    }
    .front-end-edit .readonly {
        border: none !important;
        color: #666;
    }    
    .front-end-edit #editor-xtd-buttons {
        height: 50px;
        width: 600px;
        float: left;
    }
    .front-end-edit .toggle-editor {
        height: 50px;
        width: 120px;
        float: right;
    }
    
    #jform_rules-lbl{
        display:none;
    }
    
    #access-rules a:hover{
        background:#f5f5f5 url('../images/slider_minus.png') right  top no-repeat;
        color: #444;
    }
    
    fieldset.radio label{
        width: 50px !important;
    }
</style>

<div class="question-edit front-end-edit">
    <?php if(!empty($this->item->id)): ?>
        <h1>Edit <?php echo $this->item->id; ?></h1>
    <?php else: ?>
        <h1>Add</h1>
    <?php endif; ?>

    <form id="form-question" action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <ul>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('address1'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('address1'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('address2'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('address2'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('city'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('city'); ?></div>
			</div>

			<div class="control-group">
				<?php $canState = false; ?>
					<?php $canState = $canState = JFactory::getUser()->authorise('core.edit.state','com_questionnaire'); ?>				<?php if(!$canState): ?>
				<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
					<?php
						$state_string = 'Unpublish';
						$state_value = 0;
						if($this->item->state == 1):
							$state_string = 'Publish';
							$state_value = 1;
						endif;
					?>
					<div class="controls"><?php echo $state_string; ?></div>
					<input type="hidden" name="jform[state]" value="<?php echo $state_value; ?>" />
				<?php else: ?>
					<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('state'); ?></div>					<?php endif; ?>
				</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('zip'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('zip'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('bill_address1'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('bill_address1'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('bill_address2'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('bill_address2'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('bill_city'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('bill_city'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('bill_state'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('bill_state'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('bill_zip'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('bill_zip'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('gender'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('height'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('height'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('weight'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('weight'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('fat'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('fat'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('blood_type'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('blood_type'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('birthday'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('birthday'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('measurements'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('measurements'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('weekly_activity_lvl'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('weekly_activity_lvl'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('primary_goal'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('primary_goal'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('overallgoal'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('overallgoal'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('workout_path'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('workout_path'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('equipment_available'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('equipment_available'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('experience_lvl_working_out'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('experience_lvl_working_out'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('you_train_days'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('you_train_days'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('workout_partner'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('workout_partner'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('where_you_train_day'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('where_you_train_day'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('resting_heart'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('resting_heart'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('cardiovascular_exercise_goal'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('cardiovascular_exercise_goal'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('cardiovascular_exercise_experience_lvl'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('cardiovascular_exercise_experience_lvl'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('equipment_or_amenities_you_have_available'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('equipment_or_amenities_you_have_available'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('day_start_your_fitness_cardiovascular_program'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('day_start_your_fitness_cardiovascular_program'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('eat_a_day_meal_snacks'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('eat_a_day_meal_snacks'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('nutrition_program_start'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('nutrition_program_start'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('scenario_program'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('scenario_program'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('night_sleep_hours'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('night_sleep_hours'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('question_answers'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('question_answers'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('user_id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('user_id'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('promotional_code'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('promotional_code'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('fitness_business_code'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('fitness_business_code'); ?></div>
			</div>

        </ul>
        
		<div>
			<button type="submit" class="validate"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
			<?php echo JText::_('or'); ?>
			<a href="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>

			<input type="hidden" name="option" value="com_questionnaire" />
			<input type="hidden" name="task" value="questionform.save" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
</div>
