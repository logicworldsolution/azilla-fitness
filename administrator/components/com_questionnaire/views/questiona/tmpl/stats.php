<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$db			= JFactory::getDBO();	

$usermenu_sessionObj =JFactory::getSession();
$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
$loginUserId	= (int) $user_menu_id;

$query	= 'SELECT  	gender,	height,	weight,	fat, goal_body_fat,	blood_type,	birthday, measuremts, weekly_activity_lvl FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '. "WHERE id= $loginUserId";

$db->setQuery( $query );

$question_obj	= $db->loadObject();

$measuremts_data=@json_decode($question_obj->measuremts,'Array');
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/questiona/tmpl/css/style.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel') {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
	}
</script>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->

<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit" >
        <h1 class="nw_member_info">My Stats and Information</h1>
        
        
         <form  action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.cancel'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
           <div class="menber_lable">
              <div class="control-group payment_space">
              <div class="control-label">
              
                <label >Gender : <span class="anskey"><?php
				$gender=array(0=>'No',1=>'Male',2=>'Female');
				 echo @$gender[$question_obj->gender]; ?></span></label>
              </div>
            </div>
            
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Height (in cm.): <span class="anskey"><?php echo $question_obj->height; ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Weight (in kg.): <span class="anskey"><?php echo $question_obj->weight; ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Body fat % : <span class="anskey"><?php echo $question_obj->fat; ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Goal body fat % : <span class="anskey"><?php echo $question_obj->goal_body_fat; ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Blood type : <span class="anskey"><?php echo $question_obj->blood_type; ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Birthday : <span class="anskey">
				<?php
				if($question_obj->birthday!='0000-00-00')
				echo date('d-M-Y',strtotime($question_obj->birthday)); 
				?>
				</span></label>
              </div>
            </div>
           
           <div class="restricted_area"> 
           <fieldset>
			<legend class="commen_heading  payment_info">Measuremts</legend>
            
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Neck : <span class="anskey"><?php echo @$measuremts_data['neck'];  ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Chest Line : <span class="anskey"><?php echo @$measuremts_data['chestline'];  ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Shoulders : <span class="anskey"><?php echo @$measuremts_data['shoulders'];  ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label  >Upper Arms : <span class="anskey"><?php echo @$measuremts_data['upperarms'];  ?></span></label>
              </div>
            </div>                        
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Forearms : <span class="anskey"><?php echo @$measuremts_data['forearms'];  ?></span></label>
              </div>
            </div>     
            <div class="control-group payment_space">
              <div class="control-label">
                <label  >Waist : <span class="anskey"><?php echo @$measuremts_data['waist'];  ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label  >Wrist : <span class="anskey"><?php echo @$measuremts_data['wrist'];  ?></span></label>
              </div>
            </div> 
            <div class="control-group payment_space">
              <div class="control-label">
                <label  >Hips : <span class="anskey"><?php echo @$measuremts_data['hips'];  ?></span></label>
              </div>
            </div> 
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Mid-Thighs : <span class="anskey"><?php echo @$measuremts_data['midthighs'];  ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label  >Calves : <span class="anskey"><?php echo @$measuremts_data['calves'];  ?></span></label>
              </div>
            </div>             
           </fieldset>
           </div>
           
            <div class="control-group payment_space">
              <div class="control-label">
                <label  >Weekly activity level on and off the job : <span class="anskey"><?php echo $question_obj->weekly_activity_lvl; ?></span></label>
              </div>
            </div>
          </div>
          <input type="hidden" name="task" value="" />
           <?php echo JHtml::_('form.token'); ?> 
        </form>
		</div>
            </div>
         
   
      <div class="span4 respon_span4"> <?php require_once JPATH_COMPONENT.'/questionmenua.php'; ?>
   </div></div></div>