<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$db			= JFactory::getDBO();	

$usermenu_sessionObj =JFactory::getSession();
$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
$loginUserId	= (int) $user_menu_id;

$query	= 'SELECT fitness_membership,fitness_affiliate_membership,bill_address1,bill_address2,bill_city,bill_state,bill_zip FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '. "WHERE id= $loginUserId";

$db->setQuery( $query );
$question_obj	= $db->loadObject();

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/questiona/tmpl/css/style.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel') {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
	}
</script>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->



<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
    
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit">
        <h1 class="nw_member_info">Payment and Credit Card Information</h1>
        
          <form  action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.cancel'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
         <div class="payment_top_form">
         
         
         <div class="restricted_area"> 
          <fieldset>
				<legend class="payment_info">Payment Information</legend>
            <div class="control-group payment_space">
              <div class="control-label">
                <label >Fitness Membership : <span class="anskey"><?php echo $question_obj->fitness_membership; ?></span></label>
              </div>
            </div>            
            <div class="control-group payment_space">
              <div class="control-label">
                <label  >Fitness Affiliate Membership : <span class="anskey"><?php echo $question_obj->fitness_affiliate_membership; ?></span></label>
              </div>
            </div>
          </fieldset> 
          </div>
           
         <div class="restricted_area"> 
          <fieldset>
				<legend class="payment_info">Billing Address Information</legend>
            <div class="control-group payment_space">
              <div class="control-label">
                <label>Billing Address 1 : <span class="anskey"><?php echo $question_obj->bill_address1; ?></span></label>
              </div>
            </div>            
            <div class="control-group payment_space">
              <div class="control-label">
                <label>Billing Address 2 : <span class="anskey"><?php echo $question_obj->bill_address2; ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label>City : <span class="anskey"><?php echo $question_obj->bill_city; ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label>State : <span class="anskey"><?php echo $question_obj->bill_state; ?></span></label>
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label>Zip Code : <span class="anskey"><?php echo $question_obj->bill_zip; ?></span></label>
              </div>
            </div>
          </fieldset>
          </div> 
          
          </div>
          <input type="hidden" name="task" value="" />
           <?php echo JHtml::_('form.token'); ?> 
        </form>
        </div></div>
     <div class="span4 respon_span4"> <?php require_once JPATH_COMPONENT.'/questionmenua.php'; ?>
    </div></div></div>
