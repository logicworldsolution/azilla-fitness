<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$db			= JFactory::getDBO();	

$usermenu_sessionObj =JFactory::getSession();
$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
$loginUserId	= (int) $user_menu_id;

$query	= 'SELECT  	A.workout_path, A.equipment_available, B.name experience_lvl_working_out, A.you_train_days,	A.have_workout_partner,  	A.workout_partner_days , A.where_you_train_day 	 FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' A  LEFT JOIN #__userlevel B ON A.experience_lvl_working_out=B.id '. "WHERE A.id= $loginUserId";

$db->setQuery( $query );

$user_obj	= $db->loadObject();

$user_equipments=array();
if(!empty($user_obj->equipment_available)){
	$user_equipments=json_decode($user_obj->equipment_available,'array');
}

$user_where_you_train_day=array();
if(!empty($user_obj->where_you_train_day)){
	$user_where_you_train_day=json_decode($user_obj->where_you_train_day,'array');
}
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/questiona/tmpl/css/style.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel') {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
	}
</script>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->

<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit">
        <h1 class="nw_member_info"> My Fitness Path</h1>
        
        
        <form  action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.cancel'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
          <div>
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
                <label > <span class="commen_heading  payment_info"> Choose a Workout Path: </span>  <span style="float:left;padding-left:20px" class="anskey payment_space"> <?php echo str_replace(',','<br>',$user_obj->workout_path); ?></span></label>
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label wokout_lable1">
                <label >Equipment you have available to you.</label>
              </div>
              <div class="controls">
              <ul id="equipment_id">
            <?php 
			$query	= 'SELECT A.id,A.name FROM ' . $db->quoteName( '#__questionnaire_equipment' )
				. ' AS A JOIN '.$db->quoteName( '#__questionnaire_equipment_category' ).' AS B ON  A.category_id=B.id WHERE  B.id=1 and (A.state=1 and B.state=1) order by A.ordering';
	
	
			  $db->setQuery( $query );
			
			  foreach($db->loadObjectList() as $equipments):
					if(@array_key_exists($equipments->id,$user_equipments)){
					echo	"<li class='anskey'>".$equipments->name." - "  ;
					
						$current_equipments=$user_equipments[$equipments->id];
					
						echo (@$current_equipments['days']['H'])? " H " : '';
						echo (@$current_equipments['days']['G'])? " G " : '';
						echo (@$current_equipments['days']['P'])? " P " : '';
						echo (@$current_equipments['days']['A'])? " A " : '';
						
					echo 	"</li>";		
					}
			  endforeach;			  
			  ?> </ul>          
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
                <label   for="jform_experience_lvl_working_out" > <span class="commen_heading  payment_info"> Experience level when it comes to working out : </span> <span class="anskey payment_space">
				<?php				
				echo $user_obj->experience_lvl_working_out;
				?>
				</span></label>
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
                <label   for="fitness_business_code" ><span class="commen_heading  payment_info"> Days a week can you train and on what days : </span> 
                <span class="anskey payment_space" style="float:left;"><?php echo str_replace(',','<br>',$user_obj->you_train_days); ?></span></label>
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
                <label><span class="commen_heading  payment_info"> Have a workout partner and if so what days :</span>  <span class="anskey payment_space"><?php echo ($user_obj->have_workout_partner)? 'Yes' : 'No' ; ?></span></label>
                <?php if($user_obj->have_workout_partner): ?>
                <label style="float:left;padding-left:20px">Workout partner days : <span  class="anskey"><br><?php echo str_replace(',','<br>',$user_obj->workout_partner_days); ?></span></label>
                <?php endif; ?>
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
                <label   for="fitness_business_code"><span class="commen_heading  payment_info">  Where you will be training and on what days: </span> </label>
              </div>
              <div class="controls">
              <?php $where_you_train_day=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'); ?>    
                     <ul id="where_you_train_day_id">
                     <?php 	 foreach($where_you_train_day as $where_you_train_day_val): 
					 	
								if(@array_key_exists($where_you_train_day_val,$user_where_you_train_day)): 
                             		
									echo  "<li class='anskey'>".$where_you_train_day_val." - " ; 
									
									echo (@$user_where_you_train_day[$where_you_train_day_val]['days']['H'])? " H " : '';
									echo (@$user_where_you_train_day[$where_you_train_day_val]['days']['G'])? " G " : '';
									echo (@$user_where_you_train_day[$where_you_train_day_val]['days']['P'])? " P " : '';
									echo (@$user_where_you_train_day[$where_you_train_day_val]['days']['A'])? " A " : '';

									echo  "</li>";
					     		 endif; 
						 	 endforeach ?>
                  </ul>             
              </div>
            </div>
         </div> 
           <input type="hidden" name="task" value="" />
           <?php echo JHtml::_('form.token'); ?> 
        </form>
      </div></div>
    <div class="span4 respon_span4"><?php require_once JPATH_COMPONENT.'/questionmenua.php'; ?>
     </div></div></div></div>
  
