<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$db			= JFactory::getDBO();	

$usermenu_sessionObj =JFactory::getSession();
$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
$loginUserId	= (int) $user_menu_id;

$query	= 'SELECT eat_a_day_meal_snacks, nutrition_program_start, scenario_program,	night_sleep_hours, question_answers, foods_condiments_like FROM ' . $db->quoteName( '#__questionnaire_question' ) . " WHERE id= $loginUserId";

$db->setQuery( $query );

$user_obj	= $db->loadObject();

$scenario_program=array();
if(!empty($user_obj->scenario_program)){
	$scenario_program=@json_decode($user_obj->scenario_program,'array');
}

$question_answers=array();
if(!empty($user_obj->question_answers)){
	$question_answers=@json_decode($user_obj->question_answers,'array');
}

$foods_condiments_like=array();
if(!empty($user_obj->foods_condiments_like)){
	$foods_condiments_like=@json_decode($user_obj->foods_condiments_like,'array');
}
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/questiona/tmpl/css/style.css');

?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel') {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
	}
</script>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->



<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit">
        
        <h1 class="nw_member_info">My Nutrition Path</h1>
        <form  action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.cancel'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
          <div class="menber_lable">
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
              
                <label > <span class="commen_heading  payment_info"> How many times you usually eat a day including meals and snacks:  </span>   <span class="anskey payment_space_view"><?php echo $user_obj->eat_a_day_meal_snacks; ?></span></label>
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
                <label > <span class="commen_heading  payment_info"> Day you would like your Nutrition Program to start:  </span>  <span class="anskey payment_space_view">
				<?php
				if($user_obj->nutrition_program_start!='0000-00-00')
				echo date('d-M-Y',strtotime($user_obj->nutrition_program_start)); 
				?>
				</span></label>
              </div>
            </div><br />


            <div class="restricted_area scnerio_width">
            <fieldset class="scnerio_width1">
            
			<legend style="float:left; border:none; margin:0;" > <span class="commen_heading  payment_info"> Scenario best describes your day to day lifestyle regarding meal preparation </span> </legend>
            
            <div class="control-group">
              <div class="control-label view_workout">
                <label for="jform_scenario_program_break_lunch_dinner" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> Can prepare/cook breakfast, lunch and dinner : </span>  <span style="float:left; width:30% !important"  class="anskey payment_space_view"><?php echo @$scenario_program['break_lunch_dinner']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label for="jform_scenario_program_break_dinner" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> Can prepare/cook breakfast and dinner :</span>  <span style="float:left; width:30% !important"  class="anskey payment_space_view"> <?php echo @$scenario_program['break_dinner']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label  for="jform_scenario_program_dinner" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> Can only prepare/cook dinner :</span> <span style="float:left; width:30% !important"  class="anskey payment_space_view"> <?php echo @$scenario_program['dinner']; ?></span></label>
              </div>
            </div>
            </fieldset>
           
            
            
            <div class="control-group">
              <div class="control-label">
                <label  for="jform_night_sleep_hours" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;">Hours of sleep average a night :</span> <span style="float:left; width:30% !important"  class="anskey payment_space_view"> <span class="anskey"><?php echo $user_obj->night_sleep_hours; ?></span></label>
              </div>
            </div>
             </div>
             
             
             
            
            <div class="restricted_area scnerio_width">
            <fieldset class="scnerio_width1">
            
			<legend style="float:left; border:none; margin:0;"  > <span class="commen_heading  payment_info"> Questions </span> </legend>
            <div class="control-group">
              <div class="control-label">
                <label   for="jform_eat_sweet_fruit_candy" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> If I eat something sweet such as fruit or candy I pick right up: </span>  <span style="float:left; width:30% !important"  class="anskey payment_space_view"> <?php echo @$question_answers['eat_sweet_fruit_candy']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label   for="jform_eat_meat_daily" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> I could eat red meat daily :</span>  <span style="float:left; width:30% !important"  class="anskey payment_space_view"><?php echo @$question_answers['eat_meat_daily']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label   for="jform_eat_steamed_vegetables" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> I could eat steamed or raw vegetables daily :</span> <span style="float:left; width:30% !important"  class="anskey payment_space_view"><?php echo @$question_answers['eat_steamed_vegetables']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label   for="jform_find_myself_snacking" class="payment_space_view" > <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;">I often find myself snacking throughout the day :</span>  <span style="float:left; width:30% !important"  class="anskey payment_space_view"><?php echo @$question_answers['find_myself_snacking']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label   for="jform_eat_meat_feel_right" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> If I eat meat I feel like I it picks me right up and gives me energy : </span> <span style="float:left; width:30% !important"  class="anskey payment_space_view"> <?php echo @$question_answers['eat_meat_feel_right']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label   for="jform_crave_sweets" class="payment_space_view">  <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> I often crave sweets : </span>  <span style="float:left; width:30% !important"  <span style="float:left; width:30% !important"  class="anskey payment_space_view"><?php echo @$question_answers['crave_sweets']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label   for="jform_feel_sluggish_not_eat_meat" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> I often feel sluggish if I don't eat every two or three hours :</span>  <span style="float:left; width:30% !important"  class="anskey payment_space_view"> <?php echo @$question_answers['feel_sluggish_not_eat_meat']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label   for="jform_drinking_meal_replacements" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> I often find myself drinking Meal Replacements or Protein Sakes :</span>  <span style="float:left; width:30% !important"  class="anskey payment_space_view"> <?php echo @$question_answers['drinking_meal_replacements']; ?></span></label>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label   for="jform_tend_get_constipated" class="payment_space_view"> <span class="serva_list" style="float:left; width:60% !important; margin:0 0 10px;"> I tend to get constipated : </span> <span style="float:left; width:30% !important"  class="anskey payment_space_view"><?php echo @$question_answers['tend_get_constipated']; ?></span></label>
              </div>
            </div>                                                                            
            </fieldset>
            </div>
            
            
            <div class="restricted_area scnerio_width">
            <fieldset class="scnerio_width1">
            
            
		<legend style="float:left; border:none; margin:0;"  > <span class="commen_heading  payment_info"> Foods and condiments you liked in each category</span> </legend>
            <?php
			 $query	= 'SELECT id,name FROM ' . $db->quoteName( '#__questionnaire_nutrition_category' ).'  WHERE state=1 order by name ASC';
			 
			 $db->setQuery( $query );
			  
			foreach($db->loadObjectList() as $nutritions_cat):			
			?>
			<table width="100%" style="margin-left:20px;">
            	<tr><th style="float:left"><?php echo $nutritions_cat->name; ?></th></tr>
                <tr><td>
				<?php  
                 $nutrition_cat_id=$nutritions_cat->id;

                 $query	= "SELECT id,name FROM " . $db->quoteName( '#__questionnaire_nutrition_food' ) . " WHERE category_id=$nutrition_cat_id and state=1  order by  name ASC";
                 $db->setQuery( $query );
                 $nut_arr=array();
					
					foreach($db->loadObjectList() as $nutrition_items):
						$nutrition_id = $nutrition_items->id;
						$nutrition_name = $nutrition_items->name;		
						
						if(@$foods_condiments_like[$nutrition_cat_id][$nutrition_id]==$nutrition_id){
							$nut_arr[]=$nutrition_name;
						}
					endforeach;
					echo implode(' <br > ',$nut_arr);
                 ?>	
                </td></tr>
            </table>
            <br />
			<?php
			endforeach;			
			?>
            </fieldset>
            </div>
          </div> 
           <input type="hidden" name="task" value="" />
           <?php echo JHtml::_('form.token'); ?> 
        </form>
      </div>
      </div>
      
    <div class="span4 respon_span4"> <?php require_once JPATH_COMPONENT.'/questionmenua.php'; ?>
   </div></div></div></div>
