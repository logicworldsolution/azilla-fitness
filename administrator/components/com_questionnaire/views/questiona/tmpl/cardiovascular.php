<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$db			= JFactory::getDBO();	

$usermenu_sessionObj =JFactory::getSession();
$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
$loginUserId	= (int) $user_menu_id;

$query	= 'SELECT  A.resting_heart, A.cardiovascular_exercise_goal,	B.name cardiovascular_exercise_experience_lvl, A.equipment_or_amenities_you_have_available, A.day_start_your_fitness_cardiovascular_program FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' A LEFT JOIN #__userlevel B ON A.cardiovascular_exercise_experience_lvl=B.id '. "WHERE A.id= $loginUserId";

$db->setQuery( $query );

$user_amenities_obj	= $db->loadObject();

$user_amenities=array();
if(!empty($user_amenities_obj->equipment_or_amenities_you_have_available)){
	$user_amenities=json_decode($user_amenities_obj->equipment_or_amenities_you_have_available,'array');
}
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/questiona/tmpl/css/style.css');

?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel') {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
	}
</script>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->



<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit">
        <h1 class="nw_member_info">My Cardiovascular Path</h1>
        
        
        <form  action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.cancel'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
           <div class="menber_lable">
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
              
                <label> <span class="commen_heading  payment_info"> Resting heart: </span>  <span class="anskey payment_space_view"><?php echo $user_amenities_obj->resting_heart; ?></span></label>
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
                <label> <span class="commen_heading  payment_info"> Primary goal(s) when it comes to cardiovascular exercise :</span>  <span class="anskey payment_space_view"> <?php echo str_replace(',','<br>',$user_amenities_obj->cardiovascular_exercise_goal); ?></span></label>
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
                <label> <span class="commen_heading  payment_info"> Experience level when it comes to cardiovascular exercise :</span>  <span class="anskey payment_space_view"><?php echo $user_amenities_obj->cardiovascular_exercise_experience_lvl; ?></span></label>
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label view_workout">
                <label> <span class="commen_heading  payment_info"> Equipment or amenities available to you : </span> </label>
              </div>
              <div class="controls">
              <ul>
          	 <?php 
			$query	= 'SELECT A.id,A.name FROM ' . $db->quoteName( '#__questionnaire_equipment' )
				. ' AS A JOIN '.$db->quoteName( '#__questionnaire_equipment_category' ).' AS B ON  A.category_id=B.id WHERE  B.id=2 and (A.state=1 and B.state=1) order by A.ordering';
	 
			  $db->setQuery( $query );
			  foreach($db->loadObjectList() as $equipments):
						$equipment_checked='';
							if(array_key_exists($equipments->id,$user_amenities)){
								echo "<li class='anskey'>".$equipments->name." - ";
								
								$current_equipments=$user_amenities[$equipments->id];
								echo (@$current_equipments['days']['H'])? " H  " : '';
								echo (@$current_equipments['days']['G'])? " G " : '';
								echo (@$current_equipments['days']['P'])? " P " : '';
								echo (@$current_equipments['days']['A'])? " A " : '';
								
								echo "</li>";
							}
			  endforeach;	
			  ?>
             	</ul>    
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label>Day to start your Fitness and Cardiovascular Program : <span class="anskey">
				<?php 
				if($user_amenities_obj->day_start_your_fitness_cardiovascular_program!='0000-00-00')
				echo date('d-M-Y',strtotime($user_amenities_obj->day_start_your_fitness_cardiovascular_program)); 
				?></span></label>
              </div>
            </div>
          </div>
          
          
           <input type="hidden" name="task" value="" />
           <?php echo JHtml::_('form.token'); ?> 
        </form>
     </div>
      </div>
      <div class="span4 respon_span4"> <?php require_once JPATH_COMPONENT.'/questionmenua.php'; ?>
    </div></div></div></div>
