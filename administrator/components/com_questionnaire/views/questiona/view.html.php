<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class QuestionnaireViewQuestiona extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
		}
		if(empty($this->_layout)) $this->_layout='new_member';
		
		$usermenu_sessionObj =JFactory::getSession();
		if(!empty($_GET['id'])){
			$usermenu_sessionObj->set( 'user_menu_id', $_GET['id'] );
			$user_menu_id=$_GET['id'];
		}else{
			$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		JToolBarHelper::title(JText::_('COM_QUESTIONNAIRE_TITLE_QUESTION'), 'question.png');


		if (empty($this->item->id)) {
			JToolBarHelper::cancel('question.cancel', 'Back');
		}
		else {
			JToolBarHelper::cancel('question.cancel', 'Back');
		}

	}
}
