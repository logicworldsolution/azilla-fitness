<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/question/tmpl/css/style.css');

?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel') {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
		else if(document.formvalidator.isValid(document.id('question-form'))){
			Joomla.submitform('question.savecardiovascular', document.getElementById('question-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>
<?php
$db			= JFactory::getDBO();
$usermenu_sessionObj =JFactory::getSession();
$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
$loginUserId	= (int) $user_menu_id;


$query	= 'SELECT equipment_or_amenities_you_have_available FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '
		. "WHERE id= $loginUserId";

$db->setQuery( $query );

$user_amenities_obj	= $db->loadObject();
$user_amenities=array();
if(!empty($user_amenities_obj->equipment_or_amenities_you_have_available)){
	$user_amenities=json_decode($user_amenities_obj->equipment_or_amenities_you_have_available,'array');
}


?>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->

<script type='text/javascript'>
jQuery().ready(function(){

		//code to add/remove HGPA from checkbox
			jQuery('.hgpa_main input').live('click',function(){
				if(jQuery(this).attr('checked')!='checked'){
					if(jQuery(this).parents('.hgpa_main').find('input:checked').length==0){
						jQuery(this).parents('li').find('input').removeAttr('checked');
						jQuery(this).parents('.hgpa_main').remove();
					}		
				}
			});	
			
			jQuery('.equipment_class').click(function(){
				var equp_indx=jQuery(this).val();
				if(jQuery(this).attr('checked')=='checked'){
					jQuery('.equipment_days'+jQuery(this).val()).html("<span class='hgpa_main'><span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available]["+equp_indx+"][days][H]'  >H</span><span class='hgpa'><input type='checkbox' name='jform[equipment_or_amenities_you_have_available]["+equp_indx+"][days][G]' value='1'   >G</span><span class='hgpa'><input type='checkbox' name='jform[equipment_or_amenities_you_have_available]["+equp_indx+"][days][P]' value='1'  >P</span><span class='hgpa'><input type='checkbox' name='jform[equipment_or_amenities_you_have_available]["+equp_indx+"][days][A]' value='1'  >A</span></span>");
				}else{
					jQuery('.equipment_days'+jQuery(this).val()).find('.hgpa_main').remove();
				}
			});

});
</script>
<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit">
        <h1 class="nw_member_info">My Cardiovascular Path</h1>
        
        
        
        <form action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=questionform.savecardiovascular'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
           <div class="menber_lable">
            <div class="control-group restricted_area">
              <div class="control-label wokout_lable1">
              
              
                <label   for="jform_resting_heart" >What is your resting heart?(Take your pulse for 1 minute or 15 seconds and times it by 4)</label>
              </div>
              <div class="controls fild_form11">                
                 <?php echo $this->form->getInput('resting_heart'); ?>          
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label wokout_lable1">
                <label   for="jform_cardiovascular_exercise_goal" >What is your primary goal(s) when it comes to cardiovascular exercise?</label>
              </div>
              <div class="controls wokout_lable">
              <?php echo $this->form->getInput('cardiovascular_exercise_goal'); ?>                
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label wokout_lable1">
                <label   for="jform_cardiovascular_exercise_experience_lvl" >What is your experience level when it comes to cardiovascular exercise?</label>
              </div>
              <div class="controls calender_option1">
              <?php echo $this->form->getInput('cardiovascular_exercise_experience_lvl'); ?>                
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label wokout_lable1">
                <label  >Please choose which equipment or amenities you have available to you Choose whether equipment or amenities are for Home (H) Park (P) Gym (G) or All (A))</label>
              </div>
              <div class="controls amenities_checking">
              <ul>
          	 <?php 
			  $query	= 'SELECT A.id,A.name FROM ' . $db->quoteName( '#__questionnaire_equipment' )
				. ' AS A JOIN '.$db->quoteName( '#__questionnaire_equipment_category' ).' AS B ON  A.category_id=B.id WHERE  B.id=2 and (A.state=1 and B.state=1) order by A.ordering';
	
			  $db->setQuery( $query );
			  foreach($db->loadObjectList() as $equipments):?>
				<li>
                    <?php
						$equipment_checked='';
							if(array_key_exists($equipments->id,$user_amenities)){
								$equipment_checked="checked='checked'";
							}
								
							echo "<input type='checkbox' class='equipment_class' name='jform[equipment_or_amenities_you_have_available][".$equipments->id."][id]' value='".$equipments->id."' $equipment_checked >".$equipments->name;
							
							if(!empty($equipment_checked)){								
								$current_equipments=$user_amenities[$equipments->id];
								if(!empty($current_equipments['days'])){

									$is_h_checked=(@$current_equipments['days']['H'])? "checked='checked'" : '';
									$is_g_checked=(@$current_equipments['days']['G'])? "checked='checked'" : '';
									$is_p_checked=(@$current_equipments['days']['P'])? "checked='checked'" : '';
									$is_a_checked=(@$current_equipments['days']['A'])? "checked='checked'" : '';
									
									echo "<span class='equipment_days".$equipments->id."' ><span class='hgpa_main'>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][H]' $is_h_checked >H</span>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][G]' $is_g_checked >G</span>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][P]' $is_p_checked >P</span>";
									echo "<span class='hgpa'><input type='checkbox' value='1' name='jform[equipment_or_amenities_you_have_available][$equipments->id][days][A]' $is_a_checked >A</span>";							
									echo "</span></span>";
								}else{
								echo "<span class='equipment_days".$equipments->id."'></span>";
								}
							
							}
							else{
								echo "<span class='equipment_days".$equipments->id."'></span>";
							}
					 ?>                    
                   
                </li>
			 <?php endforeach;	?>
             	</ul>    
              </div>
            </div>
            <div class="control-group restricted_area">
              <div class="control-label wokout_lable1">
                <label   for="jform_day_start_your_fitness_cardiovascular_program" >Please select what day you would like to start your Fitness and Cardiovascular Program</label>
              </div>
              <div class="controls wokout_lable2">
              <?php echo $this->form->getInput('day_start_your_fitness_cardiovascular_program'); ?>                
              </div>
            </div>
                                    
            <div class="control-group">
                <div class="control-label"> </div>
                <div class="controls wokout_button">
                  <div class="submit_member1">
                  <input type="hidden" name="action" value="cardiovascular" />
                  <input type="hidden" name="action_success_message" value="My_cardiovascular_information_saved_successfully" />
                  <input type="hidden" name="action_fail_message" value="Error_saving_my_cardiovascular_informationn" /> 
                  <input type="hidden" name="option" value="com_questionnaire" />
                  <input type="hidden" name="task" value="questionform.savecardiovascular" />
                  <?php echo JHtml::_('form.token'); ?> </div>
              </div>
            </div>
          </div>
        </form>
     </div>
      </div>
      <div class="span4 respon_span4"><?php require_once JPATH_COMPONENT.'/questionmenu.php'; ?>
   </div></div></div></div>
