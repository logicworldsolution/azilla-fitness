<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/question/tmpl/css/style.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel' || document.formvalidator.isValid(document.id('question-form'))) {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
    
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit">
        
        
        <h1 class="nw_member_info">Payment and Credit Card Information</h1>
        <form action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.save'); ?>"  method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
           <div class="menber_lable">
              <div class="control-group">
                <div class="control-label">
                <div class="restricted_area"> 
				<h4 class="commen_heading payment_info">Payment Information</h4>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_fitness_membership" >Fitness Membership</label>
              </div>
              <div class="controls promotion_input">                
                 <?php echo $this->form->getInput('fitness_membership'); ?>          
              </div>
            </div>            
            <div class="control-group payment_space">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_fitness_affiliate_membership" >Fitness Affiliate Membership</label>
              </div>
              <div class="controls promotion_input">
              <?php echo $this->form->getInput('fitness_affiliate_membership'); ?>                
              </div>
            </div>
         </div>
         
         
          <div class="restricted_area">
				<h4 class="commen_heading payment_info"> Billing Address Information</h4>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_bill_address1" >Billing Address 1</label>
              </div>
              <div class="controls promotion_input">                
                 <?php echo $this->form->getInput('bill_address1'); ?>          
              </div>
            </div>            
            <div class="control-group payment_space">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_bill_address2" >Billing Address 2</label>
              </div>
              <div class="controls promotion_input">
              <?php echo $this->form->getInput('bill_address2'); ?>                
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_bill_city" >City</label>
              </div>
              <div class="controls promotion_input">
              <?php echo $this->form->getInput('bill_city'); ?>                
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_bill_state" >State</label>
              </div>
              <div class="controls promotion_input">
              <?php echo $this->form->getInput('bill_state'); ?>                
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_bill_zip" >Zip Code</label>
              </div>
              <div class="controls promotion_input">
              <?php echo $this->form->getInput('bill_zip'); ?>                
              </div>
            </div>
         </div>
            <div class="control-group">
              <div class="control-label"> </div>
              <div class="controls">
                <div>
                  <input type="hidden" name="option" value="com_questionnaire" />
                  <input type="hidden" name="task" value="" />
                  <input type="hidden" name="action" value="payment" />
                  <input type="hidden" name="action_success_message" value="Payment_and_credit_card_information_saved_successfully" />
                  <input type="hidden" name="action_fail_message" value="Error_saving_payment_and_credit_card_information" /> 
                  <?php echo JHtml::_('form.token'); ?> </div>
              </div>
            </div>
            </div>
        </form>
       </div>
      </div>
      </div>
      </div>
      <div class="span4 respon_span4"><?php require_once JPATH_COMPONENT.'/questionmenu.php'; ?>
    </div>
    </div>
  </div>
</div>
