<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/question/tmpl/css/style.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel') {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
		else if(document.formvalidator.isValid(document.id('question-form'))){
			Joomla.submitform('question.savestats', document.getElementById('question-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>
<?php
$db			= JFactory::getDBO();

$usermenu_sessionObj =JFactory::getSession();
$user_menu_id= $usermenu_sessionObj->get( 'user_menu_id');
$loginUserId	= (int) $user_menu_id;

$query	= 'SELECT measuremts FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '
		. "WHERE id= $loginUserId";

$db->setQuery( $query );

$measuremts	= $db->loadObject();
$measuremts_data=@json_decode($measuremts->measuremts,'Array');
?>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit" >
        <h1 class="nw_member_info" style="padding-left:18px">My Stats and Information</h1>
        <form action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.savestats'); ?>"  method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
           <div class="menber_lable">
              <div class="control-group payment_space">
              <div class="control-label">
              
                <label title=""  for="jform_gender" >What is your gender?</label>
              </div>
              <div class="controls calender_option">                
                 <?php echo $this->form->getInput('gender'); ?>          
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_height" >What is your height (in cm.)?</label>
              </div>
              <div class="controls promotion_input">
             <?php echo $this->form->getInput('height'); ?>                 
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_weight" >What is your weight (in kg.)?</label>
              </div>
              <div class="controls promotion_input">
             <?php echo $this->form->getInput('weight'); ?>                 
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_fat" >What is your body fat %?</label>
              </div>
              <div class="controls promotion_input">
             <?php echo $this->form->getInput('fat'); ?>                 
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_goal_body_fat" >What is your goal body fat %?</label>
              </div>
              <div class="controls promotion_input">
             <?php echo $this->form->getInput('goal_body_fat'); ?>                 
              </div>
            </div>
            
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_blood_type" >What is your blood type?</label>
              </div>
              <div class="controls calender_option">
             <?php echo $this->form->getInput('blood_type'); ?>                 
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_birthday" >What is your birthday?</label>
              </div>
              <div class="controls promotion_input">
             <?php echo $this->form->getInput('birthday'); ?>                 
              </div>
            </div>
            
           <div class="restricted_area">
           <h4 class="commen_heading  payment_info"> What are your measuremts? </h4>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_neck" >Neck</label>
              </div>
              <div class="controls promotion_input">
              <input type="text" size="32" value="<?php echo @$measuremts_data['neck'];  ?>" id="jform_neck" name="jform[measuremts][neck]">
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_chestline" >Chest Line</label>
              </div>
              <div class="controls promotion_input">
              <input type="text" size="32" value="<?php echo @$measuremts_data['chestline'];  ?>" id="jform_chestline" name="jform[measuremts][chestline]">
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_shoulders" >Shoulders</label>
              </div>
              <div class="controls promotion_input">
              <input type="text" size="32" value="<?php echo @$measuremts_data['shoulders'];  ?>" id="jform_shoulders" name="jform[measuremts][shoulders]">
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_upperarms" >Upper Arms</label>
              </div>
              <div class="controls promotion_input">
              <input type="text" size="32" value="<?php echo @$measuremts_data['upperarms'];  ?>" id="jform_upperarms" name="jform[measuremts][upperarms]">
              </div>
            </div>                        
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_forearms" >Forearms</label>
              </div>
              <div class="controls promotion_input">
              <input type="text" size="32" value="<?php echo @$measuremts_data['forearms'];  ?>" id="jform_forearms" name="jform[measuremts][forearms]">
              </div>
            </div>     
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_waist" >Waist</label>
              </div>
              <div class="controls promotion_input">
              <input type="text" size="32" value="<?php echo @$measuremts_data['waist'];  ?>" id="jform_waist" name="jform[measuremts][waist]">
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_wrist" >Wrist</label>
              </div>
              <div class="controls promotion_input">
                <input type="text" size="32" value="<?php echo @$measuremts_data['wrist'];  ?>" id="jform_wrist" name="jform[measuremts][wrist]">
              </div>
            </div>            
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_hips" >Hips</label>
              </div>
              <div class="controls promotion_input">
              <input type="text" size="32" value="<?php echo @$measuremts_data['hips'];  ?>" id="jform_hips" name="jform[measuremts][hips]">
              </div>
            </div> 
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_midthighs" >Mid-Thighs</label>
              </div>
              <div class="controls promotion_input">
              <input type="text" size="32" value="<?php echo @$measuremts_data['midthighs'];  ?>" id="jform_midthighs" name="jform[measuremts][midthighs]">
              </div>
            </div>
            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_calves" >Calves</label>
              </div>
              <div class="controls promotion_input">
              <input type="text" size="32" value="<?php echo @$measuremts_data['calves'];  ?>" id="jform_calves" name="jform[measuremts][calves]">
              </div>
            </div>             
           </div>

            <div class="control-group payment_space">
              <div class="control-label">
                <label title=""  for="jform_weekly_activity_lvl" >What is your weekly activity level on and off the job?</label>
              </div>
              <div class="controls calender_option">
                <?php echo $this->form->getInput('weekly_activity_lvl'); ?>                 
              </div>
            </div>

            <div class="control-group">
                <div class="control-label"> </div>
                <div class="controls">
                  <div class="submit_member11">
                  <input type="hidden" name="action" value="stats" />
                  <input type="hidden" name="action_success_message" value="My_stats_and_information_saved_successfully" />
                  <input type="hidden" name="action_fail_message" value="Error_saving_my_stats_and_information" /> 
                  <input type="hidden" name="option" value="com_questionnaire" />
                  <input type="hidden" name="task" value="" />
                  <?php echo JHtml::_('form.token'); ?> </div>
              </div>
            </div>
          </div>
        </form>
       </div>
      </div>
      <div class="span4 respon_span4"><?php require_once JPATH_COMPONENT.'/questionmenu.php'; ?>
   </div></div></div></div>