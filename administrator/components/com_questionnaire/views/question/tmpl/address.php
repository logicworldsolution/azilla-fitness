<?php
/**
 * @version     1.0.0
 * @package     com_questionnaire
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      sunil <sunil@34interactive.com> - http://
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_questionnaire/views/question/tmpl/css/style.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'question.cancel' || document.formvalidator.isValid(document.id('question-form'))) {
			Joomla.submitform(task, document.getElementById('question-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>
<!-- Styling for making front end forms look OK -->
<!-- This should probably be moved to the template CSS file -->
<div class="container">
  <div class="row-fluid">
    <div class="span12" id="content">
      <div class="span8 respon_span8">
        <div class="question-edit front-end-edit" >
          <h1 class="nw_member_info">Address Information</h1>
        <form action="<?php echo JRoute::_('index.php?option=com_questionnaire&task=question.save'); ?>"  method="post" enctype="multipart/form-data" name="adminForm" id="question-form" class="form-validate" >
          <div class="menber_lable">
              <div class="control-group">
                <div class="control-label">
                <label title="" class="hasTip" for="jform_address1" >Address 1</label>
              </div>
              <div class="controls">                
                 <?php echo $this->form->getInput('address1'); ?>          
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_address2" >Address 2</label>
              </div>
              <div class="controls">
              <?php echo $this->form->getInput('address2'); ?>                
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_city" >City</label>
              </div>
              <div class="controls">
              <?php echo $this->form->getInput('city'); ?>                
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_state" >State</label>
              </div>
              <div class="controls">
              <?php echo $this->form->getInput('state'); ?>                
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label title="" class="hasTip" for="jform_zip" >Zip</label>
              </div>
              <div class="controls">
              <?php echo $this->form->getInput('zip'); ?>                
              </div>
            </div>
            <div class="control-group">
              <div class="control-label"> </div>
              <div class="controls">
                <div>
                  <input type="hidden" name="option" value="com_questionnaire" />
                  <input type="hidden" name="task" value="" />
                  <input type="hidden" name="action" value="address" />
                  <input type="hidden" name="action_success_message" value="Address_information_saved_successfully" />
                  <input type="hidden" name="action_fail_message" value="Error_saving_address_information" />      
                  <?php echo JHtml::_('form.token'); ?> </div>
              </div>
            </div>
         </div>
        </form>
       </div>
      </div>
      <div class="span4 respon_span4"><?php require_once JPATH_COMPONENT.'/questionmenu.php'; ?>
     </div>
    </div>
  </div>
</div>
