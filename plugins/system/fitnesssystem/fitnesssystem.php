<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
/**
 * Example system plugin
 */
class plgSystemFitnesssystem extends JPlugin
{
/**
* Constructor.
*
* @access protected
* @param object $subject The object to observe
* @param array   $config  An array that holds the plugin configuration
* @since 1.0
*/

	/**
	* Do something onAfterInitialise
	*/
	function onAfterInitialise()
	{
		$app	= JFactory::getApplication();
	
		$check_community_option=$app->input->get('option');
		$check_community_view=$app->input->get('view');

		if($check_community_option=='com_community' and $check_community_view=='register'){
			$app->redirect(JRoute::_("index.php?option=com_users&view=registration", false));
		}	
	}
	 
	/**
	* Do something onAfterRoute
	*/
	function onAfterRoute()
	{
	}
	 
	/**
	* Do something onAfterDispatch
	*/
	function onAfterDispatch()
	{
	}
	 
	/**
	* Do something onAfterRender
	*/
	function onAfterRender()
	{
	}
}
?>