<?php
// no direct access
defined('_JEXEC') or die;

class  plgUserFitness extends JPlugin
{
	public function onUserLogin($user, $options = array())
	{
		$loginuser	= JFactory::getUser();

		$db = JFactory::getDbo();	
		$app	= JFactory::getApplication();
		
		$check_font_end_login=$app->input->post->get('option');
		
		if($loginuser->guest==0 and $check_font_end_login!='com_login'){
			$loginUserId	= (int) $loginuser->get('id');

			$query	= 'SELECT * FROM ' . $db->quoteName( '#__questionnaire_question' ) . ' '
			. "WHERE id= $loginUserId";
	
			$db->setQuery( $query );
			
			$user_questions_obj	= $db->loadObject();
			
			if(empty($user_questions_obj->promotional_code) or empty($user_questions_obj->fitness_business_code)){
				$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=new_member&Itemid=308", false),"Please complete new mamber information.", 'notice');
			}
			
			
			if(empty($user_questions_obj->address1) or empty($user_questions_obj->address2) or empty($user_questions_obj->city) or empty($user_questions_obj->state) or empty($user_questions_obj->zip)){
				$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=address&Itemid=309", false),"Please complete address information.", 'notice');
			}
			 	 	 	 	
			if(empty($user_questions_obj->fitness_membership) or empty($user_questions_obj->fitness_affiliate_membership) or empty($user_questions_obj->bill_address1) or empty($user_questions_obj->bill_address2) or empty($user_questions_obj->bill_city) or empty($user_questions_obj->bill_state) or empty($user_questions_obj->bill_zip)){
				$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=payment&Itemid=225", false),"Please complete payment and credit card information.", 'notice');
			}
			 	 	 	 	 	 	 	 	
			if(empty($user_questions_obj->gender) or empty($user_questions_obj->height) or empty($user_questions_obj->weight) or empty($user_questions_obj->fat) or empty($user_questions_obj->goal_body_fat) or empty($user_questions_obj->blood_type) or empty($user_questions_obj->birthday) or empty($user_questions_obj->measuremts) or empty($user_questions_obj->weekly_activity_lvl)){
				$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=questionform&Itemid=234", false),"Please complete my stats and information.", 'notice');
			}
			 	 	
			if(empty($user_questions_obj->primary_goal) or empty($user_questions_obj->overallgoal)){
				$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=questionform&Itemid=234", false),"Please complete primary and overall goal(s).", 'notice');
			}			
 	 	 	 	 	 	
			if(empty($user_questions_obj->workout_path) or empty($user_questions_obj->equipment_available) or empty($user_questions_obj->experience_lvl_working_out) or empty($user_questions_obj->you_train_days) or empty($user_questions_obj->have_workout_partner) or empty($user_questions_obj->where_you_train_day)){
				$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=fitness&Itemid=249", false),"Please complete my fitness path.", 'notice');
			}	

			if(empty($user_questions_obj->resting_heart) or empty($user_questions_obj->cardiovascular_exercise_goal) or empty($user_questions_obj->cardiovascular_exercise_experience_lvl) or empty($user_questions_obj->equipment_or_amenities_you_have_available) or empty($user_questions_obj->day_start_your_fitness_cardiovascular_program)){
				$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=questionform&Itemid=234", false),"Please complete my cardiovascular path.", 'notice');
			}	

			if(empty($user_questions_obj->eat_a_day_meal_snacks) or empty($user_questions_obj->nutrition_program_start) or empty($user_questions_obj->scenario_program) or empty($user_questions_obj->night_sleep_hours) or empty($user_questions_obj->question_answers) or empty($user_questions_obj->foods_condiments_like)){
				$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=nutrition&Itemid=243", false),"Please complete my nutrition path.", 'notice');
			}				
			
			$app->redirect(JRoute::_("index.php?option=com_questionnaire&view=stats&Itemid=231", false));

		}
	}
	
	public function onUserAfterSave($data, $isNew, $result, $error)
	{
		$db = JFactory::getDbo();

		$userId	= JArrayHelper::getValue($data, 'id', 0, 'int');
	
		$db->setQuery("INSERT IGNORE INTO #__questionnaire_question(id,user_id) VALUES ($userId,$userId)");
		$db->execute();

	}	
	
	public function onUserAfterDelete($user, $success, $msg)
	{
		if (!$success)
		{
			return false;
		}
		$userId	= JArrayHelper::getValue($user, 'id', 0, 'int');

		$db = JFactory::getDbo();

		$db->setQuery("DELETE FROM #__questionnaire_question WHERE id=$userId");
		$db->execute();
	
	}	
}
?>